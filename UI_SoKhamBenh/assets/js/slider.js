$(document).ready(function () {
    $('.experts').slick({
        rows: 0,
        slidesToShow: 4,
        slidesToScroll: 1,
        slide: '.expert-item',
        arrows: true,
        infinite: true,
        autoplay: true,
        prevArrow: '<button type="button" class="slick-prev arrows fw-bold" aria-label="prev"><i class="fa-solid fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next arrows fw-bold" aria-label="next"><i class="fa-solid fa-angle-right"></i></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 798,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
    });

    // partner
    $('.partner-list').slick({
        rows: 0,
        slidesToShow: 5,
        slidesToScroll: 1,
        slide: '.partner-item',
        arrows: true,
        infinite: true,
        autoplay: false,
        prevArrow: '<button type="button" class="slick-prev arrows fw-bold" aria-label="prev"><i class="fa-solid fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next arrows fw-bold" aria-label="next"><i class="fa-solid fa-angle-right"></i></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 798,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 499,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ],
    });
});