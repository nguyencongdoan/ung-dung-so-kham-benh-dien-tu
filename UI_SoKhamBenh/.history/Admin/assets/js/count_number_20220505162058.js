$(document).ready(function() {
    // xu ly + - so luong 
    var bxNumber = $('.bx-number');
    var min = bxNumber.attr('min');
    var max = bxNumber.attr('max');


    $('.bx-minus').click(function(){
        var value = bxNumber.val();
        if(Number(value) > min) {
            bxNumber.val(Number(value) - 1);
        }
        console.log(value);
    });

    $('.bx-plus').click(function(){
        var value = bxNumber.val();
        if(Number(value) < max) {
            bxNumber.val(Number(value) + 1);
        }
        console.log(value);
    });
})