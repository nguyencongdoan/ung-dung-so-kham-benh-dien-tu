$(document).ready(function() {
    // xu ly + - so luong 
    var min = $('.bx-number').attr('min');
    var max = $('.bx-number').attr('max');

    $('.bx-minus').click(function(){
        var value = $('.bx-number').val();
        if(Number(value) > min) {
            $('.bx-number').val(Number(value) - 1);
        }
        console.log(value);
    });

    $('.bx-plus').click(function(){
        var value = $('.bx-number').val();
        if(Number(value) < max) {
            $('.bx-number').val(Number(value) + 1);
        }
        console.log(value);
    });
})