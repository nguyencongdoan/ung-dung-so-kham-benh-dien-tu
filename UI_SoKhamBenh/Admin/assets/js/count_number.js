// $(document).ready(function() {
//     // xu ly + - so luong 
//     var bxNumber = $('.bx-number');
//     var bxMinus = $('.bx-minus');
//     // var main = $('.main-item');
//     var bxPlus = $('.bx-plus');
//     var min = bxNumber.attr('min');
//     var max = bxNumber.attr('max');
    
//     // $('.bx-minus').click(function(){
//     //     var value = bxNumber.val();
//     //     if(Number(value) > min) {
//     //         bxNumber.val(Number(value) - 1);
//     //     }
//     //     console.log(value);
//     // });

//     // $('.bx-plus').click(function(){
//     //     var value = bxNumber.val();
//     //     if(Number(value) < max) {
//     //         bxNumber.val(Number(value) + 1);
//     //     }
//     //     console.log(value);
//     // });
// })

var minus = document.querySelectorAll('.bx-minus');
var plus = document.querySelectorAll('.bx-plus');

for(let i = 0; i < minus.length; i++) {
    // console.log(minus[i]);
    minus[i].addEventListener('click', function(e) {
        // lấy phần tử cùng cấp phía sau phần tử minus
        var number = e.target.nextElementSibling;
        var min = number.getAttribute('min');
        
        if(Number(number.value) > min) {
            number.value -= 1; 
        }
        // console.log(number)
    })
}

for(let i = 0; i < plus.length; i++) {
    // console.log(plus[i])
    plus[i].addEventListener('click', function(e) {
        // lấy phần tử cùng cấp phía trước phần tử plus
        var number = e.target.previousElementSibling;
        var max = number.getAttribute('max');
        
        if(Number(number.value) < max) {
            number.value = Number(number.value) + 1; 
        }
        // console.log(number)
    })
}