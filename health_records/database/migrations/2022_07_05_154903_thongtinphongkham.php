<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Thongtinphongkham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ThongTinPhongKham', function (Blueprint $table) { 
            $table->unsignedBigInteger('id')->autoIncrement();
            $table->string('DiaChi', '255');
            $table->string('SDT', '20');
            $table->string('Email', '100');
            $table->text('DiaChi_Map');
            $table->text('Banner_Content');
            $table->text('Banner_Image', '255');
            $table->string('Logo_Header', '255');
            $table->string('Logo_Header_Admin', '255');
            $table->string('Logo_Footer', '255');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ThongTinPhongKham');
    }
}
