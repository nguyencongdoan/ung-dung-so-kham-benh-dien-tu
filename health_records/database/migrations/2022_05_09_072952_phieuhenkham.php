<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Phieuhenkham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PhieuHenKham', function (Blueprint $table) {
            $table->string('MaPhieuHen', '20')->primary();
            $table->unsignedBigInteger('MaNV');
            $table->date('NgayHen');
            $table->datetime('GioHen');
            $table->string('HoTen', '50');
            $table->string('SDT', '10');
            $table->string('HoTen_NK', '50');
            $table->boolean('GioiTinh_NK');
            $table->string('DiaChi_NK', '100')->nullable();
            $table->date('NamSinh_NK')->nullable();
            $table->string('Email', '50');
            $table->boolean('TinhTrang_PH')->default(0)->nullable();
            $table->timestamps();
            
            $table->foreign('MaNV')
            ->references('id')->on('NhanVien')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PhieuHenKham');
    }
}
