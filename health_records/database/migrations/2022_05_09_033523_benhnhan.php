<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Benhnhan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BenhNhan', function (Blueprint $table) {
            $table->id();
            $table->string('HoTen', '50');
            $table->string('DiaChi', '100')->nullable();
            $table->string('SDT', '10');
            $table->string('CanNang', '10')->nullable();
            $table->boolean('GioiTinh')->default(1);
            $table->date('NgaySinh');
            $table->string('MaBHYT', '50')->nullable();
            $table->string('AnhDaiDien', '100')->nullable()->default('avatar.png');
            $table->string('email', '50');
            $table->string('password', '255');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BenhNhan');
    }
}
