<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Phieukham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PhieuKham', function (Blueprint $table) {
            $table->string('MaPhieuKham', '20')->primary();
            $table->unsignedBigInteger('MaBN');
            $table->unsignedBigInteger('MaNV');
            $table->date('NgayKham');
            $table->string('HoTenNguoiThan', '50')->nullable();
            $table->datetime('ThoiGianBD')->nullable();
            $table->datetime('ThoiGianKT')->nullable();
            $table->string('TrieuChung', '255')->nullable();
            $table->string('TienSuBenh', '255')->nullable();
            $table->integer('SoPhieu');
            $table->boolean('TinhTrangKham')->default(0)->nullable();
            $table->timestamps();

            $table->foreign('MaBN')
            ->references('id')->on('BenhNhan')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');

            $table->foreign('MaNV')
            ->references('id')->on('NhanVien')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PhieuKham');
    }
}
