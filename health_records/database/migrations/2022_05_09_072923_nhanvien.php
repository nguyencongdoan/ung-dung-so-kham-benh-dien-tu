<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Nhanvien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('NhanVien', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('MaChuyenMon');
            $table->unsignedBigInteger('MaChucVu');
            $table->string('HoTen', '50');
            $table->string('AnhDaiDien', '100')->nullable()->default('avatar.png');
            $table->string('DiaChi', '100')->nullable();
            $table->string('SDT', '10');
            $table->boolean('GioiTinh')->default(1);
            $table->date('NgaySinh');
            $table->string('email', '50');
            $table->string('password', '255');
            $table->timestamps();

            $table->foreign('MaChuyenMon')
            ->references('MaChuyenMon')->on('ChuyenMon')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
            
            $table->foreign('MaChucVu')
            ->references('MaChucVu')->on('ChucVu')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('NhanVien');
    }
}
