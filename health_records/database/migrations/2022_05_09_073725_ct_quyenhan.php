<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CtQuyenhan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CT_QuyenHan', function (Blueprint $table) { 
            $table->unsignedBigInteger('MaCT_QH')->autoIncrement();
            $table->unsignedBigInteger('MaQH');
            $table->unsignedBigInteger('MaChucVu');
            $table->boolean('TrangThai');
            $table->timestamps();

            $table->foreign('MaQH')
            ->references('MaQH')->on('QuyenHan')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
            
            $table->foreign('MaChucVu')
            ->references('MaChucVu')->on('ChucVu')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CT_QuyenHan');
    }
}
