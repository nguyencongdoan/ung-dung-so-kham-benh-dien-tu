<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lichkhambenh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LichKhamBenh', function (Blueprint $table) { 
            $table->unsignedBigInteger('MaLK')->autoIncrement();
            $table->unsignedBigInteger('MaNV');
            $table->datetime('ThoiGianBD');
            $table->datetime('ThoiGianKT');
            $table->string('TieuDe', 100);
            $table->string('ClassName', 50)->nullable();
            // $table->date('NgayLV');
            $table->timestamps();

            $table->foreign('MaNV')
            ->references('id')->on('NhanVien')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LichKhamBenh');
    }
}
