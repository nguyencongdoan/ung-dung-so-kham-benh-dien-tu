<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Baiviet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('BaiViet', function (Blueprint $table) {
            $table->unsignedBigInteger('MaBV')->autoIncrement();
            $table->unsignedBigInteger('MaTL');
            $table->unsignedBigInteger('MaNV');
            $table->string('TieuDe', '255')->nullable();
            $table->string('TomTat', '255')->nullable();
            $table->string('Thumnail', '50');
            $table->date('NgayTao');
            $table->mediumText('NoiDung');
            $table->timestamps();

            $table->foreign('MaTL')
            ->references('MaTL')->on('TheLoai_BV')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
           
            $table->foreign('MaNV')
            ->references('id')->on('NhanVien')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('BaiViet');
    }
}
