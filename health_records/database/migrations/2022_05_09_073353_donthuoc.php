<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Donthuoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DonThuoc', function (Blueprint $table) {
            $table->unsignedBigInteger('MaDonThuoc')->autoIncrement();
            $table->unsignedBigInteger('MaNV');
            $table->unsignedBigInteger('MaBN');
            $table->string('MaPhieuKham', '20');
            $table->string('ChuanDoanBenh', '255')->nullable();
            $table->date('NgayTaiKham')->nullable();
            $table->date('NgayTaoDon')->nullable();
            $table->boolean('TinhTrang_SMS')->default(0)->nullable();
            $table->timestamps();

            $table->foreign('MaPhieuKham')
            ->references('MaPhieuKham')->on('PhieuKham')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');

            $table->foreign('MaNV')
            ->references('id')->on('NhanVien')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');

            $table->foreign('MaBN')
            ->references('id')->on('BenhNhan')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DonThuoc');
    }
}
