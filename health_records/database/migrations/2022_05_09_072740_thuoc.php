<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Thuoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Thuoc', function (Blueprint $table) {
            $table->unsignedBigInteger('MaThuoc')->autoIncrement();
            $table->string('TenThuoc', '100');
            $table->integer('SoLuong');
            $table->integer('DonGia');
            $table->date('NSX');
            $table->date('HSD');
            $table->text('ThongTinThuoc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Thuoc'); //
    }
}
