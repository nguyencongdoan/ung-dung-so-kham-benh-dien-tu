<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Phieunhapthuoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PhieuNhapThuoc', function (Blueprint $table) {
            $table->unsignedBigInteger('MaPhieuNhap')->autoIncrement();
            $table->unsignedBigInteger('MaNV');
            $table->date('NgayNhap');
            $table->timestamps();

            $table->foreign('MaNV')
            ->references('id')->on('NhanVien')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PhieuNhapThuoc');
    }
}
