<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CtDonthuoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CT_DonThuoc', function (Blueprint $table) {
            $table->unsignedBigInteger('MaCT_DonThuoc')->autoIncrement();
            $table->unsignedBigInteger('MaDonThuoc');
            $table->unsignedBigInteger('MaThuoc');
            $table->unsignedBigInteger('MaDonVi');
            $table->integer('SoLuong');
            $table->string('CachDung', '255')->nullable();
            $table->string('LieuDung', '255')->nullable();
            $table->timestamps();

            $table->foreign('MaDonThuoc')
            ->references('MaDonThuoc')->on('DonThuoc')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
           
            $table->foreign('MaThuoc')
            ->references('MaThuoc')->on('Thuoc')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
           
            $table->foreign('MaDonVi')
            ->references('MaDonVi')->on('DonVi')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CT_DonThuoc');
    }
}
