<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) { 
            $table->unsignedBigInteger('id')->autoIncrement();
            $table->unsignedBigInteger('MaHD')->comment('Mã hóa đơn');
            $table->string('NoiDung')->comment('Ghi chú thanh toán');
            $table->string('Vnp_response_code')->comment('Mã phản hồi');
            $table->string('Code_bank')->comment('Mã ngân hàng');
            $table->string('Code_vnpay')->comment('Mã giao dịch');
            $table->string('ThoiGian')->comment('Thời gian chuyển khoản');

            $table->foreign('MaHD')
            ->references('MaHD')->on('HoaDon')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
