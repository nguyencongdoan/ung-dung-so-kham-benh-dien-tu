<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CtPhieunhap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CT_PhieuNhapThuoc', function (Blueprint $table) {
            $table->unsignedBigInteger('MaCT_PhieuNhap')->autoIncrement();
            $table->unsignedBigInteger('MaPhieuNhap');
            $table->unsignedBigInteger('MaDonVi');
            $table->unsignedBigInteger('MaThuoc');
            $table->integer('SoLuong');
            $table->integer('DonGia');
            $table->double('ThanhTien');
            $table->timestamps();

            $table->foreign('MaPhieuNhap')
            ->references('MaPhieuNhap')->on('PhieuNhapThuoc')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');

            $table->foreign('MaDonVi')
            ->references('MaDonVi')->on('DonVi')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');

            $table->foreign('MaThuoc')
            ->references('MaThuoc')->on('Thuoc')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CT_PhieuNhapThuoc');
    }
}
