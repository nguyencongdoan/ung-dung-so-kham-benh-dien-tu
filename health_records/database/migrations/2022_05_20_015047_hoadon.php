<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Hoadon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('HoaDon', function (Blueprint $table) { 
            $table->unsignedBigInteger('MaHD')->autoIncrement();
            $table->unsignedBigInteger('MaDonThuoc');
            $table->double('TongTien');
            $table->datetime('NgayLap');
            $table->string('PhuongThuc_TT', 100);
            $table->boolean('TinhTrang_TT')->default(0)->nullable();
            $table->timestamps();

            $table->foreign('MaDonThuoc')
            ->references('MaDonThuoc')->on('DonThuoc')
            ->onDelete('CASCADE')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('HoaDon');
    }
}
