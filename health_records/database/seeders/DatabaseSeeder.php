<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('benhnhan')->insert([
            [
                'HoTen' => 'Huy',
                'DiaChi' => 'Nha Trang',
                'SDT' => '0123456789',
                'GioiTinh' => 1,
                'NgaySinh' => '2022-05-10',
                'Email' => 'huy@gmail.com',
                'Password' => bcrypt('huy123'),
            ]
        ]);
    }
}
