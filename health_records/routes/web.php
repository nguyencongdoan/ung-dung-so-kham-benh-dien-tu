<?php

use App\Http\Controllers\Admin\BaiVietController;
use App\Http\Controllers\Admin\ChangeThemeController;
use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\PhieuHenController;
use App\Http\Controllers\Admin\PhieuKhamController;
use App\Http\Controllers\Admin\DonThuocController;
use App\Http\Controllers\Admin\HoaDonController;
use App\Http\Controllers\Admin\HoSoBenhNhanController;
use App\Http\Controllers\Admin\HoSoNhanVienController;
use App\Http\Controllers\Admin\PhieuNhapThuocController;
use App\Http\Controllers\Admin\ThongKeController;
use App\Http\Controllers\Admin\ThuocController;
use App\Http\Controllers\Api\LichKhamBenhController;
use App\Http\Controllers\Api\SendSMSController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\Login_RegisterController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\BenhNhanController;
use App\Http\Controllers\Client\TinTucController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.home');
});

Route::prefix('auth')->group(function () {
    Route::get('register', [Login_RegisterController::class, 'register'])->name('auth.register');
    Route::post('register', [Login_RegisterController::class, 'createRegister'])->name('auth.createRegister');
    
    Route::get('login', [Login_RegisterController::class, 'login'])->name('auth.login');
    Route::post('login', [Login_RegisterController::class, 'handleAuthLogin'])->name('auth.handleAuthLogin');
    
    Route::get('logout_client', [Login_RegisterController::class, 'logout_client'])->name('auth.logout.client');
    Route::get('logout_admin', [Login_RegisterController::class, 'logout_admin'])->name('auth.logout.admin');
});

Route::prefix('client')->group(function () {
    Route::get('home', [HomeController::class, 'home'])->name('client.home');
    Route::get('datlich/{id_bs?}', [HomeController::class, 'datlich'])->name('client.datlich');
    Route::get('thongtin-bs', [BenhNhanController::class, 'thongtin_bs'])->name('client.thongtin-bs');
    Route::get('edit_phieuhen/{id}', [HomeController::class, 'edit_phieuhen'])->name('client.edit_phieuhen');

    Route::middleware('auth.client')->prefix('user')->group(function () {
        Route::get('thongtin-user/{id}', [BenhNhanController::class, 'thongTinUser'])->name('client.user.thongtin-user');
        Route::get('hoso/{id}', [BenhNhanController::class, 'hoso'])->name('client.user.hoso');
        Route::get('lichsu_datlich/{sdt}', [BenhNhanController::class, 'lichsu_datlich'])->name('client.user.lichsu_datlich');
    });

    Route::prefix('tintuc')->group(function() {
        Route::get('/', [TinTucController::class, 'tintuc'])->name('client.tintuc');
        Route::get('getAllNews', [BaiVietController::class, 'getAllNews'])->name('client.tintuc.getAllNews');
        Route::get('detail_bv/{id}', [TinTucController::class, 'detail_bv'])->name('client.tintuc.detail_bv');
    });
});

Route::middleware('auth.admin')->prefix('admin')->group(function () {
    Route::get('dashboard', [AdminHomeController::class, 'dashboard'])->name('admin.dashboard');
    
    Route::middleware('auth.admin.quantri')->prefix('changetheme')->group(function () {
        Route::get('/', [ChangeThemeController::class, 'changeTheme'])->name('admin.changetheme');
        Route::post('handle_edit', [ChangeThemeController::class, 'handle_edit'])->name('admin.changetheme.handle_edit');
        Route::post('edit_slide', [ChangeThemeController::class, 'edit_slide'])->name('admin.changetheme.edit_slide');
        Route::post('create_slide', [ChangeThemeController::class, 'create_slide'])->name('admin.changetheme.create_slide');
        Route::post('remove_slide', [ChangeThemeController::class, 'remove_slide'])->name('admin.changetheme.remove_slide');
    });

    Route::middleware('auth.admin.quantri')->prefix('thongke')->group(function () {
        Route::get('/', [ThongKeController::class, 'index'])->name('admin.thongke');
        Route::get('getPhieuHen', [ThongKeController::class, 'getPhieuHen'])->name('admin.thongke.getPhieuHen');
        Route::post('searchPhieuHen', [ThongKeController::class, 'searchPhieuHen'])->name('admin.thongke.searchPhieuHen');
        // Route::post('export_Excel_PhieuHen', [ThongKeController::class, 'export_Excel_PhieuHen'])->name('admin.thongke.export_Excel_PhieuHen');
        Route::post('export_Excel_PhieuHen', [ThongKeController::class, 'export_Excel_PhieuHen'])->name('admin.thongke.export_Excel_PhieuHen');
        
        // phieu kham
        Route::get('getPhieuKham', [ThongKeController::class, 'getPhieuKham'])->name('admin.thongke.getPhieuKham');
        Route::post('searchPhieuKham', [ThongKeController::class, 'searchPhieuKham'])->name('admin.thongke.searchPhieuKham');
        Route::post('export_Excel_PhieuKham', [ThongKeController::class, 'export_Excel_PhieuKham'])->name('admin.thongke.export_Excel_PhieuKham');
    });

    Route::middleware('auth.admin.nhanvien_bs')->prefix('phieuhen')->group(function () {
        Route::get('/', [PhieuHenController::class, 'phieuhen'])->name('admin.phieuhen');
        Route::get('themphieuhen', [PhieuHenController::class, 'themphieuhen'])->name('admin.phieuhen.themphieuhen');
        Route::get('edit_phieuhen/{id}', [PhieuHenController::class, 'edit_phieuhen'])->name('admin.phieuhen.edit_phieuhen');
    });
    
    Route::middleware('auth.admin.nhanvien_bs')->prefix('phieukham')->group(function () {
        Route::get('/', [PhieuKhamController::class, 'phieukham'])->name('admin.phieukham');
        Route::get('themphieukham/{id?}', [PhieuKhamController::class, 'themphieukham'])->name('admin.phieukham.themphieukham');
        Route::get('thongtin_phieukham/{id}', [PhieuKhamController::class, 'thongtin_phieukham'])->name('admin.phieukham.thongtin_phieukham');
        Route::get('themdonthuoc/{id?}&{chuandoan?}', [PhieuKhamController::class, 'themdonthuoc'])->name('admin.phieukham.themdonthuoc');

        // phieu tai kham 
        Route::get('ds_phieutaikham', [PhieuKhamController::class, 'ds_phieutaikham'])->name('admin.phieukham.ds_phieutaikham');
    });

    Route::middleware('auth.admin.nhanvien_bs')->prefix('hoso')->group(function () {
        Route::prefix('benhnhan')->group(function () {
            Route::get('/', [HoSoBenhNhanController::class, 'index'])->name('admin.hoso.benhnhan');
            Route::get('detail/{id}', [HoSoBenhNhanController::class, 'detail'])->name('admin.hoso.benhnhan.detail');
            Route::post('searchHoSo', [HoSoBenhNhanController::class, 'searchHoSo'])->name('admin.hoso.benhnhan.searchHoSo');
        });
       
        Route::prefix('nhanvien')->group(function () {
            Route::get('/', [HoSoNhanVienController::class, 'index'])->name('admin.hoso.nhanvien');
            Route::get('create', [HoSoNhanVienController::class, 'create'])->name('admin.hoso.nhanvien.create');
            Route::post('store', [HoSoNhanVienController::class, 'store'])->name('admin.hoso.nhanvien.store');
            Route::get('detail/{id}', [HoSoNhanVienController::class, 'detail'])->name('admin.hoso.nhanvien.detail');
            Route::get('edit/{id}', [HoSoNhanVienController::class, 'edit'])->name('admin.hoso.nhanvien.edit');
            Route::post('handle_edit', [HoSoNhanVienController::class, 'handle_edit'])->name('admin.hoso.nhanvien.handle_edit');
            Route::get('delete/{id}', [HoSoNhanVienController::class, 'delete'])->name('admin.hoso.nhanvien.delete');
            Route::post('searchHoSo', [HoSoNhanvienController::class, 'searchHoSo'])->name('admin.hoso.nhanvien.searchHoSo');
        });
    });

    Route::middleware('auth.admin.nhanvien_bs')->prefix('donthuoc')->group(function () {
        Route::get('/', [DonThuocController::class, 'dsDonThuoc',])->name('admin.donthuoc');
        Route::get('detailDonThuoc/{id?}&{message?}', [DonThuocController::class, 'detailDonThuoc',])->name('admin.donthuoc.detailDonThuoc');
        Route::get('conform_donthuoc/{id?}', [DonThuocController::class, 'conformDonThuoc',])->name('admin.donthuoc.conform_donthuoc');
        Route::post('handleDonThuoc', [DonThuocController::class, 'handleDonThuoc'])->name('admin.donthuoc.handleDonThuoc');
        Route::post('exportPDF', [DonThuocController::class, 'exportPDF'])->name('admin.donthuoc.exportPDF');
        Route::get('xacnhan_donthuoc/{id}', [DonThuocController::class, 'xacnhan_donthuoc'])->name('admin.donthuoc.xacnhan_donthuoc');
        Route::get('edit/{id}', [DonThuocController::class, 'edit'])->name('admin.donthuoc.edit');
        Route::post('handle_edit', [DonThuocController::class, 'handle_edit'])->name('admin.donthuoc.handle_edit'); 
    });

    Route::middleware('auth.admin.quantri')->prefix('baiviet')->group(function () {
        Route::get('/', [BaiVietController::class, 'getAllBaiViet'])->name('admin.baiviet');
        Route::get('thembaiviet', [BaiVietController::class, 'thembaiviet'])->name('admin.baiviet.thembaiviet');
        Route::post('store', [BaiVietController::class, 'store'])->name('admin.baiviet.store');
        Route::get('detail/{id}', [BaiVietController::class, 'detail'])->name('admin.baiviet.detail');
        Route::get('edit/{id}', [BaiVietController::class, 'edit'])->name('admin.baiviet.edit');
        Route::post('handle_edit', [BaiVietController::class, 'handle_edit'])->name('admin.baiviet.handle_edit');
        Route::get('delete/{id}', [BaiVietController::class, 'delete'])->name('admin.baiviet.delete');
    });

    Route::middleware('auth.admin.quantri')->prefix('thuoc')->group(function () {
        Route::get('/', [ThuocController::class, 'index'])->name('admin.thuoc');
        Route::get('create_thuoc', [ThuocController::class, 'create'])->name('admin.thuoc.create_thuoc');
        Route::get('edit_thuoc/{id}', [ThuocController::class, 'edit'])->name('admin.thuoc.edit_thuoc');
    });
    
    Route::middleware('auth.admin.quantri')->prefix('phieunhapthuoc')->group(function () {
        Route::get('/', [PhieuNhapThuocController::class, 'index'])->name('admin.phieunhapthuoc');
        Route::get('create', [PhieuNhapThuocController::class, 'create'])->name('admin.phieunhapthuoc.create');
        Route::get('edit/{id}', [PhieuNhapThuocController::class, 'edit'])->name('admin.phieunhapthuoc.edit');
    });

    Route::prefix('hoadon')->group(function () {
        Route::get('/', [HoaDonController::class, 'index'])->name('admin.hoadon');
        Route::get('detail/{id}', [HoaDonController::class, 'detail'])->name('admin.hoadon.detail');
        Route::get('delete/{id}', [HoaDonController::class, 'delete'])->name('admin.hoadon.delete');
    });

    Route::prefix('lichkhambenh')->group(function () {
        Route::get('/',[LichKhamBenhController::class, 'index'])->name('admin.lichkhambenh');
    });
   
    Route::prefix('notify')->group(function () {
        Route::get('/',[SendSMSController::class, 'index'])->name('admin.notify');
        Route::post('handle_notify',[SendSMSController::class, 'Handle_Notify'])->name('admin.notify.handle_notify');

        Route::get('hoso_benhnhan',[SendSMSController::class, 'HoSoBenhNhan'])->name('admin.notify.hoso_benhnhan');
        Route::post('searchHoSo_BN',[SendSMSController::class, 'searchHoSo_BN'])->name('admin.notify.searchHoSo_BN');
        // Route::get('hoso_nhanvien',[SendSMSController::class, 'HoSoNhanVien'])->name('admin.notify.hoso_nhanvien');
    });
});