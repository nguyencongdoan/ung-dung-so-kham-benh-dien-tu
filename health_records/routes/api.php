<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BacSiController;
use App\Http\Controllers\Api\BaiVietController;
use App\Http\Controllers\Api\BenhNhanController;
use App\Http\Controllers\Api\ChucVuController;
use App\Http\Controllers\Api\ChuyenMonController;
use App\Http\Controllers\Api\DatLichController;
use App\Http\Controllers\Api\DonThuocController;
use App\Http\Controllers\Api\HoaDonController;
use App\Http\Controllers\Api\LichKhamBenhController;
use App\Http\Controllers\Api\PhieuHenController;
use App\Http\Controllers\Api\PhieuKhamController;
use App\Http\Controllers\Api\PhieuNhapThuocController;
use App\Http\Controllers\Api\SendSMSController;
use App\Http\Controllers\Api\ThanhToanController;
use App\Http\Controllers\Api\TheLoaiController;
use App\Http\Controllers\Api\ThuocController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('client')->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('getUser/{id?}', [BenhNhanController::class, 'getUser'])->name('api.client.user.getUser');
        Route::post('editInfoUser', [BenhNhanController::class, 'editInfoUser'])->name('api.client.user.editInfoUser');
        Route::post('changePassword', [BenhNhanController::class, 'changePassword'])->name('api.client.user.changePassword');
        Route::get('getLichSuHen/{sdt}', [BenhNhanController::class, 'getLichSuHen'])->name('api.client.user.getLichSuHen');
        Route::post('removePhieuHen', [BenhNhanController::class, 'removePhieuHen'])->name('api.client.user.removePhieuHen');
    });
    
    Route::prefix('bacsi')->group(function () {
        Route::get('getAllBS', [BacSiController::class, 'getAllBacSi'])->name('api.client.bacsi.getAllBacSi');
        Route::get('getInfoBS', [BacSiController::class, 'getInfoBacSi'])->name('api.client.bacsi.getInfoBacSi');
        Route::post('searchBacSi', [BacSiController::class, 'searchBacSi'])->name('api.client.bacsi.searchBacSi');
    });

    Route::prefix('datlich')->group(function() {
        Route::post('/', [DatLichController::class, 'datlich'])->name('api.client.datlich');
        Route::get('getDateBS', [DatLichController::class, 'getDateBS'])->name('api.client.datlich.getDateBS');
        Route::get('getTimeBS', [DatLichController::class, 'getTimeBS'])->name('api.client.datlich.getTimeBS');
    });

    Route::post('edit_phieuhen', [DatLichController::class, 'edit_phieuhen'])->name('api.client.edit_phieuhen');
    Route::get('remove_phieuhen/{id}', [DatLichController::class, 'removePhieuHen'])->name('api.client.remove_phieuhen');

    Route::prefix('baiviet')->group(function () {
        Route::get('getThreeNews', [BaiVietController::class, 'getThreeNews'])->name('api.client.baiviet.getThreeNews');
        Route::get('getAllNews', [BaiVietController::class, 'getAllNews'])->name('api.client.baiviet.getAllNews');
        Route::get('getTheLoai', [BaiVietController::class, 'getTheLoai'])->name('api.client.baiviet.getTheLoai');
        Route::post('search', [BaiVietController::class, 'search'])->name('api.client.baiviet.search');
    });
});

Route::prefix('admin')->group(function () {
    Route::get('getAllPhieuHen', [PhieuHenController::class, 'getAllPhieuHen'])->name('api.admin.getAllPhieuHen');
    Route::post('createPhieuKham', [PhieuHenController::class, 'createPhieuKham'])->name('api.admin.createPhieuKham');
    Route::post('createDonThuoc', [DonThuocController::class, 'createDonThuoc'])->name('api.admin.createDonThuoc');
    
    Route::get('getAllPhieuKham', [PhieuKhamController::class, 'getAllPhieuKham'])->name('api.admin.getAllPhieuKham');
    Route::get('getAllPhieuTaiKham', [PhieuKhamController::class, 'getAllPhieuTaiKham'])->name('api.admin.getAllPhieuTaiKham');

    Route::post('handlePhieuKham', [PhieuKhamController::class, 'handlePhieuKham'])->name('api.admin.handlePhieuKham');
    Route::post('createPhieuTaiKham', [PhieuKhamController::class, 'createPhieuTaiKham'])->name('api.admin.createPhieuTaiKham');

    Route::prefix('search')->group(function () {
        Route::post('Thuoc', [DonThuocController::class, 'searchThuoc'])->name('api.admin.search.Thuoc');
        Route::post('searchPhieuKham', [PhieuKhamController::class, 'searchPhieuKham'])->name('api.admin.search.searchPhieuKham');
        Route::post('searchPhieuTaiKham', [PhieuKhamController::class, 'searchPhieuTaiKham'])->name('api.admin.search.searchPhieuTaiKham');
        Route::post('searchPhieuHen', [PhieuHenController::class, 'searchPhieuHen'])->name('api.admin.search.searchPhieuHen');
        Route::post('searchNews', [BaiVietController::class, 'searchNews'])->name('api.admin.search.searchNews');
    });

    Route::prefix('donthuoc')->group(function () {
        Route::get('getAllDonThuoc', [DonThuocController::class, 'getAllDonThuoc'])->name('api.admin.donthuoc.getAllDonThuoc');
        Route::post('getHoSo_BN', [DonThuocController::class, 'getHoSo_BN'])->name('api.admin.donthuoc.getHoSo_BN');
        Route::post('detail_toathuoc', [DonThuocController::class, 'detail_toathuoc'])->name('api.admin.donthuoc.detail_toathuoc');
        Route::post('finishDonThuoc', [DonThuocController::class, 'finishDonThuoc'])->name('api.admin.donthuoc.finishDonThuoc');
        Route::post('searchDonThuoc', [DonThuocController::class, 'searchDonThuoc'])->name('api.admin.donthuoc.searchDonThuoc');
        // edit don thuoc
        Route::get('load_don_thuoc/{id}', [DonThuocController::class, 'load_don_thuoc'])->name('api.admin.donthuoc.load_don_thuoc');
    });

    Route::prefix('lichkhambenh')->group(function () {
        Route::post('create',[LichKhamBenhController::class, 'store'])->name('api.admin.lichkhambenh.create');
        Route::post('update',[LichKhamBenhController::class, 'update'])->name('api.admin.lichkhambenh.update');
        Route::post('delete',[LichKhamBenhController::class, 'destroy'])->name('api.admin.lichkhambenh.delete');
    });

    Route::prefix('hoadon')->group(function () {
        Route::get('getAllHoaDon', [HoaDonController::class, 'getAllHoaDon'])->name('api.admin.hoadon.getAllHoaDon');
        Route::post('search', [HoaDonController::class, 'search'])->name('api.admin.hoadon.search');
    });

    Route::prefix('thuoc')->group(function () {
        Route::get('getAllThuoc', [ThuocController::class, 'getAllThuoc'])->name('api.admin.thuoc.getAllThuoc');
        Route::post('handle_create', [ThuocController::class, 'handle_create'])->name('api.admin.thuoc.handle_create');
        Route::post('handle_edit', [ThuocController::class, 'handle_edit'])->name('api.admin.thuoc.handle_edit');
        Route::post('delete', [ThuocController::class, 'delete'])->name('api.admin.thuoc.delete');
        Route::post('search', [ThuocController::class, 'search'])->name('api.admin.thuoc.search');
    });

    Route::prefix('phieunhapthuoc')->group(function () {
        Route::get('getAllPhieuNhap', [PhieuNhapThuocController::class, 'getAllPhieuNhap'])->name('api.admin.phieunhapthuoc.getAllPhieuNhap');
        Route::post('getPhieuNhap', [PhieuNhapThuocController::class, 'getPhieuNhap'])->name('api.admin.phieunhapthuoc.getPhieuNhap');
        Route::post('handle_create', [PhieuNhapThuocController::class, 'handle_create'])->name('api.admin.phieunhapthuoc.handle_create');
        Route::post('handle_edit', [PhieuNhapThuocController::class, 'handle_edit'])->name('api.admin.phieunhapthuoc.handle_edit');
        Route::post('delete', [PhieuNhapThuocController::class, 'delete'])->name('api.admin.phieunhapthuoc.delete');
        Route::post('search', [PhieuNhapThuocController::class, 'search'])->name('api.admin.phieunhapthuoc.search');
    });

    Route::prefix('chucvu')->group(function () {
        Route::post('store',[ChucVuController::class, 'store'])->name('api.admin.chucvu.store');
    });
   
    Route::prefix('chuyenmon')->group(function () {
        Route::post('store',[ChuyenMonController::class, 'store'])->name('api.admin.chuyenmon.store');
    });
    
    Route::prefix('theloai')->group(function () {
        Route::post('store',[TheLoaiController::class, 'store'])->name('api.admin.theloai.store');
    });

    Route::post('vnpay_payment', [ThanhToanController::class, 'vnpay_payment'])->name('api.admin.thanhtoan');
    Route::get('vnpay_return', [ThanhToanController::class, 'vnpay_return'])->name('api.admin.vnpay_return');
    Route::get('sendSMS_TaiKham', [SendSMSController::class, 'sendSMS_TaiKham'])->name('api.admin.sendSMS_TaiKham');
});