<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HoaDon extends Model
{
    use HasFactory;

    protected $table = 'hoadon';  

    protected $fillable = [
        'MaHD',
        'MaDonThuoc',
        'TongTien',
        'NgayLap',
        'PhuongThuc_TT',
        'TinhTrang_TT'
    ];

    public $timestamps = true;
}
