<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThongTinPhongKham extends Model
{
    use HasFactory;

    protected $table = 'thongtinphongkham';  

    protected $fillable = [
        'id',
        'DiaChi',
        'DiaChi_Map',
        'Email',
        'SDT',
        'Banner_Content',
        'Banner_Image',
        'Logo_Header',
        'Logo_Header_Admin',
        'Logo_Footer',
    ];

    public $timestamps = false;
}
