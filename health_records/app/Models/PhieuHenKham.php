<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhieuHenKham extends Model
{
    use HasFactory;

    protected $table = 'phieuhenkham';  

    protected $fillable = [
        'MaPhieuHen',
        'MaNV',
        'NgayHen',
        'GioHen',
        'HoTen',
        'SDT',
        'HoTen_NK',
        'GioiTinh_NK',
        'DiaChi_NK',
        'NamSinh_NK',
        'Email',
        'TinhTrang_PH'
    ];

    public $timestamps = true;
}
