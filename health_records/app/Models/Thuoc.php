<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thuoc extends Model
{
    use HasFactory;

    protected $table = 'thuoc';  

    protected $fillable = [
        'MaThuoc',
        'TenThuoc',
        'SoLuong',
        'DonGia',
        'NSX',
        'HSD',
        'ThongTinThuoc'
    ];

    public $timestamps = true;
}
