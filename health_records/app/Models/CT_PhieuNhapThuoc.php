<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CT_PhieuNhapThuoc extends Model
{
    use HasFactory;

    protected $table = 'ct_phieunhapthuoc';  

    protected $fillable = [
        'MaCT_PhieuNhap',
        'MaPhieuNhap',
        'MaDonVi',
        'MaThuoc',
        'SoLuong',
        'DonGia',
        'ThanhTien'
    ];

    public $timestamps = true;
}
