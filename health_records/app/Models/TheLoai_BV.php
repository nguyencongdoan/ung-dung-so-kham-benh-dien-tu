<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TheLoai_BV extends Model
{
    use HasFactory;

    protected $table = 'theloai_bv';  

    protected $fillable = [
        'MaTL',
        'TenTL'
    ];

    public $timestamps = true;
}
