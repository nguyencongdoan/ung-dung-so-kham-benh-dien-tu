<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CT_QuyenHan extends Model
{
    use HasFactory;

    protected $table = 'ct_quyenhan';  

    protected $fillable = [
        'MaCT_QH',
        'MaQH',
        'MaChucVu',
        'TrangThai'
    ];

    public $timestamps = true;
}
