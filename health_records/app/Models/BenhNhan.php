<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class BenhNhan extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $table = 'benhnhan';  

    protected $guarded = 'benhnhan';

    protected $fillable = [
        'id',
        'HoTen',
        'DiaChi',
        'SDT',
        'CanNang',
        'GioiTinh',
        'NgaySinh',
        'MaBHYT',
        'AnhDaiDien',
        'email',
        'password'
    ];

    public $timestamps = true;

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function phieukhams() {
        return $this->hasMany('App\Models\PhieuKham', 'MaBN', 'id');
    }
}
