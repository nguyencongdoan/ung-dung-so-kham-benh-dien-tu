<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;

    protected $table = 'payments';  

    protected $fillable = [
        'id',
        'MaHD',
        'NoiDung',
        'Vnp_response_code',
        'Code_bank',
        'Code_vnpay',
        'ThoiGian',
    ];

    public $timestamps = false;
}
