<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhieuKham extends Model
{
    use HasFactory;

    protected $table = 'phieukham';  

    protected $fillable = [
        'MaPhieuKham',
        'MaBN',
        'MaNV',
        'NgayKham',
        'HoTenNguoiThan',
        'ThoiGianBD',
        'ThoiGianKT',
        'TrieuChung',
        'TienSuBenh',
        'SoPhieu',
        'TinhTrangKham'
    ];

    public $timestamps = true;

    public function benhnhans() {
        return $this->belongsto('App\Models\BenhNhan', 'id', 'MaBN');
    }
    
    public function nhanviens() {
        return $this->belongsto('App\Models\NhanVien', 'id', 'MaNV');
    }
}
