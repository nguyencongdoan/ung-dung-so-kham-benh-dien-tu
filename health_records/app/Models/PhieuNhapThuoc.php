<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhieuNhapThuoc extends Model
{
    use HasFactory;

    protected $table = 'phieunhapthuoc';  

    protected $fillable = [
        'MaPhieuNhap',
        'MaNV',
        'NgayNhap'
    ];

    public $timestamps = true;
}
