<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonThuoc extends Model
{
    use HasFactory;

    protected $table = 'donthuoc';  

    protected $fillable = [
        'MaDonThuoc',
        'MaNV',
        'MaBN',
        'MaPhieuKham',
        'ChuanDoanBenh',
        'NgayTaiKham',
        'NgayTaoDon',
        'TinhTrang_SMS'
    ];

    public $timestamps = true;
}
