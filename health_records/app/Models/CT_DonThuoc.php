<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CT_DonThuoc extends Model
{
    use HasFactory;

    protected $table = 'ct_donthuoc';  

    protected $fillable = [
        'MaCT_DonThuoc',
        'MaDonThuoc',
        'MaThuoc',
        'MaDonVi',
        'SoLuong',
        'CachDung',
        'LieuDung',
    ];

    public $timestamps = true;
}
