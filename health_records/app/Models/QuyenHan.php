<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuyenHan extends Model
{
    use HasFactory;

    protected $table = 'quyenhan';  

    protected $fillable = [
        'MaQH',
        'TenQH'
    ];

    public $timestamps = true;
}
