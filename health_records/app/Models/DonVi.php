<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonVi extends Model
{
    use HasFactory;

    protected $table = 'donvi';  

    protected $fillable = [
        'MaDonVi',
        'TenDonVi'
    ];

    public $timestamps = true;
}
