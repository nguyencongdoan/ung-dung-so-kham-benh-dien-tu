<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slide_Logo extends Model
{
    use HasFactory;

    protected $table = 'slide_logo';  

    protected $fillable = [
        'id',
        'Slide_Image',
    ];

    public $timestamps = false;
}
