<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LichKhamBenh extends Model
{
    use HasFactory;

    protected $table = 'lichkhambenh';  

    protected $fillable = [
        'MaLK',
        'MaNV',
        'ThoiGianBD',
        'ThoiGianKT',
        'TieuDe',
        'ClassName',
        // 'NgayLV'
    ];

    public $timestamps = true;
}
