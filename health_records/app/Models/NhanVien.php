<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class NhanVien extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'nhanvien';

    protected $guarded = 'admin';

    protected $fillable = [
        'id',
        'MaChuyenMon',
        'MaChucVu',
        'HoTen',
        'AnhDaiDien',
        'DiaChi',
        'SDT',
        'GioiTinh',
        'NgaySinh',
        'email',
        'password'
    ];

    public $timestamps = true;

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function phieukhams() {
        return $this->hasMany('App\Models\PhieuKham', 'MaNV', 'id');
    }
}
