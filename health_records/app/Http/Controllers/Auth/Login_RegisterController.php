<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\BenhNhan;
use App\Models\NhanVien;
use App\Models\ChucVu;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class Login_RegisterController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function createRegister(Request $request)
    {
        if ($request->isMethod('POST')) {
            $validator = Validator::make(
                $request->all(),
                [
                    'HoTen' => 'required|max:50',
                    'DiaChi' => 'max:100',
                    'SDT' => 'required|max:10',
                    'GioiTinh' => 'required|boolean',
                    'NgaySinh' => 'required|date',
                    'email' => 'required|max:50|email',
                    'password' => 'required|max:50|min:6',
                ],

                [
                    'required' => ':attribute không được để trống',
                    'max' => ':attribute không được lớn hơn :max',
                    'min' => ':attribute không được bé hơn :min',
                    'email' => ':attribute không đúng định dạng Email',
                    'unique' => ':attribute phải là duy nhất',
                ],

                [
                    'HoTen' => 'Họ tên',
                    'DiaChi' => 'Địa chỉ',
                    'SDT' => 'Số điện thoại',
                    'GioiTinh' => 'Giới tính',
                    'NgaySinh' => 'Ngày sinh',
                    'email' => 'Email',
                    'password' => 'Password',
                ]
            );

            if ($validator->fails()) {
                $request->flash();
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $user = BenhNhan::where('email', '=', $request->email)->exists();
            if (!$user) {
                $hashPassword = bcrypt($request->password);
                $benhnhan = BenhNhan::create([
                    'HoTen' => $request->HoTen,
                    'SDT' => $request->SDT,
                    'DiaChi' => $request->DiaChi,
                    'GioiTinh' => $request->GioiTinh,
                    'NgaySinh' => $request->NgaySinh,
                    'email' => $request->email,
                    'password' =>  $hashPassword,
                ]);
                if ($benhnhan) {
                    $subject = 'Đăng ký tài khoản thành công!';
                    $to_email = $request->email;
                    $view = 'auth.mail_register';

                    $mail = new SendMail($benhnhan, $subject, $view);
                    Mail::to($to_email)->send($mail);

                    if (Mail::failures()) {
                        $request->session()->flash('message', 'Gửi mail thất bại!');
                        return redirect(route('auth.login'));
                    } else {
                        $request->session()->flash('message', 'Đăng ký tài khoản thành công!');
                        return redirect(route('auth.login'));
                    }
                } else {
                    $request->session()->flash('message', 'Đăng ký tài khoản thất bại!');
                    return redirect(route('auth.login'));
                }
            } else {
                $request->flash();
                $request->session()->flash('message', 'Email đã tồn tại!');
                return redirect(route('auth.register'));
            }
        }
    }

    public function login()
    {
        return view('auth.login');
    }

    public function handleAuthLogin(Request $request)
    {
        if ($request->isMethod('POST')) {
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|max:50|email',
                    'password' => 'required|max:50',
                ],

                [
                    'required' => ':attribute không được để trống',
                    'max' => ':attribute không được lớn hơn :max',
                    'email' => ':attribute không đúng định dạng Email',
                ],

                [
                    'Email' => 'Email',
                    'Password' => 'Password',
                ]
            );

            if($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $credentials = $request->only(['email', 'password']);

            $check_user = BenhNhan::where('email', $request->email)->exists();
            $check_admin = NhanVien::where('email', $request->email)->exists();

            if ($check_user == true || $check_admin == true) {
                // nếu tài khoản của bệnh nhân sễ được chuyển hướng đến trang home của client
                if (Auth::guard('benhnhan')->attempt($credentials)) {
                    session()->flash('message', 'Đăng nhập thành công!');
                    return redirect()->route('client.home');
                }
                //ngược lại nếu là tài khoản thuộc quản trị viên thì sẽ được chuyển đến trang dashboard của admin
                else if (Auth::guard('admin')->attempt($credentials)) {
                    $macv = Auth::guard('admin')->user()->MaChucVu;
                    $chucvu = ChucVu::where('MaChucVu', $macv)->first();
                    session(['chucvu' => $chucvu->TenChucVu]);
                    session()->flash('message', 'Đăng nhập thành công!');
                    return redirect()->route('admin.dashboard');
                } else {
                    $request->session()->flash('message', 'Mật khẩu không đúng. Vui lòng nhập lại!');
                    $request->session()->flash('alert', 'dangers');
                    return redirect()->back()->withInput();
                }
            }
            else {
                $request->session()->flash('message', 'Email không tồn tại. Vui lòng nhập lại!');
                $request->session()->flash('alert', 'dangers');
                return redirect()->back()->withInput();
            }
        }
    }

    public function logout_admin()
    {
        if (Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
        }
        return redirect(route('client.home'));
    }

    public function logout_client()
    {
        if (Auth::guard('benhnhan')->check()) {
            Auth::guard('benhnhan')->logout();
        }
        return redirect(route('client.home'));
    }
}
