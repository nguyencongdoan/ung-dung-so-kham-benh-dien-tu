<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\BaiViet;
use Illuminate\Http\Request;

class TinTucController extends Controller
{
    public function tintuc() {
        return view('client.tintuc.danhsach_tintuc');
    }

    public function detail_bv($id) {
        $baiviet = BaiViet::where('MaBV', $id)
                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->first();  

        $top_bv = BaiViet::join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->orderBy('baiviet.NgayTao')
                    ->take(3)->get();
                
        $theloai = BaiViet::where('MaBV', $id)->first();
        $bv_lq = BaiViet::where('baiviet.MaTL', $theloai->MaTL)
                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->take(3)->get();
                    
        return view('client.tintuc.detail', compact(['baiviet', 'top_bv', 'bv_lq']));
    }
}
