<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use App\Models\DonThuoc;
use Illuminate\Http\Request;

class BenhNhanController extends Controller
{
    public function thongtin_bs() {
        return view('client.thongtin-bs');
    }
    
    public function thongTinUser($id) {
        $user = BenhNhan::find($id);
        return view('client.user.thongtin-user', compact('user'));
    }

    public function hoso($id) {
        $hoso = DonThuoc::join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                ->where('benhnhan.id', $id)
                ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                ->select('donthuoc.*', 'benhnhan.HoTen as HoTen_BN', 'nhanvien.HoTen as HoTen_BS', 'phieukham.*')
                ->orderBy('NgayKham', 'DESC')
                ->paginate(5);

        return view('client.user.hoso', compact('hoso'));
    }
   
    public function lichsu_datlich($sdt) {
        return view('client.user.lichsu_datlich', compact('sdt'));
    }
}
