<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\PhieuHenKham;

class HomeController extends Controller
{   
    public function home() {
        return view('client.home');
    }

    public function datlich($id_bs = null) {
        if(isset($id_bs) && !empty($id_bs)) {
            return view("client.datlich", compact('id_bs'));
        }
        return view("client.datlich");
    }

    public function edit_phieuhen($id) {
        $phieuhen = PhieuHenKham::where('MaPhieuHen', $id)->first();

        return view("client.edit_phieuhen", compact('phieuhen'));
    }
}
