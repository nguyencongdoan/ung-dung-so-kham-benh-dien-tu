<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\PhieuHenExport;
use App\Exports\PhieuKhamExport;
use App\Models\BenhNhan;
use App\Models\HoaDon;
use App\Models\PhieuHenKham;
use App\Models\PhieuKham;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ThongKeController extends Controller
{
    public function index() {
        $data_ph_hen = PhieuHenKham::select(DB::raw('COUNT(MaPhieuHen) as SoLuong, MONTH(NgayHen) as Thang'))
                            ->groupBy(DB::raw('MONTH(NgayHen)'))
                            ->OrderBy('NgayHen')
                            ->pluck('SoLuong', 'Thang');
        $data_hen = [0,0,0,0,0,0,0,0,0,0,0,0];
        foreach($data_ph_hen as $key => $value) {
            $data_hen[--$key] = $value;
        }
        
        $data_ph_kham = PhieuKham::select(DB::raw('COUNT(MaPhieuKham) as SoLuong, MONTH(NgayKham) as Thang'))
                            ->groupBy(DB::raw('MONTH(NgayKham)'))
                            ->OrderBy('NgayKham')
                            ->pluck('SoLuong', 'Thang');
        $data_kham = [0,0,0,0,0,0,0,0,0,0,0,0];
        foreach($data_ph_kham as $key => $value) {
            $data_kham[--$key] = $value;
        }

        $data_hd = HoaDon::select(DB::raw('SUM(TongTien) as TongTien, MONTH(NgayLap) as Thang'))
                            ->groupBy(DB::raw('MONTH(NgayLap)'))
                            ->OrderBy('NgayLap')
                            ->pluck('TongTien', 'Thang');
        $data_line = [0,0,0,0,0,0,0,0,0,0,0,0];
        foreach($data_hd as $key => $value) {
            $data_line[--$key] = $value;
        }
        $count_user = BenhNhan::count();
        $count_phieuhen = PhieuHenKham::count();
        $count_phieukham = PhieuKham::count();
        $sum_hoadon = HoaDon::sum('TongTien');
        
        return view('admin.thongke.thongke', compact(['data_hen', 'data_kham', 'data_line', 'count_phieukham', 'count_phieuhen', 'count_user', 'sum_hoadon']));
    }

    public function getPhieuHen() {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $month = $date->format('m');
        $start_day = '';
        $end_day = '';
        $tinhtrang = '';
        
        $phieuhen = PhieuHenKham::whereMonth('NgayHen', $month)
                    ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                    ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                    ->OrderBy('NgayHen')
                    ->OrderBy('GioHen')
                    ->paginate(10);

        return view('admin.thongke.data_phieuhen', compact('phieuhen', 'start_day', 'end_day', 'tinhtrang'));
    }

    public function searchPhieuHen(Request $request) {
        if($request->isMethod('POST') && $request->ajax()) {
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $month = $date->format('m');

            if(!empty($request->start_day) && !empty($request->end_day) && isset($request->tinhtrang)) {
                $start_day = $request->start_day;
                $end_day = $request->end_day;
                $tinhtrang = $request->tinhtrang;
                $phieuhen = PhieuHenKham::whereBetween('NgayHen', [$request->start_day, $request->end_day])
                            ->where('TinhTrang_PH', $request->tinhtrang)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->paginate(10);
            }
            else if(isset($request->tinhtrang)) {
                if(!empty($request->start_day)) {
                    $start_day = $request->start_day;
                    $end_day = '';
                    $tinhtrang = $request->tinhtrang;
                    $phieuhen = PhieuHenKham::whereDate('NgayHen', '>=', $request->start_day)
                                ->where('TinhTrang_PH', $request->tinhtrang)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                                ->orderBy('NgayHen')
                                ->OrderBy('GioHen')
                                ->paginate(10);
                }
                else if(!empty($request->end_day)) {
                    $start_day = '';
                    $end_day = $request->end_day;
                    $tinhtrang = $request->tinhtrang;
                    $phieuhen = PhieuHenKham::where('TinhTrang_PH', $request->tinhtrang)
                                ->whereDate('NgayHen', '<=', $request->end_day)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                                ->orderBy('NgayHen')
                                ->OrderBy('GioHen')
                                ->paginate(10);
                }
                else {
                    $start_day = '';
                    $end_day = '';
                    $tinhtrang = $request->tinhtrang;
                    $phieuhen = PhieuHenKham::whereMonth('NgayHen', $month)
                                ->where('TinhTrang_PH', $request->tinhtrang)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                                ->orderBy('NgayHen')
                                ->OrderBy('GioHen')
                                ->paginate(10);
                }
            }
            else if(!empty($request->start_day) && !empty($request->end_day)) {
                $start_day = $request->start_day;
                $end_day = $request->end_day;
                $tinhtrang = '';
                $phieuhen = PhieuHenKham::whereBetween('NgayHen', [$request->start_day, $request->end_day])
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->paginate(10);
            }
            else if (!empty($request->start_day)) {
                $start_day = $request->start_day;
                $end_day = '';
                $tinhtrang = '';
                $phieuhen = PhieuHenKham::whereDate('NgayHen', '>=', $request->start_day)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->paginate(10);
            }
            else if(!empty($request->end_day)){
                $start_day = '';
                $tinhtrang = '';
                $end_day = $request->end_day;
                $phieuhen = PhieuHenKham::whereDate('NgayHen', '<=', $request->end_day)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->paginate(10);
            }
            else {
                $start_day = '';
                $end_day = '';
                $tinhtrang = '';
                $phieuhen = PhieuHenKham::whereMonth('NgayHen', $month)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->paginate(10);
            }
            return view('admin.thongke.data_phieuhen', compact('phieuhen', 'start_day', 'end_day', 'tinhtrang'));
        }
    }

    // public function export_Excel_PhieuHen(Request $request) {
    //     return Excel::download(new PhieuHenExport($request->start, $request->end, $request->tinhtrang), 'phieuhen'.'.xlsx');
    // }

    public function getPhieuKham() {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $month = $date->format('m');
        $start_day = '';
        $end_day = '';
        
        $phieukham = PhieuKham::whereMonth('NgayKham', $month)
                    ->where('TinhTrangKham', '>=', 3)
                    ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                    ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                    ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                    ->OrderBy('NgayKham')
                    ->OrderBy('MaPhieuKham')
                    ->paginate(10);
        // dd($phieukham);
        return view('admin.thongke.data_phieukham', compact('phieukham', 'start_day', 'end_day'));
    }

    public function searchPhieuKham(Request $request) {
        if($request->isMethod('POST') && $request->ajax()) {
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $month = $date->format('m');
            
            if(!empty($request->start_day1) && !empty($request->end_day1)) {
                $start_day = $request->start_day1;
                $end_day = $request->end_day1;
                $phieukham = PhieuKham::whereBetween('NgayKham', [$request->start_day1, $request->end_day1])
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->paginate(10);
            }
            else if (!empty($request->start_day1)) {
                $start_day = $request->start_day1;
                $end_day = '';
                $phieukham = PhieuKham::whereDate('NgayKham', '>=', $request->start_day1)
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->paginate(10);
            }
            else if(!empty($request->end_day1)) {
                $start_day = '';
                $end_day = $request->end_day1;
                $phieukham = PhieuKham::whereDate('NgayKham', '<=', $request->end_day1)
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->paginate(10);
            }
            else {
                $start_day = '';
                $end_day = '';
                $phieukham = PhieuKham::whereMonth('NgayKham', $month)
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->paginate(10);
            }
            return view('admin.thongke.data_phieukham', compact('phieukham', 'start_day', 'end_day'));
        }
    }

    // public function export_Excel_PhieuKham(Request $request) {
    //     return Excel::download(new PhieuKhamExport($request->start, $request->end), 'phieukham'.'.xlsx');
    // }

    public function export_Excel_PhieuHen(Request $request) {
        if($request->isMethod('POST') && $request->ajax()) {
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $month = $date->format('m');

            if(!empty($request->start_day) && !empty($request->end_day) && isset($request->tinhtrang)) {
                $phieuhen = PhieuHenKham::whereBetween('NgayHen', [$request->start_day, $request->end_day])
                            ->where('TinhTrang_PH', $request->tinhtrang)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
            else if(isset($request->tinhtrang)) {
                if(!empty($request->start_day)) {
                    $phieuhen = PhieuHenKham::whereDate('NgayHen', '>=', $request->start_day)
                                ->where('TinhTrang_PH', $request->tinhtrang)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                                ->orderBy('NgayHen')
                                ->OrderBy('GioHen')
                                ->get();
                }
                else if(!empty($request->end_day)) {
                    $phieuhen = PhieuHenKham::where('TinhTrang_PH', $request->tinhtrang)
                                ->whereDate('NgayHen', '<=', $request->end_day)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                                ->orderBy('NgayHen')
                                ->OrderBy('GioHen')
                                ->get();
                }
                else {
                    $phieuhen = PhieuHenKham::whereMonth('NgayHen', $month)
                                ->where('TinhTrang_PH', $request->tinhtrang)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                                ->orderBy('NgayHen')
                                ->OrderBy('GioHen')
                                ->get();
                }
            }
            else if(!empty($request->start_day) && !empty($request->end_day)) {
                $phieuhen = PhieuHenKham::whereBetween('NgayHen', [$request->start_day, $request->end_day])
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
            else if (!empty($request->start_day)) {
                $phieuhen = PhieuHenKham::whereDate('NgayHen', '>=', $request->start_day)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
            else if(!empty($request->end_day)){
                $phieuhen = PhieuHenKham::whereDate('NgayHen', '<=', $request->end_day)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
            else {
                $phieuhen = PhieuHenKham::whereMonth('NgayHen', $month)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }

            return view('admin.thongke.data_phieuhen1', compact('phieuhen'));
        }
    }

    public function export_Excel_PhieuKham(Request $request) {
        if($request->isMethod('POST') && $request->ajax()) {
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $month = $date->format('m');
            
            if(!empty($request->start_day) && !empty($request->end_day)) {
                $phieukham = PhieuKham::whereBetween('NgayKham', [$request->start_day, $request->end_day])
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->get();
            }
            else if (!empty($request->start_day)) {
                $phieukham = PhieuKham::whereDate('NgayKham', '>=', $request->start_day)
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->get();
            }
            else if(!empty($request->end_day)) {
                $phieukham = PhieuKham::whereDate('NgayKham', '<=', $request->end_day)
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->get();
            }
            else {
                $phieukham = PhieuKham::whereMonth('NgayKham', $month)
                            ->where('TinhTrangKham', '>=', 3)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('phieukham.*', 'nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.*')
                            ->OrderBy('NgayKham')
                            ->OrderBy('MaPhieuKham')
                            ->get();
            }

            return view('admin.thongke.data_phieukham1', compact('phieukham'));
        }
    }
}
