<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use Illuminate\Http\Request;

class HoSoBenhNhanController extends Controller
{
    public function index(Request $request) {
        $hoso = BenhNhan::paginate(10);

        if($request->ajax()) {
            return view('pagination.data_hoso_benhnhan', compact('hoso'));
        }
        return view('admin.hoso.benhnhan.hoso_benhnhan', compact('hoso'));
    }

    public function searchHoSo(Request $request) {
        if($request->ajax()) {
            $hoso = BenhNhan::where('HoTen', 'like', '%'.$request->hoten.'%')
                    ->where('SDT', 'like', '%'.$request->sdt.'%')
                    ->paginate(10);

            return view('pagination.data_hoso_benhnhan', compact('hoso'));
        }
    }

    public function detail($id) {
        $benhnhan = BenhNhan::find($id);
        $hoso = BenhNhan::where('benhnhan.id', $id)
                ->join('phieukham', 'benhnhan.id', '=', 'phieukham.MaBN')
                ->join('donthuoc', 'phieukham.MaPhieuKham', '=', 'donthuoc.MaPhieuKham')
                ->select('benhnhan.*', 'donthuoc.*', 'phieukham.*')
                ->paginate(10);

        if ($benhnhan) {
            return view('admin.hoso.benhnhan.detail', compact(['benhnhan', 'hoso']));
        }
        else {
            session()->flash('message', 'Không tìm thấy hồ sơ bệnh nhân!');
            return redirect()->back();
        }
    }
}
