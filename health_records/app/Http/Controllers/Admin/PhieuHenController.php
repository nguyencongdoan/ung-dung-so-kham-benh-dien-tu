<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use App\Models\PhieuHenKham;
use Illuminate\Http\Request;

class PhieuHenController extends Controller
{
    public function phieuhen() {
        return view('admin.phieuhen');
    }
    
    public function themphieuhen() {
        return view('admin.themphieuhen');
    }

    public function edit_phieuhen($id) {
        $phieuhen = PhieuHenKham::where('MaPhieuHen', $id)->first();
        $user = BenhNhan::where('email', $phieuhen->Email)->first();

        return view("admin.edit_phieuhen", compact('phieuhen', 'user'));
    }
}
