<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slide_Logo;
use App\Models\ThongTinPhongKham;
use Illuminate\Http\Request;

class ChangeThemeController extends Controller
{
    public function changeTheme() {
        $slide = Slide_Logo::all();
        
        return view('admin.changetheme.index', compact('slide'));
    }

    public function move_img($name_img, $path) {
        $file = $name_img;
        $img_name = $name_img->getClientOriginalName();
        $filename = time().$img_name;
        $file->move(public_path($path), $filename);
        
        return $filename;
    }

    public function handle_edit(Request $request) { 
        if($request->isMethod('POST')) {
            $thongtin = ThongTinPhongKham::first();

            if($request->diachi != $thongtin->DiaChi) {
                $thongtin->DiaChi = $request->diachi;
            }
            
            if($request->sdt != $thongtin->SDT) {
                $thongtin->SDT = $request->sdt;
            }

            if($request->email != $thongtin->Email) {
                $thongtin->Email = $request->email;
            }

            if($request->diachi_map != $thongtin->DiaChi_Map) {
                $thongtin->DiaChi_Map = $request->diachi_map;
            }

            if($request->banner_content != $thongtin->Banner_Content) {
                $thongtin->Banner_Content = $request->banner_content;
            }

            if(isset($request->logo_header_admin)) {
                $path = './admin/assets/images/';
                $filename = $this->move_img($request->logo_header_admin, $path);
                $thongtin->Logo_Header_Admin = $filename;
            }

            if(isset($request->logo_header)) {
                $path = './client/assets/images/';
                $filename = $this->move_img($request->logo_header, $path);
                $thongtin->Logo_Header = $filename;
            }

            if(isset($request->logo_footer)) {
                $path = './client/assets/images/';
                $filename = $this->move_img($request->logo_footer, $path);
                $thongtin->Logo_Footer = $filename;
            }

            if(isset($request->banner_image)) {
                $path = './client/assets/images/';
                $filename = $this->move_img($request->banner_image, $path);
                $thongtin->Banner_Image = $filename;
            }
          
            $check_update = $thongtin->save();

            if($check_update) {
                session()->flash('message', 'Cập nhật thông tin website thành công!');
                session()->flash('status', 200);
            }
            else {
                session()->flash('message', 'Cập nhật thông tin website thất bại!');
                session()->flash('status', 400);
            }
            return redirect()->back()->withInput();
        }
    }

    public function edit_slide(Request $request) {
        if ($request->isMethod('POST')) { 
            $arr = $request->slide_logo;
            $arr_key = array_keys($arr);
           
            if(isset($arr_key)) {
                $path = './client/assets/images/';
                foreach($arr_key as $item) {
                    $slide = Slide_Logo::find($request->slide_id[$item]);
                    if($slide) {
                        $filename = $this->move_img($arr[$item], $path);
                        
                        $slide->Slide_Image = $filename;
                        $check = $slide->save();
                        
                        if(!$check) {
                            session()->flash('message', 'Cập nhật Slide Logo thất bại!');
                            session()->flash('status', 400);
                            return redirect()->back()->withInput();
                        }
                    }
                }

                session()->flash('message', 'Cập nhật Slide Logo thành công!');
                session()->flash('status', 200);
                return redirect()->back()->withInput();
            }
        }
    }

    public function create_slide(Request $request) {
        if($request->isMethod('POST')) {
            $arr = $request->slide_logo;

            if(isset($arr)) {
                $path = './client/assets/images/';
                for($i = 0; $i < count($arr); $i++) {
                    $filename = $this->move_img($arr[$i], $path);
                        
                    $check = Slide_Logo::create(['Slide_Image' => $filename]);

                    if(!$check) {
                        session()->flash('message', 'Thêm Slide Logo thất bại!');
                        session()->flash('status', 400);
                        return redirect()->back()->withInput();
                    }
                }

                session()->flash('message', 'Thêm Slide Logo thành công!');
                session()->flash('status', 200);
                return redirect()->back()->withInput();
            }
        }
    }

    public function remove_slide(Request $request) {
        if($request->isMethod('POST')) {
            if(isset($request->id_slide)) {
                $slide = Slide_Logo::find($request->id_slide)->delete();
                $slide = Slide_Logo::all();
                
                return response()->json([
                    'data' => $slide,
                    'status' => 200,
                    'message' => 'Xóa Slide Logo thành công!',
                    'alert' => 'success'
                ]);
            }
            return response()->json([
                'status' => 400,
                'message' => 'Xóa Slide Logo thất bại!',
                'alert' => 'error'
            ]);
        }
    }
}
