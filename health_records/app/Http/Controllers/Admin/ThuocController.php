<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Thuoc;
use Illuminate\Http\Request;

class ThuocController extends Controller
{
    public function index() {
        return view('admin.thuoc.danhsach_thuoc');
    }

    public function create() {
        return view('admin.thuoc.create_thuoc');
    }

    public function edit($id) {
        $thuoc = Thuoc::where('MaThuoc', $id)->first();

        return view('admin.thuoc.edit_thuoc', compact('thuoc'));
    }
}
