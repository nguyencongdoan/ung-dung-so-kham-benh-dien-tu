<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HoaDon;
use App\Models\Payments;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HoaDonController extends Controller
{
    public function index() {
        return view('admin.hoadon.danhsach_hd');
    }

    public function detail($id) {
        $hoadon = HoaDon::where('hoadon.MaHD', $id)
                    ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                    ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                    ->first();

        $payments = Payments::where('MaHD', $hoadon->MaHD)->first();
        
        return view('admin.hoadon.detail', compact('hoadon', 'payments'));
    }
    
    public function delete($id) {
        $hoadon = HoaDon::where('MaHD', $id)->delete();

        if($hoadon) {
            session()->flash('message', 'Xóa hoá đơn thành công!');
        }
        else {
            session()->flash('message', 'Xóa hoá đơn thất bại!');
        }
        return redirect(route('admin.hoadon'));
    }
}
