<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CT_PhieuNhapThuoc;
use App\Models\DonVi;
use Illuminate\Http\Request;

class PhieuNhapThuocController extends Controller
{
    public function index() {
        return view('admin.phieunhapthuoc.danhsach_phieunhap');
    }

    public function create() {
        $donvi = DonVi::all();
        
        return view('admin.phieunhapthuoc.create_phieunhap', compact('donvi'));
    }

    public function edit($id) {
        $phieunhap = CT_PhieuNhapThuoc::where('ct_phieunhapthuoc.MaPhieuNhap', $id)
                    ->join('thuoc', 'ct_phieunhapthuoc.MaThuoc', '=', 'thuoc.MaThuoc')
                    ->select('ct_phieunhapthuoc.*', 'thuoc.*')
                    ->get();
        
        $donvi = DonVi::select('MaDonVi', 'TenDonVi')->get();

        $donvi = json_encode($donvi);

        $id_phieunhap = $id;
        $mathuocs = array();
        foreach($phieunhap as $key => $value) {
            $mathuocs[$key] = $value->MaThuoc;
        }
        return view('admin.phieunhapthuoc.edit_phieunhap', compact('phieunhap', 'donvi', 'mathuocs', 'id_phieunhap'));
    }
}
