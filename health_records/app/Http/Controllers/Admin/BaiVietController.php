<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BaiViet;
use App\Models\TheLoai_BV;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class BaiVietController extends Controller
{
    public function getAllBaiViet() {
        return view('admin.baiviet.danhsach_bv');
    }
    
    public function getAllNews() {
        $baiviet = BaiViet::join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                            ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                            ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                            ->orderBy('NgayTao', 'DESC')
                            ->paginate(8);

        return view('client.tintuc.pagination_tintuc', compact('baiviet'));
    }

    public function thembaiviet() {
        $theloai = TheLoai_BV::all();

        return view('admin.baiviet.thembaiviet', compact('theloai'));
    }

    public function store(Request $request) {
        if($request->isMethod('POST')) { 
            $validator = Validator::make(
                $request->all(), 
                [
                    'tieude' => 'required',
                    'tomtat' => 'required',
                    'thumnail' => 'required',
                    'noidung' => 'required',
                    'theloai' => 'required',
                ],

                [
                    'required' => ':attribute không được để trống',
                    'max' => ':attribute không được lớn hơn :max',
                ],

                [
                    'tieude' => 'Tiêu đề',
                    'tomtat' => 'Tóm tắt',
                    'thumnail' => 'Thumnail',
                    'noidung' => 'Nội dung',
                    'theloai' => 'Thể loại',
                ]
            );
            
            if($validator->fails()) {
                $request->flash();
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else {
                $date = Carbon::now('Asia/Ho_Chi_Minh'); 
                $date = $date->format('Y-m-d');

                if($request->has('thumnail')) {
                    $file = $request->thumnail;
                    $ext = $request->thumnail->extension();
                    $filename = time().'.'.$ext;
                    $file->move(public_path('./uploads/images/'), $filename);
                    $request->merge(['anh' => $filename]);
                }
                
                $baiviet = BaiViet::create([
                    'MaTL' => $request->theloai,
                    'MaNV' => $request->id_user,
                    'TieuDe' => $request->tieude,
                    'TomTat' => $request->tomtat,
                    'Thumnail' => $request->anh,
                    'NgayTao' => $date,
                    'NoiDung' => $request->noidung,
                ]);

                if($baiviet) {
                    $request->session()->flash('message', 'Thêm bài viết thành công!');
                    return redirect(route('admin.baiviet.thembaiviet'));
                }
                else {
                    $request->session()->flash('message', 'Thêm bài viết thất bại!');
                    return redirect(route('admin.baiviet.thembaiviet'));
                }
            }
        }
    }

    public function detail($id) {
        $baiviet = BaiViet::where('MaBV', $id)
                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->first();  

        $top_bv = BaiViet::join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->orderBy('baiviet.NgayTao', 'DESC')
                    ->take(3)->get();
                
        $theloai = BaiViet::where('MaBV', $id)->first();
        $bv_lq = BaiViet::where('baiviet.MaTL', $theloai->MaTL)
                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->orderBy('baiviet.NgayTao', 'DESC')
                    ->take(3)->get();
                    
        return view('admin.baiviet.detail', compact(['baiviet', 'top_bv', 'bv_lq']));
    }

    public function edit($id) {
        $baiviet = BaiViet::where('MaBV', $id)
                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                    ->first();  
        $theloai = TheLoai_BV::all();

        return view('admin.baiviet.edit_baiviet', compact('baiviet', 'theloai'));
    }
   
    public function handle_edit(Request $request) {
        if($request->isMethod('POST')) { 
            $validator = Validator::make(
                $request->all(), 
                [
                    'tieude' => 'required',
                    'tomtat' => 'required',
                    'noidung' => 'required',
                    'theloai' => 'required',
                ],

                [
                    'required' => ':attribute không được để trống',
                    'max' => ':attribute không được lớn hơn :max',
                ],

                [
                    'tieude' => 'Tiêu đề',
                    'tomtat' => 'Tóm tắt',
                    'thumnail' => 'Thumnail',
                    'noidung' => 'Nội dung',
                    'theloai' => 'Thể loại',
                ]
            );
            
            if($validator->fails()) {
                $request->flash();
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            else {
                $date = Carbon::now('Asia/Ho_Chi_Minh'); 
                $date = $date->format('Y-m-d');
                
                $arr = [
                    'MaTL' => $request->theloai,
                    'MaNV' => $request->id_user,
                    'TieuDe' => $request->tieude,
                    'TomTat' => $request->tomtat,
                    'NoiDung' => $request->noidung,
                ];

                if($request->has('thumnail')) {
                    $file = $request->thumnail;
                    $ext = $request->thumnail->extension();
                    $filename = time().'.'.$ext;
                    $file->move(public_path('./uploads/images/'), $filename);
                    $request->merge(['anh' => $filename]);
                    $arr['Thumnail'] = $request->anh;
                }
                
                $baiviet = BaiViet::where('MaBV', $request->mabv)->update($arr);

                if($baiviet) {
                    $request->session()->flash('message', 'Chỉnh sửa bài viết thành công!');
                    return redirect()->back()->withInput();
                }
                else {
                    $request->session()->flash('message', 'Chỉnh sửa bài viết thất bại!');
                    return redirect()->back()->withInput();
                }
            }
        }
    }

    public function delete($id) {
        $baiviet = BaiViet::where('MaBV', $id)->delete();

        if($baiviet) {
            session()->flash('message', 'Xóa bài viết thành công!');
        }
        else {
            session()->flash('message', 'Xóa bài viết thất bại!');
        }
        return redirect(route('admin.baiviet'));
    }
}
