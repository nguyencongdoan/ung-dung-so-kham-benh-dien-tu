<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ChucVu;
use App\Models\ChuyenMon;
use App\Models\NhanVien;
use Illuminate\Http\Request;

class HoSoNhanVienController extends Controller
{
    public function index(Request $request) {
        $hoso = NhanVien::join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                        ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                        ->select('chuyenmon.*', 'chucvu.*', 'nhanvien.*')
                        ->paginate(10);

        $chucvu = ChucVu::get();
        
        if($request->ajax()) {
            return view('pagination.data_hoso_nhanvien', compact('hoso'));
        }

        return view('admin.hoso.nhanvien.hoso_nhanvien', compact(['hoso', 'chucvu']));
    }

    public function searchHoSo(Request $request) {
        if($request->ajax()) {
            $hoso = NhanVien::where('HoTen', 'like', '%'.$request->hoten.'%')
                    ->where('SDT', 'like', '%'.$request->sdt.'%')
                    ->join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                    ->where('TenChucVu', 'like', '%'.$request->chucvu.'%')
                    ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                    ->select('chuyenmon.*', 'chucvu.*', 'nhanvien.*')
                    ->paginate(10);

            $chucvu = ChucVu::get();

            return view('pagination.data_hoso_nhanvien', compact(['hoso', 'chucvu']));
        }
    }

    public function create() {
        $chucvu = ChucVu::all();
        $chuyenmon = ChuyenMon::all();

        return view('admin.hoso.nhanvien.create', compact('chucvu', 'chuyenmon'));
    }
    
    public function store(Request $request) {
        if($request->isMethod('POST')) {
            $hashPassword = bcrypt($request->password);

            if($request->has('anhdaidien')) {
                $file = $request->anhdaidien;
                $ext = $request->anhdaidien->extension();
                $filename = time().'.'.$ext;
                $file->move(public_path('./uploads/images/'), $filename);
                $request->merge(['avatar' => $filename]);
            }

            $hoso = NhanVien::create([
                'HoTen' => $request->hoten,
                'SDT' => $request->sdt,
                'NgaySinh' => $request->ngaysinh,
                'GioiTinh' => $request->gioitinh,
                'AnhDaiDien' => $request->avatar,
                'email' => $request->email,
                'password' => $hashPassword,
                'DiaChi' => $request->diachi,
                'MaChucVu' => $request->chucvu,
                'MaChuyenMon' => $request->chuyenmon,
            ]);
            $chucvu = ChucVu::all();
            $chuyenmon = ChuyenMon::all();

            if($hoso) {
                session()->flash('message', 'Thêm mới nhân viên thành công!');
                session()->flash('status', '200');
                return view('admin.hoso.nhanvien.create', compact(['chucvu', 'chuyenmon']));
            }
            else {
                session()->flash('message', 'Thêm mới nhân viên thất bại!');
                session()->flash('status', '400');
                return view('admin.hoso.nhanvien.create', compact(['chucvu', 'chuyenmon']));
            }
        }
    }

    public function detail($id) {
        $nhanvien = NhanVien::where('id', $id)
                    ->join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                    ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                    ->first();
        if ($nhanvien) {
            return view('admin.hoso.nhanvien.detail', compact('nhanvien'));
        }
        else {
            session()->flash('message', 'Không tìm thấy hồ sơ nhân viên!');
            return redirect()->back();
        }
    }

    public function edit($id) {
        if(isset($id)) {
            $hoso = NhanVien::where('id', $id)
                        ->join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                        ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                        ->select('nhanvien.*', 'chucvu.*', 'chuyenmon.*')
                        ->first();
            $chucvu = ChucVu::all();
            $chuyenmon = ChuyenMon::all();

            return view('admin.hoso.nhanvien.edit', compact('hoso', 'chucvu', 'chuyenmon'));
        }
        return redirect()->back();
    }

    public function handle_edit(Request $request) {
        if($request->isMethod('POST')) {
            $hoso = NhanVien::find($request->id_nv);
            $hoso->HoTen = $request->hoten;
            $hoso->DiaChi = $request->diachi;
            $hoso->SDT = $request->sdt;
            $hoso->NgaySinh = $request->ngaysinh;
            $hoso->GioiTinh = $request->gioitinh;
            $hoso->MaChucVu = $request->chucvu;
            $hoso->MaChuyenMon = $request->chuyenmon;
                
            if(isset($request->anhdaidien)) {
                $file = $request->anhdaidien;
                $ext = $request->anhdaidien->extension();
                $filename = time().'.'.$ext;
                $file->move(public_path('./uploads/images/'), $filename);
                $request->merge(['avatar' => $filename]);
                $hoso->AnhDaiDien = $request->avatar;
            }

            if($hoso->email != $request->email) {
                $check_email = NhanVien::where('email', $request->email)->exists();

                if(!$check_email) {
                    $hoso->email = $request->email;
                }
                else {
                    session()->flash('message', 'Email đã tồn tại. Vui lòng nhập lại!');
                    session()->flash('status', '400');
                    return redirect()->back();
                }
            }

            if(isset($request->password) && !empty($request->password)) {
                $hashPassword = bcrypt($request->password);
                $hoso->password = $hashPassword;
            }
            
            $hoso->save();
            session()->flash('message', 'Cập nhật thông tin nhân viên thành công!');
            session()->flash('status', '200');
            return redirect()->back();
        }
    }

    public function delete($id) {
        if(isset($id) && !empty($id)) {
            $hoso = NhanVien::find($id)->delete();

            if($hoso) {
                session()->flash('message', 'Xóa nhân viên thành công!');
                session()->flash('status', '200');
            }
            else {
                session()->flash('message', 'Xóa nhân viên thất bại!');
                session()->flash('status', '400');
            }
        }
        else {
            session()->flash('message', 'Không tồn tại tài khoản!');
            session()->flash('status', '400');
        }
        return redirect()->back();
    }
}
