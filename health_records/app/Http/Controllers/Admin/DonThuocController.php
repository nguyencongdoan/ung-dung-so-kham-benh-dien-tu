<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CT_DonThuoc;
use App\Models\DonThuoc;
use App\Models\DonVi;
use App\Models\HoaDon;
use App\Models\PhieuKham;
use App\Models\Thuoc;
use Illuminate\Http\Request;
use Carbon\Carbon;
require '../vendor/autoload.php';

class DonThuocController extends Controller
{
    public function handle_edit(Request $request) {
        if ($request->isMethod('POST')) {
            $arr_dt = [
                'NgayTaoDon' => $request->ngay_tao_don,
                'NgayTaiKham' => $request->ngay_tai_kham,
            ];

            $donthuoc = DonThuoc::where('MaDonThuoc', $request->madonthuoc)->update($arr_dt);

            if($donthuoc) { // cap nhat don thuoc thanh cong
                $ds_mathuoc = $request->mathuoc; 
                foreach($ds_mathuoc as $key => $item) {
                    $check = CT_DonThuoc::where('MaDonThuoc', $request->madonthuoc)
                    ->where('MaThuoc', $item)
                    ->exists();

                    if($check) {
                        $arr_ct = [
                            'MaThuoc' => $item,
                            'MaDonVi' => $request->donvi[$key],
                            'SoLuong' => $request->soluong[$key],
                            'CachDung' => $request->cachdung[$key],
                            'LieuDung' => $request->lieudung[$key],
                        ];

                        $ct_donthuoc = CT_DonThuoc::where('ct_donthuoc.MaDonThuoc', $request->madonthuoc)
                        ->where('MaThuoc', $item)
                        ->update($arr_ct);
                    }
                    else {
                        $ct_donthuoc = CT_DonThuoc::create([
                            'MaDonThuoc' => $request->madonthuoc,
                            'MaThuoc' => $item,
                            'MaDonVi' => $request->donvi[$key],
                            'SoLuong' => $request->soluong[$key],
                            'CachDung' => $request->cachdung[$key],
                            'LieuDung' => $request->lieudung[$key],
                        ]);
                    }
                }
                return redirect(route('admin.donthuoc.conform_donthuoc', $request->madonthuoc));
            }
        }
    }

    public function dsDonThuoc() {
        return view('admin.donthuoc.ds_donthuoc');
    }

    public function detailDonThuoc($id, $message = null) {
        $donthuoc = DonThuoc::where('MaDonThuoc', $id)
                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                    ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                    ->select('benhnhan.*', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
                    ->first();

        $ct_donthuoc = CT_DonThuoc::where('ct_donthuoc.MaDonThuoc', $donthuoc->MaDonThuoc)
                    ->join('donthuoc', 'ct_donthuoc.MaDonThuoc', 'donthuoc.MaDonThuoc')
                    ->join('thuoc', 'ct_donthuoc.MaThuoc', 'thuoc.MaThuoc')
                    ->join('donvi', 'ct_donthuoc.MaDonVi', 'donvi.MaDonVi')
                    ->select('ct_donthuoc.*', 'donthuoc.*', 'ct_donthuoc.SoLuong as SL', 'thuoc.*', 'donvi.*')
                    ->get();
        
        $hoadon = HoaDon::where('MaDonThuoc', $id)
                        ->where('TinhTrang_TT', 1)->exists();

        if($hoadon) {
            $hoadon = true;
        }

        return view('admin.donthuoc.detail_donthuoc', compact(['donthuoc', 'ct_donthuoc', 'hoadon', 'message']));
    }

    public function edit($id) {
        $user = DonThuoc::where('MaDonThuoc', $id)
                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                ->first();
        $donvi = DonVi::select('MaDonVi', 'TenDonVi')->get();

        $donvi = json_encode($donvi);

        $ds_thuoc = CT_DonThuoc::where('MaDonThuoc', $id)->get();

        $mathuocs = array();
        foreach($ds_thuoc as $key => $value) {
            $mathuocs[$key] = $value->MaThuoc;
            $sl_thuoc = $value->SoLuong;

            // cập nhật lại số lượng thuốc trước khi tạo
            $thuoc = Thuoc::where('MaThuoc', $mathuocs[$key])->first();
            $soluong_thuoc = $thuoc->SoLuong;
            $update_sl = $soluong_thuoc + $sl_thuoc;
            $thuoc = Thuoc::where('MaThuoc', $mathuocs[$key])->update(['SoLuong' => $update_sl]);
        }

        return view('admin.donthuoc.edit_donthuoc', compact(['id', 'user', 'donvi', 'mathuocs']));
    }

    public function conformDonThuoc($id = null) {
        $donthuoc = DonThuoc::where('MaDonThuoc', $id)
                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                    ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                    ->select('benhnhan.*', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
                    ->first();

        $ct_donthuoc = CT_DonThuoc::where('ct_donthuoc.MaDonThuoc', $donthuoc->MaDonThuoc)
                    ->join('donthuoc', 'ct_donthuoc.MaDonThuoc', 'donthuoc.MaDonThuoc')
                    ->join('thuoc', 'ct_donthuoc.MaThuoc', 'thuoc.MaThuoc')
                    ->join('donvi', 'ct_donthuoc.MaDonVi', 'donvi.MaDonVi')
                    ->select('ct_donthuoc.*', 'donthuoc.*', 'ct_donthuoc.SoLuong as SL', 'thuoc.*', 'donvi.*')
                    ->get();
        return view('admin.donthuoc.conform_donthuoc', compact(['donthuoc', 'ct_donthuoc']));
    }

    public function handleDonThuoc(Request $request) {
        if($request->isMethod('POST')) {
            // cập nhật tình trạng phiếu khám đã khám xong
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $time_end = $date->format('Y-m-d H:i:s');
            $arr = [
                'ThoiGianKT' => $time_end,
                'TinhTrangKham' => 3,
                'updated_at' => $time_end
            ];

            $phieukham = Phieukham::where('MaPhieuKham', $request->maphieukham)->update($arr);
            if($phieukham) {
                return redirect(route('admin.phieukham'));
            }
            else {
                return redirect()->back();
            }
        }
    }

    public function exportPDF(Request $request) {
        $donthuoc = DonThuoc::where('MaDonThuoc', $request->id)
                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                    ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                    ->select('benhnhan.*', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
                    ->first();
        
        $ct_donthuoc = CT_DonThuoc::where('ct_donthuoc.MaDonThuoc', $donthuoc->MaDonThuoc)
                    ->join('donthuoc', 'ct_donthuoc.MaDonThuoc', 'donthuoc.MaDonThuoc')
                    ->join('thuoc', 'ct_donthuoc.MaThuoc', 'thuoc.MaThuoc')
                    ->join('donvi', 'ct_donthuoc.MaDonVi', 'donvi.MaDonVi')
                    ->select('ct_donthuoc.*', 'donthuoc.*', 'ct_donthuoc.SoLuong as SL', 'thuoc.*', 'donvi.*')
                    ->get();
                    
        return view('admin.donthuoc.pdf_donthuoc',  compact(['donthuoc', 'ct_donthuoc']));
    }

    public function xacnhan_donthuoc($id) {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $time_end = $date->format('Y-m-d H:i:s');
        $donthuoc = DonThuoc::where('MaDonThuoc', $id)->first();
        $arr = [
            'ThoiGianKT' => $time_end,
            'TinhTrangKham' => 3
        ];
        PhieuKham::where('MaPhieuKham', $donthuoc->MaPhieuKham)->update($arr);
        
        return redirect(route('admin.phieukham'));
    }
}
