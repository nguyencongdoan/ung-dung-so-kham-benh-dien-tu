<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use App\Models\CT_DonThuoc;
use App\Models\DonVi;
use App\Models\PhieuHenKham;
use App\Models\PhieuKham;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PhieuKhamController extends Controller
{
    public function phieukham() {
        return view('admin.phieukham');
    }
   
    public function thongtin_phieukham($id) {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $date = $date->format('Y-m-d H:i:s');
        
        $data = PhieuKham::where('phieukham.MaPhieuKham', $id)
                ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                ->select('phieukham.*', 'benhnhan.*')
                ->first();
        
        $arr = [
            'ThoiGianBD' => $date,
            'TinhTrangKham' => 1,
        ];
        $phieukham = PhieuKham::where('MaPhieuKham', '=', $id)->update($arr);
        return view('admin.thongtin_pk', compact('data'));
    }

    public function themdonthuoc($id = null, $chuandoan = null) {
        $user = PhieuKham::where('MaPhieuKham', '=', $id)
                ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                ->select('benhnhan.*', 'phieukham.*')
                ->first();
        $donvi = DonVi::all();

        return view('admin.themdonthuoc', compact('user','donvi', 'id', 'chuandoan'));
    }

    public function themphieukham($id = null) {
        if(!empty($id)) {
            $user = BenhNhan::where('id', $id)->first();
            return view('admin.themphieukham', compact('user'));
        }
        return view('admin.themphieukham');
    }

    public function ds_phieutaikham() {
        return view('admin.phieutaikham.ds_phieutaikham');
    }
}
