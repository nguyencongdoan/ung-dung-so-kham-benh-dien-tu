<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ChucVu;
use Illuminate\Http\Request;
use App\Models\NhanVien;

class BacSiController extends Controller
{
    public function getAllBacSi() {
        $data = NhanVien::join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                ->where('chucvu.TenChucVu', '=', 'Bác sĩ')
                ->select('nhanvien.*', 'chucvu.*', 'chuyenmon.*')
                ->get();
        
        if($data->count() > 0) {
            return response()->json([
                'data' => $data,
                'status' => '200',
            ]);
        }
        else {
            return response()->json([
                'status' => '200',
                'message' => 'Không tìm thấy bác sĩ!',
                'alert' => 'error'
            ]);
        }
    }

    public function getInfoBacSi() {
        $bacsi = NhanVien::join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                ->where('chucvu.TenChucVu', '=', 'Bác sĩ')
                ->select('nhanvien.*', 'chucvu.*', 'chuyenmon.*')
                ->paginate(8);
        
        return view('client.pagination_info_bs', compact('bacsi'));
    }

    public function searchBacSi(Request $request) {
        $bacsi = NhanVien::join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                            ->join('chuyenmon', 'nhanvien.MaChuyenMon', '=', 'chuyenmon.MaChuyenMon')
                            ->where('chucvu.TenChucVu', '=', 'Bác sĩ')
                            ->where('HoTen', 'like', '%'.$request->hoten.'%')
                            ->select('nhanvien.*', 'chucvu.*', 'chuyenmon.*')
                            ->paginate(4);

        return view('client.pagination_info_bs', compact('bacsi'));
    }
}
