<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\HoaDon;
use App\Models\Payments;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ThanhToanController extends Controller
{
    public function vnpay_payment(Request $request) {
        if($request->isMethod('POST')) {
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $date = $date->toDateString();

            // kiểm tra nếu người dùng chọn phương thức thanh toán là đưa tiền mặt trực tiếp thì tạo hóa đơn
            if($request->thanhtoan == 0){
                $hoadon = HoaDon::create([
                    'MaDonThuoc' => $request->madonthuoc,
                    'TongTien' => $request->total,
                    'NgayLap' => $date,
                    'PhuongThuc_TT' => 'Thanh toán trực tiếp',
                    'TinhTrang_TT' => 1,
                ]);

                if($hoadon) {
                    return redirect(route('admin.donthuoc.detailDonThuoc', ['id' => $request->madonthuoc,'message' => 'Success']));
                }
                else {
                    return redirect(route('admin.donthuoc.detailDonThuoc', ['id' => $request->madonthuoc,'message' => 'Error']));
                }
            }
            else {
                error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
                date_default_timezone_set('Asia/Ho_Chi_Minh');

                $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
                $vnp_Returnurl = route('api.admin.vnpay_return');
                $vnp_TmnCode = "FBDDJ89S";//Mã website tại VNPAY 
                $vnp_HashSecret = "HYAZEZOKAHYMKLLPWFSSBISRRSHPGDHK"; //Chuỗi bí mật

                $vnp_TxnRef = $request->madonthuoc; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
                $vnp_OrderInfo = "Thanh toán hóa đơn thuốc"; // Thông tin mô tả nội dung thanh toán 
                $vnp_OrderType = "billpayment"; // loại hóa đơn "billpayment" là thanh toán hóa đơn
                $vnp_Amount = $request->total * 100; // số tiền phải thanh toán
                $vnp_Locale = "vn"; // // lựa chọn ngôn ngữ Có 2 loại Việt Nam: "vn", English: en
                $vnp_BankCode = $_POST['bank_code'];;
                $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
                
                $inputData = array(
                    "vnp_Version" => "2.1.0",
                    "vnp_TmnCode" => $vnp_TmnCode,
                    "vnp_Amount" => $vnp_Amount,
                    "vnp_Command" => "pay",
                    "vnp_CreateDate" => date('YmdHis'),
                    "vnp_CurrCode" => "VND",
                    "vnp_IpAddr" => $vnp_IpAddr,
                    "vnp_Locale" => $vnp_Locale,
                    "vnp_OrderInfo" => $vnp_OrderInfo,
                    "vnp_OrderType" => $vnp_OrderType,
                    "vnp_ReturnUrl" => $vnp_Returnurl,
                    "vnp_TxnRef" => $vnp_TxnRef,
                );

                if (isset($vnp_BankCode) && $vnp_BankCode != "") {
                    $inputData['vnp_BankCode'] = $vnp_BankCode;
                }

                //var_dump($inputData);
                ksort($inputData);
                $query = "";
                $i = 0;
                $hashdata = "";
                foreach ($inputData as $key => $value) {
                    if ($i == 1) {
                        $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
                    } else {
                        $hashdata .= urlencode($key) . "=" . urlencode($value);
                        $i = 1;
                    }
                    $query .= urlencode($key) . "=" . urlencode($value) . '&';
                }

                $vnp_Url = $vnp_Url . "?" . $query;
                if (isset($vnp_HashSecret)) {
                    $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//  
                    $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
                }
                $returnData = array('code' => '00'
                    , 'message' => 'success'
                    , 'data' => $vnp_Url);
                    if (isset($_POST['redirect'])) {
                        header('Location: ' . $vnp_Url);
                        die();
                    } else {
                        echo json_encode($returnData);
                    }
            }
        }
    }

    public function vnpay_return() {
        $vnp_SecureHash = $_GET['vnp_SecureHash'];
        $inputData = array();
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
        }

        $vnp_HashSecret = "HYAZEZOKAHYMKLLPWFSSBISRRSHPGDHK"; //Chuỗi bí mật
         
        $secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);

        if ($secureHash == $vnp_SecureHash) {
            $madonthuoc = $_GET['vnp_TxnRef'];
            if ($_GET['vnp_ResponseCode'] == '00') {
                $money = $_GET['vnp_Amount'] / 100;
                $note = $_GET['vnp_OrderInfo'];
                $vnp_response_code = $_GET['vnp_ResponseCode'];
                $code_vnpay = $_GET['vnp_TransactionNo'];
                $code_bank = $_GET['vnp_BankCode'];
                $time = $_GET['vnp_PayDate'];
                
                // khi giao dịch thành công => tạo hóa đơn và payments
                $hoadon = HoaDon::create([
                    'MaDonThuoc' => $madonthuoc,
                    'TongTien' => $money,
                    'NgayLap' => $time,
                    'PhuongThuc_TT' => "Thanh toán Online.",
                    'TinhTrang_TT' => 1,
                ])->id;

                $payments = Payments::create([
                    'MaHD' => $hoadon,
                    'NoiDung' => $note,
                    'Vnp_response_code' => $vnp_response_code,
                    'Code_bank' => $code_bank,
                    'Code_vnpay' => $code_vnpay,
                    'ThoiGian' => $time
                ]);

                if($payments) {
                    return redirect(route('admin.donthuoc.detailDonThuoc', ['id' => $madonthuoc,'message' => 'Success']));
                }
                
            } else {
                return redirect(route('admin.donthuoc.detailDonThuoc', ['id' => $madonthuoc,'message' => 'Error']));
            }
        } else {
            return redirect(route('admin.donthuoc', ['message' => 'Chữ ký không hợp lệ!']));
        }

    }
}
