<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use App\Models\CT_DonThuoc;
use App\Models\DonThuoc;
use App\Models\DonVi;
use App\Models\HoaDon;
use App\Models\PhieuKham;
use App\Models\Thuoc;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DonThuocController extends Controller
{
    // edit_donthuoc
    public function load_don_thuoc($id) {
        $donthuoc = CT_DonThuoc::where('ct_donthuoc.MaDonThuoc', $id)
                        ->join('donthuoc', 'ct_donthuoc.MaDonThuoc', '=', 'donthuoc.MaDonThuoc')
                        ->join('thuoc', 'ct_donthuoc.MaThuoc', '=', 'thuoc.MaThuoc')
                        ->join('donvi', 'ct_donthuoc.MaDonVi', 'donvi.MaDonVi')
                        ->select('ct_donthuoc.*', 'thuoc.MaThuoc', 'ct_donthuoc.SoLuong as SL', 'thuoc.TenThuoc', 'thuoc.DonGia', 'thuoc.SoLuong', 'donvi.*', 'donthuoc.NgayTaiKham')
                        ->get();
       
        if ($donthuoc->count() > 0) {
            return response()->json([
                'data' => $donthuoc,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'Chưa có đơn thuốc!',
                'status' => 400,
                'alert' => 'error'
            ]);
        }
    }

    public function detail_toathuoc(Request $request)
    {
        $ct_donthuoc = CT_DonThuoc::where('ct_donthuoc.MaDonThuoc', $request->id)
                        ->join('donthuoc', 'ct_donthuoc.MaDonThuoc', '=', 'donthuoc.MaDonThuoc')
                        ->join('thuoc', 'ct_donthuoc.MaThuoc', '=', 'thuoc.MaThuoc')
                        ->join('donvi', 'ct_donthuoc.MaDonVi', 'donvi.MaDonVi')
                        ->select('ct_donthuoc.*', 'thuoc.MaThuoc', 'thuoc.TenThuoc', 'donvi.*')
                        ->get();

        if ($ct_donthuoc->count() > 0) {
            return response()->json([
                'data' => $ct_donthuoc,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'Chưa có đơn thuốc!',
                'status' => 400,
                'alert' => 'error'
            ]);
        }
    }

    public function getHoSo_BN(Request $request)
    {
        if ($request->isMethod('POST')) {
            $hoso = DonThuoc::where('donthuoc.MaBN', $request->id)
                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                ->select('benhnhan.HoTen as HoTen_BN', 'benhnhan.NgaySinh as NgaySinh_BN', 'benhnhan.SDT', 'nhanvien.HoTen as HoTen_BS', 'donthuoc.*', 'phieukham.*')
                ->paginate(5);

            return view('pagination.data_load_hoso_bn', compact('hoso'));
        }
    }

    public function createDonThuoc(Request $request)
    {
        if ($request->isMethod('POST')) {
            $phieukham = PhieuKham::where('MaPhieuKham', '=', $request->id)->first();
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $time_end = $date->format('Y-m-d H:i:s');
            $chuandoan = $request->chuandoan;
            // dd($request->all());
            // Cập nhật cân nặng cho bệnh nhân
            if (!empty($request->hoten_nt) || !empty($request->cannang)) {
                $mabn = $phieukham->MaBN;
                $benhnhan = BenhNhan::where('id', '=', $mabn)->first();
                $benhnhan->CanNang = $request->cannang;
                $benhnhan->save();
            }
            // Cập nhật thông tin cho phiếu khám
            $arr = [
                'HoTenNguoiThan' => $request->hoten_nt,
                'TrieuChung' => $request->trieuchung,
                'TienSuBenh' => $request->tiensubenh,
                'TinhTrangKham' => 2,
                'updated_at' => $time_end
            ];

            $phieukham = DB::table('phieukham')
                ->where('MaPhieuKham', '=', $request->id)
                ->update($arr);

            return redirect(route('admin.phieukham.themdonthuoc', ['id' => $request->id, 'chuandoan' => $chuandoan]));
        }
    }

    public function searchThuoc(Request $request)
    {
        if ($request->isMethod('POST')) {
            if (!empty($request->key)) {
                $key = $request->key;

                $thuoc = Thuoc::where('TenThuoc', 'like', $key . '%')->get();

                if ($thuoc->count() > 0) {
                    return response()->json([
                        'status' => '200',
                        'message' => 'Success',
                        'data' => $thuoc,
                        'key' => $key,
                    ]);
                } else {
                    return response()->json([
                        'status' => '400',
                        'message' => 'Error',
                        'data' => 'null'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => '400',
                    'message' => 'Error',
                    'data' => 'null'
                ]);
            }
        }
    }

    public function finishDonThuoc(Request $request)
    {
        if ($request->isMethod('POST')) {
            // tạo đơn thuốc
            $donthuoc = DonThuoc::create([
                'MaNV' => $request->manv,
                'MaBN' => $request->mabn,
                'MaPhieuKham' => $request->maphieukham,
                'ChuanDoanBenh' => $request->chuandoan,
                'NgayTaiKham' => $request->ngay_tai_kham,
                'NgayTaoDon' => $request->ngay_tao_don,
            ])->id;

            $madonthuoc = $donthuoc;
            $sum = 0;
            for ($i = 0; $i < count($request->mathuoc); $i++) {
                $total = 0;
                CT_DonThuoc::create([
                    'MaDonThuoc' => $madonthuoc,
                    'MaThuoc' => $request->mathuoc[$i],
                    'MaDonVi' => $request->donvi[$i],
                    'SoLuong' => $request->soluong[$i],
                    'CachDung' => $request->cachdung[$i],
                    'LieuDung' => $request->lieudung[$i],
                ]);

                $thuoc = Thuoc::where('MaThuoc', $request->mathuoc[$i])->first();
                $sl_thuoc = $thuoc->SoLuong;
                $soluong = $sl_thuoc - $request->soluong[$i];
                $arr = [
                    'SoLuong' => $soluong,
                ];
                Thuoc::where('MaThuoc', $request->mathuoc[$i])->update($arr);

                $total = $request->soluong[$i] * $request->dongia[$i];
                $sum += $total;
            }

            return redirect(route('admin.donthuoc.conform_donthuoc', $madonthuoc));
        }
    }

    public function getAllDonThuoc()
    {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $date = $date->format('Y-m-d');
        $donthuoc = DonThuoc::whereRaw('NOT EXISTS (SELECT * FROM hoadon WHERE donthuoc.MaDonThuoc = hoadon.MaDonThuoc) AND donthuoc.NgayTaoDon = CURRENT_DATE()')
            ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
            ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
            ->select('benhnhan.HoTen as BenhNhan', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
            ->orderBy('donthuoc.NgayTaoDon')
            ->orderBy('donthuoc.MaDonThuoc')
            ->paginate(5);

        return view('pagination.data_donthuoc', compact('donthuoc'));
    }

    public function searchDonThuoc(Request $request)
    {
        if ($request->isMethod('POST')) {
            if (!empty($request->ngaytaodon)) {
                if (!empty($request->madonthuoc) || !empty($request->hoten)) {
                    $donthuoc = DonThuoc::where('NgayTaoDon', $request->ngaytaodon)
                        ->where('MaDonThuoc', 'like', $request->madonthuoc . '%')
                        ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                        ->where('benhnhan.HoTen', 'like', '%' . $request->hoten . '%')
                        ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                        ->select('benhnhan.HoTen as BenhNhan', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
                        ->orderBy('donthuoc.NgayTaoDon')
                        ->paginate(5);
                } else {
                    $donthuoc = DonThuoc::where('NgayTaoDon', $request->ngaytaodon)
                        ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                        ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                        ->select('benhnhan.HoTen as BenhNhan', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
                        ->orderBy('donthuoc.NgayTaoDon')
                        ->paginate(5);
                }
            } else {
                $donthuoc = DonThuoc::where('MaDonThuoc', 'like', $request->madonthuoc . '%')
                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                    ->where('benhnhan.HoTen', 'like', '%' . $request->hoten . '%')
                    ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                    ->select('benhnhan.HoTen as BenhNhan', 'nhanvien.HoTen as BacSi', 'donthuoc.*')
                    ->orderBy('donthuoc.NgayTaoDon')
                    ->paginate(5);
            }

            return view('pagination.data_donthuoc', compact('donthuoc'));
        }
    }
}
