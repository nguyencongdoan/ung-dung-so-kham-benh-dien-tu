<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\BenhNhan;
use App\Models\PhieuHenKham;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BenhNhanController extends Controller
{
    public function getUser($id = null)
    {
        if ($id != null) {
            $data = BenhNhan::find($id);

            if ($data->exists()) {
                return response()->json([
                    'data' => $data,
                    'status' => '200',
                    'message' => 'success'
                ]);
            } else {
                return response()->json([
                    'status' => '400',
                    'message' => 'error'
                ]);
            }
        } else {
            return response()->json([
                'status' => '400',
                'message' => 'error'
            ]);
        }
    }

    public function changePassword(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = BenhNhan::find($request->id);

            $arr = [
                'email' => $data->email,
                'password' => $request->password
            ];

            if (Auth::guard('benhnhan')->attempt($arr)) {
                if ($request->passnew == $request->passconf) {
                    if ($request->passnew == $request->password) {
                        return response()->json([
                            'status' => '400',
                            'alert' => 'error',
                            'message' => 'Mật khẩu mới trùng với mật khẩu cũ!'
                        ]);
                    } else {
                        $hashPassword = bcrypt($request->passnew);
                        $data->password = $hashPassword;
                        $data->save();

                        return response()->json([
                            'status' => '200',
                            'alert' => 'success',
                            'message' => 'Cập nhật mật khẩu thành công!'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => '400',
                        'alert' => 'error',
                        'message' => 'Nhập lại mật khẩu không đúng!'
                    ]);
                }
            } else {
                return response()->json([
                    'status' => '400',
                    'alert' => 'error',
                    'message' => 'Nhập mật khẩu hiện tại không đúng!'
                ]);
            }
        }
    }

    public function editInfoUser(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = BenhNhan::find($request->id);
            $data->HoTen = $request->HoTen;
            $data->email = $request->email;
            $data->DiaChi = $request->DiaChi;
            $data->NgaySinh = $request->NgaySinh;
            $data->GioiTinh = $request->GioiTinh;
            $data->MaBHYT = $request->MaBHYT;
            $data->SDT = $request->SDT;

            if($request->has('anhdaidien')) {
                $file = $request->anhdaidien;
                $ext = $request->anhdaidien->extension();
                $filename = time().'.'.$ext;
                $file->move(public_path('./uploads/images/'), $filename);
                $request->merge(['avatar' => $filename]);
                $data->AnhDaiDien = $request->avatar;
            }

            $data->save();

            return redirect(route('client.user.thongtin-user', $request->id));
        }
    }
    
    public function getLichSuHen($sdt)
    {
        $phieuhen = PhieuHenKham::where('phieuhenkham.SDT', $sdt)
                    ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                    ->select('nhanvien.HoTen as HoTen_BS', 'phieuhenkham.*')
                    ->orderBy('NgayHen', 'DESC')
                    ->orderBy('GioHen', 'DESC')
                    ->paginate(5);

        return view('client.user.pagination_lichsu', compact('phieuhen'));
    }

    public function send_mail_huylichhen($data)
    {
        $ngayhen = Carbon::parse($data->NgayHen)->format('d/m/Y');
        $subject = 'Hủy lịch hẹn khám ngày '.$ngayhen;
        $to_email = $data->Email;
        $view = 'vendor.mail.mail_huylichhen';

        $mail = new SendMail($data, $subject, $view);
        Mail::to($to_email)->send($mail);

        if (Mail::failures()) {
            return false;
        }
        return true;
    }
    
    public function removePhieuHen(Request $request) {
        $phieuhen = PhieuHenKham::where('MaPhieuHen', $request->id)->first();
        $this->send_mail_huylichhen($phieuhen);
        $phieuhen = PhieuHenKham::where('MaPhieuHen', $request->id)->delete();

        if($phieuhen) {
            return response()->json([
                'status' => 200,
                'message' => 'Xóa phiếu hẹn thành công!',
                'alert' => 'success'
            ]);
        }
        else {
            return response()->json([
                'status' => 400,
                'message' => 'Xóa phiếu hẹn thất bại!',
                'alert' => 'error'
            ]);
        }
    }
}
