<?php

namespace App\Http\Controllers\Api;

require '../vendor/autoload.php';
// require '../Twilio/autoload.php';

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\BenhNhan;
use App\Models\LichKhamBenh;
use App\Models\NhanVien;
use App\Models\PhieuHenKham;
use App\Models\PhieuKham;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Mail;

class DatLichController extends Controller
{
    public function getTimeBS(Request $request) {
        $request->date = Carbon::parse($request->date)->format('Y-m-d');
        
        // lấy ra danh sách lịch khám của bác sĩ
        $time_bs = LichKhamBenh::where('MaNV', $request->id_bs)
                    ->where('ClassName', 'success')
                    ->OrderBy('ThoiGianBD')
                    ->get();
        $time_start = '';
        $time_end = '';
        
        foreach ($time_bs as $item) {
            $start_day = Carbon::parse($item->ThoiGianBD)->format('Y-m-d');
            $end_day = Carbon::parse($item->ThoiGianKT)->format('Y-m-d');

            // lấy ra mã lịch khám nằm trong khoản ThoiGianBD <= date <= ThoiGianKT
            if($start_day <= $request->date && $end_day >= $request->date) {
                $time_start = $item->ThoiGianBD;
                $time_end = $item->ThoiGianKT;
            }
        }

        return response()->json([
            'time_start' => $time_start,
            'time_end' => $time_end,
            'status' => 200,
        ]);
    }

    public function getDateBS(Request $request)
    {
        $date_bs = LichKhamBenh::where('MaNV', $request->id_bs)
                    ->where('ClassName', 'success')
                    ->OrderBy('ThoiGianBD')
                    ->get();
        
        if($date_bs->count() > 0) {
            return response()->json([
                'date' => $date_bs,
                'status' => 200
            ]);
        }
        else {
            return response()->json([
                'message' => 'Bác sĩ chưa có lịch khám!',
                'status' => 400,
                'alert' => 'error'
            ]);
        }
    }

    public function getMaxPhieu($phieu, $max, $id, $so)
    {
        $max = $max;
        for ($i = 0; $i < $phieu->count(); $i++) {
            $s = substr($phieu[$i]->$id, 10, 2);
            if (strcmp($s, $so) == 0) {
                if (substr($max, 12) < substr($phieu[$i]->$id, 12)) {
                    $max = $phieu[$i]->$id;
                }
            }
        }

        $sophieu = (int)substr($max, 12) + 1;

        if ($sophieu < 10) {
            $sophieu = '0' . $sophieu;
        }

        return $sophieu;
    }

    public function generateID($ngayhen = null, $gio = null)
    {
        // lấy ngày hiện tại
        $datetime = $ngayhen;
        $date = $datetime;
        $dateformat = str_replace('-', '', $date);
        $hour = substr($gio, 0, 2);
        // dd($hour);

        if ((int)$hour < 12) {
            $so = 'AM';
        } else {
            $so = 'PM';
        }

        // lấy tất cả dữ liệu trong bảng PhieuHenKham và PhieuKham dựa vào ngày hiện tại
        $phieuhen = PhieuHenKham::where('NgayHen', '=', $date)->where('MaPhieuHen', 'like', '%' . $so . '%')->get();
        $phieukham = PhieuKham::where('NgayKham', '=', $date)->where('MaPhieuKham', 'like', '%' . $so . '%')->get();

        // kiểm tra nếu chưa có ngày đăng ký lịch khám trong bảng phiếu khám và phiếu hẹn thì tạo mới phiếu có số phiếu là SP01
        if ($phieukham->count() == 0 && $phieuhen->count() == 0) {
            $dateformat .= 'SP' . $so . '01';
            $id = $dateformat;
            return $id;
        }
        // trường hợp trong phiếu khám không có nhưng trong phiếu hẹn đã có người đăng ký trước đó ta sẽ lấy số phiếu cao nhất và cộng thêm 1
        else if ($phieukham->count() == 0 && $phieuhen->count() > 0) {
            $max = $phieuhen[0]->MaPhieuHen;
            $sophieu = $this->getMaxPhieu($phieuhen, $max, "MaPhieuHen", $so);
            $id = $dateformat . 'SP' . $so . $sophieu;
            return $id;
        }
        // trường hợp trong phiếu hẹn không có nhưng trong phiếu khám đã có người đăng ký trước đó ta sẽ lấy số phiếu cao nhất và cộng thêm 1
        else if ($phieuhen->count() == 0 && $phieukham->count() > 0) {
            $max = $phieukham[0]->MaPhieuKham;
            $sophieu = $this->getMaxPhieu($phieukham, $max, "MaPhieuKham", $so);
            $id = $dateformat . 'SP' . $so . $sophieu;
            return $id;
        }
        // trường hợp trong phiếu hẹn và phiếu khám điều có ta sẽ so sánh số phiếu và lấy số phiếu lớn hơn cộng thêm 1
        else if ($phieuhen->count() > 0 && $phieukham->count() > 0) {
            $max_sophieuhen = $this->getMaxPhieu($phieuhen, $phieuhen[0]->MaPhieuHen, "MaPhieuHen", $so);
            $max_sophieukham = $this->getMaxPhieu($phieukham, $phieukham[0]->MaPhieuKham, "MaPhieuKham", $so);
            if ($max_sophieuhen > $max_sophieukham) {
                $id = $dateformat . 'SP' . $so . $max_sophieuhen;
                return $id;
            } else {
                $id = $dateformat . 'SP' . $so . $max_sophieukham;
                return $id;
            }
        }
    }

    public function datlich(Request $request)
    {
        if ($request->isMethod('POST')) {
            $request->NgayKham = Carbon::parse($request->NgayKham)->format('Y-m-d');
            $id = $this->generateID($request->NgayKham, $request->gio);
            $giohen = $request->NgayKham . ' ' . $request->gio;
            $otp = rand(000000, 999999);

            $bacsi = NhanVien::find($request->bacsi)->HoTen;
            // trường hợp người dùng chưa đăng nhập 
            if ($request->id == "null") {
                $user = BenhNhan::where('email', '=', $request->email)->first();

                if (empty($user)) {
                    // tạo thông tin bệnh nhân
                    $hashPassword = bcrypt($request->SDT);

                    // Tạo thông tin bệnh nhân
                    BenhNhan::create([
                        'HoTen' => $request->HoTen,
                        'DiaChi' => $request->DiaChi,
                        'SDT' => $request->SDT,
                        'GioiTinh' => $request->GioiTinh,
                        'NgaySinh' => $request->NgaySinh,
                        'email' => $request->email,
                        'password' => $hashPassword
                    ]);

                    // tạo phiếu hẹn khám
                    $check_create_pk = $this->createPhieuHenKham($id, $request->bacsi, $request->NgayKham, $giohen, $request->HoTen, $request->SDT, $request->HoTen_NK, $request->GioiTinh, $request->DiaChi, $request->NgaySinh, $request->email);

                    if($check_create_pk) {
                        $data = [
                            'maphieuhen' => $id,
                            'hoten' => $request->HoTen_NK,
                            'email' => $request->email,
                            'sdt' => $request->sdt,
                            'ngayhen' => $request->NgayKham,
                            'giohen' => $request->gio,
                            'bacsi' => $bacsi,
                            'otp' => $otp
                        ];
    
                        // gửi sms
                        $sendSMS = $this->sendSMS($request->HoTen, $id, $request->NgayKham, $request->gio, $bacsi, $request->SDT, $otp);
                        if ($sendSMS == 200) {
                            $sendMail = $this->send_mail_datlich($data);
    
                            if ($sendMail) {
                                return response()->json([
                                    'status' => '200',
                                    'id' => $id,
                                    'otp' => $otp,
                                    'message' => 'Đặt lịch khám thành công!',
                                    'alert' => 'success'
                                ]);
                            } else {
                                $this->removePhieuHen($id);
                                return response()->json([
                                    'status' => '400',
                                    'message' => 'Gửi mail thất bại!',
                                    'alert' => 'error'
                                ]);
                            }
                        } else {
                            // $this->removePhieuHen($id);
                            if ($sendSMS == '21211') {
                                $this->removePhieuHen($id);
                                return response()->json([
                                    'status' => '400',
                                    'message' => 'Số điện thoại không đúng!',
                                    'alert' => 'error'
                                ]);
                            } else {
                                $sendMail = $this->send_mail_datlich($data);

                                if ($sendMail) {
                                    return response()->json([
                                        'status' => '200',
                                        'id' => $id,
                                        'otp' => $otp,
                                        'message' => 'Đặt lịch khám thành công!',
                                        'alert' => 'success'
                                    ]);
                                } else {
                                    $this->removePhieuHen($id);
                                    return response()->json([
                                        'status' => '400',
                                        'message' => 'Gửi mail thất bại!',
                                        'alert' => 'error'
                                    ]);
                                }
                                // return response()->json([
                                //     'status' => '400',
                                //     'message' => 'Số điện thoại chưa được xác minh!',
                                //     'alert' => 'error'
                                // ]);
                            }
                        }
                    }
                    else {
                        return response()->json([
                            'status' => '400',
                            'message' => 'Đặt lịch hẹn thất bại!',
                            'alert' => 'error'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => '400',
                        'message' => 'Email đã tồn tại. Vui lòng nhập lại!',
                        'alert' => 'error'
                    ]);
                }
            } else {
                $user = BenhNhan::find($request->id);

                if (!isset($request->HoTen_NK)) {
                    $hoten_nk = $user->HoTen;
                } else {
                    $hoten_nk = $request->HoTen_NK;
                }

                $check_create_pk = $this->createPhieuHenKham($id, $request->bacsi, $request->NgayKham, $giohen, $user->HoTen, $user->SDT,  $hoten_nk, $request->GioiTinh, $user->DiaChi, $user->NgaySinh, $user->email);

                if($check_create_pk) {
                    $data = [
                        'maphieuhen' => $id,
                        'hoten' => $hoten_nk,
                        'email' => $user->email,
                        'sdt' => $user->SDT,
                        'ngayhen' => $request->NgayKham,
                        'giohen' => $request->gio,
                        'bacsi' => $bacsi,
                        'otp' => $otp
                    ];
    
                    // gửi sms
                    $sendSMS = $this->sendSMS($hoten_nk, $id, $request->NgayKham, $request->gio, $bacsi, $user->SDT, $otp);
    
                    if ($sendSMS == 200) {
                        $sendMail = $this->send_mail_datlich($data);
    
                        if ($sendMail) {
                            return response()->json([
                                'status' => '200',
                                'id' => $id,
                                'otp' => $otp,
                                'message' => 'Đặt lịch khám thành công!',
                                'alert' => 'success'
                            ]);
                        } else {
                            $this->removePhieuHen($id);
                            return response()->json([
                                'status' => '400',
                                'message' => 'Gửi mail thất bại!',
                                'alert' => 'error'
                            ]);
                        }
                    } else {
                        if ($sendSMS == '21211') {
                            $this->removePhieuHen($id);
                            return response()->json([
                                'status' => '400',
                                'message' => 'Số điện thoại không đúng!',
                                'alert' => 'error'
                            ]);
                        } else {
                            $sendMail = $this->send_mail_datlich($data);
    
                            if ($sendMail) {
                                return response()->json([
                                    'status' => '200',
                                    'id' => $id,
                                    'otp' => $otp,
                                    'message' => 'Đặt lịch khám thành công!',
                                    'alert' => 'success'
                                ]);
                            } else {
                                $this->removePhieuHen($id);
                                return response()->json([
                                    'status' => '400',
                                    'message' => 'Gửi mail thất bại!',
                                    'alert' => 'error'
                                ]);
                            }
                            // return response()->json([
                            //     'status' => '400',
                            //     'message' => 'Số điện thoại chưa được xác minh!',
                            //     'alert' => 'error'
                            // ]);
                        }
                    }
                }
                else {
                    return response()->json([
                        'status' => '400',
                        'message' => 'Đặt lịch hẹn thất bại!',
                        'alert' => 'error'
                    ]);
                }
            }
        }
    }

    public function send_mail_datlich($data)
    {
        $subject = 'Đặt lịch hẹn khám thành công!';
        $to_email = $data['email'];
        $view = 'vendor.mail.mail_datlich';

        $mail = new SendMail($data, $subject, $view);
        Mail::to($to_email)->send($mail);

        if (Mail::failures()) {
            return false;
        }
        return true;
    }
    
    public function send_mail_huylichhen($data)
    {
        $ngayhen = Carbon::parse($data->NgayHen)->format('d/m/Y');
        $subject = 'Hủy lịch hẹn khám ngày '.$ngayhen;
        $to_email = $data->Email;
        $view = 'vendor.mail.mail_huylichhen';

        $mail = new SendMail($data, $subject, $view);
        Mail::to($to_email)->send($mail);

        if (Mail::failures()) {
            return false;
        }
        return true;
    }

    public function createPhieuHenKham($id, $manv, $ngayhen, $giohen, $hoten, $sdt, $hoten_nk, $gioitinh, $diachi, $ngaysinh, $email)
    {
        try {
            PhieuHenKham::create([
                'MaPhieuHen' => $id,
                'MaNV' => $manv,
                'NgayHen' => $ngayhen,
                'GioHen' => $giohen,
                'HoTen' => $hoten,
                'SDT' => $sdt,
                'HoTen_NK' => $hoten_nk,
                'GioiTinh_NK' => $gioitinh,
                'DiaChi_NK' => $diachi,
                'NamSinh_NK' => $ngaysinh,
                'Email' => $email,
            ]);
        }
        catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function sendSMS($hoten, $maphieuhen, $ngayhen, $giohen, $bacsi, $sdt, $otp)
    {
        $ngayhen = Carbon::parse($ngayhen)->format('d-m-Y');
        $giohen = Carbon::parse($giohen)->format('H:i A');

        $twilio_number = '+15732458193';
        $account_sid = 'ACcc1e3d49218a589378bf08c280ff4016';
        $auth_token = 'dd948c57bf3898c281d073fc412ff5e5';

        try {
            $client = new Client($account_sid, $auth_token);
            $client->messages->create(
                "+84" . $sdt,
                array(
                    'from' => $twilio_number,
                    'body' => "Mã OTP: ".$otp.".\r\nXin chào: ".$hoten.".\r\nBạn vừa đặt lịch hẹn khám thành công tại Phòng khám tai mũi họng của chúng tôi. Vui lòng kiểm tra lại thông tin phiếu hẹn.\r\nThông tin phiếu hẹn của anh/chị:\r\nMã phiếu hẹn: ".$maphieuhen."\r\nNgày hẹn: ".$ngayhen."\r\nGiờ hẹn: ".$giohen."\r\nBác sĩ phụ trách khám: ".$bacsi."\r\nAnh/Chị vui lòng đến đúng giờ để có một buổi khám tốt nhất.\r\nTrân trọng!"
                )
            );
            return 200;
        }
        catch (Exception $e) {
            return $e->getcode();
        }
    }

    public function removePhieuHen($id)
    {
        $phieuhen = PhieuHenKham::where('MaPhieuHen', $id)->first();
        $this->send_mail_huylichhen($phieuhen);
        $phieuhen = PhieuHenKham::where('MaPhieuHen', $id)->delete();

        if($phieuhen) {
            return response()->json([
                'status' => 200,
                'message' => 'Xóa phiếu hẹn thành công!',
                'alert' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Xóa phiếu hẹn thất bại!',
                'alert' => 'error'
            ]);
        }
    }

    public function edit_phieuhen(Request $request)
    {
        if ($request->isMethod('POST')) {
            $request->NgayKham = Carbon::parse($request->NgayKham)->format('Y-m-d');
            $giohen = $request->NgayKham . ' ' . $request->gio;

            $arr = [
                'HoTen_NK' => $request->HoTen_NK,
                'GioiTinh_NK' => $request->GioiTinh,
                'MaNV' => $request->bacsi,
                'NgayHen' => $request->NgayKham,
                'GioHen' => $giohen
            ];

            $arr_bn = [
                'HoTen' => $request->HoTen_NK,
                'GioiTinh' => $request->GioiTinh,
            ];

            $phieuhen = PhieuHenKham::where('MaPhieuHen', $request->id_phieuhen)->update($arr);
            $ph = PhieuHenKham::where('MaPhieuHen', $request->id_phieuhen)->first();
            $benhnhan = Benhnhan::where('email', $ph->Email)->update($arr_bn);

            if ($phieuhen && $benhnhan) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Chỉnh sửa phiếu hẹn thành công!',
                    'alert' => 'success'
                ]);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Chỉnh sửa phiếu hẹn thất bại!',
                    'alert' => 'error'
                ]);
            }
        }
    }
}
