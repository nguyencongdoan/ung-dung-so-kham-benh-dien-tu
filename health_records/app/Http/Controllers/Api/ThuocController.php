<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Thuoc;
use Illuminate\Http\Request;

class ThuocController extends Controller
{
    public function getAllThuoc() {
        $thuoc = Thuoc::orderBy('created_at')->paginate(10);

        return view('pagination.data_thuoc', compact('thuoc'));
    }

    public function handle_create(Request $request) {
        if($request->isMethod('POST')) {
            $thuoc = Thuoc::create([
                'TenThuoc' => $request->tenthuoc,
                'SoLuong' => $request->soluong,
                'DonGia' => $request->dongia,
                'NSX' => $request->nsx,
                'HSD' => $request->hsd,
                'ThongTinThuoc' => $request->info_thuoc,
            ]);

            if($thuoc) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Thêm thuốc mới thành công!',
                    'alert' => 'success'
                ]);
            }
            else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Thêm thuốc mới thất bại!',
                    'alert' => 'error'
                ]);
            }
        }
    }
    
    public function handle_edit(Request $request) {
        if($request->isMethod('POST')) {
            $arr = [
                'TenThuoc' => $request->tenthuoc,
                'SoLuong' => (int)$request->soluong,
                'DonGia' => (int)$request->dongia,
                'NSX' => $request->nsx,
                'HSD' => $request->hsd,
                'ThongTinThuoc' => $request->info_thuoc,
            ];
            $thuoc = Thuoc::where('MaThuoc', $request->id)->update($arr);

            if($thuoc) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Chỉnh sửa thông tin thuốc thành công!',
                    'alert' => 'success'
                ]);
            }
            else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Chỉnh sửa thông tin thuốc thất bại!',
                    'alert' => 'error'
                ]);
            }
        }
    }

    public function delete(Request $request) {
        if($request->isMethod('POST')) {
            $thuoc = Thuoc::where('MaThuoc', $request->id)->delete();

            if($thuoc) {
                return response()->json([
                    'status' => 1000,
                    'message' => 'Xóa thuốc thành công!',
                    'alert' => 'success'
                ]);
            }
            else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Xóa thuốc thất bại!',
                    'alert' => 'error'
                ]);
            }
        }
    }

    public function search(Request $request) {
        if($request->isMethod('POST')) {
            if(!empty($request->tenthuoc) && !empty($request->nsx) && !empty($request->hsd)) {
                $thuoc = Thuoc::where('TenThuoc', 'like', '%'.$request->tenthuoc.'%')
                        ->where('NSX', $request->nsx)
                        ->where('HSD', $request->hsd)
                        ->orderBy('created_at')
                        ->paginate(10);
            }
            else if(!empty($request->tenthuoc)) {
                if(!empty($request->nsx)){
                    $thuoc = Thuoc::where('TenThuoc', 'like', '%'.$request->tenthuoc.'%')
                            ->where('NSX', $request->nsx)
                            ->orderBy('created_at')
                            ->paginate(10);
                        }
                else if(!empty($request->hsd)) {
                    $thuoc = Thuoc::where('TenThuoc', 'like', '%'.$request->tenthuoc.'%')
                            ->where('HSD', $request->nsx)
                            ->orderBy('created_at')
                            ->paginate(10);
                }
                else {
                    $thuoc = Thuoc::where('TenThuoc', 'like', '%'.$request->tenthuoc.'%')
                            ->orderBy('created_at')
                            ->paginate(10);
                }
            }
            else if(!empty($request->nsx)) {
                if(!empty($request->hsd)) {
                    $thuoc = Thuoc::where('NSX', $request->nsx)
                        ->where('HSD', $request->hsd)
                        ->orderBy('created_at')
                        ->paginate(10);
                }
                else {
                    $thuoc = Thuoc::where('NSX', $request->nsx)
                        ->orderBy('created_at')
                        ->paginate(10);
                }
            }
            else if(!empty($request->hsd)){
                $thuoc = Thuoc::where('HSD', $request->hsd)
                        ->orderBy('created_at')
                        ->paginate(10);
            }
            else {
                $thuoc = Thuoc::orderBy('created_at')->paginate(10);
            }

            return view('pagination.data_thuoc', compact('thuoc'));
        }
    }
}
