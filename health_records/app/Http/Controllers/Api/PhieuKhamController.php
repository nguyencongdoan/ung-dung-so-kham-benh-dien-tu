<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use App\Models\DonThuoc;
use App\Models\PhieuHenKham;
use App\Models\PhieuKham;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class PhieuKhamController extends Controller
{
    public function getAllPhieuKham() {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $so = $date->format('A');
        $date = $date->toDateString();

        $phieukham= PhieuKham::where('NgayKham', '=', $date)
                            ->where('MaPhieuKham', 'like', '%'.$so.'%')
                            ->where('TinhTrangKham', '<>', 3)
                            ->where('TinhTrangKham', '<>', 4)
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->select('nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'phieukham.*')
                            ->orderBy('phieukham.NgayKham')
                            ->orderBy('phieukham.MaPhieuKham')
                            ->paginate(5);

        return view('pagination.data_phieukham', compact('phieukham'));
    }

    public function getMaxPhieu($phieu, $max, $id, $so) {
        $max = $max;
        for($i = 0; $i < $phieu->count(); $i++) {
            $s = substr($phieu[$i]->$id, 10, 2);
            if(strcmp($s, $so) == 0) {
                if(substr($max, 12) < substr($phieu[$i]->$id, 12)) {
                    $max = $phieu[$i]->$id;
                }
            }
        }
        
        $sophieu = (int)substr($max, 12) + 1;
        
        if($sophieu < 10){
            $sophieu = '0'.$sophieu;
        }

        return $sophieu;
    }

    public function generateID($ngayhen = null, $gio = null) {
        // lấy ngày hiện tại
        $datetime = $ngayhen;
        $date = $datetime;
        $dateformat = str_replace('-', '', $date);
        $hour = substr($gio, 0, 2);

        if((int)$hour < 12) {
            $so = 'AM';
        }
        else {
            $so = 'PM';
        }
        
        // lấy tất cả dữ liệu trong bảng PhieuHenKham và PhieuKham dựa vào ngày hiện tại
        $phieuhen = PhieuHenKham::where('NgayHen', '=', $date)->where('MaPhieuHen', 'like', '%'.$so.'%')->get();
        $phieukham = PhieuKham::where('NgayKham', '=', $date)->where('MaPhieuKham', 'like', '%'.$so.'%')->get();

        // kiểm tra nếu chưa có ngày đăng ký lịch khám trong bảng phiếu khám và phiếu hẹn thì tạo mới phiếu có số phiếu là SP01
        if($phieukham->count() == 0 && $phieuhen->count() == 0) {
            $dateformat .= 'SP'.$so.'01';
            $id = $dateformat;
            return $id;
        } 
        // trường hợp trong phiếu khám không có nhưng trong phiếu hẹn đã có người đăng ký trước đó ta sẽ lấy số phiếu cao nhất và cộng thêm 1
        else if($phieukham->count() == 0 && $phieuhen->count() > 0) {
            $max = $phieuhen[0]->MaPhieuHen;
            $sophieu = $this->getMaxPhieu($phieuhen, $max, "MaPhieuHen", $so);
            $id = $dateformat.'SP'.$so.$sophieu;
            return $id;
        }
        // trường hợp trong phiếu hẹn không có nhưng trong phiếu khám đã có người đăng ký trước đó ta sẽ lấy số phiếu cao nhất và cộng thêm 1
        else if($phieuhen->count() == 0 && $phieukham->count() > 0) {
            $max = $phieukham[0]->MaPhieuKham;
            $sophieu = $this->getMaxPhieu($phieukham, $max, "MaPhieuKham", $so);
            $id = $dateformat.'SP'.$so.$sophieu;
            return $id;
        }
        // trường hợp trong phiếu hẹn và phiếu khám điều có ta sẽ so sánh số phiếu và lấy số phiếu lớn hơn cộng thêm 1
        else if($phieuhen->count() > 0 && $phieukham->count() > 0) {
            $max_sophieuhen = $this->getMaxPhieu($phieuhen, $phieuhen[0]->MaPhieuHen, "MaPhieuHen", $so);
            $max_sophieukham = $this->getMaxPhieu($phieukham, $phieukham[0]->MaPhieuKham, "MaPhieuKham", $so);
            if($max_sophieuhen > $max_sophieukham) {
                $id = $dateformat.'SP'.$so.$max_sophieuhen;
                return $id;
            }
            else {
                $id = $dateformat.'SP'.$so.$max_sophieukham;
                return $id;
            }
        }
    }

    public function handlePhieuKham(Request $request) {
        if($request->isMethod('POST')) {
            $request->NgayKham = Carbon::parse($request->NgayKham)->format('Y-m-d');
            $id = $this->generateID($request->NgayKham, $request->gio);
            (int)$sophieu = substr($id, 12);

            // kiểm tra nếu tồn tại id_user thì tạo thông tin phiếu khám
            if(!empty($request->id_user)) {
                // tạo thông tin phiếu khám
                try {
                    PhieuKham::create([
                        'MaPhieuKham' => $id, 
                        'MaBN' => $request->id_user,
                        'MaNV' => $request->bacsi,
                        'NgayKham' => $request->NgayKham,
                        'HoTenNguoiThan' => $request->HoTenNguoiThan,
                        'TrieuChung' => $request->trieuchung,
                        'TienSuBenh' => $request->tiensubenh,
                        'SoPhieu' => $sophieu,
                        'TinhTrangKham' => 0,
                    ]);
                    
                    return response()->json([
                        'status' => '200',
                        'message' => 'Tạo phiếu khám thành công!',
                        'alert' => 'success'
                    ]);
                }
                catch (Exception $e) {
                    return response()->json([
                        'status' => '400',
                        'message' => 'Tạo phiếu khám thất bại!',
                        'alert' => 'error'
                    ]);
                }
            }
            else {
                $check_email = BenhNhan::where('email', $request->email)->exists();

                if(!$check_email) {
                    $hashPassword = bcrypt($request->SDT);

                    // Tạo thông tin bệnh nhân
                    $benhnhan = BenhNhan::create([
                        'HoTen' => $request->HoTen,
                        'DiaChi' => $request->DiaChi,
                        'SDT' => $request->SDT,
                        'GioiTinh' => $request->GioiTinh,
                        'NgaySinh' => $request->NgaySinh,
                        'email' => $request->email,
                        'password' => $hashPassword
                    ])->id;

                    // tạo thông tin phiếu khám
                    try {
                        PhieuKham::create([
                            'MaPhieuKham' => $id, 
                            'MaBN' => $benhnhan,
                            'MaNV' => $request->bacsi,
                            'NgayKham' => $request->NgayKham,
                            'HoTenNguoiThan' => $request->HoTenNguoiThan,
                            'TrieuChung' => $request->trieuchung,
                            'TienSuBenh' => $request->tiensubenh,
                            'SoPhieu' => $sophieu,
                            'TinhTrangKham' => 0,
                        ]);

                        return response()->json([
                            'status' => '200',
                            'message' => 'Tạo phiếu khám thành công!',
                            'alert' => 'success'
                        ]);
                    }
                    catch (Exception $e) {
                        return response()->json([
                            'status' => '400',
                            'message' => 'Tạo phiếu khám thất bại!',
                            'alert' => 'error'
                        ]);
                    }
                }
                else {
                    return response()->json([
                        'status' => '400',
                        'message' => 'Email đã tồn tại. Vui lòng nhập Email khác!',
                        'alert' => 'error'
                    ]);
                }
            }
        }
    }

    public function searchPhieuKham(Request $request) {
        if($request->isMethod('POST') && $request->ajax()) {
            if(!empty($request->ngaykham)) {
                if(!empty($request->maphieukham) || !empty($request->hoten) || !empty($request->sdt)) {
                    $phieukham = Phieukham::where('NgayKham', $request->ngaykham)
                            ->where('MaPhieuKham', 'like', $request->maphieukham.'%')
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->where('benhnhan.HoTen', 'like', '%'. $request->hoten .'%')
                            ->Where('benhnhan.SDT', 'like', '%'. $request->sdt .'%')
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->select('benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'nhanvien.HoTen as BacSi', 'phieukham.*')
                            ->orderBy('phieukham.NgayKham')
                            ->orderBy('phieukham.MaPhieuKham')
                            ->paginate(5);
                }
                else {
                    $phieukham = Phieukham::where('NgayKham', $request->ngaykham)
                            ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                            ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                            ->select('benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'nhanvien.HoTen as BacSi', 'phieukham.*')
                            ->orderBy('phieukham.NgayKham')
                            ->orderBy('phieukham.MaPhieuKham')
                            ->paginate(5);
                }
            }
            else {
                $phieukham = Phieukham::where('MaPhieuKham', 'like', $request->maphieukham.'%')
                        ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                        ->where('benhnhan.HoTen', 'like', '%'. $request->hoten .'%')
                        ->Where('benhnhan.SDT', 'like', '%'. $request->sdt .'%')
                        ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                        ->select('benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'nhanvien.HoTen as BacSi', 'phieukham.*')
                        ->orderBy('phieukham.NgayKham')
                        ->orderBy('phieukham.MaPhieuKham')
                        ->paginate(5);
            }
            return view('pagination.data_phieukham', compact('phieukham'));
        }
    }

    // phieu tai kham
    public function getAllPhieuTaiKham() {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $so = $date->format('A');
        $date = $date->toDateString();

        $phieutaikham= DonThuoc::where('NgayTaiKham', '=', $date)
                            ->where('donthuoc.MaPhieuKham', 'like', '%'.$so.'%')
                            ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                            ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                            ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                            ->select('nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'phieukham.*', 'donthuoc.*')
                            ->orderBy('donthuoc.NgayTaiKham')
                            ->orderBy('donthuoc.MaPhieuKham')
                            ->paginate(5);
                            
        return view('pagination.data_phieutaikham', compact('phieutaikham'));
    }

    public function searchPhieuTaiKham(Request $request) {
        if($request->isMethod('POST')) {
            if(!empty($request->ngaytaikham)) {
                if(!empty($request->madonthuoc) || !empty($request->hoten) || !empty($request->sdt)) {
                    $phieutaikham = DonThuoc::where('NgayTaiKham', '=',$request->ngaytaikham)
                                    ->where('donthuoc.MaDonThuoc', 'like', $request->madonthuoc.'%')
                                    ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                                    ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                    ->where('benhnhan.HoTen', 'like', '%'. $request->hoten .'%')
                                    ->Where('benhnhan.SDT', 'like', '%'. $request->sdt .'%')
                                    ->select('nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'phieukham.*', 'donthuoc.*')
                                    ->orderBy('donthuoc.NgayTaiKham')
                                    ->orderBy('donthuoc.MaPhieuKham')
                                    ->paginate(5);
                }
                else {
                    $phieutaikham = DonThuoc::where('NgayTaiKham', '=',$request->ngaytaikham)
                                    ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                                    ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                    ->select('nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'phieukham.*', 'donthuoc.*')
                                    ->orderBy('donthuoc.NgayTaiKham')
                                    ->orderBy('donthuoc.MaPhieuKham')
                                    ->paginate(5);
                }
            }
            else {
                $phieutaikham = DonThuoc::where('donthuoc.MaDonThuoc', 'like', $request->madonthuoc.'%')
                                ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                                ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                ->where('benhnhan.HoTen', 'like', '%'. $request->hoten .'%')
                                ->Where('benhnhan.SDT', 'like', '%'. $request->sdt .'%')
                                ->select('nhanvien.HoTen as BacSi', 'benhnhan.HoTen as BenhNhan', 'benhnhan.GioiTinh', 'benhnhan.SDT', 'phieukham.*', 'donthuoc.*')
                                ->orderBy('donthuoc.NgayTaiKham')
                                ->orderBy('donthuoc.MaPhieuKham')
                                ->paginate(5);
            }
                
            return view('pagination.data_phieutaikham', compact('phieutaikham'));
        }
    }

    public function createPhieuTaiKham(Request $request) {
        if($request->isMethod('POST')) {
            $phieutaikham = DonThuoc::where('donthuoc.MaDonThuoc', $request->id)
                            ->join('phieukham', 'donthuoc.MaPhieuKham', '=', 'phieukham.MaPhieuKham')
                            ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                            ->join('nhanvien', 'donthuoc.MaNV', '=', 'nhanvien.id')
                            ->select('donthuoc.*', 'phieukham.*', 'benhnhan.*', 'nhanvien.id as manv')
                            ->first();
            
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $gio = $date->format('H:i:s');
        
            $id = $this->generateID($phieutaikham->NgayTaiKham, $gio);
            (int)$sophieu = substr($id, 12);

            $phieukham = Phieukham::create([
                'MaPhieuKham' => $id, 
                'MaBN' => $phieutaikham->id, 
                'MaNV' => $phieutaikham->manv,
                'NgayKham' => $phieutaikham->NgayTaiKham,
                'TrieuChung' => $phieutaikham->TrieuChung,
                'TienSuBenh' => $phieutaikham->ChuanDoanBenh,
                'SoPhieu' => $sophieu,
                'TinhTrangKham' => 0,
            ]);

            if($phieukham) {
                $update_phieukham = Phieukham::where('MaPhieuKham', $phieutaikham->MaPhieuKham)->update(['TinhTrangKham' => 4]);
                if($update_phieukham) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'Tạo phiếu tái khám thành công!',
                        'alert' => 'success'
                    ]);
                }
            }
            else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Tạo phiếu tái khám thất bại!',
                    'alert' => 'error'
                ]);
            }
        }
    }
}
