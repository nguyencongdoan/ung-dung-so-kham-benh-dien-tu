<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TheLoai_BV;
use Illuminate\Http\Request;

class TheLoaiController extends Controller
{
    public function store(Request $request) {
        foreach ($request->tentheloai as $item) {
            $theloai = TheLoai_BV::create([
                'TenTL' => $item,
            ]);

            if(!$theloai) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Thêm thể loại thất bại!',
                    'alert' => 'error'
                ]);
            }
        }

        $theloai = TheLoai_BV::all();

        return response()->json([
            'data' => $theloai,
            'status' => 200,
            'message' => 'Thêm thể loại thành công!',
            'alert' => 'success'
        ]);
    }
}
