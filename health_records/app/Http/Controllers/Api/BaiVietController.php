<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BaiViet;
use App\Models\TheLoai_BV;
use Illuminate\Http\Request;

class BaiVietController extends Controller
{
    public function getThreeNews() {
        $baiviet = BaiViet::take(3)->orderBy('NgayTao', 'DESC')->get();

        if($baiviet->count() > 0) {
            return response()->json([
                'data' => $baiviet,
                'status' => 200,
                'message' => 'success'
            ]);
        }
        else {
            return response()->json([
                'status' => 400,
                'message' => 'Không có bài viết nào!',
                'alert' => 'error'
            ]);
        }
    }

    public function getAllNews() {
        $baiviet = BaiViet::join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                            ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                            ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                            ->orderBy('NgayTao')
                            ->paginate(5);

        return view('pagination.data_baiviet', compact('baiviet'));
    }

    public function getTheLoai() {
        $theloai = TheLoai_BV::all();

        if($theloai->count() > 0) {
            return response()->json([
                'data' => $theloai,
                'status' => 200,
                'message' => 'success'
            ]);
        }
        else {
            return response()->json([
                'status' => 400,
                'message' => 'Không có bài viết nào!'
            ]);
        }
    }

    public function search(Request $request) {
        if($request->isMethod('POST')) {
            if(empty($request->sort)) {
                $request->sort = 'asc';
            }

            if(!empty($request->tieude) && !empty($request->theloai)) {
                $baiviet = BaiViet::where('TieuDe', 'like', '%'.$request->tieude.'%')
                                ->where('MaTL', $request->theloai)
                                ->orderBy('NgayTao', $request->sort)
                                ->paginate(5);
            }
            else if(!empty($request->tieude)) {
                $baiviet = BaiViet::where('TieuDe', 'like', '%'.$request->tieude.'%')
                            ->orderBy('NgayTao', $request->sort)
                            ->paginate(5);
            }
            else if(!empty($request->theloai)) {
                $baiviet = BaiViet::where('MaTL', $request->theloai)
                            ->orderBy('NgayTao', $request->sort)
                            ->paginate(5);
            }
            else {
                $baiviet = BaiViet::orderBy('NgayTao', $request->sort)->paginate(5);
            }
            
            return view('client.tintuc.pagination_tintuc', compact('baiviet'));
        }
    }

    public function searchNews(Request $request) {
        if ($request->isMethod('POST')) {
            if(!empty($request->tieude) && !empty($request->theloai) && !empty($request->ngaytao)) {
                $baiviet = BaiViet::where('TieuDe', 'like', '%'.$request->tieude.'%')
                                    ->where('baiviet.MaTL', $request->theloai)
                                    ->whereDate('NgayTao', $request->ngaytao)
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
            }
            else if(!empty($request->tieude)) {
                if(!empty($request->ngaytao)) { 
                    $baiviet = BaiViet::where('TieuDe', 'like', '%'.$request->tieude.'%')
                                    ->whereDate('NgayTao', $request->ngaytao)
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
                }
                else if(!empty($request->theloai)) {
                    $baiviet = BaiViet::where('TieuDe', 'like', '%'.$request->tieude.'%')
                                    ->where('baiviet.MaTL', $request->theloai)
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
                }
                else {
                    $baiviet = BaiViet::where('TieuDe', 'like', '%'.$request->tieude.'%')
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
                }
            }
            else if(!empty($request->theloai)) {
                if(!empty($request->ngaytao)) {
                    $baiviet = BaiViet::where('baiviet.MaTL', $request->theloai)
                                    ->whereDate('NgayTao', $request->ngaytao)
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
                }
                else {
                    $baiviet = BaiViet::where('baiviet.MaTL', $request->theloai)
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
                }
            }
            else {
                    $baiviet = BaiViet::whereDate('NgayTao', $request->ngaytao)
                                    ->join('theloai_bv', 'baiviet.MaTL', '=', 'theloai_bv.MaTL')
                                    ->join('nhanvien', 'baiviet.MaNV', '=', 'nhanvien.id')
                                    ->select('baiviet.*', 'nhanvien.HoTen', 'theloai_bv.*')
                                    ->paginate(5);
            }

            return view('pagination.data_baiviet', compact('baiviet'));
        }
    }
}
