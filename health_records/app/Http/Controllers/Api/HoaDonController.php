<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\HoaDon;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HoaDonController extends Controller
{
    public function getAllHoaDon() {
        $hoadon = HoaDon::join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                        ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                        ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                        ->paginate(5);
        
        return view('pagination.data_hoadon', compact('hoadon'));
    }

    public function search(Request $request) {
        if($request->isMethod('POST') && $request->ajax()) {
            if(!empty($request->end_day)){
                $end_day = Carbon::parse($request->end_day)->addDay(1);
                $end_day = $end_day->format('Y-m-d H:i:s');
            }
            
            if(!empty($request->mahd) && !empty($request->start_day) && !empty($request->end_day)) {
                $hoadon = HoaDon::where('MaHD', 'like', $request->mahd.'%')
                            ->where('NgayLap', '>=', $request->start_day)
                            ->where('NgayLap', '<=', $end_day)
                            ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                            ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                            ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                            ->paginate(5);
            }
            else if(!empty($request->start_day) && !empty($request->end_day)) {
                $hoadon = HoaDon::where('NgayLap', '>=', $request->start_day)
                            ->where('NgayLap', '<=', $end_day)
                            ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                            ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                            ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                            ->paginate(5);
            }
            else if(!empty($request->start_day)) {
                if(!empty($request->mahd) && !empty($request->start_day)) {
                    $hoadon = HoaDon::where('MaHD', 'like', $request->mahd.'%')
                                ->where('NgayLap', '>=', $request->start_day)
                                ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                                ->paginate(5);
                }
                else {
                    $hoadon = HoaDon::whereDate('NgayLap', '>=', $request->start_day)
                                ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                                ->paginate(5);
                }
            }
            else if(!empty($request->end_day)) { 
                if(!empty($request->mahd) && !empty($request->end_day)) {
                    $hoadon = HoaDon::where('MaHD', 'like', $request->mahd.'%')
                                ->where('NgayLap', '<=', $end_day)
                                ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                                ->paginate(5);
                }
                else {
                    $hoadon = HoaDon::whereDate('NgayLap', '<=', $end_day)
                                ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                                ->paginate(5);
                }
            }
            else {
                $hoadon = HoaDon::where('MaHD', 'like', $request->mahd.'%')
                                ->join('donthuoc', 'hoadon.MaDonThuoc', 'donthuoc.MaDonThuoc')
                                ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                                ->select('benhnhan.HoTen', 'donthuoc.*', 'hoadon.*')
                                ->paginate(5);
            }

            return view('pagination.data_hoadon', compact('hoadon'));
        }
    }
}
