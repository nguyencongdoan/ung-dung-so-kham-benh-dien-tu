<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CT_PhieuNhapThuoc;
use App\Models\DonVi;
use App\Models\PhieuNhapThuoc;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PhieuNhapThuocController extends Controller
{
    public function getAllPhieuNhap() {
        $phieunhap = PhieuNhapThuoc::join('nhanvien', 'phieunhapthuoc.MaNV', '=', 'nhanvien.id')
                    ->select('nhanvien.HoTen', 'phieunhapthuoc.*')
                    ->paginate(10);

        return view('pagination.data_phieunhapthuoc', compact('phieunhap'));
    }

    public function getPhieuNhap(Request $request) {
        $phieunhap = CT_PhieuNhapThuoc::where('ct_phieunhapthuoc.MaPhieuNhap', $request->id)
                    ->join('phieunhapthuoc', 'ct_phieunhapthuoc.MaPhieuNhap', '=', 'phieunhapthuoc.MaPhieuNhap')
                    ->join('donvi', 'ct_phieunhapthuoc.MaDonVi', '=', 'donvi.MaDonVi')
                    ->join('thuoc', 'ct_phieunhapthuoc.MaThuoc', '=', 'thuoc.MaThuoc')
                    ->select('ct_phieunhapthuoc.*', 'ct_phieunhapthuoc.SoLuong as SL', 'phieunhapthuoc.*', 'donvi.*', 'thuoc.*')
                    ->get();
        $donvi = DonVi::all();

        if($phieunhap->count() > 0) {
            return response()->json([
                'data' => $phieunhap,
                'donvi' => $donvi,
                'status' => 200
            ]);
        }
        else {
            return response()->json([
                'status' => 400,
                'message' => 'Không có phiếu nhập!',
                'alert' => 'error'
            ]);
        }
    }

    public function handle_create(Request $request) {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $date = $date->format('Y-m-d');

        $thuoc = $request->mathuoc;

        $id_phieunhap = PhieuNhapThuoc::create([
            'MaNV' => $request->id_admin,
            'NgayNhap' => $date
        ])->id;

        foreach ($thuoc as $key => $item) {
            $ct_phieunhap = CT_PhieuNhapThuoc::create([
                'MaPhieuNhap' => $id_phieunhap,
                'MaDonVi' => $request->donvi[$key],
                'MaThuoc' => $item,
                'SoLuong' => $request->soluong[$key],
                'DonGia' => $request->dongia[$key],
                'ThanhTien' => $request->soluong[$key] * $request->dongia[$key],
            ]);
            if(!$ct_phieunhap) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Thêm phiếu nhập thuốc thất bại!',
                    'alert' => 'error'
                ]);
            }
        }

        return response()->json([
            'status' => 200,
            'message' => 'Thêm phiếu nhập thuốc thành công!',
            'alert' => 'success'
        ]);
    }

    public function handle_edit(Request $request) {
        $thuoc = $request->mathuoc;

        $id_phieunhap = $request->id_phieunhap;

        foreach ($thuoc as $key => $item) {
            $arr = [
                'MaDonVi' => $request->donvi[$key],
                'MaThuoc' => $item,
                'SoLuong' => $request->soluong[$key],
                'DonGia' => $request->dongia[$key],
                'ThanhTien' => $request->soluong[$key] * $request->dongia[$key],
            ];

            $check = CT_PhieuNhapThuoc::where('MaPhieuNhap', $id_phieunhap)
                                        ->where('MaThuoc', $item)->exists();
            if($check) {
                $ct_phieunhap = CT_PhieuNhapThuoc::where('MaPhieuNhap', $id_phieunhap)
                                ->where('MaThuoc', $item)->update($arr);
            }
            else {
                $ct_phieunhap = CT_PhieuNhapThuoc::create([
                    'MaPhieuNhap' => $id_phieunhap,
                    'MaDonVi' => $request->donvi[$key],
                    'MaThuoc' => $item,
                    'SoLuong' => $request->soluong[$key],
                    'DonGia' => $request->dongia[$key],
                    'ThanhTien' => $request->soluong[$key] * $request->dongia[$key],
                ]);
            }

            if(!$ct_phieunhap) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Chỉnh sửa phiếu nhập thuốc thất bại!',
                    'alert' => 'error'
                ]);
            }
        }

        return response()->json([
            'status' => 200,
            'message' => 'Chỉnh sửa phiếu nhập thuốc thành công!',
            'alert' => 'success'
        ]);
    }

    public function delete(Request $request) {
        if($request->isMethod('POST')) {
            $phieunhap = PhieuNhapThuoc::where('MaPhieuNhap', $request->id)->delete();

            if($phieunhap) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Xóa phiếu nhập thành công!',
                    'alert' => 'success'
                ]);
            }
            else {
                return response()->json([
                    'status' => 400,
                    'message' => 'Xóa phiếu nhập thất bại!',
                    'alert' => 'error'
                ]);
            }
        }
    }

    public function search(Request $request) {
        if ($request->isMethod('POST')) {
            if(!empty($request->start_day) && !empty($request->end_day)) {
                $phieunhap = PhieuNhapThuoc::where('NgayNhap', '>=', $request->start_day)
                            ->where('NgayNhap', '<=', $request->end_day)
                            ->join('nhanvien', 'phieunhapthuoc.MaNV', '=', 'nhanvien.id')
                            ->select('nhanvien.HoTen', 'phieunhapthuoc.*')
                            ->paginate(10);
            }
            else if(!empty($request->start_day)) {
                $phieunhap = PhieuNhapThuoc::where('NgayNhap', '>=', $request->start_day)
                            ->join('nhanvien', 'phieunhapthuoc.MaNV', '=', 'nhanvien.id')
                            ->select('nhanvien.HoTen', 'phieunhapthuoc.*')
                            ->paginate(10);
            }
            else if(!empty($request->start_day)) {
                $phieunhap = PhieuNhapThuoc::where('NgayNhap', '<=', $request->end_day)
                            ->join('nhanvien', 'phieunhapthuoc.MaNV', '=', 'nhanvien.id')
                            ->select('nhanvien.HoTen', 'phieunhapthuoc.*')
                            ->paginate(10);
            }
            else {
                $phieunhap = PhieuNhapThuoc::join('nhanvien', 'phieunhapthuoc.MaNV', '=', 'nhanvien.id')
                            ->select('nhanvien.HoTen', 'phieunhapthuoc.*')
                            ->paginate(10);
            }

            return view('pagination.data_phieunhapthuoc', compact('phieunhap'));
        }
    }
}
