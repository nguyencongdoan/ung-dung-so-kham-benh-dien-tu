<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\BenhNhan;
use App\Models\DonThuoc;
use App\Models\NhanVien;
use App\Models\PhieuHenKham;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

class SendSMSController extends Controller
{
    public $twilio_number = '+19379150067';
    public $account_sid = 'AC09d644bd47711e9f5762316016f1c8e0';
    public $auth_token = '732e1664575728f528499747ac31da32';

    public function sendSMS_TaiKham() {
        
        $donthuoc = DonThuoc::whereRaw('NgayTaiKham = DATE_ADD(CURDATE(), INTERVAL 1 DAY)')
                    ->where('TinhTrang_SMS', 0)
                    ->join('benhnhan', 'donthuoc.MaBN', '=', 'benhnhan.id')
                    ->select('benhnhan.*', 'donthuoc.*')
                    ->get();
                    
        if($donthuoc->count() > 0) {
            for($i = 0; $i < $donthuoc->count(); $i++) {
                $date = Carbon::parse($donthuoc[$i]->NgayTaiKham)->format('d-m-Y');

                try {
                    $client = new Client($this->account_sid, $this->auth_token);
                    $sms = $client->messages->create(
                        "+84".$donthuoc[$i]->SDT,
                        array(
                            'from' => $this->twilio_number,
                            'body' => "Xin chào: ".$donthuoc[$i]->HoTen.".\r\nAnh/Chị có một lịch tái khám tại Phòng khám tai mũi họng của chúng tôi vào ngày: ".$date."\r\nKhi đi tái khám Anh/Chị nhớ mang theo đơn thuốc.\r\nAnh/Chị vui lòng đến đúng giờ để có một buổi khám tốt nhất.\r\nTrân trọng!"
                        )
                    );
                    if($sms) {
                        $update = DonThuoc::where('MaDonThuoc', $donthuoc[$i]->MaDonThuoc)->update(['TinhTrang_SMS' => 1]);
        
                        if($update) {
                            return response()->json([
                                'id' => $update->id,
                                'status' => '200',
                                'message' => 'success'
                            ]);
                        }
                    }
                }
                catch (\Exception $e) {
                    continue;
                }
            }
        }
        else {
            return response()->json([
                'status' => '400',
                'message' => 'khong co don thuoc nao.'
            ]);
        }
        return response()->json([
            'status' => '400',
            'message' => 'Lỗi khi gửi SMS.'
        ]);
    }

    public function index() {
        return view('admin.send_sms_mail.index');
    }

    public function HoSoBenhNhan() {
        $hoso = PhieuHenKham::join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                    ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                    ->OrderBy('NgayHen', 'DESC')
                    ->OrderBy('GioHen')
                    ->paginate(5);

        return view('admin.send_sms_mail.data_benhnhan', compact('hoso'));
    }

    public function searchHoSo_BN(Request $request) {
        if($request->isMethod('POST')) {
            $hoso = PhieuHenKham::where('NgayHen', 'like', '%'. $request->ngayhen .'%')
                    ->where('GioHen', 'like', '%'. $request->giohen.'%')
                    ->where('MaNV', 'like', '%'. $request->bacsi .'%')
                    ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                    ->select('phieuhenkham.*', 'nhanvien.HoTen as BacSi')
                    ->OrderBy('NgayHen', 'DESC')
                    ->OrderBy('GioHen')
                    ->paginate(5);

            return view('admin.send_sms_mail.data_benhnhan', compact('hoso'));
        }
    }
   
    public function HoSoNhanVien() {
        $nhanvien = NhanVien::join('chucvu', 'nhanvien.MaChucVu', '=', 'chucvu.MaChucVu')
                    ->select('nhanvien.*', 'chucvu.*')
                    ->paginate(5);

        return view('admin.send_sms_mail.data_nhanvien', compact('nhanvien'));
    }

    public function send_Mail($email, $title, $content) {
        $subject = $title;
        $to_email = $email;
        $view = 'vendor.mail.mail_thongbao';

        $mail = new SendMail($content, $subject, $view);
        Mail::to($to_email)->send($mail);

        if (Mail::failures()) {
            return false;
        }
        return true;
    }

    public function Handle_Notify(Request $request) {
        if($request->isMethod('POST')) {
            foreach ($request->id_phieuhen as $key => $item) {
                $phieuhen = PhieuHenKham::where('MaPhieuHen', $item)->first();
                foreach($request->type_send as $type) {
                    if($type == 'send_sms' && $type == 'send_mail') {
                        try {
                            $client = new Client($this->account_sid, $this->auth_token);
                            $sms = $client->messages->create(
                                "+84".$phieuhen->SDT,
                                array(
                                    'from' => $this->twilio_number,
                                    'body' => $request->title."\r\n".$request->content
                                )
                            );
                        }
                        catch (\Exception $e) {
                            continue;
                        }
                        $send_mail = $this->send_Mail($phieuhen->Email, $request->title, $request->content);
                        if(!$send_mail) {
                            return response()->json([
                                'status' => '400',
                                'message' => 'Gửi thông báo thất bại!',
                                'alert' => 'error',
                            ]);
                        }
                    }
                    else if($type == 'send_mail') {
                        $send_mail = $this->send_Mail($phieuhen->Email, $request->title, $request->content);
                        if(!$send_mail) {
                            return response()->json([
                                'status' => '400',
                                'message' => 'Gửi thông báo thất bại!',
                                'alert' => 'error',
                            ]);
                        }
                    }
                    else {
                        try {
                            $client = new Client($this->account_sid, $this->auth_token);
                            $sms = $client->messages->create(
                                "+84".$phieuhen->SDT,
                                array(
                                    'from' => $this->twilio_number,
                                    'body' => $request->title."\r\n".$request->content
                                )
                            );
                        }
                        catch (\Exception $e) {
                            continue;
                        }
                    }
                }
            }
            
            return response()->json([
                'status' => '200',
                'message' => 'Gửi thông báo thành công!',
                'alert' => 'success',
            ]);
        }
    }
}
