<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ChuyenMon;
use Illuminate\Http\Request;

class ChuyenMonController extends Controller
{
    public function store(Request $request) {
        foreach ($request->tenchuyenmon as $item) {
            $chuyenmon = ChuyenMon::create([
                'TenChuyenMon' => $item,
            ]);

            if(!$chuyenmon) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Thêm chuyên môn thất bại!',
                    'alert' => 'error'
                ]);
            }
        }

        $chuyenmon = ChuyenMon::all();

        return response()->json([
            'data' => $chuyenmon,
            'status' => 200,
            'message' => 'Thêm chuyên môn thành công!',
            'alert' => 'success'
        ]);
    }
}
