<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ChucVu;
use Illuminate\Http\Request;

class ChucVuController extends Controller
{   
    public function store(Request $request) {
        foreach ($request->tenchucvu as $item) {
            $chucvu = ChucVu::create([
                'TenChucVu' => $item,
            ]);

            if(!$chucvu) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Thêm chức vụ thất bại!',
                    'alert' => 'error'
                ]);
            }
        }

        $chucvu = ChucVu::all();

        return response()->json([
            'data' => $chucvu,
            'status' => 200,
            'message' => 'Thêm chức vụ thành công!',
            'alert' => 'success'
        ]);
    }
}
