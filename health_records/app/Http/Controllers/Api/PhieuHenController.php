<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BenhNhan;
use App\Models\PhieuHenKham;
use App\Models\PhieuKham;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PhieuHenController extends Controller
{
    public function getAllPhieuHen() {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $so = $date->format('A');
        $date = $date->toDateString();

        $phieuhen = PhieuHenKham::where('NgayHen', '=', $date)
                    ->where('MaPhieuHen', 'like',  '%'.$so.'%')
                    ->where('TinhTrang_PH', 0)
                    ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                    ->select('nhanvien.HoTen as BacSi', 'phieuhenkham.*')
                    ->orderBy('phieuhenkham.NgayHen')
                    ->orderBy('phieuhenkham.MaPhieuHen')
                    ->paginate(5);
                    
        return view('pagination.data_phieuhen', compact('phieuhen'));
    }

    public function createPhieuKham(Request $request) {
        if($request->isMethod('POST')) { dd($request->all());
            // lấy thông tin phiếu hẹn
            $phieuhen = PhieuHenKham::where('MaPhieuHen', $request->id)->first();

            (int)$sophieu = substr($request->id, 12);
            
            // kiểm tra trường hợp user khi đăng ký lịch hẹn đã tạo tài khoản rồi
            $user = BenhNhan::where('email', '=', $phieuhen->Email)->first();
           
            if(!empty($user)) {
                $this->hanldePhieuKham($request->id, $user->id, $phieuhen->MaNV, $phieuhen->NgayHen, $sophieu);
                $this->updatePhieuHen($request->id);

                return response()->json([
                    'status' => '200',
                    'alert' => 'success',
                    'message' => 'Tạo phiếu khám thành công!',
                ]);
            } 
            // trường hợp user đăng ký lịch hẹn nhưng chưa đăng ký tài khoản
            else {
                // tạo mới tài khoản cho bệnh nhân
                $hashPassword = bcrypt($phieuhen->SDT);
                
                $user = BenhNhan::create([
                    'HoTen' => $phieuhen->HoTen,
                    'SDT' => $phieuhen->SDT,
                    'GioiTinh' => $phieuhen->GioiTinh_NK,
                    'NgaySinh' => $phieuhen->NamSinh_NK,
                    'email' => $phieuhen->Email,
                    'password' => $hashPassword,
                ]);

                $this->hanldePhieuKham($request->id, $user->id, $phieuhen->MaNV, $request->NgayHen, $sophieu);
                $this->updatePhieuHen($request->id);

                return response()->json([
                    'status' => '200',
                    'alert' => 'success',
                    'message' => 'Tạo phiếu khám thành công!',
                ]);
            }
        }
    }

    function hanldePhieuKham($id, $mabn, $manv, $ngaykham, $sophieu) {
        PhieuKham::create([
            'MaPhieuKham' => $id,
            'MaBN' => $mabn,
            'MaNV' => $manv,
            'NgayKham' => $ngaykham,
            'SoPhieu' => $sophieu,
        ]);
    }

    function updatePhieuHen($id) {
        PhieuHenKham::where('MaPhieuHen', $id)->update(['TinhTrang_PH' => 1]);
    }

    function deletePhieuHen($id) {
        PhieuHenKham::where('MaPhieuHen', '=', $id)->delete();
    }

    public function searchPhieuHen(Request $request) {
        if($request->isMethod('POST')) {
            if(!empty($request->ngayhen)) {
                if(!empty($request->maphieuhen) || !empty($request->hoten) || !empty($request->sdt)) {
                    $phieuhen = PhieuHenKham::where('NgayHen', $request->ngayhen)
                                ->where('MaPhieuHen', 'like', $request->maphieuhen.'%')
                                ->where('phieuhenkham.HoTen', 'like', '%'. $request->hoten .'%')
                                ->Where('phieuhenkham.SDT', 'like', '%'. $request->sdt .'%')
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('nhanvien.HoTen as BacSi', 'phieuhenkham.*')
                                ->orderBy('phieuhenkham.NgayHen')
                                ->orderBy('phieuhenkham.MaPhieuHen')
                                ->paginate(5);
                }
                else {
                    $phieuhen = PhieuHenKham::where('NgayHen', $request->ngayhen)
                                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                                ->select('nhanvien.HoTen as BacSi', 'phieuhenkham.*')
                                ->orderBy('phieuhenkham.NgayHen')
                                ->orderBy('phieuhenkham.MaPhieuHen')
                                ->paginate(5);
                }
                
            }
            else {
                $phieuhen = PhieuHenKham::where('MaPhieuHen', 'like', $request->maphieuhen.'%')
                            ->where('phieuhenkham.HoTen', 'like', '%'. $request->hoten .'%')
                            ->Where('phieuhenkham.SDT', 'like', '%'. $request->sdt .'%')
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->select('nhanvien.HoTen as BacSi', 'phieuhenkham.*')
                            ->orderBy('phieuhenkham.NgayHen')
                            ->orderBy('phieuhenkham.MaPhieuHen')
                            ->paginate(5);
            }
                
            return view('pagination.data_phieuhen', compact('phieuhen'));
        }
    }
}
