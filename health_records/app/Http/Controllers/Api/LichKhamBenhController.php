<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LichKhamBenh;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LichKhamBenhController extends Controller
{
    public function index()
    {
        $id = Auth::guard('admin')->id();

        if(request()->ajax()) 
        {
            $data = LichKhamBenh::where('MaNV', $id)->get();
            $events = [];
            foreach($data as $val) {
                $item = [
                    'id' => $val->MaLK,
                    'start' => $val->ThoiGianBD,
                    'end' => $val->ThoiGianKT,
                    'title' => $val->TieuDe,
                    'className' => $val->ClassName,
                ];
                array_push($events, $item);
            }
            return response()->json($events);
        }
        return view('admin.lichlamviec');
    }
    
   
    public function store(Request $request)
    {  
        $id = Auth::guard('admin')->id();

        $arr = [ 
            'MaNV' => $request->manv,
            'TieuDe' => $request->tieude,
            'ThoiGianBD' => $request->start,
            'ThoiGianKT' => $request->end,
            'ClassName' => $request->color
        ];

        $lichlamviec = LichKhamBenh::create($arr);   
        
        if($lichlamviec) {
            return response()->json([
                'data' => $lichlamviec,
                'message' => 'Thêm lịch thành công!',
                'status' => '200',
                'alert' => 'success',
            ]);
        }
        else {
            return response()->json([
                'message' => 'Thêm lịch thất bại',
                'status' => '400',
                'alert' => 'error',
            ]);
        }
    }
     
 
    public function update(Request $request)
    {   
        $arr = [ 
            'MaNV' => $request->manv,
            'TieuDe' => $request->tieude,
            'ThoiGianBD' => $request->start,
            'ThoiGianKT' => $request->end,
            'ClassName' => $request->color
        ];
        $event  = LichKhamBenh::where('MaLK', $request->id)->update($arr);
        $data = LichKhamBenh::where('MaLK', $request->id)->first();
        
        if($event) {
            return response()->json([
                'data' => $data,
                'message' => 'Cập nhật lịch thành công!',
                'status' => '200',
                'alert' => 'success',
            ]);
        }
        else {
            return response()->json([
                'message' => 'Cập nhật lịch thất bại',
                'status' => '400',
                'alert' => 'error',
            ]);
        }
    } 
 
 
    public function destroy(Request $request)
    {
        $event = LichKhamBenh::where('MaLK', $request->id)->delete();
   
        if($event) {
            return response()->json([
                'message' => 'Xóa thành công!',
                'status' => '200',
                'alert' => 'success',
            ]);
        }
        else {
            return response()->json([
                'message' => 'Xóa lịch thất bại',
                'status' => '400',
                'alert' => 'error',
            ]);
        }
    }    
}
