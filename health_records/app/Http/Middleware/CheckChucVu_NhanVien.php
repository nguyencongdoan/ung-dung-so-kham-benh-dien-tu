<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckChucVu_NhanVien
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->session()->has('chucvu')) {
            if($request->session()->get('chucvu') === 'Nhân viên') {
                return $next($request);
            }
        }
        session()->flash('message', 'Bạn không có quyền hạn này!');
        return redirect()->back();
    }
}
