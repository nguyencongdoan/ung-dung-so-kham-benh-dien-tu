<?php

namespace App\Exports;

use App\Models\PhieuKham;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PhieuKhamExport implements FromCollection, WithHeadings, WithColumnWidths, WithStyles, WithEvents
{
    public $start_day;
    public $end_day;

    public function __construct($start_day, $end_day) {
        $this->start_day = $start_day;
        $this->end_day = $end_day;
    }

    public function collection()
    {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $month = $date->format('m');
        
        if(!empty($this->start_day) && !empty($this->end_day)) {
            return PhieuKham::whereBetween('NgayKham', [$this->start_day, $this->end_day])
                    ->where('TinhTrangKham', '>=', 3)
                    ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                    ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                    ->selectRaw("MaPhieuKham, benhnhan.HoTen, HoTenNguoiThan, IF(benhnhan.GioiTinh = 1, 'Nam', 'Nữ'), benhnhan.SDT, DATE_FORMAT(NgayKham, '%d/%m/%Y') as 'Ngày khám', DATE_FORMAT(ThoiGianBD, '%T') as 'Giờ bắt đầu', DATE_FORMAT(ThoiGianKT, '%T') as 'Giờ kết thúc', nhanvien.HoTen as 'Bác sĩ'")
                    ->OrderBy('NgayKham')
                    ->OrderBy('MaPhieuKham')
                    ->get();
        }
        else if(!empty($this->start_day)) {
            return PhieuKham::whereDate('NgayKham', '>=', $this->start_day)
                    ->where('TinhTrangKham', '>=', 3)
                    ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                    ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                    ->selectRaw("MaPhieuKham, benhnhan.HoTen, HoTenNguoiThan, IF(benhnhan.GioiTinh = 1, 'Nam', 'Nữ'), benhnhan.SDT, DATE_FORMAT(NgayKham, '%d/%m/%Y') as 'Ngày khám', DATE_FORMAT(ThoiGianBD, '%T') as 'Giờ bắt đầu', DATE_FORMAT(ThoiGianKT, '%T') as 'Giờ kết thúc', nhanvien.HoTen as 'Bác sĩ'")
                    ->OrderBy('NgayKham')
                    ->OrderBy('MaPhieuKham')
                    ->get();
        }
        else if(!empty($this->end_day)) {
            return PhieuKham::whereDate('NgayKham', '<=', $this->end_day)
                    ->where('TinhTrangKham', '>=', 3)
                    ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                    ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                    ->selectRaw("MaPhieuKham, benhnhan.HoTen, HoTenNguoiThan, IF(benhnhan.GioiTinh = 1, 'Nam', 'Nữ'), benhnhan.SDT, DATE_FORMAT(NgayKham, '%d/%m/%Y') as 'Ngày khám', DATE_FORMAT(ThoiGianBD, '%T') as 'Giờ bắt đầu', DATE_FORMAT(ThoiGianKT, '%T') as 'Giờ kết thúc', nhanvien.HoTen as 'Bác sĩ'")
                    ->OrderBy('NgayKham')
                    ->OrderBy('MaPhieuKham')
                    ->get();
        }
        else {
            return PhieuKham::whereMonth('NgayKham', $month)
                    ->where('TinhTrangKham', '>=', 3)
                    ->join('nhanvien', 'phieukham.MaNV', '=', 'nhanvien.id')
                    ->join('benhnhan', 'phieukham.MaBN', '=', 'benhnhan.id')
                    ->selectRaw("MaPhieuKham, benhnhan.HoTen, HoTenNguoiThan, IF(benhnhan.GioiTinh = 1, 'Nam', 'Nữ'), benhnhan.SDT, DATE_FORMAT(NgayKham, '%d/%m/%Y') as 'Ngày khám', DATE_FORMAT(ThoiGianBD, '%T') as 'Giờ bắt đầu', DATE_FORMAT(ThoiGianKT, '%T') as 'Giờ kết thúc', nhanvien.HoTen as 'Bác sĩ'")
                    ->OrderBy('NgayKham')
                    ->OrderBy('MaPhieuKham')
                    ->get();
        }
    }

    public function headings(): array
    {
        return [
            'Mã phiếu khám',
            'Họ tên bệnh nhân',
            'Họ tên người thân',
            'Giới tính',
            'Số điện thoại',
            'Ngày khám',
            'Giờ bắt dầu',
            'Giờ kết thúc',
            'Họ tên bác sĩ'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 25,
            'C' => 25,
            'D' => 10,
            'E' => 20,
            'F' => 20,
            'G' => 15,
            'H' => 15,
            'I' => 25,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:I1')->getFont()->setBold(true);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A1:I1';
                $color = 'd6e9d6';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFill()
                    ->setFillType(Fill::FILL_SOLID)
                    ->getStartColor()->setRGB($color);
                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(20);

                $event->sheet->getDelegate()->getStyle($cellRange)
                ->applyFromArray(['alignment' => ['wrapText' => true]]);

                $event->sheet->getDelegate()->getStyle($cellRange)
                ->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            }
        ];
    }
}
