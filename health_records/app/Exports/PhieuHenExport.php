<?php

namespace App\Exports;

use App\Models\PhieuHenKham;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PhieuHenExport implements FromCollection, WithHeadings, WithColumnWidths, WithStyles, WithEvents
{
    public $start_day;
    public $end_day;
    public $tinhtrang;

    public function __construct($start_day, $end_day, $tinhtrang) {
        $this->start_day = $start_day;
        $this->end_day = $end_day;
        $this->tinhtrang = $tinhtrang;
    }

    public function collection()
    {
        $date = Carbon::now('Asia/Ho_Chi_Minh');
        $month = $date->format('m');
        
        if(!empty($this->start_day) && !empty($this->end_day) && isset($this->tinhtrang)) {
            return PhieuHenKham::whereBetween('NgayHen', [$this->start_day, $this->end_day])
                    ->where('TinhTrang_PH', $this->tinhtrang)
                    ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                    ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                    ->orderBy('NgayHen')
                    ->OrderBy('GioHen')
                    ->get();
        }
        else if(isset($this->tinhtrang)) {
            if(!empty($this->start_day)) {
                return PhieuHenKham::whereDate('NgayHen', '>=', $this->start_day)
                            ->where('TinhTrang_PH', $this->tinhtrang)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                           ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
            else if(!empty($this->end_day)) {
                return PhieuHenKham::where('TinhTrang_PH', $this->tinhtrang)
                            ->whereDate('NgayHen', '<=', $this->end_day)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
            else {
                return PhieuHenKham::whereMonth('NgayHen', $month)
                            ->where('TinhTrang_PH', $this->tinhtrang)
                            ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                            ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                            ->orderBy('NgayHen')
                            ->OrderBy('GioHen')
                            ->get();
            }
        }
        else if(!empty($this->start_day) && !empty($this->end_day)) {
            return PhieuHenKham::whereBetween('NgayHen', [$this->start_day, $this->end_day])
                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                ->OrderBy('NgayHen')
                ->OrderBy('GioHen')
                ->get();
        }
        else if(!empty($this->start_day)) {
            return PhieuHenKham::whereDate('NgayHen', '>=', $this->start_day)
                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                ->OrderBy('NgayHen')
                ->OrderBy('GioHen')
                ->get();
        }
        else if(!empty($this->end_day)) {
            return PhieuHenKham::whereDate('NgayHen', '<=', $this->end_day)
                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                ->OrderBy('NgayHen')
                ->OrderBy('GioHen')
                ->get();
        }
        else {
            return PhieuHenKham::whereMonth('NgayHen', $month)
                ->join('nhanvien', 'phieuhenkham.MaNV', '=', 'nhanvien.id')
                ->selectRaw("MaPhieuHen, HoTen_NK, CONCAT(YEAR(CURDATE()) - YEAR(NamSinh_NK) + 1, ' tuổi') as 'Tuổi', IF(GioiTinh_NK = 1, 'Nam', 'Nữ'), phieuhenkham.SDT, DATE_FORMAT(NgayHen, '%d/%m/%Y') as 'Ngày hẹn', DATE_FORMAT(GioHen, '%T') as 'Giờ hẹn', nhanvien.HoTen as 'Bác sĩ'")
                ->OrderBy('NgayHen')
                ->OrderBy('GioHen')
                ->get();
        }
    }

    public function headings(): array
    {
        return [
            'Mã phiếu hẹn',
            'Họ tên bệnh nhân',
            'Tuổi',
            'Giới tính',
            'Số điện thoại',
            'Ngày hẹn',
            'Giờ hẹn',
            'Họ tên bác sĩ'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 25,
            'C' => 10,
            'D' => 10,
            'E' => 20,
            'F' => 20,
            'G' => 20,
            'H' => 25,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:H1')->getFont()->setBold(true);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A1:H1';
                $color = 'd6e9d6';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFill()
                    ->setFillType(Fill::FILL_SOLID)
                    ->getStartColor()->setRGB($color);
                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(20);

                $event->sheet->getDelegate()->getStyle($cellRange)
                ->applyFromArray(['alignment' => ['wrapText' => true]]);

                $event->sheet->getDelegate()->getStyle($cellRange)
                ->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            }
        ];
    }
}
