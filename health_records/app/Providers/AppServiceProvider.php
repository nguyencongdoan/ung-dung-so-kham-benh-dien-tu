<?php

namespace App\Providers;

use App\Models\Slide_Logo;
use App\Models\ThongTinPhongKham;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Paginator::defaultView('vendor.pagination.default');
        Paginator::defaultSimpleView('vendor.pagination.simple-bootstrap-4');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $thongtin = ThongTinPhongKham::first();
        $slide = Slide_Logo::all();
        view()->share(compact(['thongtin', 'slide']));
    }
}
