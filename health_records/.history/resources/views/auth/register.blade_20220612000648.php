<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />
    <link rel="stylesheet" href="{{ asset('./client/assets/css/login.css') }}">
</head>

<body>
    <div class="container">
        <div id="register" class="col-11 col-md-6 col-sm-10">
            <div class="login-form">
                @if (session()->has('message')) 
                    <label for="" class="{{ session('alert') }}">
                        {{ session('message') }}
                    </label>
                @endif

                <div class="form-title">
                    <h1>Đăng ký</h1>
                </div>

                <form action="{{ route('auth.createRegister') }}" method="post">
                    @csrf
                    <label for="">
                        <input type="text" placeholder="Họ tên" name="HoTen" id="input" value="{{ old('HoTen') }}">
                        @if($errors->has('HoTen'))
                            <div class="alert-error">{{ $errors->first('HoTen') }}</div>
                        @endif
                    </label>
    
                    <label for="">
                        <input type="text" placeholder="Địa chỉ" name="DiaChi" id="input" value="{{ old('DiaChi') }}">
                        @if($errors->has('DiaChi'))
                            <div class="alert-error">{{ $errors->first('DiaChi') }}</div>
                        @endif
                    </label>
    
                    <label for="">
                        <input type="text" placeholder="Số điện thoại" name="SDT" id="input" value="{{ old('SDT') }}">
                         @if($errors->has('SDT'))
                            <div class="alert-error">{{ $errors->first('SDT') }}</div>
                        @endif
                    </label>
    
                    <div class="form-check">
                        <div class="radio-left">
                            <input class="form-check-input" type="radio" name="GioiTinh" id="radio-nam" value="1" checked>
                            <label class="form-check-label" for="radio-nam">
                                Nam
                            </label>
                        </div>
                        <div>
                            <input class="form-check-input" type="radio" name="GioiTinh" id="radio-nu" value="0">
                            <label class="form-check-label" for="radio-nu">
                                Nữ
                            </label>
                        </div>
                    </div>
    
                   <label for="">
                        <input type="date" placeholder="Ngày sinh" name="NgaySinh" id="input" value="{{ old('NgaySinh') }}">
                         @if($errors->has('NgaySinh'))
                            <div class="alert-error">{{ $errors->first('NgaySinh') }}</div>
                        @endif 
                    </label>
    
                    <label for="">
                        <input type="email" placeholder="Email" name="email" id="input" value="{{ old('email') }}">
                        @if($errors->has('email'))
                            <div class="alert-error">{{ $errors->first('email') }}</div>
                        @endif
                    </label>
    
                    <label for="">
                        <input type="password" placeholder="Mật khẩu" name="password" id="input" value="{{ old('password') }}">
                         @if($errors->has('password'))
                            <div class="alert-error">{{ $errors->first('password') }}</div>
                        @endif
                    </label>
    
                    <button class="red res" id="res" type="submit">
                        <i class="fa fa-user-plus"></i>&nbsp;Đăng ký
                    </button>
                </form>
            </div>
        </div>
     </div>
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</body>

</html>