<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>Login and Registration Form in HTML | CodingNepal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />
    <link rel="stylesheet" href="{{ asset('./client/assets/css/login.css') }}">
</head>
<html>

<body>
    <div class="container">
        <div id="login" class="col-11 col-md-6 col-sm-8">
            <div class="login-form">
                <div class="form-title">
                    <h1>Đăng nhập</h1>
                </div>

                <form action="{{ route('auth.handleAuthLogin') }}" method="post">
                    @csrf
                    @if (session()->has('message')) 
                        <label for="" class="{{ session('alert') }} mb-3">
                            {{ session('message') }}
                        </label>
                    @endif
                    
                    <label for="">
                        <input type="email" placeholder="Email" name="email" value="{{ old('email') }}" id="input">
                        @if($errors->has('email'))
                            <div class="alert-error">{{ $errors->first('email') }}</div>
                        @endif
                    </label>
    
                    <label for="">
                        <input type="password" placeholder="Mật khẩu" name="password" value="{{ old('password') }}" id="input">
                        @if($errors->has('password'))
                            <div class="alert-error">{{ $errors->first('password') }}</div>
                        @endif
                    </label>
    
                    <div class="remember-me">
                        <input type="checkbox" class="form-check-input" checked id="checkbox">
                        <label for="checkbox" class="label-checkbox">
                            <span class="txt1">Ghi nhớ</span>
                        </label>
                    </div>
    
                    <button class="red" type="submit">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Login
                    </button>
    
                    <p class="login-with-social">Đăng nhập với...</p>
                    <div class="social-media" style="padding-top: 10px;">
                        <button class="social-icons" type="button">
                            <i class="fa-brands fa-facebook-f" style="--icon-color: blue" aria-hidden="true"></i>
                        </button>
    
                        <button class="social-icons" type="button">
                            <i class="fa-brands fa-twitter" style="--icon-color: aqua" aria-hidden="true"></i>
                        </button>
    
                        <button class="social-icons" type="button">
                            <i class="fa-brands fa-google-plus-g" style="--icon-color: red" aria-hidden="true"></i>
                        </button>
                    </div>
    
                    <div class="d-flex justify-content-evenly flex-wrap">
                        <a class="me-2 mb-3" href="{{ route('auth.register') }}">
                            <button type="button" class="red" name="register" style="margin-right: 25px;">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Đăng ký
                            </button>
                        </a>
                        <a class="ms-2 mb-3" href="{{ route('auth.register') }}">
                            <button  type="button" class="red">
                                <i class="fa fa-unlock-alt"></i>&nbsp;Quên mật khẩu
                            </button>
                        </a>
                    </div>
                </form>
                
            </div>
        </div>
        
    </div>
</body>

</html>