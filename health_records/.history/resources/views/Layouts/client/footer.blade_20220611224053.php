@section('footer')
    <footer class="footer">
        <div class="container mt-auto">
            <div class="row align-items-center justify-content-evenly">
                <div class="col-md-3 d-flex flex-column align-items-center justify-content-center col-sm-6 col-6 mb-4">
                    <div class="logo-footer">
                        <a href="#">
                            <img src="{{ asset('./client/assets/images/logo.png') }}" alt="logo" />
                        </a>
                    </div>
                    <div class="social-media mt-2 d-flex">
                        <div class="social-icon-item pe-2">
                            <a href="#" class="icon-link">
                                <i class="fa-brands fa-youtube"></i>
                            </a>
                        </div>
                        <div class="social-icon-item pe-2">
                            <a href="#" class="icon-link">
                                <i class="fa-brands fa-facebook-f"></i>
                            </a>
                        </div>
                        <div class="social-icon-item pe-2">
                            <a href="#" class="icon-link">
                                <i class="fa-brands fa-google-plus-g"></i>
                            </a>
                        </div>
                        <div class="social-icon-item pe-2">
                            <a href="#" class="icon-link">
                                <i class="fa-brands fa-twitter"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-4 navbar navbar-expand-lg d-none d-md-block">
                    <ul class="navbar-nav mx-auto align-items-center justify-content-center">
                        <li class="nav-item pe-5">
                            <a href="{{ route('client.datlich') }}" class="nav-link">Lịch khám bệnh</a>
                        </li>
                        <li class="nav-item pe-5">
                            <a href="{{ route('client.thongtin-bs') }}" class="nav-link">Thông tin bác sĩ</a>
                        </li>
                        <li class="nav-item pe-5">
                            <a href="{{ route('client.tintuc') }}" class="nav-link">Tin Tức</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 mb-4 col-6 col-sm-6 infomation">
                    <h4>Liên hệ</h4>
                    <div class="address">
                        <i class="fa-solid fa-location-dot"></i>
                        <span class="ms-2">41A Quang Trung – Nha Trang.</span>
                    </div>
                    <div class="phone">
                        <i class="fa-solid fa-phone"></i>
                        <span class="ms-2">012 345 6789</span>
                    </div>
                    <div class="mail">
                        <i class="fa-solid fa-envelope"></i>
                        <span class="ms-2">demo@gmail.com</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection