@section('header')
    <header class="sticky-top">
        <div class="container">
            <div class="row">
                <nav class="navbar pt-3 justify-content-between navbar-expand-lg">
                    <div class="logo-header">
                        <a href="{{ route('client.home') }}">
                            <img src="{{ asset('./client/assets/images/logo.png') }}" alt="logo" />
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                        data-bs-target="#navbarOffcanvasLg" aria-controls="navbarOffcanvasLg">
                        <i class="fa-solid fa-bars icon-bar"></i>
                    </button>
                    <div class="offcanvas offcanvas-end col-10" tabindex="-1" id="navbarOffcanvasLg"
                        aria-labelledby="navbarOffcanvasLgLabel">
                        <ul class="navbar-nav nav-menu ms-auto me-auto me-sm-auto align-items-center">
                            <li class="nav-item">
                                <a href="{{ route('client.home') }}" class="nav-link">Trang chủ</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('client.datlich') }}" class="nav-link">Đặt lịch khám</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('client.thongtin-bs') }}" class="nav-link">Thông tin bác sĩ</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('client.tintuc') }}" class="nav-link">Tin Tức</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="fa-solid fa-bell text-white"></i>
                                </a>
                            </li>
                            @if(Auth::guard('benhnhan')->check())
                                <li class="nav-item dropdown">
                                    <a href="#" class="" role="button" id="dropUser" data-bs-toggle="dropdown">
                                        @php $url = "./uploads/images/".Auth::guard('benhnhan')->user()->AnhDaiDien @endphp
                                        <img src="{{ asset($url) }}" style="width: 50px; height: 50px; border-radius:50%" >
                                    </a>
                                    <ul class="dropdown-menu menu" aria-labelledby="dropUser">
                                        <li><a class="dropdown-item" href="#">Hi: {{ Auth::guard('benhnhan')->user()->HoTen }}</a></li>
                                        <li><a class="dropdown-item" href="{{ route('client.user.thongtin-user', ['id' => Auth::guard('benhnhan')->id()]) }}">Quản lý tài khoản</a></li>
                                        <li>
                                            <a class="dropdown-item d-flex align-items-center" href="{{ route('auth.logout.client') }}">
                                                <img src="{{ asset('./Client/assets/images/logout.png') }}" class="me-2" style="width: 20px; height: 20px;" alt="">
                                                Đăng xuất
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                </li>
                            @else
                                <li class="nav-item mb-3">
                                    <a href="{{ route('auth.login') }}" class="btn btn-hover">Đăng nhập</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('auth.register') }}" class="btn btn-hover">Đăng ký</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>
@endsection