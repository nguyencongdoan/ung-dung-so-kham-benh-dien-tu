@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script> 
    <!-- jquery -->
    <script src="{{ asset('./js/jquery.min.js') }}"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('./js/toast.js') }}"></script>
    <script src="{{ asset('./client/assets/js/menu.js') }}"></script>
@endsection