<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Page Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />
    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
    <!-- CSS only -->
    <link rel="stylesheet" href="{{ asset('./css/bootstrap.min.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('./admin/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('./css/toast.css') }}">
    @yield('style')
</head>
<body class="sidebar-menu-collapsed">
    @include('layouts.admin.header')
    @include('layouts.admin.footer')
    @include('layouts.admin.scripts')

    @yield('header')

    <section class="main-content">
        @yield('content')
    </section>

    @yield('footer') 
    
    @yield('scripts') 

    @stack('scripts')
</body>
</html>