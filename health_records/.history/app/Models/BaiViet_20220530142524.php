<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaiViet extends Model
{
    use HasFactory;

    protected $table = 'BaiViet';  

    protected $fillable = [
        'MaBV',
        'MaTL',
        'MaNV',
        'TieuDe',
        'TomTat',
        'Thumnail',
        'NgayTao',
        'NoiDung'
    ];

    public $timestamps = true;
}
