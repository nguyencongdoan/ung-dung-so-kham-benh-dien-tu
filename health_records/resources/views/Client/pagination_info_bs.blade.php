@if ($bacsi->count() > 0)
    @foreach ($bacsi as $item)
        <div class="expert-item col-sm-6 mb-4 card align-items-center">
            <a href="#">
                @php $url = './client/assets/images/'.$item->AnhDaiDien; @endphp
                <img src="{{ asset($url) }}" alt="bs-top" class="card-img-top mx-auto" />
            </a>
            <div class="card-body text-center">
                <a href="#">
                    <h5 class="card-title">{{ $item->HoTen }}</h4>
                </a>
                <p class="card-subtitle mb-3">{{ $item->TenChucVu }} {{ $item->TenChuyenMon }}</p>
                <div>
                    <a href="{{ route('client.datlich', $item->id) }}"
                        class="btn btn-outline outline-primary my-auto mb-3">
                        <i class="fa-solid fa-calendar-check me-2"></i> Đặt lịch khám
                    </a>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-11">
        {{ $bacsi->links() }}
    </div>
@endif