@extends('layouts.client')

@section('content')
    <div class="container-fluid">
        <div class="row px-3 pt-5 justify-content-around">
            <div class="col-12 col-md-7 mt-4 text-justify overflow-hidden" data-aos="fade-right">
                <h1 class="fw-bold text-center">{{ $baiviet->TieuDe }}</h1>
                <p class="subtitle my-3">
                    {{ $baiviet->TomTat }}
                </p>
                <div class="col-12">
                    {!! $baiviet->NoiDung !!}
                </div>
                <div class="mb-4 d-flex flex-wrap">
                    <p class="fw-bold me-4">Được viết bởi: {{ $baiviet->HoTen }}</p>
                    <p class="fw-bold">Ngày tạo: {{ $baiviet->NgayTao }}</p>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-4" data-aos="fade-down">
                <div class="booking py-3 mb-5">
                    <h3 class="fw-bold text-center">Bài viết gần đây</h3>
                    @if ($top_bv->count() > 0)
                        @foreach ($top_bv as $item)
                            <a class="bb-line d-block" href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}">
                                <div class="d-flex my-4 align-items-center">
                                    @php $url = './uploads/images/'.$item->Thumnail; @endphp
                                    <img src="{{ asset($url) }}" class="img-detail">
                                    <p class="text-detail ms-3 fw-bold">{{ $item->TieuDe }}</p>
                                </div>
                            </a>
                        @endforeach
                    @else
                        <p class="fs-4 my-4 text-danger fw-bold">Chưa có bài viết nào.</p>
                    @endif
                </div>
            </div>
            <h3 class="fw-bold mb-4" data-aos="fade-up">Bài viết liên quan</h3>
            <div class="list-news col-12 m-auto row" data-aos="fade-up">
                @if ($bv_lq->count() > 0)
                    @foreach ($bv_lq as $item)
                        <div class="new-item card mb-5">
                            <a href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}">
                                @php $url = './uploads/images/'.$item->Thumnail; @endphp
                                <img class="card-img-top" src="{{ asset($url) }}" alt="" />
                            </a>
                            <div class="card-body mb-2">
                                <a href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}">
                                    <h4 class="card-title">{{ $item->TieuDe }}</h4>
                                </a>
                                <p class="card-subtitle mb-3">{{ $item->TomTat }}</p>
                                <a href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}"
                                    class="card-link fw-bold">Xem chi tiết</a>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="fs-4 my-4 text-danger fw-bold">Chưa có bài viết nào.</p>
                @endif
            </div>
        </div>
    </div>
@endsection
