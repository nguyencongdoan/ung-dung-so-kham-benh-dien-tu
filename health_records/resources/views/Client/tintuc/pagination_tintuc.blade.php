@if ($baiviet->count() > 0)
    @foreach ($baiviet as $item)
        <div class="new-item item-tt card mb-5">
            <a href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}">
                @php $url = './uploads/images/'.$item->Thumnail; @endphp
                <img class="card-img-top" src="{{ asset($url) }}" alt="" />
            </a>
            <div class="card-body mb-2">
                <a href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}">
                    <h4 class="card-title">{{ $item->TieuDe }}</h4>
                </a>
                <p class="card-subtitle mb-3">{{ $item->TomTat }}</p>
                <a href="{{ route('client.tintuc.detail_bv', $item->MaBV) }}" class="card-link fw-bold">Xem chi tiết</a>
            </div>
        </div>
    @endforeach
@endif
{{ $baiviet->links() }}