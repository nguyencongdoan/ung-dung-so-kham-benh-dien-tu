@extends('layouts.client')

@section('content')
    <div id="toast"></div>
    <div class="container">
        <div class="row">
            <section class="news my-5" data-aos="fade-down" data-aos-duration="1500">
                <h1 class="mb-5 text-center">Tin tức</h1>
                <div class="search-experts my-5 col-md-10">
                    <h3>Tìm kiếm</h3>
                    <form action="" id="formSearch">
                        <div class="row">
                            @csrf
                            <div class="col-md-3 col-6 mb-3">
                                <label for="search_bs" class="form-label mb-2">Tên bài viết</label>
                                <input name="tieude" id="search_bs" type="text" class="form-control"
                                    placeholder="Search...">
                            </div>
                            <div class="col-md-3 col-6 mb-3">
                                <label for="search_bs" class="form-label mb-2">Thể loại</label>
                                <select name="theloai" id="theloai" class="form-select">
                                    <option value="">- Chọn -</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-6 mb-3">
                                <label for="search_bs" class="form-label mb-2">Sắp xếp</label>
                                <select name="sort" class="form-select">
                                    <option value="">- Chọn -</option>
                                    <option value="desc">Mới nhất</option>
                                    <option value="asc">cũ nhất</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="list-news m-auto d-flex flex-wrap col-md-12"></div>
                </div>
            </section>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            loadAllNews();

            function loadAllNews() {
                $.ajax({
                    url: "{{ route('client.tintuc.getAllNews') }}",
                    type: 'GET',
                    success: function(res) {
                        $('.list-news').html(res);
                    }
                });
            }

            loadTheLoai();

            function loadTheLoai() {
                $.ajax({
                    url: "{{ route('api.client.baiviet.getTheLoai') }}",
                    type: "GET",
                    dataType: "json",
                    success: function(res) {
                        if (res.status == 200) {
                            var _option = '';
                            $.each(res.data, function(key, item) {
                                _option += `
                                    <option value="${item.MaTL}">${item.TenTL}</option>
                                `;
                            });
                            $('#theloai').append(_option);
                        }
                    }
                });
            }

            page = 1;
            searchNews();

            function searchNews() {
                $('#formSearch').on('keyup', function() {
                    searchAjax(page);
                });

                $('#formSearch').on('change', function() {
                    searchAjax(page);
                });
            }

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault(); 
                var page = $(this).attr('href').split('page=')[1];
                searchAjax(page);
            });

            function searchAjax(page) {
                var data = $('#formSearch').serialize();
                $.ajax({
                    url: "/api/client/baiviet/search?page=" + page,
                    type: 'post',
                    data: data,
                    success: function(res) {
                        $('.list-news').html(res);
                    }
                });
            }
        });
    </script>
@endpush
