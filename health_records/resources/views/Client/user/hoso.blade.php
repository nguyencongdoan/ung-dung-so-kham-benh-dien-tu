@extends('layouts.client')

@section('content') 
    <div id="toast"></div>
    <div class="container-fluid">
        <div class="row justify-content-center users-profile">
            <div class="col-md-3 my-5 users-left">
                <div class="info-users">
                    <div class="info-item">
                        <a href="{{ route('client.user.thongtin-user', Auth::guard('benhnhan')->id()) }}">
                            <i class="uil uil-chat-bubble-user"></i> 
                            <span>Thông tin khách hàng</span>
                        </a>
                    </div>
                    <div class="info-item active w-fit-content pe-4">
                        <a href="{{route('client.user.hoso', Auth::guard('benhnhan')->id())}}">
                            <i class="uil uil-clipboard-notes"></i>
                            <span>Hồ sơ khám bệnh</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{route('client.user.lichsu_datlich', Auth::guard('benhnhan')->user()->SDT)}}">
                            <i class="uil uil-history"></i>
                            <span>Lịch sử đặt lịch</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{ route('auth.logout.client') }}">
                            <i class="fa-solid fa-arrow-right-from-bracket"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </div>
                </div>
            </div>
            <i class="fa-solid fa-bars icon-bar btn-toggle-profile fs-2 mt-3 d-lg-none col-1" data-aos="fade-right"></i>
            <div class="col-md-8 mb-5 mt-3 my-md-5 users-right">
                <div class="row my-4 mx-2">
                    <h3 class="text-center">Hồ sơ khám bệnh</h3>
                    <div class="overflow-scroll">
                        <table class="table">
                            <thead>
                                <th>STT</th>
                                <th>Mã đơn thuốc</th>
                                <th>Mã phiếu khám</th>
                                <th>Họ tên bệnh nhân</th>
                                <th>Chuẩn đoán bệnh</th>
                                <th>Ngày khám</th>
                                <th>Ngày tái khám</th>
                                <th>Bác sĩ khám</th>
                            </thead>
                            <tbody class="tbody">
                                @if ($hoso->count() > 0)
                                    @foreach ($hoso as $key => $item)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $item->MaDonThuoc }}</td>
                                            <td>{{ $item->MaPhieuKham }}</td>
                                            <td>{{ $item->HoTen_BN }}</td>
                                            <td>{{ $item->ChuanDoanBenh }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->NgayKham)->format('d-m-Y') }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->NgayTaiKham)->format('d-m-Y') }}</td>
                                            <td>{{ $item->HoTen_BS }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">
                                            <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    {{ $hoso->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
