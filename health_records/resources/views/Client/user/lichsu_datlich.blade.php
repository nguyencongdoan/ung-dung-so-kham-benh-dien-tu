@extends('layouts.client')

@section('content') 
    <div id="toast"></div>
    <div class="container-fluid">
        <div class="row justify-content-center users-profile">
            <div class="col-md-3 my-5 users-left">
                <div class="info-users">
                    <div class="info-item">
                        <a href="{{ route('client.user.thongtin-user', Auth::guard('benhnhan')->id()) }}">
                            <i class="uil uil-chat-bubble-user"></i> 
                            <span>Thông tin khách hàng</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{route('client.user.hoso', Auth::guard('benhnhan')->id())}}">
                            <i class="uil uil-clipboard-notes"></i>
                            <span>Hồ sơ khám bệnh</span>
                        </a>
                    </div>
                    <div class="info-item active w-fit-content pe-4">
                        <a href="{{route('client.user.lichsu_datlich', Auth::guard('benhnhan')->user()->SDT)}}">
                            <i class="uil uil-history"></i>
                            <span>Lịch sử đặt lịch</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{ route('auth.logout.client') }}">
                            <i class="fa-solid fa-arrow-right-from-bracket"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </div>
                </div>
            </div>
            <i class="fa-solid fa-bars icon-bar btn-toggle-profile fs-2 mt-3 d-lg-none col-1" data-aos="fade-right"></i>
            <div class="col-md-8 mb-5 mt-3 my-md-5 users-right">
                <div class="row my-4 mx-2">
                    <h3 class="text-center">Lịch sử đặt lịch</h3>
                    <div class="lichsu_hen"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function removeLichHen(e) {
           var check = confirm("Bạn có chắc muốn xóa phiếu hẹn này không?");
           if(check) {
                $.ajax({
                    url: "{{route('api.client.user.removePhieuHen')}}",
                    type: 'post',
                    dataType: 'json',
                    data: {id: e.value},
                    success: function (res) {
                        toast_message(res);
                        loadLichSuHen();
                    }
                });
           }
        }

        page = 1;
        loadLichSuHen(page);

        function loadLichSuHen(page) {
            $.ajax({
                url: "/api/client/user/getLichSuHen/{{$sdt}}?page=" + page,
                type: "GET",
                success: function (res) {
                   $('.lichsu_hen').html(res);
                }
            })
        }

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            loadLichSuHen(page);
        });
    </script>
@endpush
