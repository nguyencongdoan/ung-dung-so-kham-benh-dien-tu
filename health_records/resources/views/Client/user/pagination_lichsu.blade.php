<div class="overflow-scroll">
    <table class="table">
        <thead>
            <th>STT</th>
            <th>Mã phiếu hẹn</th>
            <th>Họ tên người hẹn</th>
            <th>Họ tên bệnh nhân</th>
            <th>Ngày hẹn</th>
            <th>Giờ hẹn</th>
            <th>Bác sĩ khám</th>
            <th>Tình trạng phiếu hẹn</th>
        </thead>
        <tbody class="tbody">
            @if ($phieuhen->count() > 0)
                @foreach ($phieuhen as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->MaPhieuHen }}</td>
                        <td>{{ $item->HoTen }}</td>
                        <td>{{ $item->HoTen_NK }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->NgayHen)->format('d-m-Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->GioHen)->format('H:i A') }}</td>
                        <td>{{ $item->HoTen_BS }}</td>
                        <td>
                            @if ($item->TinhTrang_PH == 1)
                                <p style="color: lightgreen;">Đã tạo phiếu khám</p>
                            @else
                                <p style="color: red;">Chưa tạo phiếu khám</p>
                            @endif

                        </td>
                        @if ($item->TinhTrang_PH == 0)
                            <td>
                                <a href="{{ route('client.edit_phieuhen', $item->MaPhieuHen) }}"
                                    class="fw-bold text-primary btn-edit bg-none">
                                    <i class="uil uil-pen"></i>
                                </a>
                            </td>
                            <td>
                                <button type="button" onclick="removeLichHen(this)" value="{{ $item->MaPhieuHen }}"
                                    class="fw-bold text-danger bg-none">
                                    <i class="uil uil-trash-alt"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $phieuhen->links() }}
