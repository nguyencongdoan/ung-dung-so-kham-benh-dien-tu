@extends('layouts.client')

@section('content') 
    <div id="toast"></div>
    <div class="container">
        <div class="row justify-content-center users-profile">
            <div class="col-md-3 my-5 users-left">
                <div class="info-users">
                    <div class="info-item active w-fit-content pe-4">
                        <a href="{{ route('client.user.thongtin-user', Auth::guard('benhnhan')->id()) }}">
                            <i class="uil uil-chat-bubble-user"></i> 
                            <span>Thông tin khách hàng</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{route('client.user.hoso', Auth::guard('benhnhan')->id())}}">
                            <i class="uil uil-clipboard-notes"></i>
                            <span>Hồ sơ khám bệnh</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{route('client.user.lichsu_datlich', Auth::guard('benhnhan')->user()->SDT)}}">
                            <i class="uil uil-history"></i>
                            <span>Lịch sử đặt lịch</span>
                        </a>
                    </div>
                    <div class="info-item">
                        <a href="{{ route('auth.logout.client') }}">
                            <i class="fa-solid fa-arrow-right-from-bracket"></i>
                            <span>Đăng xuất</span>
                        </a>
                    </div>
                </div>
            </div>
            <i class="fa-solid fa-bars icon-bar btn-toggle-profile fs-2 mt-3 d-lg-none col-1" data-aos="fade-right"></i>
            <div class="col-md-8 mb-5 mt-3 my-md-5 users-right">
                <div class="row my-4 mx-2">
                    <div class="col-md-3 ms-auto btn-edit-user">
                        <a href="#" role="button" data-bs-toggle="modal" data-bs-target="#changeProfile">
                            <i class="uil uil-pen"></i>
                            <span>Sửa thông tin</span>
                        </a>
                    </div>
                    <div class="col-12 my-4 d-flex justify-content-center">
                        <div class="img-profile">
                            @php $url = "./uploads/images/".Auth::guard('benhnhan')->user()->AnhDaiDien @endphp
                            <img src="{{ asset($url) }}" alt="">
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-chat-bubble-user"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Họ tên</label>
                            <p class="m-0 HoTen"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-envelope-alt"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Email</label>
                            <p class="m-0 email"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-map-marker"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Địa chỉ</label>
                            <p class="m-0 DiaChi"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-mobile-android"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Số điện thoại</label>
                            <p class="m-0 SDT"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="icon-gt"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Giới tính</label>
                            <p class="m-0 GioiTinh"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-calendar-alt"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Ngày sinh</label>
                            <p class="m-0 NgaySinh"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-postcard"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Mã BHYT</label>
                            <p class="m-0 bhyt"></p>
                        </div>
                    </div>
                    <div class="info-user-item p-3 col-5 my-2 d-flex align-items-center">
                        <i class="uil uil-padlock"></i>
                        <div class="info-content ms-2">
                            <label class="form-label m-0">Password</label>
                            <p class="m-0">************</p>
                        </div>
                        <a href="#" role="button" class="ms-auto" data-bs-toggle="modal" data-bs-target="#changePassword">
                            <i class="uil uil-pen"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- Modal chỉnh sửa thông tin user -->
     <div class="modal fade" id="changeProfile">
        <div class="modal-dialog modal-lg">
          <div class="modal-content pb-3">
            <div class="container-fluid">
                <form action="{{ route('api.client.user.editInfoUser') }}" method="post" id="formInfoUser" action="" enctype="multipart/form-data" novalidate>
                    @csrf
                    <i class="uil uil-times-circle ms-auto w-fit-content fs-3 cusor-pointer" data-bs-dismiss="modal"></i>
                    <div class="row mx-auto">
                        <div class="col-md-4 col-sm-12 modal-left d-flex justify-content-center align-items-center bg-sm-none">
                            <label for="file-img" class="label-modal-icon label-img">
                                <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                            </label>
                            <input type="file" id="file-img" name="anhdaidien" class="d-none">
                        </div>
                        
                        <div class="col-md-8 col-sm-12 mt-4 modal-right">
                            <h1 class="text-center fw-bold">Thay đổi thông tin</h1>
                            <input type="hidden" value="{{Auth::guard('benhnhan')->id()}}" name="id" class="id">
                            <div class="row modal-profile mt-4">
                                <div class="col-sm-6 my-2">
                                    <span class="fw-bold">Họ tên</span>
                                    <input type="text" name="HoTen" class="form-control mt-1 hoten" value="" required>
                                    <div class="invalid-feedback">
                                        Vui lòng nhập họ tên
                                    </div>
                                </div>
                                <div class="col-sm-6 my-2">
                                    <span class="fw-bold">Email</span>
                                    <input type="email" name="email" class="form-control mt-1 email" value="" required>
                                    <div class="invalid-feedback">
                                        Vui lòng nhập lại email
                                    </div>
                                </div>
                                <div class="col-sm-6 my-2">
                                    <span class="fw-bold">Số điện thoại</span>
                                    <input type="number" name="SDT" class="form-control mt-1 sdt" value="" required>
                                    <div id="valid-sdt" class="invalid-feedback">
                                        Vui lòng nhập số điện thoại
                                    </div>
                                </div>
                                <div class="col-sm-6 my-2">
                                    <span class="fw-bold">Ngày sinh</span>
                                    <input type="date" name="NgaySinh" class="form-control mt-1 ngaysinh" value="" required>
                                    <div class="invalid-feedback">
                                        Vui lòng chọn ngày sinh
                                    </div>
                                </div>
                                <div class="col-sm-6 my-2">
                                    <span class="fw-bold">Mã BHYT</span>
                                    <input type="text" name="MaBHYT" class="form-control bhyt mt-1" value="">
                                </div>
                                <div class="col-sm-6 my-2">
                                    <span class="fw-bold">Giới tính</span>
                                    <div class="form-check">
                                        <input type="radio" name="GioiTinh" id="gt-nam" class="form-check-input gt-nam-in" value="1">
                                        <label for="gt-nam" class="form-check-">Nam</label>
                                    </div>
                                    <div class="form-check">
                                        <input type="radio" name="GioiTinh" id="gt-nu" class="form-check-input gt-nu-in" value="0">
                                        <label for="gt-nu" class="form-check-">Nữ</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <span class="fw-bold">Địa chỉ</span>
                                    <textarea name="DiaChi" id="" cols="20" rows="3" class="form-control mt-1 diachi">{{$user->DiaChi}}</textarea>
                                </div>
                                <div class="col-sm-12 d-block mx-auto">
                                    <button class="btn btn-outline outline-primary d-block mx-auto edit-user" disabled type="submit">Cập nhật</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    <!-- End modal chỉnh sửa thông tin user -->

    <!-- Modal change password -->
    <div class="modal fade" id="changePassword">
        <div class="modal-dialog modal-lg relative">
            <div class="modal-content">
                <i class="uil uil-times-circle fs-3 cusor-pointer me-2 ms-auto w-fit-content" data-bs-dismiss="modal"></i>
                <div class="container-fluid">
                    <form action="" method="post" id="formChangePass">
                        <div class="row flex-column align-items-center modal-changePassword justify-content-center">
                            <h1 class="text-center fw-bold">Thay đổi mật khẩu</h1>
                            <input type="hidden" value="{{Auth::guard('benhnhan')->id()}}" name="id" class="id">
                            <div class="col-sm-6 modal-pass-item mt-3">
                                <label class="fw-bold">Mật khẩu hiện tại</label>
                                <input type="password" name="password" value="" class="form-control mt-1 modal-pass-item">
                            </div>
                            <div class="col-sm-6 mt-3 modal-pass-item">
                                <label class="fw-bold">Mật khẩu mới</label>
                                <input type="password" name="passnew" value="" class="form-control mt-1 modal-pass-item">
                            </div>
                            <div class="col-sm-6 mt-3">
                                <label class="fw-bold">Nhập lại mật khẩu</label>
                                <input type="password" name="passconf" value="" class="form-control mt-1 modal-pass-item">
                            </div>
                            <div class="col-sm-12 mt-0 mb-4">
                                <button class="btn btn-outline outline-primary d-block mx-auto edit-pass" type="submit" name="changePassword" disabled>Thay đổi</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal change password -->
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            checkSubmit();
            getUser();

            $('#file-img').on('change', function() {
                filePreview('img-user', this);
            })

            function getUser() {
                $.ajax({
                    url: "{{ route('api.client.user.getUser', ['id' => $user->id]) }}",
                    type: "get",
                    dateType:"json", 
                    success : function (res){
                        if(res.status == 200) {
                            getInput(res);
                            getInputModal(res);
                        }
                    }
                });
            }

            function getInputModal(res) {
                $('.hoten').val(res.data.HoTen);
                $('.email').val(res.data.email);
                $('.ngaysinh').val(res.data.NgaySinh);
                $('.sdt').val(res.data.SDT);
                $('.diachi').val(res.data.DiaChi);
                $('.bhyt').val(res.data.MaBHYT);
                if(res.data.AnhDaiDien != '') {
                    $('.uil-image-plus').hide();
                    var img = `<img src="{{asset('./uploads/images/${res.data.AnhDaiDien}')}}" class="img-user">`;
                    $('.label-img').append(img);
                    $('.label-img').css('padding', '30px');
                    $('.uil-image-plus').hide();
                }
                else {
                    $('.uil-image-plus').show();
                    $('.img-user').remove();
                }

                if(res.data.GioiTinh == 1) {
                    $('.gt-nam-in').attr('checked', 'checked');
                } else {
                    $('.gt-nu-in').attr('checked', 'checked');
                }
            }

            function getInput(res) {
                ngaysinh = moment(res.data.NgaySinh).format('DD-MM-YYYY')
                $('.HoTen').text(res.data.HoTen);
                $('.email').text(res.data.email);
                $('.NgaySinh').text(ngaysinh);
                $('.SDT').text(res.data.SDT);
                $('.DiaChi').text(res.data.DiaChi);
                $('.bhyt').text(res.data.MaBHYT);
                if(res.data.GioiTinh == 1) {
                    icon = '<i class="uil uil-mars"></i>';
                    gioitinh = 'Nam';
                } else {
                    icon = '<i class="uil uil-venus"></i>';
                    gioitinh = 'Nữ';
                }
                $('.icon-gt').html(icon);
                $('.GioiTinh').text(gioitinh);
            }
            
            function checkSubmit() {
                var form = document.getElementById('formInfoUser');
                form.addEventListener('submit', function (event) {
                    var sdt = $('.sdt').val();
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }
                    if(sdt.length < 10 || sdt.length > 10) {
                        $('#valid-sdt').text('Số điện thoại không đúng!');
                        $('#valid-sdt').css('display', 'block');
                        form.classList.add('was-validated');
                        event.preventDefault()
                        event.stopPropagation()
                    }
                    form.classList.add('was-validated')
                }, false)
            }

            $('#formChangePass').on('submit', function (e) {
                e.preventDefault();
                formData = $(this).serialize();
                $.ajax({
                    url: "{{ route('api.client.user.changePassword') }}",
                    type: "post",
                    dateType:"json", 
                    data: formData,
                    success : function (res){
                        toast_message(res);
                    }
                })
            });
            
            $('#formInfoUser').on('change', function (e) {
                $('.edit-user').removeAttr('disabled');
            });
            
            $('#formChangePass').on('change', function (e) {
                $('.edit-pass').removeAttr('disabled');
            });
            
        });
    </script>
@endpush
