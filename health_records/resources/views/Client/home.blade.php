@extends('layouts.client')

@section('style')
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
@endsection

@section('content')
    @if (session()->has('message'))
        <div id="toast"></div>
        <input type="hidden" id="message" value="{{ session('message') }}">
    @endif
    <section class="banner overflow-sm-hidden">
        <div class="container">
            <div class="row d-flex justify-content-between">
                <div class="content mt-md-5 mt-4 col-md-5" data-aos="fade-right" data-aos-duration="1000">
                    {!! $thongtin->Banner_Content !!}
                    <a href="{{ route('client.datlich') }}" class="btn btn-hover">
                        <i class="fa-regular fa-calendar-check me-2"></i> Đặt hẹn ngay
                    </a>
                </div>
                <div class="banner-img col-md-6 col-sm-12 mb-sm-5 mt-md-5 mt-3" data-aos="fade-left"
                    data-aos-duration="1000">
                    @php $url = './client/assets/images/'.$thongtin->Banner_Image; @endphp
                    <img src="{{ asset($url) }}" class="col-md-12 col-sm-9 col-9 mx-auto" alt="banner-bs" />
                </div>
            </div>
        </div>
    </section>
    <div class="container mt-5">
        <div class="row">
            <section class="reason my-5" data-aos="fade-up"
            data-aos-anchor-placement="top-bottom">
                <div class="reason-title">
                    <h2 class="mb-5 mt-3 mt-sm-5 mt-md-0 text-center">Lý do bạn nên chọn chúng tôi</h2>
                </div>
                <div class="reason-list d-flex justify-content-evenly">
                    <div class="reason-item col-md-3" data-aos="fade-right" data-aos-duration="2000">
                        <img src="{{ asset('./client/assets/images/service-1.png') }}" class="col-md-4 m-auto mb-2"
                            alt="">
                        <div class="reason-content text-center">
                            <h4>Bác sĩ giàu kinh nghiệm</h4>
                            <p>
                                Đội ngũ bác sĩ có bằng cấp, uy tín, kinh nghiệm
                            </p>
                        </div>
                    </div>
                    <div class="reason-item col-md-3" data-aos="fade-up" data-aos-duration="2000">
                        <img src="{{ asset('./client/assets/images/service-2.png') }}" class="col-md-4 m-auto mb-2"
                            alt="">
                        <div class="reason-content text-center">
                            <h4>Chi phí hợp lý</h4>
                            <p>
                                Giá thành hợp lý với thu nhập của khách hàng
                            </p>
                        </div>
                    </div>
                    <div class="reason-item col-md-3" data-aos="fade-left" data-aos-duration="2000">
                        <img src="{{ asset('./client/assets/images/service-3.png') }}" class="col-md-4 m-auto mb-2"
                            alt="">
                        <div class="reason-content text-center">
                            <h4>+5000 khách hàng</h4>
                            <p>
                                Nhiều khách hàng hài lòng về chất lượng
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <h2 class="my-5 text-center">Các chuyên gia của chúng tôi</h2>
            <section class="experts d-flex" data-aos="fade-up"
            data-aos-anchor-placement="top-center">
                <div class="group-arrows"></div>
            </section>
            <section class="news d-none my-5">
                <h3 class="mb-4">Tin tức</h3>
                <div class="list-news m-auto row relative">
                    <img src="{{ asset('./client/assets/images/c.png') }}" class="right-news d-none" data-aos="fade-down-left" />
                </div>
                <div class="news-more d-flex justify-content-center align-items-center">
                    <img class="img-line" src="{{ asset('./client/assets/images/line.svg') }}" fill="red"
                        alt="" />
                    <a href="{{ route('client.tintuc') }}" class="fw-bold">Xem thêm</a>
                    <img class="img-line" src="{{ asset('./client/assets/images/line.svg') }}" fill="red"
                        alt="" />
                </div>
            </section>
            <section class="times mt-5">
                <div class="row justify-content-around">
                    <div class="col-md-4 time-left mb-5" data-aos="fade-right">
                        <div class="time-title d-flex justify-content-center align-items-center mb-4">
                            <i class="fa-solid fa-business-time fs-2"></i>
                            <h4 class="ms-2 m-0">Giờ làm việc</h4>
                        </div>
                        <div class="time-content">
                            <div class="time-content-item d-flex justify-content-between align-items-center">
                                <p class="m-0">Thứ Hai - Thứ Bảy</p>
                                <p class="m-0">7:30 - 17:30</p>
                            </div>
                            <hr />
                            <div class="time-content-item d-flex align-items-center justify-content-between">
                                <p class="m-0">Chủ nhật</p>
                                <p class="m-0">8:00 - 12:00</p>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a href="#" class="btn btn-outline">
                                    <i class="fa-solid fa-calendar-days me-2"></i>Lịch khám
                                    bệnh
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 benefit time-right mb-5" data-aos="fade-left">
                        <div class="benefit-title d-flex align-items-center mb-4">
                            <i class="fa-solid fa-calendar-plus fs-2"></i>
                            <h4 class="ms-2 m-0">
                                Lợi ích của việc đặt hẹn khám
                            </h4>
                        </div>
                        <div class="benefit-content">
                            <p class="mb-2">
                                Bạn cần gặp Bác sĩ khám và tư vấn theo yêu cầu vì có công
                                việc gấp?
                            </p>
                            <p class="mb-2">
                                Chúng tôi đã xây dựng hệ thống đặt hẹn đảm bảo:
                            </p>
                            <ul class="menu">
                                <li class="menu-item mb-1">
                                    <i class="me-2 fa-solid fa-user-doctor"></i>
                                    Khám đúng lịch Bác sĩ theo yêu cầu.
                                </li>
                                <li class="menu-item mb-1">
                                    <i class="me-2 uil uil-phone"></i>
                                    Đội ngũ chăm sóc khách hàng nhắc giờ hẹn.
                                </li>
                                <li class="menu-item mb-1">
                                    <i class="me-2 fa-solid fa-users"></i>
                                    Không phải đợi để đăng ký khám.
                                </li>
                                <li class="menu-item mb-1">
                                    <i class="me-2 fa-regular fa-clock"></i>
                                    Thời gian khám chính xác, tiện lợi.
                                </li>
                            </ul>
                            <div class="d-flex justify-content-center">
                                <a href="{{ route('client.datlich') }}" class="btn btn-outline mt-0">
                                    <i class="fa-regular fa-calendar-check me-2"></i> Đặt hẹn
                                    khám
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="address my-5 row justify-content-center">
                <h3 class="my-5">Thông tin phòng khám</h3>
                <div class="address-info col-md-3 mb-4" data-aos="fade-right">
                    <img src="{{ asset('./client/assets/images/your-location.svg') }}" alt="address" />
                    <div class="address-content mx-auto col-10 col-sm-8 col-md-12">
                        <h4 class="mb-3">Phòng khám tai - mũi - họng</h4>
                        <p class="address">
                            <i class="fa-solid fa-location-dot text-reb"></i>
                            <span class="ms-2">{{ $thongtin->DiaChi }}</span>
                        </p>
                        <p class="phone">
                            <i class="fa-solid fa-phone text-reb"></i>
                            <span class="ms-2">0{{ number_format($thongtin->SDT, 0, ' ', ' ') }}</span>
                        </p>
                        <p class="mail d-flex align-items-center">
                            <i class="fa-solid fa-envelope text-reb"></i>
                            <span class="ms-2 overflow-hidden">{{ $thongtin->Email }}</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-8 address-map mb-4" data-aos="fade-left">
                    <iframe src="{{ $thongtin->DiaChi_Map }}" width="100%" height="450" style="border: 0"
                        allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
                    </iframe>
                </div>
            </section>
            <section class="partner mb-5">
                <div class="partner-title col-md-8 mx-auto text-center" data-aos="fade-up">
                    <h2>Đối tác thương hiệu của chúng tôi</h2>
                    <p>
                        Đội ngũ bác sĩ chuyên gia đầu ngành của phòng khám chúng tôi!
                        Gặp gỡ nhân viên của chúng tôi và thăm chúng tôi cho vấn đề tiếp
                        theo của bạn!
                    </p>
                </div>
                @if ($slide->count() > 0)
                    <div class="partner-list d-flex justify-content-center align-items-center" data-aos="fade-up">
                        @foreach ($slide as $item)
                            <div class="partner-item">
                                <a href="#">
                                    @php $url = './client/assets/images/'.$item->Slide_Image; @endphp
                                    <img src="{{ asset($url) }}" alt="" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </section>
        </div>
    </div>
@endsection

{{-- su dung push de day vao stack. Moi lan push se day vao cuoi stack --}}
@push('scripts')
    <!-- slick slider js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.4.0/jquery-migrate.min.js"
        integrity="sha512-QDsjSX1mStBIAnNXx31dyvw4wVdHjonOwrkaIhpiIlzqGUCdsI62MwQtHpJF+Npy2SmSlGSROoNWQCOFpqbsOg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"
        integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        loadBacSi();
        loadNews();

        function slider() {
            $('.experts').not('.slick-initialized').slick({
                rows: 0,
                slidesToShow: 4,
                slidesToScroll: 1,
                slide: '.expert-item',
                arrows: true,
                infinite: true,
                autoplay: true,
                prevArrow: '<button type="button" class="slick-prev arrows fw-bold" aria-label="prev"><i class="fa-solid fa-angle-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next arrows fw-bold" aria-label="next"><i class="fa-solid fa-angle-right"></i></button>',
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            infinite: true
                        }
                    },
                    {
                        breakpoint: 990,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 798,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ],
            });
        }

        function loadBacSi() {
            $.ajax({
                url: "{{ route('api.client.bacsi.getAllBacSi') }}",
                type: "get",
                dateType: "json",
                success: function(res) {
                    if (res.status == '200') {
                        var list_bacsi = res.data;
                        _html = '';
                        i = 1;
                        list_bacsi.forEach(function(item) {
                            _html += `
                                <div class="expert-item mb-4 card align-items-center">
                                    <a href="/client/datlich/${item.id}">
                                        <img src="{{ asset('./client/assets/images/${item.AnhDaiDien}') }}" alt="bs-top" class="card-img-top mx-auto" />
                                    </a>
                                    <div class="card-body text-center">
                                        <a href="/client/datlich/${item.id}">
                                            <h5 class="card-title">${item.HoTen}</h5>
                                        </a>
                                        <p class="card-subtitle mb-3">${item.TenChucVu} ${item.TenChuyenMon}</p>
                                        <div>
                                            <a href="/client/datlich/${item.id}" class="btn btn-outline outline-primary my-auto mb-3">
                                                <i class="fa-solid fa-calendar-check me-2"></i> Đặt lịch khám
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            `;
                        });
                        $('.experts').append(_html);
                        slider()
                    }
                }
            });
        }

        function loadNews() {
            var _new = '';
            $.ajax({
                url: "{{ route('api.client.baiviet.getThreeNews') }}",
                dataType: 'json',
                type: 'GET',
                success: function(res) {
                    if (res.status == 200) {
                        $.each(res.data, function(key, item) {
                            _new += `
                                <div class="new-item card mb-5" data-aos="fade-up">
                                    <a href="/client/tintuc/detail_bv/${item.MaBV}">
                                        <img class="card-img-top" src="{{ asset('./uploads/images/${item.Thumnail}') }}" alt="" />
                                    </a>
                                    <div class="card-body mb-2">
                                        <a href="/client/tintuc/detail_bv/${item.MaBV}">
                                            <h4 class="card-title">${item.TieuDe}</h4>
                                        </a>
                                        <p class="card-subtitle mb-3">${item.TomTat}</p>
                                        <a href="/client/tintuc/detail_bv/${item.MaBV}" class="card-link fw-bold">Xem chi tiết</a>
                                    </div>
                                </div>
                            `;
                        });
                        $('.list-news').append(_new);
                        if (res.data.length >= 3) {
                            $('.right-news').removeClass('d-none');
                        }
                        if (res.data.length > 0) {
                            $('.news').removeClass('d-none');
                        }
                    }
                }
            });
        }
    </script>
    <!-- js -->
    <script src="{{ asset('./client/assets/js/slider.js') }}"></script>
@endpush

@push('scripts')
    <script>
        $(document).ready(function() {
            var message = $('#message').val();
            if (message != '') {
                var res = {
                    'status': 200,
                    'message': message,
                    'alert': 'success'
                }
                toast_message(res);
            }
        });
    </script>
@endpush
