@extends('layouts.client')

@section('content')
    <div id="toast"></div>
    <div class="container">
        <div class="row">
            <section class="doctors my-4" data-aos="fade-down" data-aos-duration="1500">
                <div class="doctor-title">
                    <h1 class="my-5 text-center">Các chuyên gia của chúng tôi</h1>
                </div>
                <div class="search-experts my-3">
                    <h3>Tìm kiếm</h3>
                    <form action="" id="formSearch">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 col-6">
                                <label for="search_bs" class="form-label mb-2">Tên bác sĩ</label>
                                <input name="hoten" id="search_bs" type="text" class="form-control"
                                    placeholder="Search...">
                            </div>
                        </div>
                    </form>
                </div>
                <section class="experts d-flex flex-wrap"></section>
            </section>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        loadBacSi();

        function loadBacSi() {
            $.ajax({
                url: "{{ route('api.client.bacsi.getInfoBacSi') }}",
                type: "get",
                success: function(res) {
                    $('.experts').html(res);
                }
            });
        }

        page = 1;

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });
       
        searchBacSi(page);
        
        function searchBacSi(page) {
            $('#formSearch').on('keyup', function() {
                searchAjax(page);
                var hoten = $('#search_bs').val();
                if(hoten == "") {
                    loadBacSi();
                }
            });
        }

        function searchAjax(page) {
            var data = $('#formSearch').serialize();
            $.ajax({
                url: "/api/client/bacsi/searchBacSi?page=" + page,
                type: 'post',
                data: data,
                success: function(res) {
                    $('.experts').html(res);
                }
            });
        }
    </script>
@endpush
