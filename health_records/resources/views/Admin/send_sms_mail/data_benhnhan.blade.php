<div class="overflow-scroll pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>
                    <div class="form-check">
                        <input class="form-check-input" id="check_all" type="checkbox" onclick="checkedAll(this)">
                        <label class="form-check-label" for="check_all"></label>
                    </div>
                </th>
                <th>Mã phiếu hẹn</th>
                <th>Họ tên bệnh nhân</th>
                <th>Tuổi</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
                <th>Ngày hẹn</th>
                <th>Giờ hẹn</th>
                <th>Họ tên bác sĩ</th>
            </tr>
        </thead>
        <tbody class="tbody_hoso">
            @if ($hoso->count() > 0)
                @foreach ($hoso as $key => $item)
                    <tr>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input" name="id_phieuhen[]" type="checkbox" value="{{$item->MaPhieuHen}}" id="{{$item->MaPhieuHen}}">
                                <label class="form-check-label" for="{{$item->MaPhieuHen}}"></label>
                            </div>
                        </td>
                        <td> {{ $item->MaPhieuHen }} </td>
                        <td>{{ $item->HoTen_NK }}</td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $datetime = $date->toDateString();
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NamSinh_NK)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>{{ $item->GioiTinh_NK == 1 ? 'Nam' : 'Nữ' }}</td>
                        <td>0{{ number_format($item->SDT, 0, ' ', ' ') }}</td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->NgayHen)->format('d-m-Y') }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->GioHen)->format('H:i A') }}
                        </td>
                        <td>{{ $item->BacSi }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $hoso->links() }}
