@if (!empty(session()->get('chucvu')))
    @if (session()->get('chucvu') == 'Admin')
        @php $disabled = ''; @endphp
    @else
        @php $disabled = 'disabled-link'; @endphp
    @endif
@endif
<div class="overflow-scroll pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th></th>
                <th>STT</th>
                <th>Mã nhân viên</th>
                <th>Họ tên nhân viên</th>
                <th>Tuổi</th>
                <th>Số điện thoại</th>
                <th>Giới tính</th>
                <th>Chức vụ</th>
                <th>Chuyên môn</th>
            </tr>
        </thead>
        <tbody class="tbody_hoso">
            @if ($nhanvien->count() > 0)
                @foreach ($nhanvien as $key => $item)
                    <tr>
                        <td>
                            <div class="form-check">
                                <input class="form-check-input" name="id_nv[]" type="checkbox" value="{{$item->id}}" id="{{$item->id}}">
                                <label class="form-check-label" for="{{$item->id}}"></label>
                            </div>
                        </td>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->HoTen }}</td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $datetime = $date->toDateString();
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NgaySinh)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>0{{ number_format($item->SDT, 0, ' ', ' ') }}</td>
                        <td>{{ $item->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</td>
                        <td>{{ $item->TenChucVu }}</td>
                        <td>{{ $item->TenChuyenMon }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $nhanvien->links() }}
