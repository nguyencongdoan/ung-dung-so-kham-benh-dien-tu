@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <div class="row">
            <h1 class="fw-bold text-center mb-3 fs-1">Gửi thông báo</h1>
            <div class="booking mx-auto py-4 col-md-10 col-12">
                <form id="formNotify" novalidate>
                    @csrf
                    <div class="form-group col-md-8 col-12 mx-auto my-4">
                        <label for="title" class="form-label">Tiêu đề</label>
                        <input type="text" class="form-control" name="title" id="title" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập tiêu đề
                        </div>
                    </div>
                    <div class="form-group col-md-8 col-12 mx-auto my-4">
                        <label for="content" class="form-label">Nội dung</label>
                        <textarea type="text" class="form-control" rows="5" name="content" id="content" required></textarea>
                        <div class="invalid-feedback">
                            Vui lòng nhập nội dung
                        </div>
                    </div>
                    <div class="col-md-8 col-12 mx-auto d-flex flex-wrap align-items-center pl-0">
                        <div class="form-check">
                            <label for="" class="form-label">Gửi SMS</label>
                            <input class="form-check-input" name="type_send[]" type="checkbox" id="send_sms" value="send_sms">
                            <label class="form-check-label" for="send_sms"></label>
                        </div>
                        <div class="form-check">
                            <label for="" class="form-label">Gửi Mail</label>
                            <input class="form-check-input" name="type_send[]" type="checkbox" id="send_mail" value="send_mail">
                            <label class="form-check-label" for="send_mail"></label>
                        </div>
                        <div class="invalid-feedback mx-3 type_send">
                            Vui lòng chọn hình thức thông báo
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline outline-primary my-4 mx-auto d-block">
                        <i class="fa-solid fa-paper-plane me-2"></i> Gửi thông báo
                    </button>
                </form>
            </div>
            <div class="mx-auto my-5 py-4 col-md-10 booking col-12">
                <div class="col-12 mx-4">
                    <div class="d-flex align-items-center my-3">
                        <img src="{{ asset('./admin/assets/images/filter.png')}}" class="me-2">
                        <p class="fw-bold fs-5 text-reb">Lọc theo phiếu hẹn</p>
                    </div>
                    <form action="" id="formSearch">
                        @csrf
                        <div class="d-flex flex-wrap">
                            <div class="mb-3 col-6 col-md-4 pl-0">
                                <label for="ngayhen" class="form-label-control fw-bold mb-2">Ngày hẹn</label>
                                <input class="form-control" type="date" name="ngayhen">
                            </div>
                            <div class="mb-3 col-6 col-md-4 pl-0">
                                <label for="ngayhen" class="form-label-control fw-bold mb-2">Giờ hẹn</label>
                                <select name="giohen" id="" class="gio form-select">
                                    <option value="">- Chọn -</option>
                                    @php
                                        $time= '07:00:00';
                                    @endphp
                                    @for($i = 0; $i < 21; $i++)
                                        @php
                                            $time = \Carbon\Carbon::parse($time)->addMinutes(30);
                                            $time_format = $time->format('H:i');
                                        @endphp
                                        @if($time_format == '12:00' || $time_format == '12:30' || $time_format > '16:30')
                                            @continue
                                        @else
                                            <option value="{{$time_format}}">{{$time_format}}</option>
                                        @endif
                                    @endfor
                                </select>
                            </div>
                            <div class="mb-3 col-6 col-md-4 pl-0">
                                <label for="bacsi" class="form-label mb-2 fw-bold">Bác sĩ</label>
                                <select name="bacsi" id="bacsi" class="form-select">
                                    <option value="">- Chọn -</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="data_benhnhan"></div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function checkedAll(myCheckBox) {
            var checkboxs = document.querySelectorAll('input[name="id_phieuhen[]"]');
            if(myCheckBox.checked == true) {
                checkboxs.forEach(function(checkbox) {
                    checkbox.checked = true;
                });
            }
            else {
                checkboxs.forEach(function(checkbox) {
                    checkbox.checked = false;
                });
            }
        }
        
        load_ho_so_benhnhan();
        function load_ho_so_benhnhan() {
            $.ajax({
                url: "{{route('admin.notify.hoso_benhnhan')}}",
                type: "GET",
                success: function (res) {
                    $('.data_benhnhan').html(res);
                }
            });
        }
        
        load_BacSi();
        function load_BacSi() {
            $.ajax({
                url: "{{route('api.client.bacsi.getAllBacSi')}}",
                type: "GET",
                dataType: "json",
                success: function (res) {
                    var _opt = '';
                    $.each(res.data, function (index, item) {
                        _opt += `
                            <option value="${item.id}">${item.HoTen}</option>
                        `;
                    })
                    $('#bacsi').append(_opt);
                }
            });
        }

        send_Notification();
        function send_Notification() {
            $('#formNotify').on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                console.log('1')
                var form = document.getElementById('formNotify');

                if(form.checkValidity()) {
                    var checkboxed = $('input[name="id_phieuhen[]"]:checked');
                    var id_phieuhen = [];
                    var data = $(this).serializeArray();
                    $.each(checkboxed, function (index, item) {
                        id_phieuhen.push(item.value);
                        data.push({name: "id_phieuhen[]", value: item.value});
                    });
    
                    if(id_phieuhen.length > 0) {
                        form.classList.remove('was-validated');
                        console.log(1);
                        $.ajax({
                            url: "{{route('admin.notify.handle_notify')}}",
                            type: "POST",
                            dataType: "json",
                            data: data,
                            success: function (res) {
                                console.log("🚀 ~ res", res)
                                toast_message(res);
                            }
                        });
                    }
                    else {
                        var res = {
                            status: 400,
                            message: "Vui lòng chọn người nhận",
                            alert: 'error'
                        }
                        form.classList.add('was-validated');
                        toast_message(res);
                    }
                }
                else {
                    if($('input[name="type_send[]"]').is(':checked') == false) {
                        form.classList.add('was-validated');
                        $('.type_send').show();
                    }
                    else {
                        form.classList.remove('was-validated');
                        $('.type_send').hide();
                    }
                    form.classList.add('was-validated');
                }
            });
        }

        page = 1;
        page1 = 1;

        searchForm();
        function searchForm() {
            $("#formSearch").on('change', function() {
                var check = check_form();
                if(check == false) {
                    searchAjax(page);
                }
            });
        }

        function searchAjax(page) {
            var formData = $('#formSearch').serialize();
            $.ajax({
                url: "/admin/notify/searchHoSo_BN?page=" + page,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.data_benhnhan').html(res);
                }
            });
        }

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });

        function check_form() {
            var ngayhen = $("input[name='ngayhen']").val();
            var giohen = $("select[name='giohen']").val();
            var bacsi = $("select[name='bacsi']").val();

            if (ngayhen == '' && giohen == '' && bacsi == '') {
                load_ho_so_benhnhan();
                return true;
            }
            return false;
        }
    </script>
@endpush