@extends('layouts.admin')

@section('content') 
    <div id="toast"></div>
    <div class="container">
        <div class="row">
            <h1 class="text-center my-5 fw-bold" data-aos="fade-down">Chỉnh sửa phiếu hẹn</h1>
            <form action="" novalidate method="post" class="col-md-6 m-auto mb-5 form-booking" id="form-booking" data-aos="fade-up">
                @csrf
                <div class="row booking m-auto">
                    <input type="hidden" class="id_phieuhen" name="id_phieuhen" value="">
                    <div class="col-6 my-2">
                        <label for="hoten" class="form-label">Họ tên người đăng ký</label>
                        <input type="text" id="HoTen" class="form-control" name="HoTen" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập họ tên người đăng ký
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" id="email" class="form-control" name="email" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập lại email
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="sdt" class="form-label">Số điện thoại</label>
                        <input type="number" min="10" id="SDT" class="form-control" name="SDT" required>
                        <div id="valid-sdt" class="invalid-feedback">
                            Vui lòng nhập số điện thoại
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="hoten" class="form-label">Họ tên người khám</label>
                        <input type="text" id="HoTen_NK" class="form-control" name="HoTen_NK" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập họ tên người khám
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="ngaysinh" class="form-label">Ngày sinh</label>
                        <input type="date" id="NgaySinh" class="form-control" name="NgaySinh" required>
                        <div class="invalid-feedback">
                            Vui lòng chọn ngày sinh
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="" class="form-label">Giới tính</label>
                        <div class="form-check">
                            <input type="radio" name="GioiTinh" id="gt_nam" class="form-check-input" checked value="1">
                            <label for="gt_nam" class="form-check-label">Nam</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" name="GioiTinh" id="gt_nu" class="form-check-input" value="0">
                            <label for="gt_nu" class="form-check-label">Nữ</label>
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="hoten" class="form-label">Địa chỉ</label>
                        <textarea name="DiaChi" id="DiaChi" cols="5" rows="4" class="form-control"></textarea>
                    </div>
                    <div class="col-6 my-2">
                        <label for="ngaysinh" class="form-label">Chọn bác sĩ</label>
                        <select name="bacsi" id="bacsi" class="form-select select-bs" required>
                            <option value="">- Chọn -</option>
                        </select>
                        <div class="invalid-feedback">
                            Vui lòng chọn bác sĩ
                        </div>
                    </div>
                    <div class="col-6 my-2 relative parent-datepicker">
                        <label for="ngaysinh" class="form-label">Chọn ngày khám</label>
                        <input type='text' id='datepicker' name="NgayKham" placeholder="dd-mm-yyyy" class="form-control"
                        required />
                        <i class="fa-solid fa-calendar-days calendar-icon"></i>
                        <div class="invalid-feedback">
                            Vui lòng chọn ngày khám
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="ngaysinh" class="form-label">Chọn giờ khám</label>
                        <select name="gio" class="gio form-select" required>
                            <option value="">- Chọn -</option>
                        </select>
                        <div class="invalid-feedback">
                            Vui lòng chọn giờ khám
                        </div>
                    </div>
                    <div class="col-md-12 my-3">
                        <button type="submit" class="btn btn-outline outline-primary m-auto d-block">
                            <i class="fa-solid fa-calendar-check me-2"></i> Chỉnh sửa
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            getDate();
            getUser();
            getBacSi();
            edit_phieuhen();
            var SelectedDates = {};
            var arr_day = [];

            function getDate() {
                var date = moment().format("YYYY-MM-DD")
                $('#ngaykham').attr('min', date);
                var giokham = moment("{{$phieuhen->GioHen}}").format("HH:mm");
                var _opt = `
                    <option value="${giokham}" selected>${giokham}</option>
                `;
                $('.gio').append(_opt);

                $('.id_phieuhen').val("{{$phieuhen->MaPhieuHen}}");
            }

            function getInput(res) {
                var hoten = $('#HoTen');
                var hoten_nk = $('#HoTen_NK');
                var email = $('#email');
                var DiaChi = $('#DiaChi');
                var SDT = $('#SDT');
                var NgaySinh = $('#NgaySinh');
                var GioiTinh = $('#GioiTinh');

                hoten.val(res.data.HoTen);
                hoten_nk.val(res.data.HoTen);
                hoten.attr('disabled', 'disabled');

                email.val(res.data.email);
                email.attr('disabled', 'disabled');

                DiaChi.val(res.data.DiaChi);
                DiaChi.attr('disabled', 'disabled');

                SDT.val(res.data.SDT);
                SDT.attr('disabled', 'disabled');
                
                NgaySinh.val(res.data.NgaySinh);
                NgaySinh.attr('disabled', 'disabled');
                
                if(res.data.GioiTinh == 1) {
                    $('#gt_nam').attr('checked', 'checked');
                } else {
                    $('#gt_nu').attr('checked', 'checked');
                }
            }

            function getUser() {
                $.ajax({
                    url: "{{ route('api.client.user.getUser', Auth::guard('benhnhan')->id()) }}",
                    type: "get",
                    dataType: "json",
                    success : function (res){
                        if(res.status == 200) {
                            getInput(res);
                        }
                    }
                });
            }

            function getBacSi() {
                var id = {{!empty($phieuhen->MaNV) ? $phieuhen->MaNV : ''}}
                
                $.ajax({
                    url: "{{ route('api.client.bacsi.getAllBacSi') }}",
                    type: "get",
                    dataType: "json",
                    success: function (res) {
                        if(res.status == 200) {
                            _list = res.data;
                            _html = '';
                            _list.forEach(function(item) {
                                if(item.id == id) {
                                    _html += `<option value="${item.id}" selected>BS. ${item.HoTen}</option>`;
                                }
                                else {
                                    _html += `<option value="${item.id}">BS. ${item.HoTen}</option>`;
                                }
                            });
                            $('.select-bs').append(_html);
                            check_bs();
                        }
                    }
                });
            }

            function edit_phieuhen() {
                var form = document.getElementById('form-booking');
                $('#form-booking').on('submit', function(e) {
                    e.preventDefault();
                    e.stopPropagation(); 
                    var sdt = 0;
                    var formData = $(this).serialize();

                    if(form.checkValidity()) {
                        var sdt = $('#SDT').val();
                        if(sdt.length < 10 || sdt.length > 10) {
                            $('#valid-sdt').text('Số điện thoại không đúng!');
                            $('#valid-sdt').css('display', 'block');
                            form.classList.add('was-validated');
                        }
                        else {
                            form.classList.remove('was-validated');
                            $('#valid-sdt').css('display', 'none');
                            $.ajax({
                                url: "{{ route('api.client.edit_phieuhen') }}",
                                type: "post",
                                dataType: "json",
                                data: formData,
                                success: function (res) {
                                    toast_message(res);
                                }
                            });
                        }
                    }
                    else {
                        form.classList.add('was-validated');
                    }
                });
            }

            $('#bacsi').on('change', function() {
                HighlightTime();
            });

            function check_bs() {
                var bs = $('#bacsi').val();
                if(bs) {
                    HighlightTime();
                }
            }
            
            function HighlightTime() {
                var bs = $('#bacsi').val();
                var date_now = moment().format('YYYY/MM/DD');
                $('.gio').html('<option value="">- Chọn -</option>');

                $.ajax({
                    url: "{{ route('api.client.datlich.getDateBS') }}",
                    type: 'GET',
                    data: {
                        id_bs: bs
                    },
                    dataType: 'json',
                    success: function(res) {
                        if (res.status == 200) {
                            // trước khi thay đổi lịch phải remove input $datepicker cũ và add lại
                            // mới thay đổi được ngày
                            $('#datepicker').remove();
                            _input = ` 
                                <input type='text' id='datepicker' name="NgayKham" placeholder="dd-mm-yyyy" class="form-control" required/>
                            `;
                            $('.parent-datepicker').append(_input);
                            var SelectedDates = {};
                            var date = '';
                            arr_day = [];

                            $.each(res.date, function(key, item) {
                                // đếm số ngày từ ngày bắt đầu đến ngày kết thúc
                                var count_day = moment(item.ThoiGianBD).from(moment(
                                    item.ThoiGianKT));

                                // kiểm tra nếu thời gian từ ngày bd -> ngày kt là 1 ngày thì cho count_day = 1
                                if (count_day.indexOf("a day ago") != -1 ||
                                    count_day.indexOf("hours") != -1 || count_day
                                    .indexOf("minutes") != -1 || count_day.indexOf(
                                        "seconds") != -1) {
                                    count_day = 1;
                                } else if (count_day.indexOf("month") != -1 || 
                                    count_day.indexOf("months") != -1) { // nếu là tháng thì số tháng * 30
                                    if(count_day.indexOf("a month ago") != -1) {
                                        count_day = 30;
                                    }
                                    else {
                                        count_day = count_day.match(/\d/g); // xóa ký tự '4 day ago' => 4
                                        count_day = count_day * 30;
                                    }
                                } else if (count_day.indexOf("year") != -1 ||
                                    count_day.indexOf("years") != -1) { // nếu là tháng thì số năm * 365
                                    if(count_day.indexOf("a year ago") != -1) {
                                        count_day = 365;
                                    }
                                    else {
                                        count_day = count_day.match(/\d/g); // xóa ký tự '4 day ago' => 4
                                        count_day = count_day * 365; // chưa kiểm tra năm nhuận / không nhuận
                                    }
                                } else { //ngược lại thì xóa ký tự và lấy số 
                                    count_day = count_day.match(/\d/g); // xóa ký tự '4 day ago' => 4
                                }

                                if (count_day.length > 1) {
                                    count_day = count_day.join("");
                                }

                                var add_day = moment(item.ThoiGianBD);

                                // cộng ngày từ ngày bắt đầu -> ngày kết thúc vào mảng
                                for (var i = 0; i < count_day; i++) {
                                    if (moment(add_day).format('YYYY/MM/DD') >=
                                        date_now) {
                                        arr_day.push(add_day);
                                    }
                                    add_day = moment(add_day).add(1, 'days');
                                }
                            });

                            // thêm ngày vào danh sách SelectedDates
                            $.each(arr_day, function(key, item) {
                                date = moment(item).format('YYYY/MM/DD');
                                SelectedDates[new Date(date)] = new Date(date);
                            });

                            $('#datepicker').datepicker({
                                showAnim: 'slide',
                                changeMonth: true,
                                changeYear: true,
                                beforeShowDay: function(date) {
                                    var Highlight = SelectedDates[date];
                                    if (Highlight) {
                                        return [true, "Highlighted", Highlight];
                                    } else {
                                        return [false, '', ''];
                                    }
                                }
                            });
                            getTimeBS();
                        } else {
                            $('#datepicker').attr('disabled', 'disabled');
                            $('#datepicker').val('');
                            toast_message(res);
                        }
                    }
                });
            }

            function getTimeBS() {
                if ($('#datepicker').val() == '') {
                    $('.gio').html('<option value="">- Chọn -</option>');
                }

                $('#datepicker').on('change', function() {
                    var date = $(this).val();
                    var id_bs = $('#bacsi').val();

                    $.ajax({
                        url: "{{ route('api.client.datlich.getTimeBS') }}",
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            id_bs: id_bs,
                            date: date
                        },
                        success: function(res) {
                            if (res.status == 200) {
                                var _opt = '';
                                var time_now = moment().format('HH:mm');
                                time_start = moment(res.time_start).format('HH:mm');
                                time_end = moment(res.time_end).format('HH:mm');

                                day_start = moment(res.time_start).format('YYYY-MM-DD');
                                day_end = moment(res.time_end).format('YYYY-MM-DD');
                                var day_selected = moment(date).format('YYYY-MM-DD');
                                var day_now = moment().format('YYYY-MM-DD');

                                for (var i = 0; i < 48; i++) {
                                    // kiểm tra nếu thời gian bắt đầu nằm trong khoảng 07:30 -> 11:30 || 13:30 -> 16:30
                                    // thì thêm vào thẻ option
                                    if (time_start > '07:00' && time_start < '12:00' ||
                                        time_start > '13:00' && time_start < '17:00') {
                                        // kiểm tra nếu ngày chọn = ngày hiện tại thì lấy thời gian >= giờ hiện tại
                                        if (day_selected == day_now) {
                                            if (time_start >= time_now) {
                                                _opt += `
                                                    <option value="${time_start}">${time_start}</option>
                                                `;
                                            }
                                        } else {
                                            // nếu giờ phù hợp điều kiện nhưng lại gặp vấn đề giờ kết thúc < giờ bắt đầu
                                            // time_start = 07:30 nhưng time_end = 00:00 vì nếu cùng trong ngày thì time_end luôn > time_start nên trường hợp ở đây là ngày bắt đầu < ngày kết thúc
                                            if (time_start <= time_end || day_start < day_end) {
                                                _opt += `
                                                    <option value="${time_start}">${time_start}</option>
                                                `;
                                            }
                                        }
                                    }
                                    time_start = day_start + ' ' + time_start + ':00';

                                    time_start = moment(time_start).add(30, 'minutes');
                                    time_start = moment(time_start).format('HH:mm');
                                }
                                if (_opt == '') {
                                    _opt += `
                                        <option value="">Hết giờ làm việc</option>
                                    `;
                                }
                                $('.gio').html(_opt);
                            }
                        }
                    })
                });
            }
        });
    </script>
@endpush