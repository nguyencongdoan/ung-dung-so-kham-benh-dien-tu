@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <section class="ds_thuoc">
            <div class="col-12 mx-auto">
                <nav aria-label="breadcrumb" data-aos="fade-right">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Danh sách phiếu nhập thuốc</li>
                    </ol>
                </nav>
                <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách phiếu nhập thuốc</h1>
                <div class="mb-3" data-aos="fade-right">
                    <a href="{{ route('admin.phieunhapthuoc.create') }}" class="btn btn-outline outline-primary d-block w-fit mt-2 mb-3 mx-3">
                        <i class="uil uil-plus"></i> Thêm phiếu nhập
                    </a>
                </div>
                <div class="container-fluid mb-3" data-aos="fade-right">
                    <form action="" method="post" id="formSearch">
                        <div class="row align-items-center">
                            <div class="col-6 col-sm-4 col-md-3">
                                <label for="start_day" class="form-label-control fw-bold mb-2">Ngày nhập (start)</label>
                                <input class="form-control" placeholder="Search Here..." type="date" name="start_day">
                            </div>
                            <div class="col-6 col-sm-4 col-md-3">
                                <label for="end_day" class="form-label-control fw-bold mb-2">Ngày nhập (end)</label>
                                <input class="form-control" placeholder="Search Here..." type="date" name="end_day">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="data_phieunhapthuoc mb-5" data-aos="fade-up"></div>
            </div>             
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        getAllPhieuNhap();

        function remove_thuoc(e) {
           var check = confirm("Bạn có chắc muốn xóa thuốc này không?");
           if(check) {
                $.ajax({
                    url: "{{route('api.admin.phieunhapthuoc.delete')}}",
                    type: 'post',
                    dataType: 'json',
                    data: {id: e.value},
                    success: function (res) {
                        toast_message(res);
                        getAllPhieuNhap();
                    }
                })
           }
        }

        function getAllPhieuNhap() {
            $.ajax({
                url: "{{route('api.admin.phieunhapthuoc.getAllPhieuNhap')}}",
                type: "GET",
                success: function (res) {
                    $('.data_phieunhapthuoc').html(res);
                }
            })
        }

        page = 1;

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });

        search();
        function search(){
            $('#formSearch').on('change', function() {
                var start_day = $('input[name="start_day"]').val();
                var end_day = $('input[name="end_day"]').val();

                if(start_day >= end_day && end_day != '' && start_day != '') {
                    var res = {
                        status: 400,
                        message: 'Ngày nhập (start) không được >= ngày nhập (end)!',
                        alert: 'error'
                    }
                    toast_message(res);
                }
                else {
                    searchAjax(page);
                }
            });
        }

        function searchAjax(page) {
            var data = $('#formSearch').serialize();
            $.ajax({
                url: "/api/admin/phieunhapthuoc/search?page=" + page,
                type: "post",
                data: data,
                success: function (res) {
                    $('.data_phieunhapthuoc').html(res);
                }
            });
        }
    </script>
@endpush