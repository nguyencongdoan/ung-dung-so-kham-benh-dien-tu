@extends('layouts.admin')

@section('content') 
    <div id="toast"></div>
    <div class="container-fluid">
        <a href="{{route('admin.phieunhapthuoc')}}" class="ms-5 btn btn-outline outline-primary d-block col-md-2 col-sm-3 col-5 mb-3" data-aos="fade-right">
            <i class="uil uil-corner-up-left-alt"></i> Quay lại
        </a>
        <div class="row">
            <div class="col-12 mx-auto">
                <h1 class="fw-bold fs-1 text-center my-5" data-aos="fade-down">Thêm phiếu nhập thuốc</h1>
                <form action="" method="post" class="mb-5 form-booking" id="form-booking" novalidate data-aos="fade-up">
                    @csrf
                    <input type="hidden" name="id_admin" value="{{Auth::guard('admin')->id()}}">
                    <div class="search-ajax mb-3">
                        <div class="search-box d-flex relative">
                            <input class="search-input search" placeholder="Search Here..." type="search" id="search">
                            <button class="search-form" type="submit" value="">
                                <span class="fa fa-search"></span>
                            </button>
                        </div>
                    </div>
                    <div class="overflow-scroll pr-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Mã thuốc</th>
                                    <th>Tên thuốc</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Đơn vị</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="tbody">
                                <tr class="tr_search">
                                    <td colspan="6" class="text-center">
                                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn btn-outline outline-primary m-auto btn_create">
                        <i class="uil uil-plus me-1"></i> Thêm phiếu nhập thuốc</>
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('./admin/assets/js/count_number.js')}}"></script>
    <script>
        create_PhieuNhapThuoc();

        function create_PhieuNhapThuoc() {
            var form = document.getElementById('form-booking');
            $('#form-booking').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation(); 

                var soluong = document.querySelectorAll('input[name="soluong[]"]');
                if(form.checkValidity()) {
                    var check_sl = 0;
                    for(var i = 0; i < soluong.length; i++) {
                        var max_sl = soluong[i].getAttribute('max');
                        if(Number(soluong[i].value) < 1) {
                            alert('Số lượng không được < 1!');
                            check_sl = 0;
                            break;
                        }
                        else if(Number(soluong[i].value) > Number(max_sl)) {
                            alert('Số lượng không được > ' + max_sl);
                            check_sl = 0;
                            break;
                        }
                        else {
                            check_sl = 1;
                        }
                    };
                    if(check_sl == 1) {
                        form.classList.remove('was-validated');
                        var formData = $('#form-booking').serialize();
                        $.ajax({
                            url: "{{ route('api.admin.phieunhapthuoc.handle_create') }}",
                            type: "post",
                            dataType: "json",
                            data: formData,
                            success: function (res) {
                                toast_message(res);
                            }
                        });
                    }
                }
                else {
                    form.classList.add('was-validated');
                }
            });
        }

        $(document).ready(function() {
            seachThuoc();
            check_tr_search();
            $('.btn_create').hide();

            function check_tr_search() {
                check_tr_search = 1;

                $('.search').on('keydown', function() {
                    if($('.tr_search').length) {
                        // đã có tr_search nên đổi check sang 0
                        check_tr_search = 0;
                    }
                    else {
                        // chưa có tr_search nên đổi check sang 1
                        check_tr_search = 1;
                    }
                });
            }

            function removeNode() {
                $('.icon_remove').on('click', function(e) {
                    var id = e.target.nextElementSibling;
                    id = id.value;
                    var arr_new = arr_key;
                    vitri = -1;
                    for (var i = 0; i < arr_new.length; i++) {
                        if (arr_new[i] == id) {
                            vitri = i;
                            arr_new.splice(vitri, 1);
                            arr_key = arr_new;
                            break;
                        }
                    }
                    var td = e.target.parentElement;
                    var tr = td.parentElement;
                    tr.remove();
                    var class_thuoc = '.thuoc-' + id;
                    $(class_thuoc).remove();
                    if(arr_key.length == 0) {
                        _tr = `
                            <tr class="tr_search">
                                <td colspan="6" class="text-center">
                                    <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                </td>
                            </tr>
                        `;
                        $('.tbody').html(_tr);
                        $('.btn_create').hide();
                    }
                });
            }

            function addTr(val, arr_key) {
                var dongia = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'VND' }).format(val.DonGia);
                _tr += `
                    <tr>
                        <td class="text-center">${val.MaThuoc}</td>
                        <td>${val.TenThuoc}</td>
                        <td>
                            <div class="col-12 main-item justify-content-center">
                                <i class="uil uil-minus bx-minus ${val.MaThuoc}" onclick="minus_click(this)"></i>
                                <input type="text" class="form-control mx-2 bx-number" min="1" max="${val.SoLuong}" name="soluong[]" value="${val.SoLuong}" required>
                                <i class="uil uil-plus bx-plus ${val.MaThuoc}" onclick="plus_click(this)"></i>
                            </div>
                        </td>
                        <td>
                            <p class="fw-bold text-danger">${dongia}</p>    
                        </td>
                        <td>
                            <select name="donvi[]" class="form-select donvi" required>
                                <option value="">- Chọn -</option>
                                @foreach($donvi as $item) 
                                    <option value="{{ $item->MaDonVi }}">{{ $item->TenDonVi }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <i class="uil uil-trash-alt fs-4 icon_remove"></i>
                            <input type="hidden" value="${val.MaThuoc}" />
                            <div class="d-none">
                                <input type="hidden" name="mathuoc[]" value="${val.MaThuoc}" />
                                <input type="hidden" name="tenthuoc[]" value="${val.TenThuoc}" />
                                <input type="hidden" name="dongia[]" value="${val.DonGia}" />
                            </div>
                        </td>
                    </tr>
                `;
            }

            function seachThuoc() {
                arr_key = [];
                $('.search').on('keyup', function() {
                    key = $(this).val();
                    if(key != '') {
                        $.ajax({
                            url: "{{ route('api.admin.search.Thuoc') }}",
                            type: "POST",
                            dataType: "json",
                            data: {key: key},
                            success: function (res) {
                                if(res.status == 200) {
                                    // console.log(res.data);
                                    $('.tr_search').remove();
                                    _tr = '';
                                    $.each(res.data, function (key, val) {
                                        // Kiểm tra arr_key có phần tử nào chưa nếu chưa sẽ push mã thuốc vào arr_key và append tr vào tbody
                                        if(arr_key.length == 0) {
                                            arr_key.push(val.MaThuoc);
                                            addTr(val, arr_key);
                                        } // ngược lại sẽ kiểm tra xem val trả về đã từng được thêm trước đó chưa nếu rồi sẽ trả về true và ngược lại là false
                                        else {
                                            var doneTasks = arr_key.includes(val.MaThuoc);
                                            // trường hợp không tìm thấy tức là chưa từng được thêm thì sẽ push mã thuốc vào arr_key và append tr vào tbody
                                            if(!doneTasks) {
                                                arr_key.push(val.MaThuoc);
                                                // console.log(arr_key);
                                                addTr(val, arr_key);
                                            }
                                            // console.log('ket qua loc = '+ doneTasks);
                                        }
                                    });
                                    $('.tr_search').remove();
                                    $('tbody').append(_tr);
                                    $('.btn_create').css('display', 'block');
                                    removeNode();
                                }
                                else {
                                   if(check_tr_search == 1) {
                                        _tr = `
                                            <tr class="tr_search">
                                                <td colspan="6" class="text-center">
                                                    <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                                </td>
                                            </tr>
                                        `;
                                        $('.tbody').append(_tr);
                                        $('.btn_create').hide();
                                   }
                                }
                            }
                        });
                    }
                });
            }
        });

        function minus_click(id) {
            number = id.nextElementSibling;
            var min = number.getAttribute('min');
            
            if(Number(number.value) > min) {
                number.value -= 1; 
            }
        }

        function plus_click(id) {
            number = id.previousElementSibling;
            var max = number.getAttribute('max');
            
            number.value = Number(number.value) + 1; 
        }
    </script>
@endpush