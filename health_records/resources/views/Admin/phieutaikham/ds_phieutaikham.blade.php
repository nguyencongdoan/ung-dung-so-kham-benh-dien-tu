@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <nav aria-label="breadcrumb" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách phiếu tái khám</li>
            </ol>
        </nav>
        <section class="my-5">
            <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách phiếu tái khám</h1>
            <h2 class="fw-bold mb-3 text-center text-time" data-aos="fade-down"></h2>
            {{-- <div class="mb-3">
                <a href="{{ route('admin.phieukham.themphieukham') }}" class="btn mt-2 mb-3 mr-3">Thêm phiếu khám</a>
            </div> --}}
            <div class="container-fluid mt-5 mb-3" data-aos="fade-right">
                <form action="" method="post" id="formSearch">
                    <div class="row align-items-center mb-md-4 mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="madonthuoc" class="form-label-control fw-bold mb-2">Mã đơn thuốc</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="madonthuoc">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="hoten" class="form-label-control fw-bold mb-2">Họ tên bệnh nhân</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="hoten">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="sdt" class="form-label-control fw-bold mb-2">Số điện thoại</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="sdt">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="ngaytaikham" class="form-label-control fw-bold mb-2">Ngày tái khám</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="ngaytaikham">
                        </div>
                    </div>
                </form>
            </div>
            <div class="data_phieutaikham" data-aos="fade-up"></div>
        </section>             
    </div>
@endsection

@push('scripts')
    <script>
        getAllPhieuTaiKham();
        var myInterval = setInterval(() => {
            getAllPhieuTaiKham();
        }, 5000);
        var check = false;
        var date = moment().format('DD-MM-YYYY');

        $('.text-time').show();
        $('.text-time').html("Ngày: " + date);

        function getAllPhieuTaiKham() {
            $.ajax({
                url: "{{ route('api.admin.getAllPhieuTaiKham') }}",
                type: "get",
                success: function (res) {
                    $('.data_phieutaikham').html(res);
                }
            });
        }

        function createPhieuTaiKham(e) {
            var id = e.value;
            $.ajax({
                url: "{{route('api.admin.createPhieuTaiKham')}}",
                type: 'POST',
                data: {id: id},
                dataType: 'json',
                success: function (res) {
                    toast_message(res);
                    getAllPhieuTaiKham();
                }
            })
        }

        page = 1;
        searchForm();
        function searchForm() {
            $("input[name='ngaytaikham']").on('change', function() {
                searchAjax(page);
                check_form();
                ngaytaikham = $(this).val();
                if(ngaytaikham != '') {
                    ngaytaikham = moment(ngaytaikham).format('DD-MM-YYYY');
                    $('.text-time').show();
                    $('.text-time').html("Ngày: " + ngaytaikham);
                }
                else {
                    $('.text-time').html("Ngày: " + date);
                }
            });

            $('#formSearch').on('keyup', function() {
                searchAjax(page);
                $('.text-time').hide();
                check_form();
            });
        }

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });
        
        function searchAjax(page) {
            var formData = $('#formSearch').serialize();
            $.ajax({
                url: "/api/admin/search/searchPhieuTaiKham?page=" + page,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.data_phieutaikham').html(res);
                    clearInterval(myInterval);
                }
            });
        }
        
        function check_form(){
            var madonthuoc = $("input[name='madonthuoc']").val();
            var hoten = $("input[name='hoten']").val();
            var sdt = $("input[name='sdt']").val();
            var ngaytaikham = $("input[name='ngaytaikham']").val();

            if(madonthuoc == '' && hoten == '' && sdt == '' && ngaytaikham == ''){
                getAllPhieuTaiKham();
                $('.text-time').show();
                $('.text-time').html("Ngày: " + date);
            }
        }
    </script>
@endpush