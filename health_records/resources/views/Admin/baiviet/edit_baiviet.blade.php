@extends('layouts.admin')

@section('content')
    @if (session()->has('message'))
        <input type="hidden" id="message" value="{{ session('message') }}">
        <div id="toast"></div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <a href="{{route('admin.baiviet')}}" class="ms-5 btn btn-outline outline-primary d-block col-md-2 col-sm-3 col-5 mb-3" data-aos="fade-right">
                <i class="uil uil-corner-up-left-alt"></i> Quay lại
            </a>
            <h1 class="fw-bold text-center mb-4" data-aos="fade-down">Sửa bài viết</h1>
            <form action="{{route('admin.baiviet.handle_edit')}}" method="post" class="col-10 mx-auto" enctype="multipart/form-data" data-aos="fade-up">
                @csrf
                <input type="hidden" name="id_user" value="{{ Auth::guard('admin')->id() }}">
                <input type="hidden" name="mabv" value="{{ $baiviet->MaBV }}">
                <div class="col-12 my-3">
                    <label for="thumnail" class="form-label-control mb-3">Thumnail</label>
                    @php $url = './uploads/images/'.$baiviet->Thumnail; @endphp
                    <label for="thumnail" class="d-block">
                        <img class="card-img-top rounded-5 col-6 d-block p-0 mb-4" src="{{ asset($url) }}" alt="" />
                    </label>
                    <input type="file" name="thumnail" id="thumnail" class="img-file">
                </div>
                <div class="col-12 my-3">
                    <label for="" class="form-label-control">Tiêu đề</label>
                    <textarea name="tieude" class="form-control" id="" cols="5" rows="4">{{ old('tieude') ? old('tieude') : $baiviet->TieuDe }}</textarea>
                    @if($errors->has('tieude'))
                        <div class="alert-error">{{ $errors->first('tieude') }}</div>
                    @endif
                </div>
                <div class="col-12 my-3">
                    <label for="" class="form-label-control">Tóm tắt</label>
                    <textarea name="tomtat" class="form-control" id="tomtat" cols="5" rows="4">{{ old('tomtat') ? old('tomtat') : $baiviet->TomTat }}</textarea>
                    @if($errors->has('tomtat'))
                        <div class="alert-error">{{ $errors->first('tomtat') }}</div>
                    @endif
                </div>
                <div class="col-12 col-sm-8 my-3">
                    <label for="" class="form-label-control">Thể loại</label>
                    <div class="d-flex flex-wrap align-items-center">
                        <select id="theloai" class="form-select col-md-9 col-sm-10 col-9" name="theloai">
                            <option value="">--Chọn--</option>
                            @foreach ($theloai as $item)
                                @if($baiviet->MaTL == $item->MaTL)
                                    <option value="{{ $item->MaTL }}" selected>{{ $item->TenTL }}</option>
                                @else 
                                    <option value="{{ $item->MaTL }}">{{ $item->TenTL }}</option>
                                @endif
                            @endforeach
                        </select>
                        <a role="button" data-bs-toggle="modal" data-bs-target="#themtheloai">
                            <i class="uil uil-plus-circle fs-3 text-primary mx-2"></i>
                        </a>
                        @if($errors->has('theloai'))
                            <div class="alert-error">{{ $errors->first('theloai') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-12 my-3">
                    <label for="" class="form-label-control">Nội dung</label>
                    <textarea class="d-none" name="noidung" class="form-control" id="noidung" cols="5" rows="4">{{ old('noidung') ? old('noidung') : $baiviet->NoiDung }}</textarea>
                    <div id="summernote">{!! old('noidung') ? old('noidung') : $baiviet->NoiDung !!}</div>
                    @if($errors->has('noidung'))
                        <div class="alert-error">{{ $errors->first('noidung') }}</div>
                    @endif
                </div>
                <div class="col-12 my-3">
                    <button type="submit" class="fw-bold btn btn-outline outline-primary m-auto d-block">
                        <i class="uil uil-file-edit-alt"></i> Sửa bài viết
                    </button>
                </div>
            </form>
        </div>
    </div>
    {{-- Modal theloai --}}
    <div class="modal fade" id="themtheloai">
        <div class="modal-dialog modal-md">
            <div class="modal-content pb-3">
                <div class="container-fluid">
                    <h3 class="text-center fw-bold my-4">Thêm thể loaị mới</h3>
                    <form id="form-theloai" novalidate>
                        <div class="row">
                            <div class="col-8 mx-auto theloai">
                                <label for="tentheloai" class="form-label">Tên thể loại</label>
                                <div class="d-flex align-items-center theloai-items my-3">
                                    <input type="text" name="tentheloai[]" class="form-control col-12" required>
                                    <i class="uil uil-plus-circle fs-3 text-primary mx-2" onclick="addInput(this)"></i>
                                    <i class="uil uil-trash-alt fs-3 text-danger remove-items"
                                        onclick="removeNode(this)"></i>
                                </div>
                            </div>
                        </div>
                        <button type="submit"
                            class="btn btn-outline outline-primary my-3 mx-auto d-block w-fit btn_theloai">
                            <i class="uil uil-plus"></i> Thêm thể loại
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end modal theloai --}}
@endsection

@push('scripts')
    <!-- jquery -->
    <!-- include libraries(jQuery, bootstrap) -->
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script>
        var i = 0;
        $('.remove-items').hide();

        function checkNode(i) {
            if (i == 0) {
                $('.remove-items').hide();
            } else {
                $('.remove-items').show();
            }
        }

        function addInput(e) {
            i += 1;
            var parent = e.parentNode;

            var _input = document.createElement('div');
            _input.classList.add(`d-flex`, `align-items-center`, `my-3`, `theloai-items`);
            _input.innerHTML = parent.innerHTML;
            $('.theloai').append(_input);
            checkNode(i);
        }

        function removeNode(e) {
            var parent = e.parentNode;
            parent.remove();
            i -= 1;
            checkNode(i);
        }

        createChucVu();

        function createChucVu() {
            var form = document.getElementById('form-theloai');

            form.addEventListener('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (form.checkValidity()) {
                    form.classList.remove('was-validated');
                    var data = $('#form-theloai').serialize();
                    $.ajax({
                        url: "{{ route('api.admin.theloai.store') }}",
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        success: function(res) {
                            if (res.status == 200) {
                                var _opt = '';
                                $.each(res.data, function(key, item) {
                                    _opt += `
                                        <option value="${item.MaTL}">${item.TenTL}</option>
                                    `;
                                });
                                $('#theloai').html(_opt);
                                var name = 'tentheloai[]';
                                removeFormModal(name);
                            }
                            toast_message(res);
                        }
                    })
                } else {
                    form.classList.add('was-validated');
                }
            })
        }

        function removeFormModal(name) {
            var list_input = $(`input[name="${name}"]`);
            var tamp = '';
            $.each(list_input, function(key, value) {
                if (key == 0) {
                    addInput(value);
                }
                removeNode(value);
            });
        }

        $(document).ready(function() {
            $('.img-file').on('change', function() {
                filePreview('card-img-top', this);
            });

            $('#summernote').summernote({
                height: 400,
                tabsize: 2,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks: {
                    onChange: function(contents, $editable) {
                        $('#noidung').val(contents);
                    }
                }
            });

            var message = $('#message').val();
            if(message != '') {
                var res = {
                    'status': 200,
                    'message': message,
                    'alert': 'success'
                }
                toast_message(res);
            }
        });
    </script>
@endpush
