@extends('layouts.admin')

@section('content')
    @if(session()->has('message'))
        <input type="hidden" id="message" value="{{ session('message') }}">
        <div id="toast"></div>
    @endif
    <div class="container-fluid">
        <nav aria-label="breadcrumb" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách bài viết</li>
            </ol>
        </nav>
        <section>
            <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách bài viết</h1>
            <h2 class="fw-bold fs-1 mb-3 text-center text-time"></h2>
            <div class="search-ajax mb-3 flex-wrap" data-aos="fade-right">
                <a href="{{ route('admin.baiviet.thembaiviet') }}" class="btn btn-outline outline-primary fw-bold w-fit d-block fw-bold mt-2 mb-3 mx-3">
                    <i class="uil uil-plus"></i>
                    Thêm bài viết
                </a>
            </div>
            <div class="container-fluid mb-3">
                <form action="" method="post" id="formSearch" data-aos="fade-right">
                    @csrf
                    <div class="row align-items-center mb-md-4 mb-2">
                        <div class="col-md-3 col-6">
                            <label for="search_bs" class="form-label mb-2">Tên bài viết</label>
                            <input name="tieude" id="search_bs" type="text" class="form-control"
                                placeholder="Search...">
                        </div>
                        <div class="col-md-3 col-6">
                            <label for="search_bs" class="form-label mb-2">Thể loại</label>
                            <select name="theloai" id="theloai" class="form-select">
                                <option value="">- Chọn -</option>
                            </select>
                        </div>
                        <div class="col-6 col-sm-4 col-md-3">
                            <label for="ngaytao" class="form-label-control fw-bold mb-2">Ngày tạo bài viết</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="ngaytao">
                        </div>
                    </div>
                </form>
            </div>
            <div class="data_baiviet mb-5" data-aos="fade-up"></div>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            loadBaiViet();
            
            function loadBaiViet() {
                $.ajax({
                    url: "{{route('api.client.baiviet.getAllNews')}}",
                    type: "get",
                    success: function (res) {
                        $('.data_baiviet').html(res);
                    }
                });
            }

            loadTheLoai();
            function loadTheLoai() {
                $.ajax({
                    url: "{{ route('api.client.baiviet.getTheLoai') }}",
                    type: "GET",
                    dataType: "json",
                    success: function(res) {
                        if (res.status == 200) {
                            var _option = '';
                            $.each(res.data, function(key, item) {
                                _option += `
                                    <option value="${item.MaTL}">${item.TenTL}</option>
                                `;
                            });
                            $('#theloai').append(_option);
                        }
                    }
                });
            }

            function check_form(){
                var tieude = $("input[name='tieude']").val();
                var theloai = $("select[name='theloai']").val();
                var ngaytao = $("input[name='ngaytao']").val();

                if(tieude == '' && theloai == '' && ngaytao == ''){
                    loadBaiViet();
                }
            }

            page = 1;
            searchNews();
            function searchNews() {
                $('#formSearch').on('keyup', function() {
                    searchAjax(page);
                    check_form();
                });

                $('#formSearch').on('change', function() {
                    searchAjax(page);
                    check_form();
                });
            }

            function searchAjax(page) {
                var data = $('#formSearch').serialize();
                $.ajax({
                    url: "{{ route('api.admin.search.searchNews') }}",
                    type: 'post',
                    data: data,
                    success: function(res) {
                        $('.data_baiviet').html(res);
                    }
                });
            }

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault(); 
                var page = $(this).attr('href').split('page=')[1];
                searchAjax(page);
            });
            
            var message = $('#message').val();
            if(message != '') {
                var res = {
                    'status': 200,
                    'message': message,
                    'alert': 'success'
                }
                toast_message(res);
            }
        }) 
    </script>
@endpush
