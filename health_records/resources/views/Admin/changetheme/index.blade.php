@extends('layouts.admin')

@section('content')
    @if (session()->has('message'))
        <input type="hidden" id="message" value="{{ session('message') }}">
        <input type="hidden" id="status" value="{{ session('status') }}">
        <div id="toast"></div>
    @endif
    <div class="container">
        <h1 class="fw-bold text-center" data-aos="fade-down">Giao diện Website phòng khám Tai - Mũi - Họng</h1>
        <div class="booking p-5 my-5" data-aos="fade-up">
            <form method="post" action="{{ route('admin.changetheme.handle_edit') }}" enctype="multipart/form-data"
                id="formChangeTheme">
                @csrf
                <div class="row justify-content-center align-items-center">
                    <p class="my-2">
                        <a href="#" class="w-fit booking text-danger d-block mb-2 fw-bold rounded-3 hidden_logo">
                            Ẩn / Hiện Logo
                        </a>
                    </p>
                    <div class="col-12 col-sm-6 col-md-4 my-3 groups_logo">
                        <label for="logo_header_admin" class="form-label">Logo trang quản trị</label>
                        <input type="file" name="logo_header_admin" id="logo_header_admin" class="form-control d-none">
                        <div class="col-12 mt-3 mx-auto">
                            <label for="logo_header_admin"
                                class="logo_header {{ isset($thongtin->Logo_Header_Admin) && !empty($thongtin->Logo_Header_Admin) ? 'p-2' : '' }}">
                                @if (isset($thongtin->Logo_Header_Admin) && !empty($thongtin->Logo_Header_Admin))
                                    @php $url = './admin/assets/images/'.$thongtin->Logo_Header_Admin; @endphp
                                    <img src="{{ asset($url) }}" alt="" class="logo_header_admin">
                                @else
                                    <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 my-2 groups_logo">
                        <label for="logo_header" class="form-label">Logo header</label>
                        <input type="file" name="logo_header" id="logo_header" class="form-control d-none">
                        <div class="col-12 mt-3 mx-auto">
                            <label for="logo_header"
                                class="logo_header {{ isset($thongtin->Logo_Header) && !empty($thongtin->Logo_Header) ? 'p-2' : '' }}">
                                @if (isset($thongtin->Logo_Header) && !empty($thongtin->Logo_Header))
                                    @php $url = './client/assets/images/'.$thongtin->Logo_Header; @endphp
                                    <img src="{{ asset($url) }}" alt="" class="logo_header">
                                @else
                                    <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 my-2 groups_logo">
                        <label for="logo_footer" class="form-label">Logo footer</label>
                        <input type="file" name="logo_footer" id="logo_footer" class="form-control d-none">
                        <div class="col-12 mt-3 mx-auto">
                            <label for="logo_footer"
                                class="logo_footer {{ isset($thongtin->Logo_Footer) && !empty($thongtin->Logo_Footer) ? 'p-2' : '' }}">
                                @if (isset($thongtin->Logo_Footer) && !empty($thongtin->Logo_Footer))
                                    @php $url = './client/assets/images/'.$thongtin->Logo_Footer; @endphp
                                    <img src="{{ asset($url) }}" alt="" class="logo_footer">
                                @else
                                    <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                @endif
                            </label>
                        </div>
                    </div>
                    <p class="my-2">
                        <a href="#" class="w-fit booking text-danger d-block mb-3 fw-bold rounded-3 hidden_banner">Ẩn
                            / Hiện Banner</a>
                    </p>
                    <div class="col-12 my-3 banner">
                        <label for="banner_img" class="form-label">Hình ảnh banner</label>
                        <input type="file" name="banner_image" id="banner_img" class="form-control d-none">
                        <div class="col-12 mt-3 mx-auto">
                            <label for="banner_img"
                                class="label-modal-icon {{ isset($thongtin->Banner_Image) && !empty($thongtin->Banner_Image) ? 'p-2' : '' }}">
                                @if (isset($thongtin->Banner_Image) && !empty($thongtin->Banner_Image))
                                    @php $url = './client/assets/images/'.$thongtin->Banner_Image; @endphp
                                    <img src="{{ asset($url) }}" alt="" class="banner_img">
                                @else
                                    <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                @endif
                            </label>
                        </div>
                    </div>
                    <p class="my-2">
                        <a href="#" class="w-fit booking text-danger d-block mb-3 fw-bold rounded-3 hidden_address">Ẩn
                            /
                            Hiện Địa Chỉ</a>
                    </p>
                    <div class="col-12 col-sm-6 my-3 groups_address">
                        <label for="diachi" class="form-label">Địa chỉ</label>
                        <textarea name="diachi" id="diachi" cols="5" rows="6" class="form-control">{{ old('diachi') ? old('diachi') : $thongtin->DiaChi }}</textarea>
                    </div>
                    <div class="col-12 col-sm-6 my-3 groups_address">
                        <label for="diachi_map" class="form-label">Địa chỉ Google Map</label>
                        <textarea name="diachi_map" id="diachi_map" cols="5" rows="6" class="form-control">{{ old('diachi_map') ? old('diachi_map') : $thongtin->DiaChi_Map }}</textarea>
                    </div>
                    @if (isset($thongtin->DiaChi_Map))
                        <div class="mt-3 col-12 address-map groups_address">
                            <iframe src="{{ $thongtin->DiaChi_Map }}" width="100%" height="450" style="border: 0"
                                allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
                            </iframe>
                        </div>
                    @endif
                    <p class="my-2">
                        <a href="#"
                            class="w-fit booking text-danger d-block mb-2 fw-bold rounded-3 hidden_sdt_email">Ẩn / Hiện Số
                            điện thoại & Email</a>
                    </p>
                    <div class="col-12 col-sm-6 my-3 groups_sdt_email">
                        <label for="sdt" class="form-label">Số điện thoại</label>
                        <input type="text" name="sdt" id="sdt" class="form-control"
                            value="{{ old('sdt') ? old('sdt') : $thongtin->SDT }}">
                    </div>
                    <div class="col-12 col-sm-6 my-3 groups_sdt_email">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" name="email" id="email" class="form-control"
                            value="{{ old('email') ? old('email') : $thongtin->Email }}">
                    </div>
                    <p class="my-2">
                        <a href="#"
                            class="w-fit booking text-danger d-block mb-3 fw-bold rounded-3 hidden_content">Ẩn /
                            Hiện Nội Dung</a>
                    </p>
                    <div class="col-12 my-3 content">
                        <label for="banner_content" class="form-label">Nội dung banner</label>
                        <textarea name="banner_content" id="banner_content" class="form-control d-none">{!! old('banner_content') ? old('banner_content') : $thongtin->Banner_Content !!}</textarea>
                        <div id="summernote">{!! old('banner_content') ? old('banner_content') : $thongtin->Banner_Content !!}</div>
                    </div>
                    <div class="mt-5">
                        <button type="submit" class="fw-bold btn btn-outline outline-primary m-auto w-fit d-block btn-1">
                            <i class="uil uil-file-edit-alt"></i> Cập nhật giao diện
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="booking p-5 my-5" data-aos="fade-up">
            <h3 class="fw-bold text-center mb-5">Slide Logo hiện tại</h3>
            <form action="{{ route('admin.changetheme.edit_slide') }}" method="post" enctype="multipart/form-data"
                id="formEditSlide">
                @csrf
                <p class="mx-2 mb-2">
                    <a href="#" class="w-fit booking text-danger d-block mb-2 fw-bold rounded-3 hidden_slide">
                        Ẩn / Hiện Slide Logo
                    </a>
                </p>
                <div class="col-12 my-3 slide_logo_p">
                    <label for="slide_logo" class="form-label label_slide">Slide logo</label>
                    <div class="slide_logo slide_logo_list d-flex flex-wrap align-items-center">
                        @if ($slide->count() > 0)
                            @foreach ($slide as $key => $item)
                                <div class="col-12 col-sm-6 col-md-4 mb-3 slide_logo_items">
                                    <input type="file" name="slide_logo[]" id="slide_logo{{ $key }}"
                                        class="form-control d-none" onchange="slide_logo_preview(this)">
                                    <div class="col-12 mt-3 mx-auto parent_label">
                                        <input type="hidden" name="slide_id[]" value="{{ $item->id }}">
                                        <i class="uil uil-times-circle w-fit fs-5 icon-slide text-reb cusor-pointer" onclick="removeSlide(this)"></i>
                                        <label for="slide_logo{{ $key }}"
                                            class="slide_logo_label {{ isset($item->Slide_Image) && !empty($item->Slide_Image) ? 'p-3' : '' }}">
                                            @if (isset($item->Slide_Image) && !empty($item->Slide_Image))
                                                @php $url = './client/assets/images/'.$item->Slide_Image; @endphp
                                                <img src="{{ asset($url) }}" alt="" class="slide_logo_img">
                                            @else
                                                <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-12 col-sm-6 col-md-4 slide_logo_items">
                                <input type="file" name="slide_logo[]" id="slide_logo" class="form-control d-none">
                                <div class="col-12 mt-3 mx-auto parent_label">
                                    <label for="slide_logo" class="slide_logo_label">
                                        <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                    </label>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="my-5">
                        <button type="submit" class="fw-bold btn btn-outline outline-primary m-auto w-fit d-block btn-2">
                            <i class="uil uil-file-edit-alt"></i> Cập nhật Slide
                        </button>
                    </div>
                </div>
            </form>
            <hr />
            <h3 class="fw-bold text-center my-5">Thêm Slide Logo</h3>
            <form class="form" action="{{ route('admin.changetheme.create_slide') }}" method="post"
                enctype="multipart/form-data" id="formCreateSlide">
                @csrf
                <div class="col-12">
                    <p class="mx-2 mb-3">
                        <a href="#" class="w-fit booking text-danger d-block fw-bold rounded-3 create_slide">
                            Thêm Slide Logo
                        </a>
                    </p>
                    <div class="slide-logos d-flex flex-wrap align-items-center">
                        <div class="col-12 col-sm-6 col-md-4 mb-3 slide-logo-items">
                            <input type="file" name="slide_logo[]" id="slide_logo" class="form-control d-none">
                            <i class="uil uil-times-circle w-fit fs-5 icon-slide text-reb cusor-pointer" onclick="removeSlide_Create(this)" style="top: 16px; right: 35px; z-index: 2;"></i>
                            <div class="col-12 mt-3 mx-auto parent_label">
                                <label for="slide_logo" class="slide_logo_label">
                                    <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="fw-bold btn btn-outline outline-primary m-auto w-fit d-block btn-3">
                    <i class="uil uil-plus"></i> Thêm Slide
                </button>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- jquery -->
    <!-- include libraries(jQuery, bootstrap) -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script>
        function stop_preventDefault(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        function removeSlide(e) {
            var id = e.previousElementSibling.value;
            var token = $('input[name="_token"]').val();

            $.ajax({
                url: "{{route('admin.changetheme.remove_slide')}}",
                type: "POST",
                dataType: "JSON",
                data:{id_slide: id, _token: token},
                success: function(res) {
                    if(res.status == 200) {
                        var _html = '';
                        $.each(res.data, function(key, item) {
                            _img = '';
                            _class = '';
                            if(item.Slide_Image) {
                                _img = `
                                    <img src="/client/assets/images/${item.Slide_Image}" alt="" class="slide_logo_img">
                                `;
                                _class = 'p-3';
                            }
                            else {
                                _img = `
                                    <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                                `;
                            }
                            _html += `
                                <div class="col-12 col-sm-6 col-md-4 mb-3 slide_logo_items">
                                    <input type="file" name="slide_logo[]" id="slide_logo${key}"
                                        class="form-control d-none" onchange="slide_logo_preview(this)">
                                    <div class="col-12 mt-3 mx-auto parent_label">
                                        <input type="hidden" name="slide_id[]" value="${item.id}">
                                        <i class="uil uil-times-circle w-fit fs-5 icon-slide text-reb cusor-pointer" onclick="removeSlide(this)"></i>
                                        <label for="slide_logo${key}" class="slide_logo_label ${_class}">
                                            ${_img}
                                        </label>
                                    </div>
                                </div>
                            `;
                        });
                        $('.slide_logo_list').html(_html);
                    }
                    toast_message(res);
                }
            });
        }

        function removeSlide_Create(e) {
            var length = $('.slide-logo-items').length;
            if(length > 1) {
                var parent = e.parentElement;
                parent.remove();
            }
        }

        disableBtn();

        function disableBtn() {
            $('.btn-1').attr('disabled', true);
            $('.btn-2').attr('disabled', true);
            $('.btn-3').attr('disabled', true);

            $('#formChangeTheme').on('change', function() {
                $('.btn-1').removeAttr('disabled');
            });

            $('.content').on('keyup', function() {
                $('.btn-1').removeAttr('disabled');
            });

            $('#formEditSlide').on('change', function() {
                $('.btn-2').removeAttr('disabled');
            });

            $('#formCreateSlide').on('change', function() {
                // console.log("🚀 ~change");
                // var lengt_img = $('.slide_img').length;
                // if(length_img > 0) {
                    
                // }
                $('.btn-3').removeAttr('disabled');
            });
        }

        reload_slide_items();

        function reload_slide_items() {
            var slide_logo_items = document.querySelectorAll('.slide-logo-items');

            $.each(slide_logo_items, function(index, item) {
                item.addEventListener('change', function(e) {
                    var div = item.firstElementChild;
                    slide_logo_preview(div);
                });
            })
        }

        function slide_logo_preview(e) {
            console.log("🚀 ~ e", e)
            var parent_label = e.querySelector('.parent_label');
            // trường hợp cập nhật slide onchange ở thẻ input thì lấy nút cha để lấy ra parent label
            if(!parent_label) {
                parent_label = e.parentElement;
            }
            var label = parent_label.querySelector('label');
            var img = label.querySelector('img');
            // trường hợp chưa có thẻ img 
            if(!img) {
                label.innerHTML = '<img src="" alt="" class="slide_logo_img slide_img">';
                // cập nhật lại thẻ img để gán src
                img = label.querySelector('img');
                console.log("🚀 ~ img", img)
            }

            label.classList.add('p-3');

            if (e.files && e.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    img.setAttribute('src', e.target.result);
                };
                reader.readAsDataURL(e.files[0]);
            }
        }

        preview_images();

        function preview_images() {
            $('#banner_img').on('change', function() {
                filePreview('banner_img', this);
            });

            $('#logo_header_admin').on('change', function() {
                filePreview('logo_header_admin', this);
            });

            $('#logo_header').on('change', function() {
                filePreview('logo_header', this);
            });

            $('#logo_footer').on('change', function() {
                filePreview('logo_footer', this);
            });
        }

        $('#summernote').summernote({
            height: 200,
            tabsize: 2,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
            callbacks: {
                onChange: function(contents, $editable) {
                    $('#banner_content').val(contents);
                }
            }
        });

        create_element();

        function create_element() {
            $('.create_slide').on('click', function(e) {
                stop_preventDefault(e);
                var length = $('.slide-logo-items').length;
                var slide_logo_items = `
                    <div class="col-12 col-sm-6 col-md-4 mb-3 slide-logo-items">
                        <input type="file" name="slide_logo[]" id="slide_logo_${length}" class="form-control d-none">
                        <i class="uil uil-times-circle w-fit fs-5 icon-slide text-reb cusor-pointer" onclick="removeSlide_Create(this)" style="top: 16px; right: 35px; z-index: 2;"></i>
                        <div class="col-12 mt-3 mx-auto">
                            <label for="slide_logo_${length}" class="slide_logo_label">
                                <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                            </label>
                        </div>
                    </div>
                `;

                $('.slide-logos').append(slide_logo_items);
                reload_slide_items();
            });
        }

        hidden_element();

        function hidden_element() {
            $('.hidden_logo').on('click', function(e) {
                stop_preventDefault(e);
                $('.groups_logo').fadeToggle();
            });
            $('.hidden_address').on('click', function(e) {
                stop_preventDefault(e);
                $('.groups_address').fadeToggle();
            });
            $('.hidden_banner').on('click', function(e) {
                stop_preventDefault(e);
                $('.banner').fadeToggle();
            });
            $('.hidden_content').on('click', function(e) {
                stop_preventDefault(e);
                $('.content').fadeToggle();
            });
            $('.hidden_sdt_email').on('click', function(e) {
                stop_preventDefault(e);
                $('.groups_sdt_email').fadeToggle();
            });
            $('.hidden_slide').on('click', function(e) {
                stop_preventDefault(e);
                $('.slide_logo_p').fadeToggle();
            });
        }

        check_toast();

        function check_toast() {
            var message = $('#message').val();
            var status = $('#status').val();
            if (message != '') {
                if (status == 400) {
                    var res = {
                        'status': 400,
                        'message': message,
                        'alert': 'error'
                    }
                } else {
                    var res = {
                        'status': 200,
                        'message': message,
                        'alert': 'success'
                    }
                }
                toast_message(res);
            }
        }
    </script>
@endpush
