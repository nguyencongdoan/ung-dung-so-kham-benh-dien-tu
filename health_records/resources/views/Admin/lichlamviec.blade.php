@extends('layouts.admin')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css"
    integrity="sha512-liDnOrsa/NzR+4VyWQ3fBzsDBzal338A1VfUpQvAcdt+eL88ePCOd3n9VQpdA0Yxi4yglmLy/AmH+Lrzmn0eMQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{asset('./admin/assets/css/calendar.css')}}">
@endsection

@section('content')
    <div id="toast"></div>
    <div class="row justify-content-evenly relative">
        <h1 class="fs-1 fw-bold my-5 text-center">Lịch làm việc</h1>
        <div class="col-3 booking mb-5 lich users-left">
            <form action="" method="post" id="createCalendar" novalidate>
                @csrf
                <input type="hidden" name="manv" value="{{ Auth::guard('admin')->id() }}">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="className" value="">
                <div class="col-12 my-3">
                    <button type="submit" class="btn_create btn btn-outline fw-bold outline-primary m-auto d-block">
                        <i class="uil uil-plus"></i> Thêm lịch làm việc
                    </button>
                </div>
                <div class="col-12 my-3">
                    <label for="tieude" class="form-label-control">Tiêu đề</label>
                    <input type="text" id="tieude" name="tieude" class="form-control" required>
                </div>
                <div class="col-12 my-3">
                    <label for="start" class="form-label-control">Ngày bắt đầu</label>
                    <input type="datetime-local" id="start" name="start" class="form-control" required>
                </div>
                <div class="col-12 my-3">
                    <label for="end" class="form-label-control">Ngày kết thúc</label>
                    <input type="datetime-local" id="end" name="end" class="form-control" required>
                </div>
                <div class="col-12 my-3 d-flex align-items-center flex-wrap">
                    <label class="form-check-label me-3">Màu sắc</label>
                    <div class="d-flex align-items-center mx-auto flex-wrap justify-content-center">
                        <div class="form-check me-3">
                            <input type="radio" value="info" name="color" class="form-check-input i-info" required>
                            <label for="" class="form-check-label info"></label>
                        </div>
                        <div class="form-check me-3">
                            <input type="radio" value="primary" name="color" class="form-check-input i-primary" required>
                        </div>
                        <div class="form-check me-3">
                            <input type="radio" value="warning" name="color" class="form-check-input i-warning" required>
                        </div>
                        <div class="form-check me-3">
                            <input type="radio" value="important" name="color" class="form-check-input i-important" required>
                        </div>
                        <div class="form-check me-3">
                            <input type="radio" value="success" name="color" class="form-check-input i-success" required>
                        </div>
                    </div>
                </div>
                <div class="col-12 my-3 flex-wrap d-none justify-content-evenly groups_btn">
                    <button type="button" class="btn btn-outline outline-primary mb-3 fw-bold edit">
                        <i class="uil uil-pen"></i>
                        Cập nhật
                    </button>
                    <button type="button" class="btn btn-outline outline-primary mb-3 fw-bold delete">
                        <i class="uil uil-trash-alt"></i>
                        Xóa
                    </button>
                    <button type="button" class="btn btn-outline outline-primary mb-3 fw-bold cancel">
                        Hủy bỏ
                    </button>
                </div>
            </form>
            <hr class="my-4"/>
            <div class="col-12 my-3">
                <div class="d-flex align-items-center">
                    <img src="{{ asset('./admin/assets/images/filter.png')}}" class="me-2">
                    <p class="fw-bold fs-5">Bộ lọc</p>
                </div>
                <div class="mt-4">
                    <form action="" id="filter-color">
                        <div class="form-check p-0 col-12">
                            <input class="form-check-input" name="checked_color[]" type="checkbox" value="info" id="color_check2">
                            <label class="form-check-label check_color" for="color_check2">
                                <span class="info"></span>
                            </label>
                        </div>
                        <div class="form-check p-0 col-12">
                            <input class="form-check-input" name="checked_color[]" type="checkbox" value="primary" id="color_check3">
                            <label class="form-check-label check_color" for="color_check3">
                                <span class="primary"></span>
                            </label>
                        </div>
                        <div class="form-check p-0 col-12">
                            <input class="form-check-input" name="checked_color[]" type="checkbox" value="warning" id="color_check4">
                            <label class="form-check-label check_color" for="color_check4">
                                <span class="warning"></span>
                            </label>
                        </div>
                        <div class="form-check p-0 col-12">
                            <input class="form-check-input" name="checked_color[]" type="checkbox" value="important" id="color_check5">
                            <label class="form-check-label check_color" for="color_check5">
                                <span class="important"></span>
                            </label>
                        </div>
                        <div class="form-check p-0 col-12">
                            <input class="form-check-input" name="checked_color[]" type="checkbox" value="success" id="color_check6">
                            <label class="form-check-label check_color" for="color_check6">
                                <span class="success"></span>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <i class="fa-solid fa-bars icon-bar btn-toggle-profile w-fit fs-2 my-3 d-lg-none col-2 mr-auto mx-4"></i>
        <div id='calendar' class="col-md-8 col-11 mb-5 py-4 booking lich-right"></div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>

    <script>
        $(document).ready(function() {
            createLich();
            editLich();
            deleteLich();
            cancel();

            function check_radio() {
                var radio = $('input[name="color"]');
                var className = $('input[name="className"]').val();

                $.each(radio, function(index, val) {
                    if(val.value == className) {
                        val.setAttribute('checked', 'checked');
                    }
                    else {
                        val.removeAttribute('checked');
                    }
                });
            }
            
            function createLich() {
                $('#createCalendar').on('submit', function(e) {
                    var data = $(this).serialize();
                    e.preventDefault();
                    e.stopPropagation();
                    var form = document.getElementById('createCalendar');
                    if(form.checkValidity()) {
                        form.classList.remove('was-validated');
                        $.ajax({
                            url: "{{ route('api.admin.lichkhambenh.create') }}",
                            data: data,
                            type: "POST",
                            success: function(res) {
                                if(res.status == 200) {
                                    $('#calendar').fullCalendar('renderEvent', {
                                            id: res.data.id,
                                            title: res.data.TieuDe,
                                            start: res.data.ThoiGianBD,
                                            end: res.data.ThoiGianKT,
                                            // allDay: allDay,
                                            className: res.data.ClassName,
                                        },
                                        true
                                    );
                                    toast_message(res);
                                    $('#createCalendar').removeClass('was-validated');
                                    document.getElementById('createCalendar').reset()
                                }
                                else {
                                    toast_message(res);
                                }
                            }
                        });
                    }
                    else {
                        form.classList.add('was-validated');
                    }
                });
            }

            function editLich() {
                $('.edit').on('click', function(e) {
                    var color = $('input[name="color"]:checked').val();
                    var tieude = $('#tieude').val();
                    var start = $('#start').val();
                    var end = $('#end').val();
                    if(color == '' || tieude == '' || start == '' || end == '') {
                        $('#createCalendar').addClass('was-validated');
                    }
                    else {
                        $('#createCalendar').removeClass('was-validated');
                        var data = $('#createCalendar').serialize();
                        var id = $('input[name="id"]').val();
                        $.ajax({
                            url: "{{route('api.admin.lichkhambenh.update')}}",
                            type: "POST",
                            data: data,
                            dataType: 'json',
                            success: function (res) {
                                if(res.status == 200) {
                                    $('#calendar').fullCalendar('removeEvents', id);
                                    $('#calendar').fullCalendar('renderEvent', {
                                            id: res.data.id,
                                            title: res.data.TieuDe,
                                            start: res.data.ThoiGianBD,
                                            end: res.data.ThoiGianKT,
                                            className: res.data.ClassName,
                                        },
                                        true
                                    );
                                    toast_message(res);
                                    document.getElementById('createCalendar').reset()
                                    $('.groups_btn').removeClass('d-flex');
                                    $('.btn_create').removeAttr('disabled');
                                    $('#createCalendar').removeClass('was-validated');
                                }
                                else {
                                    toast_message(res);
                                }
                            }
                        });
                    }
                });
            }
            
            function deleteLich() {
                $('.delete').on('click', function() {
                    var data = $('#createCalendar').serialize();
                    var id = $('input[name="id"]').val();
                    $.ajax({
                        url: "{{route('api.admin.lichkhambenh.delete')}}",
                        type: "POST",
                        data: data,
                        dataType: 'json',
                        success: function (res) {
                            if(res.status == 200) {
                                $('#calendar').fullCalendar('removeEvents', id);
                                toast_message(res);
                                document.getElementById('createCalendar').reset()
                                $('.groups_btn').removeClass('d-flex');
                                $('.btn_create').removeAttr('disabled');
                                $('#createCalendar').removeClass('was-validated');
                            }
                            else {
                                toast_message(res);
                            }
                        }
                    })
                });
            }

            function cancel() {
                $('.cancel').on('click', function() {
                    document.getElementById('createCalendar').reset()
                    $('.groups_btn').removeClass('d-flex');
                    $('.btn_create').removeAttr('disabled');
                    $('input[name="className"]').val('');
                    check_radio();
                });
            }
            var arr = [];

            filter()
            function filter() {
                $('input[name="checked_color[]"]').on('change', function(event) {
                    $('#calendar').fullCalendar('rerenderEvents');
                });
            }

            let menu = document.querySelector('.btn-toggle-profile');
            let profile = document.querySelector('.users-left');
            var id_calendar = document.querySelector('#calendar');

            menu.addEventListener("click", function() {
                profile.classList.add("active");
                document.addEventListener("click", function(e) {
                    if(!profile.contains(e.target) && !e.target.matches(".btn-toggle-profile") && !id_calendar.contains(e.target)) {
                        profile.classList.remove("active");
                    }
                });
            });

            var calendar = $('#calendar').fullCalendar({
                events: "{{ route('admin.lichkhambenh') }}",
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                displayEventTime: true,
                editable: true,
                eventRender: function eventRender(event, element, view) {
                    if (event.allDay === 'true') {
                        event.allDay = true;
                    } else {
                        event.allDay = false;
                    }
                    var arr = [];
                    var a = '';
                    
                    var check_box = $('input[name="checked_color[]"]:checked');
                    $.each(check_box, function (i, item) {
                        arr.push(item.value)
                    })

                    if(arr.includes('info')) {
                        if(arr.includes('primary')) {
                            if(arr.includes('warning')) {
                                if(arr.includes('important')) {
                                    if(arr.includes('success')) {
                                        a = '29';
                                    }
                                    else {
                                        a = '23';
                                    }
                                }
                                else if(arr.includes('success')) {
                                    a = '24';
                                }
                                else {
                                    a = '15';
                                }
                            }
                            else if(arr.includes('important')) {
                                a = '16';
                            }
                            else if(arr.includes('success')) {
                                a = '17';
                            }
                            else {
                                a = '6';
                            }
                        }
                        else if(arr.includes('warning')) {
                            if(arr.includes('important')) {
                                if(arr.includes('success')) {
                                    a = '27';
                                }
                                else {
                                    a = '22';
                                }
                            }
                            else if(arr.includes('success')) {
                                a = '26';
                            }
                            else {
                                a = '7';
                            }
                        }
                        else if(arr.includes('important')) {
                            a = '8';
                        }
                        else if(arr.includes('success')) {
                            a = '9';
                        }
                        else {
                            a = '1';
                        }
                    }
                    else if(arr.includes('primary')) {
                        if(arr.includes('warning')) {
                            if(arr.includes('important')) {
                                if(arr.includes('success')) {
                                    a = '25';
                                }
                                else {
                                    a = '18';
                                }
                            }
                            else if(arr.includes('success')) {
                                a = '19';
                            }
                            else {
                                a = '10';
                            }
                        }
                        else if(arr.includes('important')) {
                            if(arr.includes('success')) {
                                a = '28';
                            }
                            else {
                                a = '11';
                            }
                        }
                        else if(arr.includes('success')) {
                            a = '12';
                        }
                        else {
                            a = '2';
                        }
                    }
                    else if(arr.includes('warning')) {
                        if(arr.includes('important')) {
                            if(arr.includes('success')) {
                                a = '20';
                            }
                            else {
                                a = '13';
                            }
                        }
                        else if(arr.includes('success')) {
                            a = '14';
                        }
                        else {
                            a = '3';
                        }
                    }
                    else if(arr.includes('important')) {
                        if(arr.includes('success')) {
                            a = '21';
                        }
                        else {
                            a = '4';
                        }
                    }
                    else if(arr.includes('success')) {
                        a = '5';
                    }

                    return a == 1 ? event.className[0] == 'info' : a == 2 ? event.className[0] == 'primary' : a == 3 ? event.className[0] == 'warning' : a == 4 ? event.className[0] == 'important' : a == 5 ? event.className[0] == 'success' : a == 6 ? event.className[0] == 'info' || event.className[0] == 'primary' : a == 7 ? event.className[0] == 'info' || event.className[0] == 'warning' : a == 8 ? event.className[0] == 'info' || event.className[0] == 'important' : a == 9 ? event.className[0] == 'info' || event.className[0] == 'success' : a == 10 ? event.className[0] == 'primary' || event.className[0] == 'warning' : a == 11 ? event.className[0] == 'primary' || event.className[0] == 'important' : a == 12 ? event.className[0] == 'primary' || event.className[0] == 'success' : a == 13 ? event.className[0] == 'warning' || event.className[0] == 'important' : a == 14 ? event.className[0] == 'warning' || event.className[0] == 'success' : a == 15 ? event.className[0] == 'info' || event.className[0] == 'primary' || event.className[0] == 'warning' : a == 16 ? event.className[0] == 'info' || event.className[0] == 'primary' || event.className[0] == 'important' : a == 17 ? event.className[0] == 'info' || event.className[0] == 'primary' || event.className[0] == 'success' : a == 18 ? event.className[0] == 'primary' || event.className[0] == 'warning' || event.className[0] == 'important' : a == 19 ? event.className[0] == 'primary' || event.className[0] == 'warning' || event.className[0] == 'success' : a == 20 ? event.className[0] == 'warning' || event.className[0] == 'important' || event.className[0] == 'success' : a == 21 ? event.className[0] == 'important' || event.className[0] == 'success' : a == 22 ? event.className[0] == 'warning' || event.className[0] == 'info'|| event.className[0] == 'important' : a == 23 ? event.className[0] == 'warning' || event.className[0] == 'info'|| event.className[0] == 'important' || event.className[0] == 'primary' : a == 24 ? event.className[0] == 'warning' || event.className[0] == 'info'|| event.className[0] == 'success' || event.className[0] == 'primary' : a == 25 ? event.className[0] == 'primary' || event.className[0] == 'warning' || event.className[0] == 'important'|| event.className[0] == 'success' : a == 26 ? event.className[0] == 'info'|| event.className[0] == 'success' || event.className[0] == 'warning' : a == 27 ? event.className[0] == 'info'|| event.className[0] == 'success' || event.className[0] == 'warning' || event.className[0] == 'important' : a == 28 ? event.className[0] == 'primary' || event.className[0] == 'important'|| event.className[0] == 'success' : a == 29 ? true : true;
                },
                selectable: true,
                selectHelper: true,
                select: function(start, end, allDay) {
                    document.getElementById('createCalendar').reset();
                    $('input[name="className"]').val('');
                    var checked = $('input[name="color"]:checked');
                    if(checked.length > 0) {
                        checked.removeAttr('checked');
                    }

                    var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                    $('#start').val(moment(start).format('YYYY-MM-DDTHH:mm'));
                    $('#end').val(moment(end).format('YYYY-MM-DDTHH:mm'));

                    // calendar.fullCalendar('unselect');
                },

                eventDrop: function(event) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    var manv = $('input[name="manv"]').val();
                    $.ajax({
                        url: "{{ route('api.admin.lichkhambenh.update') }}",
                        data: {
                            'tieude': event.title,
                            'start': start,
                            'end': end,
                            'id': event.id,
                            'color': event.className[0],
                            'manv': manv
                        },
                        type: "POST",
                        success: function(res) {
                            if(res.status == 200) {
                                toast_message(res);
                            }
                            else {
                                toast_message(res);
                            }
                        }
                    });
                },
                eventClassNames: function(event) {
                    console.log(event);
                },

                eventClick: function(event) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $('#start').val(moment(start).format('YYYY-MM-DDTHH:mm'));
                    $('#end').val(moment(end).format('YYYY-MM-DDTHH:mm'));
                    $('#tieude').val(event.title);
                    $('.groups_btn').addClass('d-flex');
                    $('.btn_create').attr('disabled', 'disabled');
                    $('input[name="id"]').val(event.id);
                    $('input[name="className"]').val(event.className[0]);
                    check_radio();
                    profile.classList.add("active");
                    document.addEventListener('click', function(e) {
                        if(!id_calendar.contains(e.target) && !profile.contains(e.target) && !e.target.matches(".btn-toggle-profile")) {
                            profile.classList.remove("active");
                        }
                    });
                }
            });
        });
    </script>
@endpush