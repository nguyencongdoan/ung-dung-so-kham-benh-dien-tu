@extends('layouts.admin')

@section('content')
    @if (session()->has('message'))
        <input type="hidden" id="status" value="{{ session('status') }}">
        <input type="hidden" id="message" value="{{ session('message') }}">
        <div id="toast"></div>
    @endif
    <div id="toast"></div>
    <div class="container">
        <div class="row">
            <div class="my-3 mx-5" data-aos="fade-right">
                <a href="{{ route('admin.hoso.nhanvien') }}" class="btn btn-outline outline-primary d-block w-fit">
                    <i class="uil uil-corner-up-left-alt"></i> Quay lại
                </a>
            </div>
            <form novalidate id="form-create" action="{{ route('admin.hoso.nhanvien.store') }}" class="my-3"
                method="post" enctype="multipart/form-data" data-aos="fade-up">
                @csrf
                <div class="row mx-auto col-11 col-md-8 booking">
                    <h1 class="text-center fw-bold mt-2 mb-5">Thêm nhân viên mới</h1>
                    <div class="col-12 mb-3 modal-left d-flex justify-content-center align-items-center bg-sm-none">
                        <label for="file-img" class="label-modal-icon label-img">
                            <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                        </label>
                        <input type="file" id="file-img" name="anhdaidien" class="d-none">
                    </div>
                    <div class="col-12 col-sm-6 my-2">
                        <label for="hoten" class="form-label">Họ tên nhân viên</label>
                        <input type="text" id="hoten" class="form-control" name="hoten" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập họ tên
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 my-2">
                        <label for="sdt" class="form-label">Số điện thoại</label>
                        <input type="text" id="sdt" min="10" max="10" class="form-control"
                            name="sdt" required>
                        <div class="invalid-feedback valid-sdt">
                            Vui lòng nhập số điện thoại
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 my-2">
                        <label for="gioitinh" class="form-label">Giới tính</label>
                        <div class="d-flex flex-wrap">
                            <div class="form-check mx-4">
                                <input type="radio" name="gioitinh" id="gt-nam" class="form-check-input" checked
                                    value="1">
                                <label for="gt-nam" class="form-check-">Nam</label>
                            </div>
                            <div class="form-check mx-2">
                                <input type="radio" name="gioitinh" id="gt-nu" class="form-check-input"
                                    value="0">
                                <label for="gt-nu" class="form-check-">Nữ</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 my-2">
                        <label for="ngaysinh" class="form-label">Ngày sinh</label>
                        <input type="date" id="ngaysinh" class="form-control" name="ngaysinh" required>
                        <div class="invalid-feedback">
                            Vui lòng chọn ngày sinh
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 my-2">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" id="email" class="form-control" name="email" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập email
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 my-2">
                        <label for="password" class="form-label">Mật khẩu</label>
                        <input type="password" id="password" min="6" class="form-control" name="password" required>
                        <div class="invalid-feedback valid-pass">
                            Vui lòng nhập mật khẩu
                        </div>
                    </div>
                    <div class="col-12 my-2">
                        <label for="hoten" class="form-label">Địa chỉ</label>
                        <textarea name="diachi" id="" cols="20" rows="3" class="form-control"></textarea>
                    </div>
                    <div class="col-12 col-md-6 my-2">
                        <label for="chucvu" class="form-label-control fw-bold mb-2">Chức vụ</label>
                        <div class="d-flex align-items-center flex-wrap">
                            <select name="chucvu" id="chucvu" class="form-select col-md-9 col-sm-10 col-9" required>
                                <option value="">--Chọn--</option>
                                @if ($chucvu->count() > 0)
                                    @foreach ($chucvu as $item)
                                        <option value="{{ $item->MaChucVu }}">{{ $item->TenChucVu }}</option>
                                    @endforeach
                                @else
                                    <option value="">Chưa có chức vụ nào</option>
                                @endif
                            </select>
                            <a role="button" data-bs-toggle="modal" data-bs-target="#themchucvu">
                                <i class="uil uil-plus-circle fs-3 text-primary mx-2"></i>
                            </a>
                            <div class="invalid-feedback col-12">
                                Vui lòng chọn chức vụ
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 my-2">
                        <label for="chuyenmon" class="form-label-control fw-bold mb-2">Chuyên môn</label>
                        <div class="d-flex align-items-center flex-wrap">
                            <select name="chuyenmon" id="chuyenmon" class="form-select col-md-9 col-sm-10 col-9"
                                required>
                                <option value="">--Chọn--</option>
                                @if ($chuyenmon->count() > 0)
                                    @foreach ($chuyenmon as $item)
                                        <option value="{{ $item->MaChuyenMon }}">{{ $item->TenChuyenMon }}</option>
                                    @endforeach
                                @else
                                    <option value="">Chưa có chuyên môn nào</option>
                                @endif
                            </select>
                            <a role="button" data-bs-toggle="modal" data-bs-target="#themchuyenmon">
                                <i class="uil uil-plus-circle fs-3 text-primary mx-2"></i>
                            </a>
                            <div class="invalid-feedback col-12">
                                Vui lòng chọn chuyên môn
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline outline-primary my-3 mx-auto d-block w-fit">
                        <i class="uil uil-plus"></i> Thêm nhân viên
                    </button>
                </div>
            </form>
        </div>
    </div>

    {{-- Modal chuyenmon --}}
    <div class="modal fade" id="themchuyenmon">
        <div class="modal-dialog modal-md">
            <div class="modal-content pb-3">
                <div class="container-fluid">
                    <h3 class="text-center fw-bold my-4">Thêm chuyên môn mới</h3>
                    <form id="form-chuyenmon" novalidate>
                        <div class="row">
                            <div class="col-8 mx-auto chuyenmon">
                                <label for="tenchuyenmon" class="form-label">Tên chuyên môn</label>
                                <div class="d-flex align-items-center chuyenmon-items my-3">
                                    <input type="text" name="tenchuyenmon[]" class="form-control col-12" required>
                                    <i class="uil uil-plus-circle fs-3 text-primary mx-2" onclick="addInput(this)"></i>
                                    <i class="uil uil-trash-alt fs-3 text-danger remove-items"
                                        onclick="removeNode(this)"></i>
                                </div>
                            </div>
                        </div>
                        <button type="submit"
                            class="btn btn-outline outline-primary my-3 mx-auto d-block w-fit btn_chuyenmon">
                            <i class="uil uil-plus"></i> Thêm chuyên môn
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end modal chucvu --}}

    {{-- Modal chucvu --}}
    <div class="modal fade" id="themchucvu">
        <div class="modal-dialog modal-md">
            <div class="modal-content pb-3">
                <div class="container-fluid">
                    <h3 class="text-center fw-bold my-4">Thêm chức vụ mới</h3>
                    <form id="form-chucvu" novalidate>
                        <div class="row">
                            <div class="col-8 mx-auto chucvu">
                                <label for="tenchucvu" class="form-label">Tên chuyên môn</label>
                                <div class="d-flex align-items-center chucvu-items my-3">
                                    <input type="text" name="tenchucvu[]" class="form-control col-12" required>
                                    <i class="uil uil-plus-circle fs-3 text-primary mx-2" onclick="addInput(this)"></i>
                                    <i class="uil uil-trash-alt fs-3 text-danger remove-items"
                                        onclick="removeNode(this)"></i>
                                </div>
                            </div>
                        </div>
                        <button type="submit"
                            class="btn btn-outline outline-primary my-3 mx-auto d-block w-fit btn_chucvu">
                            <i class="uil uil-plus"></i> Thêm chức vụ
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end modal chucvu --}}
@endsection

@push('scripts')
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>
    <script>
        var i = 0;
        $('.remove-items').hide();

        $('#file-img').on('change', function() {
            $('.label-img').html('<img src="" class="img-user w-200">');
            $('.label-img').addClass('p-2');
            filePreview('img-user', this);
        });
        
        function checkNode(i) {
            if (i == 0) {
                $('.remove-items').hide();
            } else {
                $('.remove-items').show();
            }
        }

        function addInput(e) {
            i += 1;
            var parent = e.parentNode;
            var parent1 = parent.parentNode;
            var name_item = '';
            var node_item = '';

            if (parent1.matches(".chucvu")) {
                name_item = 'chucvu-items';
                node_item = '.chucvu';
            } else {
                name_item = 'chuyenmon-items';
                node_item = '.chuyenmon';
            }

            var _input = document.createElement('div');
            _input.classList.add(`d-flex`, `align-items-center`, `my-3`, `${name_item}`);
            _input.innerHTML = parent.innerHTML;
            $(node_item).append(_input);
            checkNode(i);
        }

        function removeNode(e) {
            var parent = e.parentNode;
            parent.remove();
            i -= 1;
            checkNode(i);
        }

        themNhanVien();

        function themNhanVien() {
            var form = document.getElementById('form-create');
            form.addEventListener('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (form.checkValidity()) {
                    var formData = $(this).serialize();
                    var sdt = $('#sdt').val();
                    var pass = $('#password').val();

                    if (sdt.length < 10 || sdt.length > 10) {
                        $('.valid-sdt').text('Số điện thoại không đúng!');
                        $('.valid-sdt').css('display', 'block');
                        if (pass.length < 6) {
                            $('.valid-pass').text('Mật khẩu phải > 6 ký tự!');
                            $('.valid-pass').css('display', 'block');
                        } else {
                            $('.valid-pass').css('display', 'none');
                        }
                        form.classList.add('was-validated');
                    } else if (pass.length < 6) {
                        $('.valid-pass').text('Mật khẩu phải > 6 ký tự!');
                        $('.valid-pass').css('display', 'block');
                        form.classList.add('was-validated');
                    } else {
                        form.classList.remove('was-validated');
                        $('.valid-sdt').css('display', 'none');
                        $('.valid-pass').css('display', 'none');
                        form.submit();
                    }
                } else {
                    form.classList.add('was-validated');
                }
            });
        }

        createChucVu();

        function createChucVu() {
            var form = document.getElementById('form-chucvu');

            form.addEventListener('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (form.checkValidity()) {
                    form.classList.remove('was-validated');
                    var data = $('#form-chucvu').serialize();
                    $.ajax({
                        url: "{{ route('api.admin.chucvu.store') }}",
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        success: function(res) {
                            if (res.status == 200) {
                                var _opt = '';
                                $.each(res.data, function(key, item) {
                                    _opt += `
                                        <option value="${item.MaChucVu}">${item.TenChucVu}</option>
                                    `;
                                });
                                $('#chucvu').html(_opt);
                                var name = 'tenchucvu[]';
                                removeFormModal(name);
                            }
                            toast_message(res);
                        }
                    })
                } else {
                    form.classList.add('was-validated');
                }
            })
        }

        createChuyenMon();

        function createChuyenMon() {
            var form = document.getElementById('form-chuyenmon');

            form.addEventListener('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (form.checkValidity()) {
                    form.classList.remove('was-validated');
                    var data = $('#form-chuyenmon').serialize();
                    $.ajax({
                        url: "{{ route('api.admin.chuyenmon.store') }}",
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        success: function(res) {
                            if (res.status == 200) {
                                var _opt = '';
                                $.each(res.data, function(key, item) {
                                    _opt += `
                                        <option value="${item.MaChuyenMon}">${item.TenChuyenMon}</option>
                                    `;
                                });
                                $('#chuyenmon').html(_opt);
                                var name = 'tenchuyenmon[]';
                                removeFormModal(name);
                            }
                            toast_message(res);
                        }
                    })
                } else {
                    form.classList.add('was-validated');
                }
            })
        }

        function removeFormModal(name) {
            var list_input = $(`input[name="${name}"]`);
            var tamp = '';
            $.each(list_input, function(key, value) {
                if (key == 0) {
                    addInput(value);
                }
                removeNode(value);
            });
        }

        alert();

        function alert() {
            var status = $('#status').val();
            var message = $('#message').val();

            if (message) {
                if (status == 400) {
                    var res = {
                        'status': status,
                        'message': message,
                        'alert': 'error'
                    }
                } else {
                    var res = {
                        'status': status,
                        'message': message,
                        'alert': 'success'
                    }
                }
                toast_message(res);
            }
        }
    </script>
@endpush
