@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid my-5">
        <div class="info-user booking w-fit p-4 mx-auto my-5" data-aos="fade-up">
            <h1 class="fw-bold text-center mb-4">Thông tin nhân viên</h1>
            <div class="col-12 mb-3 modal-left d-flex justify-content-center align-items-center bg-sm-none">
                <label for="file-img" class="label-modal-icon label-img {{isset($nhanvien->AnhDaiDien) ? 'p-0' : ''}}">
                    @if(isset($nhanvien->AnhDaiDien)) 
                        @php $url = './uploads/images/'.$nhanvien->AnhDaiDien @endphp
                        <img src="{{asset($url)}}" class="img-user w-200">
                    @else 
                        <i class="uil uil-image-plus cusor-pointer fs-2"></i>
                    @endif
                </label>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Họ tên: </p>
                <p class="ms-2">{{$nhanvien->HoTen}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Tuổi: </p>
                <p class="ms-2">
                    @php
                        $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                        $datetime = $date->toDateString();
                        $year = $date->format('Y');
                        $year_bn = \Carbon\Carbon::parse($nhanvien->NgaySinh)->format('Y');
                        $age = (int) $year - (int) $year_bn + 1;
                    @endphp
                    {{ $age }} tuổi
                </p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Giới tính: </p>
                <p class="ms-2">{{$nhanvien->GioiTinh == 1 ? 'Nam' : 'Nữ'}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Số điện thoại: </p>
                <p class="ms-2">0{{ number_format($nhanvien->SDT, 0, ' ', ' ') }}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Địa chỉ: </p>
                <p class="ms-2">{{$nhanvien->DiaChi}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Chức vụ: </p>
                <p class="ms-2">{{$nhanvien->TenChucVu}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Chuyên môn: </p>
                <p class="ms-2">{{$nhanvien->TenChuyenMon}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Email: </p>
                <p class="ms-2">{{$nhanvien->email}}</p>
            </div>
            <div class="col-12 my-3">
                <a href="{{route('admin.hoso.nhanvien')}}" class="m-auto btn btn-outline outline-primary d-block col-sm-4 col-11">
                    <i class="uil uil-corner-up-left-alt"></i> Quay lại
                </a>
            </div>
        </div>
    </div>
@endsection