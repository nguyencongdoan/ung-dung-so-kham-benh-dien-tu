@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <section class="ds_thuoc">
            <div class="col-12 mx-auto">
                <nav aria-label="breadcrumb" data-aos="fade-right">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Hồ sơ nhân viên</li>
                    </ol>
                </nav>
                <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Hồ sơ nhân viên</h1>
                <div data-aos="fade-right">
                    <a href="{{route('admin.hoso.nhanvien.create')}}" class="btn btn-outline outline-primary d-block w-fit">
                        <i class="uil uil-plus"></i> Thêm nhân viên
                    </a>
                </div>
                <div class="container-fluid mt-5 mb-3" data-aos="fade-right">
                    <form action="" method="post" id="formSearch">
                        @csrf
                        <div class="row align-items-center">
                            <div class="mb-3 col-6 col-sm-4 col-md-3">
                                <label for="hoten" class="form-label-control fw-bold mb-2">Họ tên nhân viên</label>
                                <input class="form-control" placeholder="Search Here..." type="text" name="hoten">
                            </div>
                            <div class="mb-3 col-6 col-sm-4 col-md-3">
                                <label for="sdt" class="form-label-control fw-bold mb-2">Số điện thoại</label>
                                <input class="form-control" placeholder="Search Here..." type="text" name="sdt">
                            </div>
                            <div class="mb-3 col-6 col-sm-4 col-md-3">
                                <label for="chucvu" class="form-label-control fw-bold mb-2">Chức vụ</label>
                                <select name="chucvu" id="chucvu" class="form-select">
                                    <option value="">--Chọn--</option>
                                    @if($chucvu->count() > 0)
                                        @foreach($chucvu as $item) 
                                            <option value="{{$item->TenChucVu}}">{{$item->TenChucVu}}</option>
                                        @endforeach
                                    @else
                                        <option value="">Chưa có chức vụ nào</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="data_hoso" data-aos="fade-up">
                    @include('pagination.data_hoso_nhanvien')
                </div>
            </div>             
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        searchHoSo();
        page = 1;
        function searchHoSo() {
            $('#formSearch').on('keyup', function() {
                searchAjax(page);
            });
            $('#chucvu').on('change', function() {
                searchAjax(page);
            });
        }

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });

        function searchAjax(page) {
            var data = $('#formSearch').serialize();
            $.ajax({
                url: "/admin/hoso/nhanvien/searchHoSo?page=" + page,
                type: "post",
                data: data,
                success: function (res) {
                    $('.data_hoso').html(res);
                }
            });
        }
    </script>
@endpush