@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid my-5">
        <div class="mb-3" data-aos="fade-right">
            <a href="{{route('admin.hoso.benhnhan')}}" class="btn btn-outline outline-primary d-block col-md-2 col-sm-3 col-5">
                <i class="uil uil-corner-up-left-alt"></i> Quay lại
            </a>
        </div>
        <h1 class="fw-bold text-center" data-aos="fade-down">Chi tiết hồ sơ bệnh nhân</h1>
        <div class="info-user booking col-md-4 col-sm-11 mx-auto my-5" data-aos="fade-up">
            <h3 class="fw-bold text-center mb-3">Thông tin bệnh nhân</h3>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Họ tên: </p>
                <p class="ms-2">{{$benhnhan->HoTen}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Tuổi: </p>
                <p class="ms-2">
                    @php
                        $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                        $datetime = $date->toDateString();
                        $year = $date->format('Y');
                        $year_bn = \Carbon\Carbon::parse($benhnhan->NgaySinh)->format('Y');
                        $age = (int) $year - (int) $year_bn + 1;
                    @endphp
                    {{ $age }} tuổi
                </p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Giới tính: </p>
                <p class="ms-2">{{$benhnhan->GioiTinh == 1 ? 'Nam' : 'Nữ'}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Số điện thoại: </p>
                <p class="ms-2">0{{ number_format($benhnhan->SDT, 0, ' ', ' ') }}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Cân nặng: </p>
                <p class="ms-2">{{$benhnhan->CanNang}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Địa chỉ: </p>
                <p class="ms-2">{{$benhnhan->DiaChi}}</p>
            </div>
            <div class="info-item d-flex flex-wrap">
                <p class="fw-bold">Email: </p>
                <p class="ms-2">{{$benhnhan->email}}</p>
            </div>
        </div>
        <div class="hoso-user booking" data-aos="fade-up">
            <h2 class="fw-bold text-center">Hồ sơ bệnh nhân</h2>
            <div class="overflow-scroll pr-2">
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã đơn thuốc</th>
                            <th>Mã phiếu khám</th>
                            <th>Ngày khám</th>
                            <th>Triệu chứng</th>
                            <th>Chuẩn đoán bệnh</th>
                            <th>Ngày tái khám</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tbody_hoso">
                        @if ($hoso->count() > 0)
                            @foreach ($hoso as $key => $item)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $item->MaDonThuoc }}</td>
                                    <td>{{ $item->MaPhieuKham }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->NgayKham)->format('d-m-Y') }}</td>
                                    <td>{{ $item->TrieuChung }}</td>
                                    <td>{{ $item->ChuanDoanBenh }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->NgayTaiKham)->format('d-m-Y') }}</td>
                                    <td>
                                        @if ($datetime == $item->NgayTaiKham)
                                            @if($item->TinhTrangKham == 4)
                                                <p style="color: #85d385;">Đã tạo phiếu tái khám</p>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        <button type="button" onclick="load_modal_donthuoc(this)" value="{{$item->MaDonThuoc}}" class="btn btn-table-details">Xem chi tiết đơn thuốc</button>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="text-center">
                                    <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            {{ $hoso->links() }}
        </div>
    </div>
    <!-- Modal ho so -->
    <div class="modal fade" id="detail_donthuoc">
        <div class="modal-dialog modal-xl relative">
            <div class="modal-content pb-5">
                <i class="uil uil-times-circle fs-3 cusor-pointer me-2 ms-auto w-fit-content" data-bs-dismiss="modal"></i>
                <div class="container-fluid">
                    <form action="" method="post" id="formHoSo">
                        <div class="row flex-column m-auto align-items-center modal-changePassword">
                            <h1 class="text-center fw-bold mb-4">Đơn thuốc</h1>
                            <div class="overflow-scroll pr-2">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã đơn thuốc</th>
                                            <th>Mã thuốc</th>
                                            <th>Tên thuốc</th>
                                            <th>Số lượng</th>
                                            <th>Đơn vị</th>
                                            <th>Cách dùng</th>
                                            <th>Liều dùng</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_modal"></tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal ho so -->
@endsection

@push('scripts')
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>
    <script>
        function load_modal_donthuoc(id_donthuoc) {
            var id = id_donthuoc.value;
            $.ajax({
                url: "{{route('api.admin.donthuoc.detail_toathuoc')}}",
                type: 'POST',
                data: {id: id},
                dataType: 'json',
                success: function (res) {
                    var _modal = '';
                    if(res.status == 200) {
                        $.each(res.data, function(key, item) {
                            _modal += `
                                <tr>
                                    <td>${++key}</td>    
                                    <td>${item.MaDonThuoc}</td>    
                                    <td>${item.MaThuoc}</td>    
                                    <td>${item.TenThuoc}</td>    
                                    <td>${item.SoLuong}</td>    
                                    <td>${item.TenDonVi}</td>    
                                    <td>${item.CachDung}</td>    
                                    <td>${item.LieuDung}</td>    
                                </tr>
                            `;
                        });
                        $('.tbody_modal').html(_modal);
                        $('#detail_donthuoc').modal('show');
                    }
                    else {
                        toast_message(res);
                    }
                }
            });
        }
    </script>
@endpush