@extends('layouts.admin') 

@section('content')
    <div id="toast"></div>
    <h1 class="text-center fs-1 fw-bold" data-aos="fade-down">Chỉnh sửa đơn thuốc</h1>
    <div class="d-flex flex-wrap mx-4">
        <div class="col-12" data-aos="fade-right">
            <button type="button" value="{{$user->id}}" class="btn btn-outline outline-primary d-block btn_hoso">
                <i class="uil uil-file-edit-alt"></i> Xem hồ sơ bệnh nhân
            </button>
           <div class="hoso_benhnhan"></div>
        </div>
        <div class="col-12 mx-auto">
            <div class="search-ajax my-3" data-aos="fade-right">
                <div class="search-box d-flex relative">
                    <form action="" method="post">
                        <input class="search-input search" placeholder="Search Here..." type="search" id="search">
                        <button class="search-form" type="submit" value="">
                            <span class="fa fa-search"></span>
                        </button>
                    </form>
                </div>
            </div>
            <div class="overflow-scroll pr-2" data-aos="fade-up">
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Mã thuốc</th>
                            <th>Tên thuốc</th>
                            <th>Số lượng</th>
                            <th>Đơn vị</th>
                            <th>Cách dùng</th>
                            <th>Liều dùng</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        <tr class="tr_search">
                            <td colspan="8" class="text-center">
                                <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-10 col-sm-8 booking mx-auto my-5 py-4" data-aos="fade-up">
            <h2 class="fw-bold fs-2 text-center mb-4">Đơn thuốc</h2>
            <form action="{{ route('admin.donthuoc.handle_edit') }}" method="post" id="formThuoc">
                @csrf
                <div class="d-flex flex-wrap align-items-center justify-content-between">
                    <input type="hidden" name="manv" value="{{$user->MaNV}}">
                    <input type="hidden" name="mabn" value="{{$user->id}}">
                    <input type="hidden" name="maphieukham" value="{{$user->MaPhieuKham}}">
                    <input type="hidden" name="chuandoan" value="{{$user->ChuanDoanBenh}}">
                    <input type="hidden" name="madonthuoc" value="{{$user->MaDonThuoc}}">
                    <div class="row px-2 info-bn">
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Họ tên: </span>
                            <span>{{ $user->HoTen }}</span>
                        </div>
                        <div class="col-3 mb-3 info-item">
                            <span class="fw-bold">Giới tính: </span>
                            <span>{{ $user->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</span>
                        </div>
                        <div class="col-4 mb-3 info-item">
                            <span class="fw-bold">Cân nặng: </span>
                            <span>{{ $user->CanNang }}</span>
                        </div>
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Sinh ngày: </span>
                            <span>{{\Carbon\Carbon::parse($user->NgaySinh)->format('d-m-Y')}}</span>
                            <span class="ngaysinh">
                                @php
                                    $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                    $datetime = $date->toDateString();
                                    $year = $date->format('Y');
                                    $year_bn = \Carbon\Carbon::parse($user->NgaySinh)->format('Y');
                                    $age = (int) $year - (int) $year_bn + 1;
                                @endphp
                                {{ $age }} tuổi
                            </span>
                        </div>
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Địa chỉ: </span>
                            <span>{{ $user->DiaChi }}</span>
                        </div>
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Số điện thoại: </span>
                            <span>{{ $user->SDT }}</span>
                        </div>
                        <div class="col-6 mb-3 info-item">
                            <span class="fw-bold">Chuẩn đoán: </span>
                            <span>{{ $user->ChuanDoanBenh }}</span>
                        </div>
                        <input type="hidden" id="js_donvi" value="{{$donvi}}">
                    </div>
                    <hr/>
                    <div class="list_thuoc col-12"></div>
                    <hr/>
                    <div class="col-12">
                        <p class="fw-bold text-danger">
                            * Khám lại nếu bệnh nhân có một trong những dấu hiệu sau:
                        </p>
                        <div class="d-flex justify-content-center flex-wrap">
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                - Co giật
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                - Ngủ li bì hay vật vã, bứt rứt
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                - Thở mệt
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                - Nôn ói nhiều
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                - Sốt cao liên tục
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                - Tiêu chảy
                            </p>
                        </div>
                    </div>
                    <hr/>
                    <div class="d-flex flex-wrap justify-content-between col-12">
                        <div class="mb-2 px-2">
                            <span class="fw-bold text-danger">* Ngày tạo đơn: </span>
                            <input type="date" class="form-control my-2 ngay_tao_don" name="ngay_tao_don" value="">
                        </div>
                        <div class="mb-2 px-2">
                            <span class="fw-bold text-danger">* Ngày tái khám: </span>
                            <input type="date" class="form-control my-2 ngay_tai_kham" name="ngay_tai_kham" value="">
                        </div>
                    </div>
                    <div class="mb-2 px-2 col-12">
                        <span class="fw-bold text-danger fst-italic">*Lưu ý: </span>
                        <span class="text-dark">
                            Mang theo toa khi tái khám, toa thuốc có giá trị cho lần khám này
                        </span>
                    </div>
                    <div class="col-md-12 mt-3">
                        <button type="submit" disabled class="btn btn-outline outline-primary m-auto d-block btn_create">
                            <i class="uil uil-file-edit-alt"></i> Cập nhật đơn thuốc
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
 
    <!-- Modal ho so -->
    <div class="modal fade" id="detail_donthuoc">
        <div class="modal-dialog modal-xl relative">
            <div class="modal-content pb-5">
                <i class="uil uil-times-circle fs-3 cusor-pointer me-2 ms-auto w-fit-content" data-bs-dismiss="modal"></i>
                <div class="container-fluid">
                    <form action="" method="post" id="formHoSo">
                        <div class="row flex-column m-auto align-items-center modal-changePassword">
                            <h1 class="text-center fw-bold mb-4">Đơn thuốc</h1>
                            <div class="overflow-scroll pr-2">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã đơn thuốc</th>
                                            <th>Mã thuốc</th>
                                            <th>Tên thuốc</th>
                                            <th>Số lượng</th>
                                            <th>Đơn vị</th>
                                            <th>Cách dùng</th>
                                            <th>Liều dùng</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_modal"></tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal ho so -->
@endsection

@push('scripts')
    <script src="{{ asset('./admin/assets/js/count_number.js') }}"></script>
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>

    <script>
        var i = 1;
        var total = 0;
        var sumTotal = 0;
        var arr_key = [];

        var mathuocs = {{ json_encode($mathuocs) }};
        $.each(mathuocs, function(key, item) {
            arr_key.push(item);
        });

        var _tr = '';

        loadDonThuoc();

        function loadDonThuoc() {
            $.ajax({
                url: "{{route('api.admin.donthuoc.load_don_thuoc', $id)}}",
                type: "GET",
                dataType: "json",
                success: function (res) {
                    if(res.status == 200) {
                        $.each(res.data, function (key, val) {
                            $('.ngay_tai_kham').val(val.NgayTaiKham);
                            addTr(val, arr_key);
                        });
                        $('.tbody').html(_tr);
                    }
                    else {
                        _tr = `
                            <tr class="tr_search">
                                <td colspan="8" class="text-center">
                                    <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                </td>
                            </tr>
                        `;
                        $('.tbody').html(_tr);
                    }
                }
            });
        }

        function check_checked() {
            var ds_thuoc = $("input[name='ds_thuoc[]']:checked");
            if(ds_thuoc.length > 0) {
                $('.btn_create').removeAttr('disabled');
            }
            else {
                $('.btn_create').attr('disabled', 'disabled');
            }
        }

        function handle_checked(e, class_thuoc) {
            var parent = e.parentElement;
            var str = parent.nextElementSibling.innerHTML;
            var obj = JSON.parse(str);
            if(e.checked) {
                var td = parent.parentElement;
                var tr = td.parentElement;
                var td_last = tr.lastElementChild;
                var td1 = td_last.previousElementSibling;
                var lieudung = td1.firstElementChild.value;

                var td2 = td1.previousElementSibling;
                var cachdung = td2.firstElementChild.value;

                var td3 = td2.previousElementSibling;
                var parSl = td3.firstElementChild;
                var tendonvi = parSl.options[parSl.selectedIndex].text;
                var madonvi = parSl.options[parSl.selectedIndex].value;

                var td4 = td3.previousElementSibling;
                var main_item = td4.firstElementChild;
                var minus = main_item.firstElementChild;
                var soluong = minus.nextElementSibling;
                max_sl = soluong.getAttribute('max');
                soluong = soluong.value;

                var id = obj.id;
                var tenthuoc = obj.name;
                var dongia = obj.dongia;
                total = soluong * dongia;
                var message = '';

                function alert_message(message) {
                    $('.btn_create').attr('disabled', 'disabled');
                    var res = {
                        status: 400,
                        message: message,
                        alert: 'error'
                    };
                    toast_message(res);
                }

                if(Number(soluong) > Number(max_sl)) {
                    message = 'Số lượng không thể > Số lượng còn lại trong kho!. Số lượng còn lại là: ' + max_sl;
                    alert_message(message);
                }
                else if(Number(soluong) <= 0) {
                    message = 'Số lượng không được < 0!';
                    alert_message(message);
                }
                else if(soluong == '' || madonvi == '' || cachdung == '' || lieudung == '') {
                    message = 'Vui lòng chọn đầy đủ thông tin!';
                    alert_message(message);
                }
                else {
                    _html = `
                        <div class="thongtin-thuoc thuoc-${id}">
                            <input type="hidden" name="mathuoc[]" value="${id}"/>
                            <input type="hidden" name="soluong[]" value="${soluong}"/>
                            <input type="hidden" name="donvi[]" value="${madonvi}"/>
                            <input type="hidden" name="cachdung[]" value="${cachdung}"/>
                            <input type="hidden" name="lieudung[]" value="${lieudung}"/>
                            <input type="hidden" name="dongia[]" value="${dongia}"/>
                            <div class="col-8 col-sm-10 mb-3">
                                <span class="text-dark fw-bold">${id}. ${tenthuoc}</span>
                                <p class="mx-3">
                                    <span class="text-dark">${cachdung},&nbsp;</span>
                                    <span class="text-dark text-lowercase">${lieudung}</span>
                                </p>
                            </div>
                            <div class="col-2 col-sm-1 mb-3">
                                <span class="text-dark">${soluong}</span>
                            </div>
                            <div class="col-2 col-sm-1 mb-3">
                                <span class="text-dark">${tendonvi}</span>
                            </div>
                        </div>
                    `;
                    $('.list_thuoc').append(_html);
                    ++i;
                    check_checked();
                }
            }
            else {
                var class_thuoc = class_thuoc;
                $(class_thuoc).remove();
                i--;
            }
        }

        date();
        seachThuoc();
        check_tr_search();

        function check_tr_search() {
            check_tr_search = 1;

            $('.search').on('keydown', function() {
                if($('.tr_search').length) {
                    // đã có tr_search nên đổi check sang 0
                    check_tr_search = 0;
                }
                else {
                    // chưa có tr_search nên đổi check sang 1
                    check_tr_search = 1;
                }
            });
        }

        function date() {
            var date = moment().format("YYYY-MM-DD");
            $('.ngay_tao_don').attr('min', date);
            $('.ngay_tai_kham').attr('min', date);
            $('.ngay_tao_don').val(date);
            var ngaytaikham = $('.ngay_tai_kham').val();
            if(ngaytaikham != null) {
                var ngay_tai_kham = moment(date).add(3, 'days');
                ngay_tai_kham = moment(ngay_tai_kham).format("YYYY-MM-DD");
                var ngaytaikham = $('.ngay_tai_kham').val(ngay_tai_kham);
            }
        }

        function removeNode() {
            $('.icon_remove').on('click', function(e) {
                var id = e.target.nextElementSibling;
                id = id.value;
                var arr_new = arr_key;
                vitri = -1;
                for (var i = 0; i < arr_new.length; i++) {
                    if (arr_new[i] == id) {
                        vitri = i;
                        arr_new.splice(vitri, 1);
                        arr_key = arr_new;
                        break;
                    }
                }
                var td = e.target.parentElement;
                var tr = td.parentElement;
                tr.remove();
                var class_thuoc = '.thuoc-' + id;
                $(class_thuoc).remove();
                if(arr_key.length == 0) {
                    _tr = `
                        <tr class="tr_search">
                            <td colspan="8" class="text-center">
                                <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                            </td>
                        </tr>
                    `;
                    $('.tbody').html(_tr);
                }
            });
        }
      
        var donvi = $('#js_donvi').val();
        donvi = JSON.parse(donvi);

        function addTr(val, arr_key) {
            _select = '';
            _tr += `
                <tr>
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" name="ds_thuoc[]" onclick="handle_checked(this, '.thuoc-${val.MaThuoc}')" type="checkbox" value="${val.MaThuoc}" id="${val.MaThuoc}">
                            <label class="form-check-label" for="${val.MaThuoc}"></label>
                        </div>
                        <div class="d-none">{"id": ${val.MaThuoc}, "name": "${val.TenThuoc}", "dongia": ${val.DonGia}}</div>
                    </td>
                    <td>${val.MaThuoc}</td>
                    <td>${val.TenThuoc}</td>
                    <td>
                        <div class="col-12 main-item">
                            <i class="uil uil-minus bx-minus ${val.MaThuoc}" onclick="minus_click(this)"></i>
                            <input type="text" class="form-control mx-2 bx-number" min="1" max="${val.SoLuong}" name="soluong" value="${val.SL != null ? val.SL : 1}" required>
                            <i class="uil uil-plus bx-plus ${val.MaThuoc}" onclick="plus_click(this)"></i>
                        </div>
                    </td>
                `;
                $.each(donvi, function(index, item) {
                    _select += `
                        <option value="${item.MaDonVi}" ${item.MaDonVi == val.MaDonVi ? 'selected' : ''}>
                            ${item.TenDonVi}
                        </option>
                    `;
                });
                _tr += `
                    <td>
                        <select name="donvi" class="form-select donvi">
                            <option value="">- Chọn -</option>
                            ${_select}
                        </select>
                    </td>
                    <td>
                        <input type="text" id="cachdung" value="${val.CachDung != null ? val.CachDung : ''}" class="form-control">
                    </td>
                    <td>
                        <input type="text" id="lieudung" value="${val.LieuDung != null ? val.LieuDung : ''}" class="form-control">
                    </td>
                    <td>
                        <i class="uil uil-trash-alt fs-4 icon_remove"></i>
                        <input type="hidden" value="${val.MaThuoc}" />
                    </td>
                </tr>
            `;
        }

        function seachThuoc() {
            $('.search').on('keyup', function() {
                key = $(this).val();
                if(key != '') {
                    $.ajax({
                        url: "{{ route('api.admin.search.Thuoc') }}",
                        type: "POST",
                        dataType: "json",
                        data: {key: key},
                        success: function (res) {
                            if(res.status == 200) {
                                // console.log(res.data);
                                $('.tr_search').remove();
                                _tr = '';
                                $.each(res.data, function (key, val) {
                                    // Kiểm tra arr_key có phần tử nào chưa nếu chưa sẽ push mã thuốc vào arr_key và append tr vào tbody
                                    if(arr_key.length == 0) {
                                        arr_key.push(val.MaThuoc);
                                        addTr(val, arr_key);
                                    } // ngược lại sẽ kiểm tra xem val trả về đã từng được thêm trước đó chưa nếu rồi sẽ trả về true và ngược lại là false
                                    else {
                                        var doneTasks = arr_key.includes(val.MaThuoc);
                                        // trường hợp không tìm thấy tức là chưa từng được thêm thì sẽ push mã thuốc vào arr_key và append tr vào tbody
                                        if(!doneTasks) {
                                            arr_key.push(val.MaThuoc);
                                            // console.log(arr_key);
                                            addTr(val, arr_key);
                                        }
                                        // console.log('ket qua loc = '+ doneTasks);
                                    }
                                });
                                $('.tr_search').remove();
                                $('.tbody').append(_tr);
                                removeNode();
                            }
                            else {
                                if(check_tr_search == 1) {
                                    _tr = `
                                        <tr class="tr_search">
                                            <td colspan="8" class="text-center">
                                                <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                                            </td>
                                        </tr>
                                    `;
                                    $('.tbody').append(_tr);
                                }
                            }
                        }
                    });
                }
            });
        }
    </script>

    <script>
        var _tr_hoso = '';
        
        var check_toggle = 1;
        $('.btn_hoso').on('click', function() {
            if(check_toggle == 0) {
                $('.hoso_benhnhan').hide();
                check_toggle = 1;
            }
            else if(check_toggle == 1) {
                $('.hoso_benhnhan').css('display', 'block');
                check_toggle = 0;
            }
        });
        loadHoSo();

        function loadHoSo() {
            var id_bn = $('.btn_hoso').val();

            $.ajax({
                url: "{{route('api.admin.donthuoc.getHoSo_BN')}}",
                type: 'POST',
                // dataType: 'json',
                data: {id: id_bn},
                success: function(res) {
                    $('.hoso_benhnhan').html(res);
                }
            });
        }

        function load_modal_donthuoc(e) {
            var id = e.value;
            $.ajax({
                url: "{{route('api.admin.donthuoc.detail_toathuoc')}}",
                type: 'POST',
                data: {id: id},
                dataType: 'json',
                success: function (res) {
                    var _modal = '';
                    if(res.status == 200) {
                        $.each(res.data, function(key, item) {
                            _modal += `
                                <tr>
                                    <td>${++key}</td>    
                                    <td>${item.MaDonThuoc}</td>    
                                    <td>${item.MaThuoc}</td>    
                                    <td>${item.TenThuoc}</td>    
                                    <td>${item.SoLuong}</td>    
                                    <td>${item.TenDonVi}</td>    
                                    <td>${item.CachDung}</td>    
                                    <td>${item.LieuDung}</td>    
                                </tr>
                            `;
                        });
                        $('.tbody_modal').html(_modal);
                        $('#detail_donthuoc').modal('show');
                    }
                }
            });
        }
    </script>
@endpush