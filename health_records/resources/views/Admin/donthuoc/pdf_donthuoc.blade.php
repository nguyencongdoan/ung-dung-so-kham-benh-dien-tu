<div class="container" style="color: black;">
    <div class="row">
        <div class="col-11 col-md-10 mx-auto my-5 py-3">
            <div class="d-flex flex-wrap justify-content-between">
                <div class="col-md-3 col-12 mr-auto mt-2 px-0 mb-4 fw-bold">
                    <p>Số hồ sơ: {{$donthuoc->MaDonThuoc}}</p>
                </div>
                <div class="ml-auto text-center col-md-9 px-0 col-12 mt-2 mb-4">
                    <p class="fw-bold text-uppercase">Phòng khám chuyên khoa tai - mũi họng</p>
                    <p><b>ĐC:</b> 41A Quang Trung, TP Nha Trang, Khánh Hòa</p>
                    <p><b>Giờ khám:</b> T2 đến T7: 7h30 - 11h30, 13h30 - 17h30. CN: Nghỉ</p>
                </div>
            </div>
            @php 
                $ngaytaikham = date_create($donthuoc->NgayTaiKham);
                $ngaytaikham = date_format($ngaytaikham, 'd-m-Y');

                $ngaysinh = date_create($donthuoc->NgaySinh);
                $ngaysinh = date_format($ngaysinh, 'Y');

                $yearNow = date('Y');
                $age = $yearNow - $ngaysinh + 1;

                $ngaytaodon = date_create($donthuoc->ngaytaodon);
                $ngay = date_format($ngaytaodon, 'd');
                $thang = date_format($ngaytaodon, 'm');
                $nam = date_format($ngaytaodon, 'Y');
                $ngaytaodon = 'Ngày '. $ngay. ' tháng '. $thang. ' năm ' . $nam; 
            @endphp
            <h2 class="fw-bold fs-2 text-center mb-4">Đơn thuốc</h2>
            <div class="d-flex flex-wrap align-items-center justify-content-between">
                <input type="hidden" name="madonthuoc" value="{{ $donthuoc->MaDonThuoc }}">
                <div class="row px-2 info-bn">
                    <div class="col-5 mb-3 info-item">
                        <span class="fw-bold">Họ tên: </span>
                        <span>{{ $donthuoc->HoTen }}</span>
                    </div>
                    <div class="col-3 mb-3 info-item">
                        <span>{{ $donthuoc->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</span>
                    </div>
                    <div class="col-4 mb-3 info-item">
                        <span class="fw-bold">Cân nặng: </span>
                        <span>{{ $donthuoc->CanNang }}</span>
                    </div>
                    <div class="col-5 mb-3 info-item">
                        <span class="fw-bold">Tuổi: </span>
                        <span>{{$age}} tuổi</span>
                    </div>
                    <div class="col-6 mb-3 info-item">
                        <span class="fw-bold">Số điện thoại: </span>
                        <span>{{ $donthuoc->SDT }}</span>
                    </div>
                    <div class="col-12 mb-3 info-item">
                        <span class="fw-bold">Địa chỉ: </span>
                        <span>{{ $donthuoc->DiaChi }}</span>
                    </div>
                    <div class="col-12 mb-3 info-item">
                        <span class="fw-bold">Chuẩn đoán: </span>
                        <span>{{ $donthuoc->ChuanDoanBenh }}</span>
                    </div>
                </div>
                <hr />
                <div class="list_thuoc col-12 pl-0">
                    @foreach ($ct_donthuoc as $key => $value)
                        <div class="thongtin-thuoc thuoc-${id}">
                            <div class="col-8 col-sm-10 mb-3">
                                <span class="text-dark fw-bold">{{ ++$key }}. {{ $value->TenThuoc }}</span>
                                <p class="mx-3">
                                    <span class="text-dark">{{ $value->CachDung }},&nbsp;</span>
                                    <span class="text-dark text-lowercase">{{ $value->LieuDung }}</span>
                                </p>
                            </div>
                            <div class="col-2 col-sm-1 mb-3">
                                <span class="text-dark">{{ $value->SL }}</span>
                            </div>
                            <div class="col-2 col-sm-1 mb-3">
                                <span class="text-dark">{{ $value->TenDonVi }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <hr />
                <div class="col-12">
                    <p class="fw-bold">
                        * Khám lại nếu bệnh nhân có một trong những dấu hiệu sau:
                    </p>
                    <div class="d-flex justify-content-center flex-wrap">
                        <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                            - Co giật
                        </p>
                        <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                            - Thở mệt
                        </p>
                        <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-5">
                            - Ngủ li bì hay vật vã, bứt rứt
                        </p>
                        <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                            - Nôn ói nhiều
                        </p>
                        <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                            - Tiêu chảy
                        </p>
                        <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-5">
                            - Sốt cao liên tục
                        </p>
                    </div>
                </div>
                <hr />
                <div class="d-flex flex-wrap col-12">
                    <div class="mb-2 text-center ml-auto">
                        <span class="fw-bold ngaytaodon mb-2 ml-2">{{ $ngaytaodon }}</span>
                        <p class="bacsi fw-bold mt-5 pt-4">BS.{{$donthuoc->BacSi}}</p>
                    </div>
                </div>
                <div class="mb-2 col-12">
                    <span class="fw-bold">* Ngày tái khám: </span>
                    <span class="fw-bold ngaytaikham ml-2">{{ $ngaytaikham }}</span>
                </div>
                <div class="my-2 px-2 col-12">
                    <span class="fw-bold fst-italic">* Lưu ý: </span>
                    <span class="text-dark">
                        Mang theo toa khi tái khám, toa thuốc có giá trị cho lần khám này
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>