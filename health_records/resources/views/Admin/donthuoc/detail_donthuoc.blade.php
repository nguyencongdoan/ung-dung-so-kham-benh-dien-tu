@extends('layouts.admin')

@section('content')
    @if (isset($message))
        <input type="hidden" name="message" value="{{ $message }}">
        <div id="toast"></div>
    @endif

    <div class="container-fluid">
        <div class="ms-5 mb-3" data-aos="fade-right">
            <a href="{{ route('admin.donthuoc') }}"
                class="btn btn-outline outline-primary d-block col-md-2 col-sm-3 col-5">
                <i class="uil uil-corner-up-left-alt"></i> Quay lại
            </a>
        </div>
        <form action="{{ route('api.admin.thanhtoan') }}" method="post" data-aos="fade-right">
            @csrf
            <input type="hidden" name="bank_code">
            <div class="row">
                <div class="col-10 col-sm-8 col-md-6 booking mx-auto my-5 py-4">
                    <h2 class="fw-bold fs-2 text-center mb-4">Đơn thuốc</h2>
                    <div class="d-flex flex-wrap align-items-center justify-content-between">
                        <input type="hidden" name="madonthuoc" id="madonthuoc" value="{{ $donthuoc->MaDonThuoc }}">
                        <div class="row px-2 info-bn">
                            <div class="col-5 mb-3 info-item">
                                <span class="fw-bold">Họ tên: </span>
                                <span>{{ $donthuoc->HoTen }}</span>
                            </div>
                            <div class="col-3 mb-3 info-item">
                                <span class="fw-bold">Giới tính: </span>
                                <span>{{ $donthuoc->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</span>
                            </div>
                            <div class="col-4 mb-3 info-item">
                                <span class="fw-bold">Cân nặng: </span>
                                <span>{{ $donthuoc->CanNang }}</span>
                            </div>
                            <div class="col-5 mb-3 info-item">
                                <span class="fw-bold">Sinh ngày: </span>
                                <span class="ngaysinh">
                                    {{ \Carbon\Carbon::parse($donthuoc->NgaySinh)->format('d-m-Y') }}
                                </span>
                                <span>
                                    @php
                                        $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                        $datetime = $date->toDateString();
                                        $year = $date->format('Y');
                                        $year_bn = \Carbon\Carbon::parse($donthuoc->NgaySinh)->format('Y');
                                        $age = (int) $year - (int) $year_bn + 1;
                                    @endphp
                                    {{ $age }} tuổi
                                </span>
                            </div>
                            <div class="col-5 mb-3 info-item">
                                <span class="fw-bold">Địa chỉ: </span>
                                <span>{{ $donthuoc->DiaChi }}</span>
                            </div>
                            <div class="col-5 mb-3 info-item">
                                <span class="fw-bold">Số điện thoại: </span>
                                <span>0{{ number_format($donthuoc->SDT, 0, ' ', ' ') }}</span>
                            </div>
                            <div class="col-6 mb-3 info-item">
                                <span class="fw-bold">Chuẩn đoán: </span>
                                <span>{{ $donthuoc->ChuanDoanBenh }}</span>
                            </div>
                        </div>
                        <hr />
                        <div class="list_thuoc col-12">
                            @foreach ($ct_donthuoc as $key => $value)
                                <div class="thongtin-thuoc thuoc-${id}">
                                    <div class="col-8 col-sm-10 mb-3">
                                        <span class="text-dark fw-bold">{{ ++$key }}.
                                            {{ $value->TenThuoc }}</span>
                                        <p class="mx-3">
                                            <span class="text-dark">{{ $value->CachDung }}, &nbsp;</span>
                                            <span class="text-dark text-lowercase">{{ $value->LieuDung }}</span>
                                        </p>
                                    </div>
                                    <div class="col-2 col-sm-1 mb-3">
                                        <span class="text-dark">{{ $value->SL }}</span>
                                    </div>
                                    <div class="col-2 col-sm-1 mb-3">
                                        <span class="text-dark">{{ $value->TenDonVi }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <hr />
                        <div class="col-12">
                            <p class="fw-bold text-danger">
                                * Khám lại nếu bệnh nhân có một trong những dấu hiệu sau:
                            </p>
                            <div class="d-flex justify-content-center flex-wrap">
                                <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                    - Co giật
                                </p>
                                <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                    - Thở mệt
                                </p>
                                <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                    - Ngủ li bì hay vật vã, bứt rứt
                                </p>
                                <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                    - Nôn ói nhiều
                                </p>
                                <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                    - Tiêu chảy
                                </p>
                                <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-4">
                                    - Sốt cao liên tục
                                </p>
                            </div>
                        </div>
                        <hr />
                        <div class="d-flex flex-wrap justify-content-between col-12">
                            <div class="mb-2 px-2">
                                <span class="fw-bold text-danger">* Ngày tạo đơn: </span>
                                <span
                                    class="fw-bold ml-2">{{ \Carbon\Carbon::parse($donthuoc->NgayTaoDon)->format('d-m-Y') }}</span>
                            </div>
                            <div class="mb-2 px-2">
                                <span class="fw-bold text-danger">* Ngày tái khám: </span>
                                <span
                                    class="fw-bold ml-2">{{ \Carbon\Carbon::parse($donthuoc->NgayTaiKham)->format('d-m-Y') }}</span>
                            </div>
                        </div>
                        <div class="mb-2 px-2 col-12">
                            <span class="fw-bold text-danger fst-italic">* Lưu ý: </span>
                            <span class="text-dark">
                                Mang theo toa khi tái khám, toa thuốc có giá trị cho lần khám này
                            </span>
                        </div>
                        <div class="d-flex justify-content-center align-items-center flex-wrap my-2 col-12">
                            <a href="{{ $hoadon == true ? '#' : route('admin.donthuoc.edit', $donthuoc->MaDonThuoc) }}"
                                class="btn btn-outline outline-primary d-block w-fit fw-bold  {{ $hoadon == true ? 'disabled-link' : '' }}">
                                <i class="uil uil-file-edit-alt"></i> Chỉnh sửa đơn thuốc
                            </a>
                            <button id="btn_pdf" type="button"
                                class="btn btn-outline outline-primary my-3 w-fit d-block fw-bold">
                                <i class="uil uil-print"></i> In đơn thuốc
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-sm-8 col-md-4 mb-5 my-md-5 mx-auto thanhtoan">
                    <div class="booking py-4" data-aos="fade-right">
                        <h3 class="fw-bold fs-3 mb-3 text-center">Thông tin thanh toán</h3>
                        <div class="d-flex mb-2 justify-content-between align-items-center">
                            <p class="fw-bold">Mã đơn thuốc: </p>
                            <p>{{ $donthuoc->MaDonThuoc }}</p>
                        </div>
                        <div class="d-flex mb-2 justify-content-between align-items-center">
                            <p class="fw-bold">Họ tên: </p>
                            <p>{{ $donthuoc->HoTen }}</p>
                        </div>
                        <hr />
                        @php $sum = 0 @endphp
                        @foreach ($ct_donthuoc as $key => $value)
                            @php
                                $total = 0;
                                $total = $value->SL * $value->DonGia;
                                $sum += $total;
                            @endphp
                            <div class="d-flex mb-2 justify-content-between align-keys-center">
                                <p class="fw-bold">{{ ++$key }}. {{ $value->TenThuoc }}</p>
                                <p>{{ $value->SL }}</p>
                            </div>
                        @endforeach
                        <input type="hidden" name="total" value="{{ $sum }}">
                        <hr />
                        <div class="d-flex mb-2 justify-content-between align-items-center">
                            <p class="fw-bold">Tổng tiền: </p>
                            <p class="text-red fw-bold">{{ number_format($sum, 0, ',', '.') }} VND</p>
                        </div>
                        <hr>
                        <div class="col-12 mt-3">
                            <p class="fw-bold text-danger mb-2">* Chọn phương thức thanh toán:</p>
                            <div class="d-flex flex-wrap justify-content-start">
                                <div class="form-check mx-auto">
                                    <input class="form-check-input" type="radio" name="thanhtoan" value="0"
                                        id="cod" checked>
                                    <label class="form-check-label" for="cod">
                                        Thanh toán trực tiếp
                                    </label>
                                </div>
                                <div class="form-check mx-auto">
                                    <input class="form-check-input" type="radio" name="thanhtoan" value="1"
                                        id="online">
                                    <label class="form-check-label" for="online">
                                        Thanh toán online
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex mb-2 mt-4 justify-content-center">
                            <button type="submit" name="redirect" class="fs-5 col-10 btn btn-outline outline-primary"
                                {{ $hoadon == true ? 'disabled' : '' }}>
                                <i class="fa-regular fa-credit-card"></i>
                                {{ $hoadon == true ? 'Đã thanh toán' : 'Thanh toán' }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.bundle.min.js"
        integrity="sha512-w3u9q/DeneCSwUDjhiMNibTRh/1i/gScBVp2imNVAMCt6cUHIw6xzhzcPFIaL3Q1EbI2l+nu17q2aLJJLo4ZYg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.min.js"
        integrity="sha512-2ziYH4Qk1Cs0McWDB9jfPYzvRgxC8Cj62BUC2fhwrP/sUBkkfjYk3142xTKyuCyGWL4ooW8wWOzMTX86X1xe3Q=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
    <script>
        $(document).ready(function() {
            let btn = document.getElementById('btn_pdf');
            let id = document.getElementById('madonthuoc').value;
            let token = $('input[name="_token"]').val();

            var opt = {
                width: 500,
                filename: 'donthuoc' + id + '.pdf',
                image: {
                    type: 'jpeg',
                    quality: 0.98
                },
                jsPDF: {
                    orientation: 'portrait',
                    unit: 'in',
                    format: 'a4',
                    putOnlyUsedFonts: true,
                    floatPrecision: 16
                },
                html2canvas: {
                    imageTimeout: 0,
                    scale: 2,
                }
            };
            btn.addEventListener('click', function(e) {
                $.ajax({
                    url: "{{ route('admin.donthuoc.exportPDF') }}",
                    type: "POST",
                    data: {
                        id: id,
                        _token: token
                    },
                    dataType: 'text',
                    success: function(res) {
                        html2pdf(res, opt);
                        var res = {
                            status: 200,
                            message: 'In đơn thuốc thành công!',
                            alert: 'success'
                        }
                        toast_message(res);
                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var message = $("input[name='message']");
            if (message.val() != "") {
                if (message.val() == "Success") {
                    var res = {
                        'status': '200',
                        'message': 'Thanh toán thành công!',
                        'alert': 'success'
                    }
                    toast_message(res);
                } else {
                    var res = {
                        'status': '400',
                        'message': 'Thanh toán thất bại!',
                        'alert': 'error'
                    }
                    toast_message(res);
                }
            }
        });
    </script>
@endpush
