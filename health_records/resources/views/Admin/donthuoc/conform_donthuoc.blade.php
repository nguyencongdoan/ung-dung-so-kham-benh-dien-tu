@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container">
        <div class="row" id="html2pdf">
            <h1 class="fw-bold text-center" data-aos="fade-down">Xác nhận thông tin đơn thuốc</h1>
            <input type="hidden" id="madonthuoc" name="madonthuoc" value="{{ $donthuoc->MaDonThuoc }}">
            <div class="col-11 col-md-8 booking mx-auto my-5 py-4" data-aos="fade-up">
                <div class="d-flex flex-wrap justify-content-between">
                    <div class="col-md-4 col-12 mr-auto mt-2 mb-4 fw-bold">
                        <p>BS.{{$donthuoc->BacSi}}</p>
                        <p>Mã đơn thuốc: {{$donthuoc->MaDonThuoc}}</p>
                    </div>
                    <div class="ml-auto text-center col-md-8 col-12 mt-2 mb-4">
                        <p class="fw-bold text-uppercase">Phòng khám chuyên khoa tai - mũi họng</p>
                        <p>41A Quang Trung, TP Nha Trang, Khánh Hòa</p>
                        <p>Giờ khám: T2 đến T7: 7h30 - 11h30, 13h30 - 17h30. CN: Nghỉ</p>
                    </div>
                </div>
                @php 
                    $ngaytaikham = date_create($donthuoc->NgayTaiKham);
                    $ngaytaikham = date_format($ngaytaikham, 'd-m-Y');

                    $ngaysinh = date_create($donthuoc->NgaySinh);
                    $ngaysinh = date_format($ngaysinh, 'Y');

                    $yearNow = date('Y');
                    $age = $yearNow - $ngaysinh + 1;

                    $ngaytaodon = date_create($donthuoc->ngaytaodon);
                    $ngay = date_format($ngaytaodon, 'd');
                    $thang = date_format($ngaytaodon, 'm');
                    $nam = date_format($ngaytaodon, 'Y');
                    $ngaytaodon = 'Ngày '. $ngay. ' tháng '. $thang. ' năm ' . $nam; 
                @endphp
                <h2 class="fw-bold fs-2 text-center mb-4">Đơn thuốc</h2>
                <div class="d-flex flex-wrap align-items-center justify-content-between">
                    <div class="row px-2 info-bn">
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Họ tên: </span>
                            <span>{{ $donthuoc->HoTen }}</span>
                        </div>
                        <div class="col-3 mb-3 info-item">
                            <span class="fw-bold">Giới tính: </span>
                            <span>{{ $donthuoc->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</span>
                        </div>
                        <div class="col-4 mb-3 info-item">
                            <span class="fw-bold">Cân nặng: </span>
                            <span>{{ $donthuoc->CanNang }}</span>
                        </div>
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Tuổi: </span>
                            <span class="ngaysinh">{{ $age }} tuổi</span>
                        </div>
                        <div class="col-5 mb-3 info-item">
                            <span class="fw-bold">Số điện thoại: </span>
                            <span>{{ $donthuoc->SDT }}</span>
                        </div>
                        <div class="col-12 mb-3 info-item">
                            <span class="fw-bold">Địa chỉ: </span>
                            <span>{{ $donthuoc->DiaChi }}</span>
                        </div>
                        <div class="col-12 mb-3 info-item">
                            <span class="fw-bold">Chuẩn đoán: </span>
                            <span>{{ $donthuoc->ChuanDoanBenh }}</span>
                        </div>
                    </div>
                    <hr />
                    <div class="list_thuoc col-12">
                        @foreach ($ct_donthuoc as $key => $value)
                            <div class="thongtin-thuoc thuoc-${id}">
                                <div class="col-8 col-sm-10 mb-3">
                                    <span class="text-dark fw-bold">{{ ++$key }}. {{ $value->TenThuoc }}</span>
                                    <p class="mx-3">
                                        <span class="text-dark">{{ $value->CachDung }},&nbsp;</span>
                                        <span class="text-dark text-lowercase">{{ $value->LieuDung }}</span>
                                    </p>
                                </div>
                                <div class="col-2 col-sm-1 mb-3">
                                    <span class="text-dark">{{ $value->SL }}</span>
                                </div>
                                <div class="col-2 col-sm-1 mb-3">
                                    <span class="text-dark">{{ $value->TenDonVi }}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <hr />
                    <div class="col-12">
                        <p class="fw-bold text-danger">
                            * Khám lại nếu bệnh nhân có một trong những dấu hiệu sau:
                        </p>
                        <div class="d-flex justify-content-center flex-wrap">
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                                - Co giật
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                                - Thở mệt
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-5">
                                - Ngủ li bì hay vật vã, bứt rứt
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                                - Nôn ói nhiều
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-3">
                                - Tiêu chảy
                            </p>
                            <p class="text-hidden fw-bold text-dark col-12 col-sm-6 col-md-5">
                                - Sốt cao liên tục
                            </p>
                        </div>
                    </div>
                    <hr />
                    <div class="d-flex flex-wrap justify-content-between col-12">
                        <div class="mb-2">
                            <span class="fw-bold text-danger">* Ngày tái khám: </span>
                            <span class="fw-bold ngaytaikham ml-2">{{ $ngaytaikham }}</span>
                        </div>
                        <div class="mb-2 text-center ml-auto">
                            <span class="fw-bold ngaytaodon mb-2 ml-2">{{ $ngaytaodon }}</span>
                            <p class="bacsi fw-bold mt-5">BS.{{$donthuoc->BacSi}}</p>
                        </div>
                    </div>
                    <div class="mt-2 mb-3 px-2 col-12">
                        <span class="fw-bold text-danger fst-italic">* Lưu ý: </span>
                        <span class="text-dark">
                            Mang theo toa khi tái khám, toa thuốc có giá trị cho lần khám này
                        </span>
                    </div>
                </div>
                @csrf
                <div class="d-flex flex-wrap justify-content-between">
                    <div class="m-md-auto mt-4 p-0">
                        <a href="{{route('admin.donthuoc.edit', $donthuoc->MaDonThuoc)}}" class="btn btn-outline outline-primary w-fit d-block fw-bold">
                            <i class="uil uil-file-edit-alt"></i> Chỉnh sửa
                        </a>
                    </div>
                    <div class="m-md-auto mt-4 p-0">
                        <a href="{{route('admin.donthuoc.xacnhan_donthuoc', $donthuoc->MaDonThuoc)}}" class="btn btn-outline outline-primary w-fit d-block fw-bold">
                            <i class="uil uil-check"></i> Xác nhận
                        </a>
                    </div>
                    <div class="m-md-auto mt-4 p-0">
                        <button id="btn_pdf" type="button" class="btn btn-outline outline-primary w-fit d-block fw-bold">
                            <i class="uil uil-print"></i> In đơn thuốc
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.bundle.min.js" integrity="sha512-w3u9q/DeneCSwUDjhiMNibTRh/1i/gScBVp2imNVAMCt6cUHIw6xzhzcPFIaL3Q1EbI2l+nu17q2aLJJLo4ZYg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.8.0/html2pdf.min.js" integrity="sha512-2ziYH4Qk1Cs0McWDB9jfPYzvRgxC8Cj62BUC2fhwrP/sUBkkfjYk3142xTKyuCyGWL4ooW8wWOzMTX86X1xe3Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>  
    <script>
        $(document).ready(function() {
            let html = document.getElementById('html2pdf');
            let btn = document.getElementById('btn_pdf');
            let id = document.getElementById('madonthuoc').value;
            let token = $('input[name="_token"]').val();

            var opt = {
                width: 500,
                filename: 'donthuoc' + id + '.pdf',
                image: { type: 'jpeg', quality: 0.98 },
                jsPDF: { 
                    orientation: 'portrait',
                    unit: 'in',
                    format: 'a4',
                    putOnlyUsedFonts:true,
                    floatPrecision: 16 
                },
                html2canvas: { 
                    imageTimeout: 0,
                    scale: 2,
                }
            };
            btn.addEventListener('click', function(e) {
                $.ajax({
                    url: "{{route('admin.donthuoc.exportPDF')}}",
                    type: "POST",
                    data: {id: id, _token: token},
                    dataType: 'text',
                    success: function (res) {
                        html2pdf(res, opt);
                        var res = {
                            status: 200,
                            message: 'In đơn thuốc thành công!',
                            alert: 'success'
                        }
                        toast_message(res);
                    }
                });
            });
        });
    </script>
@endpush