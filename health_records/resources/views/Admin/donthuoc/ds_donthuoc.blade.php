@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <nav aria-label="breadcrumb" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách đơn thuốc</li>
            </ol>
        </nav>
        <div class="mb-5">
            <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách đơn thuốc</h1>
            <h2 class="fw-bold mb-3 text-center text-time" data-aos="fade-down"></h2>
            <div class="container-fluid my-5">
                <form action="" method="post" id="formSearch" data-aos="fade-right">
                    <div class="row align-items-center mb-md-4 mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="madonthuoc" class="form-label-control fw-bold mb-2">Mã đơn thuốc</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="madonthuoc">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="hoten" class="form-label-control fw-bold mb-2">Họ tên bệnh nhân</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="hoten">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="ngaytaodon" class="form-label-control fw-bold mb-2">Ngày tạo đơn</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="ngaytaodon">
                        </div>
                    </div>
                </form>
            </div>
            <div class="data_donthuoc" data-aos="fade-up"></div>
        </div>          
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            var date = moment().format('DD-MM-YYYY');
            $('.text-time').show();
            $('.text-time').html("Ngày: " + date);

            check = false;
            getAllDonThuoc()
            var myInterval = setInterval(() => {
                getAllDonThuoc();
            }, 5000);

            searchDonThuoc()

            function getAllDonThuoc() {
                $.ajax({
                    url: "{{ route('api.admin.donthuoc.getAllDonThuoc') }}",
                    type: "GET",
                    success: function (res) {
                        $('.data_donthuoc').html(res);
                    }
                });
            }

            page = 1;
            function searchDonThuoc() {
                $('input[name="ngaytaodon"]').on('change', function() {
                    searchAjax(page);
                    check_form();
                    ngaytaodon = $(this).val();
                    if(ngaytaodon != '') {
                        ngaytaodon = moment(ngaytaodon).format('DD-MM-YYYY');
                        $('.text-time').show();
                        $('.text-time').html("Ngày: " + ngaytaodon);
                    }
                    else {
                        $('.text-time').html("Ngày: " + date);
                    }
                });

                $('#formSearch').on('keyup', function() {
                    searchAjax(page);
                    $('.text-time').hide();
                    check_form();
                });
            }

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault(); 
                var page = $(this).attr('href').split('page=')[1];
                searchAjax(page);
            });

            function searchAjax(page) {
                var formData = $('#formSearch').serialize();
                $.ajax({
                    url: "/api/admin/donthuoc/searchDonThuoc?page=" + page,
                    type: 'POST',
                    data: formData,
                    success: function (res) {
                        $('.data_donthuoc').html(res);
                        clearInterval(myInterval);
                    }
                });
            }

            function check_form(){
                var madonthuoc = $("input[name='madonthuoc']").val();
                var hoten = $("input[name='hoten']").val();
                var ngaytaodon = $("input[name='ngaytaodon']").val();

                if(madonthuoc == '' && hoten == '' && ngaytaodon == ''){
                    getAllDonThuoc();
                    $('.text-time').show();
                    $('.text-time').html("Ngày: " + date);
                }
            }
        });
    </script>
@endpush