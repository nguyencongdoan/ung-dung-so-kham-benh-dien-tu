@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <section class="ds_thuoc">
            <nav aria-label="breadcrumb" data-aos="fade-right">
                <ol class="breadcrumb my-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Danh sách thuốc</li>
                </ol>
            </nav>
            <div class="my-5">
                <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách thuốc</h1>
                <div class="mb-3" data-aos="fade-right">
                    <a href="{{ route('admin.thuoc.create_thuoc') }}" class="btn btn-outline outline-primary d-block w-fit mt-2 mb-3 mx-3">
                        <i class="uil uil-plus"></i> Thêm thuốc
                    </a>
                </div>
                <div class="container-fluid mb-3" data-aos="fade-right">
                    <form action="" method="post" id="formSearch">
                        <div class="row align-items-center">
                            <div class="col-6 col-sm-4 col-md-3 mb-3">
                                <label for="tenthuoc" class="form-label-control fw-bold mb-2">Tên thuốc</label>
                                <input class="form-control" placeholder="Search Here..." type="text" name="tenthuoc">
                            </div>
                            <div class="col-6 col-sm-4 col-md-3 mb-3">
                                <label for="nsx" class="form-label-control fw-bold mb-2">Ngày sản xuất</label>
                                <input class="form-control" placeholder="Search Here..." type="date" name="nsx">
                            </div>
                            <div class="col-6 col-sm-4 col-md-3 mb-3">
                                <label for="hsd" class="form-label-control fw-bold mb-2">Hạn sử dụng</label>
                                <input class="form-control" placeholder="Search Here..." type="date" name="hsd">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="data_thuoc mb-5" data-aos="fade-up"></div>
            </div>             
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        getAllThuoc();

        function remove_thuoc(e) {
           var check = confirm("Bạn có chắc muốn xóa thuốc này không?");
           if(check) {
                $.ajax({
                    url: "{{route('api.admin.thuoc.delete')}}",
                    type: 'post',
                    dataType: 'json',
                    data: {id: e.value},
                    success: function (res) {
                        toast_message(res);
                        getAllThuoc();
                    }
                });
           }
        }
        var page = 1;
        function getAllThuoc(page) {
            $.ajax({
                url: "/api/admin/thuoc/getAllThuoc?page=" + page,
                type: "GET",
                success: function (res) {
                    $('.data_thuoc').html(res);
                }
            });
        }

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            console.log("🚀 ~ page", page)
            searchAjax(page);
        });

        search();
        function search() {
            $('#formSearch').on('keyup', function(e) {
                searchAjax(page);
                check_form()
            });
            $('#formSearch').on('change', function(e) {
                var nsx = $('input[name="nsx"]').val();
                var hsd = $('input[name="hsd"]').val();

                if(nsx >= hsd && hsd != '' && nsx != '') {
                    var res = {
                        status: 400,
                        message: 'Ngày sản xuất không được >= hạn sử dụng!',
                        alert: 'error'
                    }
                    toast_message(res);
                }
                else {
                    searchAjax(page);
                    check_form();
                }
            });
        }

        function searchAjax(page) {
            var data = $('#formSearch').serialize();
            $.ajax({
                url: "/api/admin/thuoc/search?page=" + page,
                type: "post",
                data: data,
                success: function (res) {
                    $('.data_thuoc').html(res);
                }
            });
        }

        function check_form() {
            var tenthuoc = $("input[name='tenthuoc']").val();
            var nsx = $("input[name='nsx']").val();
            var hsd = $("input[name='hsd']").val();

            if(tenthuoc == '' && nsx == '' && hsd == ''){
                getAllThuoc();
            }
        }
    </script>
@endpush