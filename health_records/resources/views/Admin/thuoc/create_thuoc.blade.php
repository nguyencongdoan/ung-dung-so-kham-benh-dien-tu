@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="col-12">
        <a href="{{ route('admin.thuoc') }}" class="ms-5 btn btn-outline outline-primary d-block col-md-2 col-sm-3 col-5 mb-3"
            data-aos="fade-right">
            <i class="uil uil-corner-up-left-alt"></i> Quay lại
        </a>
        <h1 class="fw-bold fs-1 text-center my-5" data-aos="fade-down">Thêm thuốc</h1>
        <form action="" method="post" class="col-md-6 m-auto mb-5 form-booking" id="form-booking" novalidate
            data-aos="fade-up">
            @csrf
            <div class="row booking m-auto">
                <div class="col-12 my-2">
                    <label for="tenthuoc" class="form-label">Tên thuốc</label>
                    <input type="text" id="tenthuoc" class="form-control" name="tenthuoc" required>
                    <div class="invalid-feedback">
                        Vui lòng nhập tên thuốc
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="soluong" class="form-label">Só lượng</label>
                    <div class="col-12 main-item">
                        <i class="uil uil-minus bx-minus" onclick="minus_click(this)"></i>
                        <input type="text" class="form-control mx-2 bx-number" min="1" name="soluong"
                            value="1" required>
                        <i class="uil uil-plus bx-plus" onclick="plus_click(this)"></i>
                        <div id="valid-sl" class="invalid-feedback">
                            Sớ lượng không được < 1 </div>
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="dongia" class="form-label">Đơn giá</label>
                        <input type="number" id="dongia" class="form-control" name="dongia" required>
                        <div id="valid-sdt" class="invalid-feedback">
                            Vui lòng nhập đơn giá
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="nsx" class="form-label">Ngày sản xuất</label>
                        <input type="date" id="nsx" class="form-control" name="nsx" required>
                        <div class="invalid-feedback">
                            Vui lòng nhập ngày sản xuất
                        </div>
                    </div>
                    <div class="col-6 my-2">
                        <label for="hsd" class="form-label">Hạn sử dụng</label>
                        <input type="date" id="hsd" class="form-control" name="hsd" required>
                        <div class="invalid-feedback">
                            Vui lòng chọn hạn sử dụng
                        </div>
                    </div>
                    <div class="col-12 my-2">
                        <label for="info_thuoc" class="form-label">Thông tin thuốc</label>
                        <textarea name="info_thuoc" id="info_thuoc" cols="5" rows="4" class="form-control" required></textarea>
                        <div class="invalid-feedback">
                            Vui lòng nhập thông tin thuốc
                        </div>
                    </div>
                    <div class="col-md-12 my-3">
                        <button type="submit" class="btn btn-outline outline-primary m-auto d-block">
                            <i class="uil uil-plus me-1"></i> Thêm thuốc
                        </button>
                    </div>
                </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('./admin/assets/js/count_number.js') }}"></script>
    <script>
        create_thuoc();

        function create_thuoc() {
            var form = document.getElementById('form-booking');
            $('#form-booking').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                if (form.checkValidity()) {
                    var soluong = $('input[name="soluong"]').val();
                    var nsx = $('#nsx').val();
                    var hsd = $('#hsd').val();
                    var check_form = true;

                    if(soluong < 1) {
                        $('#valid-sl').text('Số lượng không được < 1!');
                        $('#valid-sl').css('display', 'block');
                        form.classList.add('was-validated');
                        check_form = false;
                    }
                    else {
                        check_form = true;
                    }

                    if(nsx >= hsd) {
                        var res = {
                            status: 400,
                            message: 'Ngày sản xuất không được >= hạn sử dụng!',
                            alert: 'error'
                        }
                        toast_message(res);
                        form.classList.add('was-validated');
                        check_form = false;
                    }
                    else {
                        check_form = true;
                    }

                    if(check_form) {
                        form.classList.remove('was-validated');
                        $('#valid-sl').css('display', 'none');
                        var formData = $('#form-booking').serialize();
                        $.ajax({
                            url: "{{ route('api.admin.thuoc.handle_create') }}",
                            type: "post",
                            dataType: "json",
                            data: formData,
                            success: function(res) {
                                toast_message(res);
                                document.getElementById('form-booking').reset();
                            }
                        });
                    }
                } else {
                    form.classList.add('was-validated');
                }
            });
        }

        function minus_click(id) {
            number = id.nextElementSibling;
            var min = number.getAttribute('min');

            if (Number(number.value) > min) {
                number.value -= 1;
            }
        }

        function plus_click(id) {
            number = id.previousElementSibling;
            var max = number.getAttribute('max');

            number.value = Number(number.value) + 1;
        }
    </script>
@endpush
