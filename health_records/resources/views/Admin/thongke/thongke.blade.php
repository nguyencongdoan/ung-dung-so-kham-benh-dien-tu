@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <!-- content -->
    <div class="container-fluid">
        <nav aria-label="breadcrumb" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Thống kê</li>
            </ol>
        </nav>

        <!-- statistics data -->
        <div class="statistics" data-aos="fade-up">
            <div class="row">
                <div class="col-xl-6 pr-xl-2">
                    <div class="row">
                        <div class="col-sm-6 pr-sm-2 statistics-grid">
                            <div class="booking py-4 rounded-3 d-flex justify-content-between align-items-center card-border-top">
                                <img src="{{asset('./admin/assets/images/visits.png')}}" alt="" style="max-width: 64px;">
                                <div>
                                    <h3 class="text-primary number">{{$count_user}}</h3>
                                    <p class="stat-text">Số lượng bệnh nhân</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 pl-sm-2 statistics-grid">
                            <div class="booking py-4 rounded-3 d-flex justify-content-between align-items-center card-border-top">
                                <img src="{{asset('./admin/assets/images/visits.png')}}" alt="" style="max-width: 64px;">
                                <div>
                                    <h3 class="text-secondary number">{{$count_phieukham}}</h3>
                                    <p class="stat-text">Số lượng phiếu khám</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 pl-xl-2">
                    <div class="row">
                        <div class="col-sm-6 pr-sm-2 statistics-grid">
                            <div class="booking py-4 rounded-3 d-flex justify-content-between align-items-center card-border-top">
                                <img src="{{asset('./admin/assets/images/visits.png')}}" alt="" style="max-width: 64px;">
                               <div>
                                    <h3 class="text-success number">{{$count_phieuhen}}</h3>
                                    <p class="stat-text">Số lượng phiếu hẹn</p>
                               </div>
                            </div>
                        </div>
                        <div class="col-sm-6 pl-sm-2 statistics-grid">
                            <div class="booking py-4 rounded-3 d-flex justify-content-between align-items-center card-border-top">
                                <img src="{{asset('./admin/assets/images/salary.png')}}" alt="" style="max-width: 64px;">
                                <div>
                                    <h3 class="text-danger number">{{ number_format($sum_hoadon, 0, ',', '.') }} VND</h3>
                                    <p class="stat-text">Tổng tiền</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //statistics data -->

        <!-- charts -->
        <div class="chart my-5" data-aos="fade-up">
            <div class="row">
                <div class="col-lg-6 pr-lg-2 chart-grid">
                    <div class="booking text-center">
                        <div class="card-header chart-grid__header">
                            Thống kê số lượng phiếu hẹn, phiếu khám trong năm
                            <div class="d-flex align-items-center flex-wrap my-3 fs-6">
                                <label for="" class="form-label me-2">Năm</label>
                                <select name="" id="" class="form-select">
                                    <option value="">--Chọn--</option>
                                    <option value="">2022</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- bar chart -->
                            <div id="container">
                                <canvas id="barchart"></canvas>
                            </div>
                            <!-- //bar chart -->
                        </div>
                        <div class="card-footer text-muted chart-grid__footer">
                            Cập nhật sau 30 giây
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 pl-lg-2 chart-grid">
                    <div class="booking text-center">
                        <div class="card-header chart-grid__header">
                            Thống kê tổng tiền trong năm
                            <div class="d-flex align-items-center flex-wrap my-3 fs-6">
                                <label for="" class="form-label me-2">Năm</label>
                                <select name="" id="" class="form-select">
                                    <option value="">--Chọn--</option>
                                    <option value="">2022</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- line chart -->
                            <div id="container">
                                <canvas id="linechart"></canvas>
                            </div>
                            <!-- //line chart -->
                        </div>
                        <div class="card-footer text-muted chart-grid__footer">
                            Cập nhật sau 30 giây
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //charts -->
        <div class="booking">
            <div class="my-5" data-aos="fade-up">
                <h3 class="text-center fw-bold mb-5">Thống kê số lượng phiếu hẹn trong tháng</h3>
                <form action="" method="post" id="formSearch">
                    @csrf
                    <div class="row align-items-center mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="start_day" class="form-label-control fw-bold mb-2">Ngày bắt đầu</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="start_day">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="end_day" class="form-label-control fw-bold mb-2">Ngày kết thúc</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="end_day">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="end_day" class="form-label-control fw-bold mb-2">Tình trạng phiếu hẹn</label>
                            <select name="tinhtrang" class="form-select">
                                <option value="">--Chọn--</option>
                                <option value="0">Chưa tạo phiếu khám</option>
                                <option value="1">Đã tạo phiếu khám</option>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="data_phieuhen"></div>
            </div>
            <hr>
            <div class="my-5" data-aos="fade-up">
                <h3 class="text-center fw-bold mb-5">Thống kê số lượng phiếu khám trong tháng</h3>
                <form action="" method="post" id="formSearchPK">
                    @csrf
                    <div class="row align-items-center mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="start_day1" class="form-label-control fw-bold mb-2">Ngày bắt đầu</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="start_day1">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="end_day1" class="form-label-control fw-bold mb-2">Ngày kết thúc</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="end_day1">
                        </div>
                    </div>
                </form>
                <div class="data_phieukham"></div>
            </div>
        </div>
    </div>
    <!-- //content -->
@endsection

@push('scripts')
    <!-- chart js -->
    <script src="{{ asset('./admin/assets/js/Chart.min.js') }}"></script>
    <script src="{{ asset('./admin/assets/js/utils.js') }}"></script>
    <script src="{{ asset('./js/tableToExcel.js') }}"></script>
    <!-- //chart js -->

    <script>
        getPhieuHen();
        getPhieuKham();

        function exportPhieuHen() {
            var form = $('#form-phieuhen');
            var data = form.serialize();
            $.ajax({
                url: "{{route('admin.thongke.export_Excel_PhieuHen')}}",
                type: "POST",
                data: data,
                success: function(res) {
                    var div = document.createElement('div');
                    div.innerHTML = res;
                    var table = div.querySelector('table');
                    TableToExcel.convert(table, {
                        name: "thongkephieuhen.xlsx",
                        sheet: {
                            name: "Thống kê số lượng phiếu hẹn"
                        }
                    });
                }
            });
        }
        
        function exportPhieuKham() {
            var form = $('#form-phieukham');
            var data = form.serialize();
            $.ajax({
                url: "{{route('admin.thongke.export_Excel_PhieuKham')}}",
                type: "POST",
                data: data,
                success: function(res) {
                    var div = document.createElement('div');
                    div.innerHTML = res;
                    var table = div.querySelector('table');
                    TableToExcel.convert(table, {
                        name: "thongkephieukham.xlsx",
                        sheet: {
                            name: "Thống kê số lượng phiếu khám"
                        }
                    });
                }
            });
        }

        var myInterval = setInterval(() => {
            getPhieuHen();
            getPhieuKham();
        }, 5000);

        function getPhieuHen() {
            $.ajax({
                url: "{{ route('admin.thongke.getPhieuHen') }}",
                type: "get",
                success: function(res) {
                    $('.data_phieuhen').html(res);
                }
            });
        }
       
        function getPhieuKham() {
            $.ajax({
                url: "{{ route('admin.thongke.getPhieuKham') }}",
                type: "get",
                success: function(res) {
                    $('.data_phieukham').html(res);
                }
            });
        }

        page = 1;
        page1 = 1;

        searchForm();
        function searchForm() {
            $("#formSearch").on('change', function() {
                var check = check_form();
                if(check == false) {
                    searchAjax(page);
                }
            });
        }
        
        $(document).on('click', '.pagination a', function(e){
            e.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            var page1 = $(this).attr('href').split('page=')[1];
            if(e.target.tagName.toLowerCase() == 'a') {
                var a = e.target.href;
                if(a.indexOf('PhieuHen') != -1) {
                    searchAjax(page);
                }
                else {
                    searchAjaxPK(page1);
                }
            }
            else {
                var a = e.target.parentElement.href;
                if(a.indexOf('PhieuHen') != -1) {
                    searchAjax(page);
                }
                else {
                    searchAjaxPK(page1);
                }
            }
        });

        function searchAjax(page) {
            var formData = $('#formSearch').serialize();
            $.ajax({
                url: "/admin/thongke/searchPhieuHen?page=" + page,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.data_phieuhen').html(res);
                    clearInterval(myInterval);
                }
            });
        }

        function check_form() {
            var start_day = $("input[name='start_day']").val();
            var end_day = $("input[name='end_day']").val();
            var tinhtrang = $("select[name='tinhtrang']").val();

            if (start_day == '' && end_day == '' && tinhtrang == '') {
                getPhieuHen();
                return true;
            }
            else if(end_day < start_day && end_day != '') {
                var res = {
                    status: 400,
                    message: 'Ngày bắt đầu không thể > ngày kết thúc',
                    alert: 'error'
                }
                toast_message(res);
                return true;
            }
            return false;
        }

        // phieu khám
        searchFormPK();
        function searchFormPK() {
            $("#formSearchPK").on('change', function() {
                var check = check_form_pk();
                if(check == false) {
                    searchAjaxPK(page1);
                }
            });
        }

        function searchAjaxPK(page1) {
            var formData = $('#formSearchPK').serialize();
            $.ajax({
                url: "/admin/thongke/searchPhieuKham?page=" + page1,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.data_phieukham').html(res);
                    clearInterval(myInterval);
                }
            });
        }

        function check_form_pk() {
            var start_day = $("input[name='start_day1']").val();
            var end_day = $("input[name='end_day1']").val();

            if (start_day == '' && end_day == '') {
                getPhieuKham();
                return true;
            }
            else if(end_day < start_day && end_day != '') {
                var res = {
                    status: 400,
                    message: 'Ngày bắt đầu không thể > ngày kết thúc',
                    alert: 'error'
                }
                toast_message(res);
                return true;
            }
            return false;
        }
    </script>
    
    <script>
        loadBarChart();
        setInterval(() => {
            loadBarChart();
            lineChart();
        },30000);

       function loadBarChart() {
            var options = {
                responsive: true,
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Tháng'
                            }
                        }
                    ],
                yAxes: [
                        {
                            scaleLabel: {
                                display: true,
                                labelString: 'Giá trị'
                            },
                            ticks: {
                                beginAtZero: true,
                            }
                        }
                    ]
                }
            }
            
            var data_chart = {
                labels: Samples.utils.months({count: 12}),
                datasets: [
                    {
                        data: {{json_encode($data_hen)}},
                        label: 'Phiếu hẹn',
                        borderColor: 'rgba(255, 159, 64)',
                        backgroundColor: 'rgba(255, 159, 64, 0.2)',
                        hoverBackgroundColor: '#fcefdc',
                        borderWidth: 1
                    },
                    {
                        data: {{json_encode($data_kham)}},
                        label: 'Phiếu khám',
                        borderColor: 'rgba(92, 184, 92, 1)',
                        backgroundColor: 'rgba(92, 184, 92, 0.2)',
                        hoverBackgroundColor: '#e2f0e2',
                        borderWidth: 1
                    },
                ]
            };

            var bar = $("#barchart");
            var barGrap = new Chart(bar, {
                type: 'bar',
                data: data_chart,
                options: options
            });
       }

        lineChart();
        function lineChart() {
            new Chart(document.getElementById("linechart"), {
                type: 'line',
                data: {
                    labels: Samples.utils.months({count: 12}),
                    datasets: [{
                        label: 'Tổng tiền',
                        backgroundColor: window.chartColors.purple,
                        borderColor: window.chartColors.purple,
                        data: {{json_encode($data_line)}},
                        fill: false,
                    }],
                },
                options: {
                    responsive: true,
                    // title: {
                    // 	display: true,
                    // 	text: 'Chart.js Line Chart'
                    // },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Tháng'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Giá trị'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    </script>
@endpush