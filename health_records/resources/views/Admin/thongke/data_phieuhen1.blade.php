<table class="table mt-3" id="table-phieuhen" data-cols-width="5,20,20,10,10,20,15,10,20,20">
    <thead>
        <tr data-height="36">
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                STT
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Mã phiếu hẹn
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Họ tên bệnh nhân
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Tuổi
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Giới tính
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Số điện thoại
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Ngày hẹn
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Giờ hẹn
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Họ tên Bác sĩ
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Tình trạng
            </th>
        </tr>
    </thead>
    <tbody class="tbody">
        @if ($phieuhen->count() > 0)
            @foreach ($phieuhen as $key => $item)
                <tr data-height="36">
                    <td data-t="n" data-a-h="center" data-a-v="middle"> {{ ++$key }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle"> {{ $item->MaPhieuHen }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle"> {{ $item->HoTen_NK }} </td>
                    <td data-a-h="center" data-a-v="middle">
                        @php
                            $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                            $datetime = $date->toDateString();
                            $year = $date->format('Y');
                            $year_bn = \Carbon\Carbon::parse($item->NgaySinh)->format('Y');
                            $age = (int) $year - (int) $year_bn + 1;
                        @endphp
                        {{ $age }} tuổi
                    </td>
                    <td data-a-h="center" data-a-v="middle">
                        @if ($item->GioiTinh_NK == 1)
                            Nam
                        @else
                            Nữ
                        @endif
                    </td>
                    <td data-a-wrap="true" data-num-fmt="0000000000" data-t="n" data-a-h="center"
                        data-a-v="middle"> {{ $item->SDT }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        {{ \Carbon\Carbon::parse($item->NgayHen)->format('d-m-Y') }}
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        {{ \Carbon\Carbon::parse($item->GioHen)->format('H:i A') }}
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle"> {{ $item->BacSi }} </td>
                    @if ($item->TinhTrang_PH == 1)
                        <td style="color: #85d385;" data-a-wrap="true" data-a-h="center" data-a-v="middle">
                            Đã tạo phiếu khám
                        </td>
                    @else
                        <td style="color: red;" data-a-wrap="true" data-a-h="center" data-a-v="middle">
                            Chưa tạo phiếu khám
                        </td>
                    @endif
                </tr>
            @endforeach
        @endif
    </tbody>
</table>