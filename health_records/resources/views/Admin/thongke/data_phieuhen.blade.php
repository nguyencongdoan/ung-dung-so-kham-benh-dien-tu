<form action="{{route('admin.thongke.export_Excel_PhieuHen')}}" method="POST" id="form-phieuhen">
    @csrf
    <input type="hidden" name="start_day" value="{{$start_day}}">
    <input type="hidden" name="end_day" value="{{$end_day}}">
    <input type="hidden" name="tinhtrang" value="{{$tinhtrang}}">
    <div class="mb-4">
        <button type="button" class="btn btn-outline outline-primary mr-auto d-block" onclick="exportPhieuHen()">
            <i class="uil uil-print"></i>
            Xuất file excel
        </button>
    </div>
</form>
<div class="overflow-scroll pr-2">
    <table class="table mt-3" id="table-phieuhen">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã phiếu hẹn</th>
                <th>Họ tên bệnh nhân</th>
                <th>Tuổi</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
                <th>Ngày hẹn</th>
                <th>Giờ hẹn</th>
                <th>Họ tên Bác sĩ</th>
                <th>Tình trạng</th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($phieuhen->count() > 0)
                @foreach ($phieuhen as $key => $item)
                    <tr>
                        <td> {{ ++$key }} </td>
                        <td> {{ $item->MaPhieuHen }} </td>
                        <td> {{ $item->HoTen_NK }} </td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NgaySinh)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>
                            @if ($item->GioiTinh_NK == 1)
                                Nam
                            @else
                                Nữ
                            @endif
                        </td>
                        <td> {{ $item->SDT }} </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->NgayHen)->format('d-m-Y') }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->GioHen)->format('H:i A') }}
                        </td>
                        <td> {{ $item->BacSi }} </td>
                        @if ($item->TinhTrang_PH == 1)
                            <td style="color: #85d385;">
                                Đã tạo phiếu khám
                            </td>
                        @else
                            <td style="color: red;">
                                Chưa tạo phiếu khám
                            </td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="11" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $phieuhen->links() }}