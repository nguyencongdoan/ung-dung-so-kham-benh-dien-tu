<table class="table mt-3" id="table-phieukham" data-cols-width="5,20,20,20,10,20,15,15,15,20,20">
    <thead>
        <tr data-height="36">
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                STT
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Mã phiếu khám
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Họ tên bệnh nhân
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Họ tên người thân
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Giới tính
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Số điện thoại
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Ngày khám
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Giờ bắt đầu
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Giờ kết thúc
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Họ tên bác sĩ
            </th>
            <th data-f-bold="true" data-a-h="center" data-a-v="middle" data-fill-color="ffdbc7"
                data-f-color="ea4848">
                Tình Trạng
            </th>
        </tr>
    </thead>
    <tbody class="tbody">
        @if ($phieukham->count() > 0)
            @foreach ($phieukham as $key => $item)
                <tr data-height="36">
                    <td data-t="n" data-a-h="center" data-a-v="middle"> {{ ++$key }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle"> {{ $item->MaPhieuKham }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle"> {{ $item->BenhNhan }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        {{ !empty($item->HoTenNguoiThan) ? $item->HoTenNguoiThan : '' }}
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        @if ($item->GioiTinh == 1)
                            Nam
                        @else
                            Nữ
                        @endif
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle" data-num-fmt="0000000000"
                        data-t="n"> {{ $item->SDT }} </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        {{ \Carbon\Carbon::parse($item->NgayKham)->format('d-m-Y') }}
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        {{ !empty($item->ThoiGianBD) ? \Carbon\Carbon::parse($item->ThoiGianBD)->format('H:i A') : '' }}
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        {{ !empty($item->ThoiGianKT) ? \Carbon\Carbon::parse($item->ThoiGianKT)->format('H:i A') : '' }}
                    </td>
                    <td data-a-wrap="true" data-a-h="center" data-a-v="middle"> {{ $item->BacSi }}</td>
                    <td style="color: #85d385;" data-a-wrap="true" data-a-h="center" data-a-v="middle">
                        @if ($item->TinhTrangKham == 1)
                            Đang khám
                        @elseif ($item->TinhTrangKham == 2)
                            Đang tạo đơn thuốc
                        @elseif ($item->TinhTrangKham == 3)
                            Đã khám xong
                        @elseif ($item->TinhTrangKham == 4)
                            Đã tái khám
                        @else
                            <a href="{{ route('admin.phieukham.thongtin_phieukham', $item->MaPhieuKham) }}"
                                class="btn btn-table-details">Vào khám</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>