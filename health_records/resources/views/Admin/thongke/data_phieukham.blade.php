<form action="{{route('admin.thongke.export_Excel_PhieuKham')}}" id="form-phieukham" method="POST">
    @csrf
    <input type="hidden" name="start_day" value="{{$start_day}}">
    <input type="hidden" name="end_day" value="{{$end_day}}">
    <div class="mb-4">
        <button type="button" class="btn btn-outline outline-primary mr-auto d-block" onclick="exportPhieuKham()">
            <i class="uil uil-print"></i>
            Xuất file excel
        </button>
    </div>
</form>
<div class="overflow-scroll pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã phiếu khám</th>
                <th>Họ tên bệnh nhân</th>
                <th>Họ tên người thân</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
                <th>Ngày khám</th>
                <th>Giờ bắt đầu</th>
                <th>Giờ kết thúc</th>
                <th>Họ tên bác sĩ</th>
                <th>Tình trạng</th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($phieukham->count() > 0)
                @foreach ($phieukham as $key => $item)
                    <tr>
                        <td> {{ ++$key }} </td>
                        <td> {{ $item->MaPhieuKham }} </td>
                        <td> {{ $item->BenhNhan }} </td>
                        <td> {{ !empty($item->HoTenNguoiThan) ? $item->HoTenNguoiThan : '' }} </td>
                        <td>
                            @if ($item->GioiTinh == 1)
                                Nam
                            @else
                                Nữ
                            @endif
                        </td>
                        <td> {{ $item->SDT }} </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->NgayKham)->format('d-m-Y') }}
                        </td>
                        <td>
                            {{ !empty($item->ThoiGianBD) ? \Carbon\Carbon::parse($item->ThoiGianBD)->format('H:i A') : '' }}
                        </td>
                        <td>
                            {{ !empty($item->ThoiGianKT) ? \Carbon\Carbon::parse($item->ThoiGianKT)->format('H:i A') : '' }}
                        </td>
                        <td> {{ $item->BacSi }}</td>
                        <td style="color: #85d385;">
                            @if ($item->TinhTrangKham == 1)
                                Đang khám
                            @elseif ($item->TinhTrangKham == 2)
                                Đang tạo đơn thuốc
                            @elseif ($item->TinhTrangKham == 3)
                                Đã khám xong
                            @elseif ($item->TinhTrangKham == 4)
                                Đã tái khám
                            @else
                                <a href="{{ route('admin.phieukham.thongtin_phieukham', $item->MaPhieuKham) }}"
                                    class="btn btn-table-details">Vào khám</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="11" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $phieukham->links() }}
