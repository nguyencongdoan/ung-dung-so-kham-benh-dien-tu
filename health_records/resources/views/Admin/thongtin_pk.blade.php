@extends('layouts.admin')

@section('content')
    <div class="col-12">
        <h1 class="text-center py-5 fs-1 fw-bold" data-aos="fade-down">Thông tin phiếu khám</h1>
        <form action="{{ route('api.admin.createDonThuoc') }}" method="post" class="col-md-6 m-auto mb-5 form-booking" data-aos="fade-up">
            @csrf
            <input type="hidden" name="id" value="{{ $data->MaPhieuKham }}">
            <input type="hidden" name="time_start" class="timestart" value="">
            <div class="row booking m-auto">
                <div class="sophieu d-flex align-items-center justify-content-end">
                    <h3 class="fw-bold fs-5 mt-2">Số phiếu:</h3>
                    <span class="fw-bold fs-5 ms-2 text-dark">{{ $data->SoPhieu }}</span>
                </div>
                <h3 class="fw-bold fs-3 mt-2">Thông tin bệnh nhân</h3>
                <div class="col-12 col-sm-6 my-2">
                    <label for="hoten" class="form-label">Họ tên người khám</label>
                    <input type="text" id="hoten" readonly class="form-control" name="hoten" value="{{ $data->HoTen }}">
                </div>
                <div class="col-12 col-sm-6 my-2">
                    <label for="hoten_nt" class="form-label">Họ tên người thân</label>
                    <input type="text" id="hoten_nt" class="form-control" name="hoten_nt" value="{{ $data->HoTenNguoiThan }}">
                </div>
                <div class="col-12 col-sm-6 my-2">
                    <label for="sdt" class="form-label">Số điện thoại</label>
                    <input type="text" id="sdt" readonly class="form-control" name="sdt" value="{{ $data->SDT }}">
                </div>
                <div class="col-12 col-sm-6 my-2">
                    <label for="cannang" class="form-label">Cân nặng</label>
                    <input type="text" id="cannang" class="form-control" name="cannang" value="{{ $data->CanNang}}">
                </div>
                <div class="col-12 col-sm-6 my-2">
                    <label for="ngaysinh" class="form-label">Ngày sinh</label>
                    <div class="d-flex align-items-center">
                        <input type="date" readonly id="ngaysinh" class="form-control col-8 me-3" name="ngaysinh" value="{{ $data->NgaySinh}}">
                        <span class="age"></span>
                    </div>
                </div>
                <div class="col-12 col-sm-6 my-2">
                    <label for="" class="form-label">Giới tính</label>
                    <div class="form-check">
                        <input type="radio" name="gt" id="gt_nam" class="form-check-input"
                        @if($data->GioiTinh == 1) checked @endif value="1">
                        <label for="gt_nam" class="form-check-label">Nam</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" name="gt" id="gt_nu" class="form-check-input"
                        @if($data->GioiTinh == 0) checked @endif value="0">
                        <label for="gt_nu" class="form-check-label">Nữ</label>
                    </div>
                </div>
                <div class="col-12 my-2">
                    <label for="diachi" class="form-label">Địa chỉ</label>
                    <textarea name="diachi" id="diachi" rows="3" readonly class="form-control">{{ $data->DiaChi }}</textarea>
                </div>
                <h3 class="fw-bold fs-3 mt-2">Thông tin khám</h3>
                <div class="col-12 my-2 d-flex justify-content-between">
                    <p>
                        <label for="hoten" class="form-label">Thời gian bắt đầu:</label>
                        <span class="time_start"></span>
                    </p>
                    <p>
                        <label for="hoten" class="form-label">Thời gian kết thúc:</label>
                        <span class="time_end"></span>
                    </p>
                </div>
                <div class="col-12 my-2">
                    <label for="trieuchung" class="form-label">Triệu chứng</label>
                    <textarea name="trieuchung" id="trieuchung" rows="3" class="form-control">{{$data->TrieuChung ? $data->TrieuChung : ''}}</textarea>
                </div>
                <div class="col-12 my-2">
                    <label for="tiensubenh" class="form-label">Tiền sử bệnh</label>
                    <textarea name="tiensubenh" id="tiensubenh" rows="3" class="form-control">{{$data->TienSuBenh ? $data->TienSuBenh : ''}}</textarea>
                </div>
                <div class="col-12 my-2">
                    <label for="chuandoan" class="form-label">Chuẩn đoán bệnh</label>
                    <textarea name="chuandoan" id="chuandoan" rows="3" class="form-control" required></textarea>
                </div>
                <div class="col-md-12 my-3">
                    <button type="submit" class="btn btn-outline outline-primary m-auto d-block">
                        <i class="uil uil-file-edit-alt"></i> Tạo đơn thuốc
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script>
        datetime = moment().format('YYYY-MM-DD HH:mm:ss');
        year = moment().format('YYYY');
        time_start = moment().format('HH:mm:ss A');
        year_user = $('#ngaysinh').val();
        year_user = moment(year_user).format('YYYY');   
        age = Number(year) - Number(year_user) + 1;
        $('.age').text(age + ' ' + 'tuổi');
        $('.time_start').text(time_start);
        $('.timestart').val(datetime);
        setInterval(() => {
            $('.time_end').text(moment().format('HH:mm:ss A'));
        }, 1000);
    </script>
@endpush