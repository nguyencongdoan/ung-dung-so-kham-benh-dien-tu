@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="col-12">
        <h1 class="fw-bold fs-1 text-center my-5" data-aos="fade-down">Đặt lịch hẹn khám</h1>
        <form action="" method="post" class="col-md-6 m-auto mb-5 form-booking" id="form-booking" novalidate data-aos="fade-up">
            @csrf
            <div class="row booking m-auto">
                <input type="hidden" class="id" name="id" value="null">
                <div class="col-6 my-2">
                    <label for="hoten" class="form-label">Họ tên người đăng ký</label>
                    <input type="text" id="HoTen" class="form-control" name="HoTen" required>
                    <div class="invalid-feedback">
                        Vui lòng nhập họ tên người đăng ký
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" id="email" class="form-control" name="email" required>
                    <div class="invalid-feedback">
                        Vui lòng nhập lại email
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="sdt" class="form-label">Số điện thoại</label>
                    <input type="number" id="SDT" class="form-control" name="SDT" required>
                    <div id="valid-sdt" class="invalid-feedback">
                        Vui lòng nhập số điện thoại
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="hoten" class="form-label">Họ tên người khám</label>
                    <input type="text" id="HoTen_NK" class="form-control" name="HoTen_NK" required>
                    <div class="invalid-feedback">
                        Vui lòng nhập họ tên người khám
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="ngaysinh" class="form-label">Ngày sinh</label>
                    <input type="date" id="NgaySinh" class="form-control" name="NgaySinh" required>
                    <div class="invalid-feedback">
                        Vui lòng chọn ngày sinh
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="" class="form-label">Giới tính</label>
                    <div class="form-check">
                        <input type="radio" name="GioiTinh" id="gt_nam" class="form-check-input" checked
                            value="1">
                        <label for="gt_nam" class="form-check-label">Nam</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" name="GioiTinh" id="gt_nu" class="form-check-input" value="0">
                        <label for="gt_nu" class="form-check-label">Nữ</label>
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="hoten" class="form-label">Địa chỉ</label>
                    <textarea name="DiaChi" id="DiaChi" cols="5" rows="4" class="form-control"></textarea>
                </div>
                <div class="col-6 my-2">
                    <label for="ngaysinh" class="form-label">Chọn bác sĩ</label>
                    <select name="bacsi" id="bacsi" class="form-select select-bs" required>
                        <option value="">- Chọn -</option>
                    </select>
                    <div class="invalid-feedback">
                        Vui lòng chọn bác sĩ
                    </div>
                </div>
                <div class="col-6 my-2 mx-auto relative parent-datepicker">
                    <label for="ngaysinh" class="form-label">Chọn ngày khám</label>
                    <input type='text' id='datepicker' name="NgayKham" placeholder="dd-mm-yyyy" class="form-control"
                        required />
                    <i class="fa-solid fa-calendar-days calendar-icon"></i>
                    <div class="invalid-feedback">
                        Vui lòng chọn ngày khám
                    </div>
                </div>
                <div class="col-6 my-2">
                    <label for="ngaysinh" class="form-label">Chọn giờ khám</label>
                    <select name="gio" id="" class="gio form-select" required>
                        <option value="">- Chọn -</option>
                    </select>
                    <div class="invalid-feedback">
                        Vui lòng chọn giờ khám
                    </div>
                </div>
                <div class="col-md-12 my-3">
                    <button type="submit" class="btn btn-outline outline-primary m-auto d-block btn-form">
                        <i class="fa-solid fa-calendar-check me-2"></i> Đặt lịch khám
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- Modal xác nhận lịch hẹn -->
    <div class="modal fade" id="conformOTP" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content pb-3">
                <div class="container-fluid">
                    <div class="col-md-8 col-11 mt-4 py-4 mx-auto booking">
                        <div class="time mb-3 d-flex justify-content-center fs-4">
                            <div class="time_down me-2">
                                <span class="minus"></span>
                            </div>
                            <div class="time_down">
                                <span class="seconds"></span>
                            </div>
                        </div>
                        <h5 class="text-danger text-center fw-bold">Vui lòng nhập mã OTP để xác nhận lịch hẹn</h5>
                        <div class="d-flex justify-content-center align-items-center mt-4 mx-auto">
                            <div class="col-md-6 col-4 me-2">
                                <input type="text" class="form-control otp">
                            </div>
                            <button type="submit" class="btn btn-outline outline-primary m-0 d-block send-otp">
                                <i class="fa-solid fa-paper-plane me-2"></i> Gửi
                            </button>
                        </div>
                        <p class="fst-italic mt-4 text-danger msg text-center"></p>
                        <p class="fst-italic mt-4">
                            <span class="fw-bold text-danger">Lưu ý:</span>
                            nếu không nhập mã OTP phiếu hẹn sẽ được hủy sau 10 phút
                        </p>
                        <button type="submit" class="btn btn-outline outline-primary mx-auto d-block remove-ph">
                            <i class="uil uil-file-times-alt"></i> Hủy phiếu hẹn
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('./js/jquery.min.js') }}"></script>
    <script src="{{ asset('./js/jquery-ui.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            getBacSi();
            datlich();

            $('#datepicker').datepicker({
                showAnim: 'slide',
                changeMonth: true,
                changeYear: true
            });
            var arr_day = [];

            function getBacSi() {
                $.ajax({
                    url: "{{ route('api.client.bacsi.getAllBacSi') }}",
                    type: "get",
                    dataType: "json",
                    success: function(res) {
                        if (res.status == 200) {
                            _list = res.data;
                            _html = '';
                            _list.forEach(function(item) {
                                _html +=
                                `<option value="${item.id}">BS. ${item.HoTen}</option>`;
                            });
                            $('.select-bs').append(_html);
                        }
                    }
                });
            }

            function datlich() {
                var form = document.getElementById('form-booking');
                $('#form-booking').on('submit', function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    var bool = true;
                    var val_day = $('#datepicker').val();
                    val_day = moment(val_day).format('DD-MM-YYYY');

                    // kiểm tra ngày user chọn với ngày làm việc của bs
                    $.each(arr_day, function(i, e) {
                        item = moment(e).format('DD-MM-YYYY');
                        if (item == val_day) {
                            bool = true;
                            return false;
                        } else {
                            bool = false;
                        }
                    });

                    if (bool) {
                        if (form.checkValidity()) {
                            var formData = $(this).serialize();
                            var sdt = $('#SDT').val();
                            if (sdt.length < 10 || sdt.length > 10) {
                                $('#valid-sdt').text('Số điện thoại không đúng!');
                                $('#valid-sdt').css('display', 'block');
                                $('.calendar-icon').hide();
                                form.classList.add('was-validated');
                            } else {
                                form.classList.remove('was-validated');
                                $('#valid-sdt').css('display', 'none');
                                $('.btn-form').attr('disabled', 'disabled');
                                $('.calendar-icon').show();
                                $.ajax({
                                    url: "{{ route('api.client.datlich') }}",
                                    type: "post",
                                    dataType: "json",
                                    data: formData,
                                    success: function(res) {
                                        toast_message(res);
                                        $('.btn-form').removeAttr('disabled');
                                        if (res.status == 200) {
                                            $(".select-bs").val("");
                                            $('.gio').html('<option value="">- Chọn -</option>');
                                            // reset ngay
                                            $('#datepicker').remove();
                                            _input = ` 
                                                <input type='text' id='datepicker' name="NgayKham" placeholder="dd-mm-yyyy" class="form-control" required/>
                                            `;
                                            $('.parent-datepicker').append(_input);
                                            $('#datepicker').datepicker({
                                                showAnim: 'slide',
                                                changeMonth: true,
                                                changeYear: true
                                            });
                                            setTimeout(() => {
                                                handleOTP(res.id, res.otp);
                                            }, 2000);
                                        }
                                    }
                                });
                            }
                        } else {
                            $('.calendar-icon').hide();
                            form.classList.add('was-validated');
                        }
                    } else {
                        var obj = {
                            status: 400,
                            message: 'Bác sĩ chưa có lịch khám vào ngày: ' + val_day,
                            alert: 'error'
                        };
                        toast_message(obj);
                    }
                });
            }

            function handleOTP(id, otp) {
                $('#conformOTP').modal('show');
                var time_start = '2022-05-12 00:10:00';

                var set_time = setInterval(() => {
                    var time = moment(time_start).subtract(1, 'seconds').format('YYYY-MM-DD HH:mm:ss');
                    minus = moment(time).format('mm');
                    seconds = moment(time).format('ss');

                    $('.minus').text(minus);
                    $('.seconds').text(seconds);

                    time_start = time;

                    if (minus == '00' && seconds <= '05') {
                        $('.seconds').css('color', 'red');
                    }

                    if (time_start == '2022-05-12 00:00:00') {
                        $.ajax({
                            url: "{{ route('api.client.user.removePhieuHen') }}",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                id: id
                            },
                            success: function(res) {
                                toast_message(res);
                                $('#conformOTP').modal('hide');
                                clearInterval(set_time);
                            }
                        });
                    }
                }, 1000);

                var send_otp = $('.send-otp');
                var remove_ph = $('.remove-ph');

                send_otp.on('click', function() {
                    var val_otp = $('.otp').val();
                    if (val_otp == otp) {
                        var msg = {
                            status: 200,
                            message: 'Xác nhận phiếu hẹn thành công!',
                            alert: 'success'
                        }
                        $('.otp').val('');
                        toast_message(msg);
                        $('#conformOTP').modal('hide');
                        clearInterval(set_time);
                    } else {
                        $('.msg').text('Mã OTP không đúng. Vui lòng nhập lại!');
                    }
                });

                remove_ph.on('click', function() {
                    var check = confirm('Bạn có chắc muốn hủy phiếu hẹn không?');
                    remove_ph.attr('disabled', 'disabled');

                    if (check) {
                        $.ajax({
                            url: "{{ route('api.client.user.removePhieuHen') }}",
                            type: 'post',
                            dataType: 'json',
                            data: {
                                id: id
                            },
                            success: function(res) {
                                toast_message(res);
                                remove_ph.removeAttr('disabled');
                                $('.otp').val('');
                                $('#conformOTP').modal('hide');
                                clearInterval(set_time);
                            }
                        });
                    }
                    else {
                        remove_ph.removeAttr('disabled');
                    }
                });
            }

            HighlightTime();

            function HighlightTime() {
                $('#bacsi').on('change', function() {
                    var bs = $(this).val();
                    var date_now = moment().format('YYYY/MM/DD');
                    $('.gio').html('<option value="">- Chọn -</option>');

                    $.ajax({
                        url: "{{ route('api.client.datlich.getDateBS') }}",
                        type: 'GET',
                        data: {
                            id_bs: bs
                        },
                        dataType: 'json',
                        success: function(res) {
                            if (res.status == 200) {
                                // trước khi thay đổi lịch phải remove input $datepicker cũ và add lại
                                // mới thay đổi được ngày
                                $('#datepicker').remove();
                                _input = ` 
                                    <input type='text' id='datepicker' name="NgayKham" placeholder="dd-mm-yyyy" class="form-control" required/>
                                `;
                                $('.parent-datepicker').append(_input);
                                var SelectedDates = {};
                                var date = '';
                                arr_day = [];

                                $.each(res.date, function(key, item) {
                                    // đếm số ngày từ ngày bắt đầu đến ngày kết thúc
                                    var count_day = moment(item.ThoiGianBD).from(moment(
                                        item.ThoiGianKT));

                                    // kiểm tra nếu thời gian từ ngày bd -> ngày kt là 1 ngày thì cho count_day = 1
                                    if (count_day.indexOf("a day ago") != -1 ||
                                        count_day.indexOf("hours") != -1 || count_day
                                        .indexOf("minutes") != -1 || count_day.indexOf(
                                            "seconds") != -1) {
                                        count_day = 1;
                                    } else if (count_day.indexOf("month") != -1 || 
                                        count_day.indexOf("months") != -1) { // nếu là tháng thì số tháng * 30
                                        if(count_day.indexOf("a month ago") != -1) {
                                            count_day = 30;
                                        }
                                        else {
                                            count_day = count_day.match(/\d/g); // xóa ký tự '4 day ago' => 4
                                            count_day = count_day * 30;
                                        }
                                    } else if (count_day.indexOf("year") != -1 ||
                                        count_day.indexOf("years") != -1) { // nếu là tháng thì số năm * 365
                                        if(count_day.indexOf("a year ago") != -1) {
                                            count_day = 365;
                                        }
                                        else {
                                            count_day = count_day.match(/\d/g); // xóa ký tự '4 day ago' => 4
                                            count_day = count_day * 365; // chưa kiểm tra năm nhuận / không nhuận
                                        }
                                    } else { //ngược lại thì xóa ký tự và lấy số 
                                        count_day = count_day.match(/\d/g); // xóa ký tự '4 day ago' => 4
                                    }

                                    if (count_day.length > 1) {
                                        count_day = count_day.join("");
                                    }

                                    var add_day = moment(item.ThoiGianBD);
                                    var date_now = moment().format('YYYY/MM/DD');

                                    // cộng ngày từ ngày bắt đầu -> ngày kết thúc vào mảng
                                    for (var i = 0; i < count_day; i++) {
                                        if (moment(add_day).format('YYYY/MM/DD') >=
                                            date_now) {
                                            arr_day.push(add_day);
                                        }
                                        add_day = moment(add_day).add(1, 'days');
                                    }
                                });

                                // thêm ngày vào danh sách SelectedDates
                                $.each(arr_day, function(key, item) {
                                    date = moment(item).format('YYYY/MM/DD');
                                    SelectedDates[new Date(date)] = new Date(date);
                                });

                                $('#datepicker').datepicker({
                                    showAnim: 'slide',
                                    changeMonth: true,
                                    changeYear: true,
                                    beforeShowDay: function(date) {
                                        var Highlight = SelectedDates[date];
                                        if (Highlight) {
                                            return [true, "Highlighted", Highlight];
                                        } else {
                                            return [false, '', ''];
                                        }
                                    }
                                });
                                getTimeBS();
                            } else {
                                $('#datepicker').attr('disabled', 'disabled');
                                $('#datepicker').val('');
                                toast_message(res);
                            }
                        }
                    });
                });
            }

            function getTimeBS() {
                if ($('#datepicker').val() == '') {
                    $('.gio').html('<option value="">- Chọn -</option>');
                }

                $('#datepicker').on('change', function() {
                    var date = $(this).val();
                    var id_bs = $('#bacsi').val();

                    $.ajax({
                        url: "{{ route('api.client.datlich.getTimeBS') }}",
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            id_bs: id_bs,
                            date: date
                        },
                        success: function(res) {
                            if (res.status == 200) {
                                var _opt = '';
                                var time_now = moment().format('HH:mm');
                                time_start = moment(res.time_start).format('HH:mm');
                                time_end = moment(res.time_end).format('HH:mm');

                                day_start = moment(res.time_start).format('YYYY-MM-DD');
                                day_end = moment(res.time_end).format('YYYY-MM-DD');
                                var day_selected = moment(date).format('YYYY-MM-DD');
                                var day_now = moment().format('YYYY-MM-DD');

                                for (var i = 0; i < 48; i++) {
                                    // kiểm tra nếu thời gian bắt đầu nằm trong khoảng 07:30 -> 11:30 || 13:30 -> 16:30
                                    // thì thêm vào thẻ option
                                    if (time_start > '07:00' && time_start < '12:00' ||
                                        time_start > '13:00' && time_start < '17:00') {
                                        // kiểm tra nếu ngày chọn = ngày hiện tại thì lấy thời gian >= giờ hiện tại
                                        if (day_selected == day_now) {
                                            if (time_start >= time_now) {
                                                _opt += `
                                                    <option value="${time_start}">${time_start}</option>
                                                `;
                                            }
                                        } else {
                                            // nếu giờ phù hợp điều kiện nhưng lại gặp vấn đề giờ kết thúc < giờ bắt đầu
                                            // time_start = 07:30 nhưng time_end = 00:00 vì nếu cùng trong ngày thì time_end luôn > time_start nên trường hợp ở đây là ngày bắt đầu < ngày kết thúc
                                            if (time_start <= time_end || day_start < day_end) {
                                                _opt += `
                                                    <option value="${time_start}">${time_start}</option>
                                                `;
                                            }
                                        }
                                    }
                                    time_start = day_start + ' ' + time_start + ':00';

                                    time_start = moment(time_start).add(30, 'minutes');
                                    time_start = moment(time_start).format('HH:mm');
                                }
                                if (_opt == '') {
                                    _opt += `
                                        <option value="">Hết giờ làm việc</option>
                                    `;
                                }
                                $('.gio').html(_opt);
                            }
                        }
                    })
                });
            }
        });
    </script>
@endpush
