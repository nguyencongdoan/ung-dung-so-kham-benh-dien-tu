@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="booking col-md-5 col-10 py-3 mx-auto" data-aos="fade-up">
                <h1 class="fw-bold text-center">Thông tin hóa đơn</h1>
                <div class="d-flex flex-wrap flex-column">
                    <div class="my-2">
                        <p class="fw-bold d-inline-block">Mã hóa đơn: </p>
                        <span>{{ $hoadon->MaHD }}</span>
                    </div>
                    <div class="my-2">
                        <p class="fw-bold d-inline-block">Họ tên người thanh toán: </p>
                        <span>{{ $hoadon->HoTen }}</span>
                    </div>
                    <div class="my-2">
                        <p class="fw-bold d-inline-block">Tổng tiền: </p>
                        <span class="text-danger fw-bold">{{ number_format($hoadon->TongTien, 0, ',', '.') }} VND</span>
                    </div>
                    <div class="my-2">
                        <p class="fw-bold d-inline-block">Ngày lập: </p>
                        @php
                            $ngaylap = date_create($hoadon->NgayLap);
                            $ngaylap = date_format($ngaylap, 'd-m-Y H:i A');
                        @endphp
                        <span>{{ $ngaylap }}</span>
                    </div>
                    <div class="my-2">
                        <p class="fw-bold d-inline-block">Phương thức thanh toán: </p>
                        <span>{{ $hoadon->PhuongThuc_TT }}</span>
                    </div>
                    @if (!empty($payments->Code_bank))
                        <div class="my-2">
                            <p class="fw-bold d-inline-block">Mã ngân hàng: </p>
                            <span>{{ $payments->Code_bank }}</span>
                        </div>
                    @endif
                    @if (!empty($payments->Code_vnpay))
                        <div class="my-2">
                            <p class="fw-bold d-inline-block">Mã giao dịch: </p>
                            <span>{{ $payments->Code_vnpay }}</span>
                        </div>
                    @endif
                    <div class="my-2">
                        <p class="fw-bold d-inline-block">Tình trạng thanh toán: </p>
                        @if ($hoadon->TinhTrang_TT == 1)
                            <span>đã thanh toán</span>
                        @else
                            <span>chưa thanh toán</span>
                        @endif
                    </div>
                    <div class="my-2">
                        @if (!empty($payments->NoiDung))
                            <p class="fw-bold d-inline-block">Nội dung thanh toán: </p>
                            <span>{{ $payments->NoiDung }}</span>
                        @endif
                    </div>
                    <a href="{{ route('admin.hoadon') }}"
                        class="m-auto mt-3 btn btn-outline outline-primary fw-bold w-fit d-block fw-bold">
                        <i class="uil uil-corner-up-left-alt"></i> Quay lại
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
