@extends('layouts.admin')

@section('content')
    @if (session()->has('message'))
        <input type="hidden" id="message" value="{{ session('message') }}">
        <div id="toast"></div>
    @endif
    <div id="toast" class="d-none"></div>
    <div class="container-fluid">
        <nav aria-label="breadcrumb w-fit" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách hóa dơn</li>
            </ol>
        </nav>
        <section>
            <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách hóa đơn</h1>
            <div class="container-fluid my-5">
                <form action="" method="post" id="formSearch" data-aos="fade-right">
                    @csrf
                    <div class="row align-items-center mb-md-4 mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="mahd" class="form-label-control fw-bold mb-2">Mã hóa đơn</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="mahd">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="start_day" class="form-label-control fw-bold mb-2">Ngày lập (start)</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="start_day">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="end_day" class="form-label-control fw-bold mb-2">Ngày lập (end)</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="end_day">
                        </div>
                    </div>
                </form>
            </div>
            <div class="data_hoadon mb-5" data-aos="fade-up"></div>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            var message = $('#message').val();
            if (message != '') {
                var res = {
                    'status': 400,
                    'message': message,
                    'alert': 'error'
                }
                toast_message(res);
            }
            getAllHoaDon();

            function getAllHoaDon() {
                $.ajax({
                    url: "{{route('api.admin.hoadon.getAllHoaDon')}}",
                    type: 'GET',
                    success: function (res) {
                        $('.data_hoadon').html(res);
                    }
                })
            }
            
            page = 1;
            $('#formSearch').on('keyup', function() {
                searchAjax(page);
            });
            
            $('#formSearch').on('change', function() {
                var start_day = $('input[name="start_day"]').val();
                var end_day = $('input[name="end_day"]').val();

                if(start_day >= end_day && end_day != '' && start_day != '') {
                    var res = {
                        status: 400,
                        message: 'Ngày nhập (start) không được >= ngày nhập (end)!',
                        alert: 'error'
                    }
                    $('#toast').removeClass('d-none');
                    toast_message(res);
                }
                else {
                    searchAjax(page);
                }
            });

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault(); 
                var page = $(this).attr('href').split('page=')[1];
                searchAjax(page);
            });


            function searchAjax(page) {
                var data = $('#formSearch').serialize();
                $.ajax({
                    url: "/api/admin/hoadon/search?page=" + page,
                    type: 'POST',
                    data: data,
                    success: function(res) {
                        $('.data_hoadon').html(res);
                    }
                });
            }
        })

        function check_form(){
            var mahd = $("input[name='mahd']").val();
            var end_day = $("input[name='end_day']").val();
            var start_day = $("input[name='start_day']").val();

            if(mahd == '' && end_day == '' && start_day == ''){
                getAllHoaDon();
            }
        }
    </script>
@endpush
