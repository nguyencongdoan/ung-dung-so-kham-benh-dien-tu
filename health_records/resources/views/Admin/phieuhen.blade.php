@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <nav aria-label="breadcrumb" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách phiếu hẹn</li>
            </ol>
        </nav>
        <div class="mb-5">
            <h1 class="fw-bold fs-1 mb-3 text-center"data-aos="fade-down">Danh sách phiếu hẹn</h1>
            <h2 class="fw-bold mb-3 text-center text-time"data-aos="fade-down"></h2>
            <div class="search-ajax mb-3 flex-wrap" data-aos="fade-right">
                <a href="{{ route('admin.phieuhen.themphieuhen') }}" class="btn btn-outline outline-primary d-block w-fit mt-2 mb-3 mx-3">
                    <i class="uil uil-plus"></i> Thêm phiếu hẹn
                </a>
            </div>
            <div class="container-fluid mb-3" data-aos="fade-right">
                <form action="" method="post" id="formSearch">
                    <div class="row align-items-center mb-md-4 mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="maphieuhen" class="form-label-control fw-bold mb-2">Mã phiếu hẹn</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="maphieuhen">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="hoten" class="form-label-control fw-bold mb-2">Họ tên người hẹn</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="hoten">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="sdt" class="form-label-control fw-bold mb-2">Số điện thoại</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="sdt">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="ngayhen" class="form-label-control fw-bold mb-2">Ngày hẹn khám</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="ngayhen">
                        </div>
                    </div>
                </form>
            </div>
            <div class="data_phieuhen" data-aos="fade-up"></div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        getPhieuHen();
        var check = false;
        var date = moment().format('DD-MM-YYYY');
        $('.text-time').show();
        $('.text-time').html("Ngày: " + date);

        var myInterval = setInterval(() => {
            getPhieuHen();
        }, 5000);

        function remove_phieuhen(e) {
           var check = confirm("Bạn có chắc muốn xóa phiếu hẹn này không?");
           if(check) {
                $.ajax({
                    url: "{{route('api.client.user.removePhieuHen')}}",
                    type: 'post',
                    dataType: 'json',
                    data: {id: e.value},
                    success: function (res) {
                        toast_message(res);
                        getPhieuHen();
                    }
                })
           }
        }

        function getPhieuHen() {
            $.ajax({
                url: "{{ route('api.admin.getAllPhieuHen') }}",
                type: "get",
                success: function(res) {
                    $('.data_phieuhen').html(res);
                }
            });
        }

        function createPhieuKham(e) {
            id = e.value;
            $.ajax({
                url: "{{ route('api.admin.createPhieuKham') }}",
                type: 'post',
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function(res) {
                    console.log(res);
                    toast_message(res);
                    getPhieuHen();
                }
            });
        }

        page = 1;
        searchForm();
        function searchForm() {
            $("input[name='ngayhen']").on('change', function() {
                searchAjax(page);
                check_form();
                ngayhen = $(this).val();
                if (ngayhen != '') {
                    ngayhen = moment(ngayhen).format('DD-MM-YYYY');
                    $('.text-time').show();
                    $('.text-time').html("Ngày: " + ngayhen);
                } else {
                    $('.text-time').html("Ngày: " + date);
                }
            });

            $('#formSearch').on('keyup', function() {
                searchAjax(page);
                $('.text-time').hide();
                check_form();
            });
        }
        
        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });

        function searchAjax(page) {
            var formData = $('#formSearch').serialize();
            $.ajax({
                url: "/api/admin/search/searchPhieuHen?page=" + page,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.data_phieuhen').html(res);
                    clearInterval(myInterval);
                }
            });
        }

        function check_form() {
            var maphieuhen = $("input[name='maphieuhen']").val();
            var hoten = $("input[name='hoten']").val();
            var sdt = $("input[name='sdt']").val();
            var ngayhen = $("input[name='ngayhen']").val();

            if (maphieuhen == '' && hoten == '' && sdt == '' && ngayhen == '') {
                getPhieuHen();
                $('.text-time').show();
                $('.text-time').html("Ngày: " + date);
            }
        }
    </script>
@endpush