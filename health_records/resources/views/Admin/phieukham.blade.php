@extends('layouts.admin')

@section('content')
    <div id="toast"></div>
    <div class="container-fluid">
        <nav aria-label="breadcrumb" data-aos="fade-right">
            <ol class="breadcrumb my-breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Danh sách phiếu khám</li>
            </ol>
        </nav>
        <section class="my-5">
            <h1 class="fw-bold fs-1 mb-3 text-center" data-aos="fade-down">Danh sách phiếu khám</h1>
            <h2 class="fw-bold mb-3 text-center text-time" data-aos="fade-down"></h2>
            <div class="my-5 mx-3" data-aos="fade-right">
                <a href="{{ route('admin.phieukham.themphieukham') }}" class="btn btn-outline outline-primary d-block w-fit">
                    <i class="uil uil-plus"></i> Thêm phiếu khám
                </a>
            </div>
            <div class="container-fluid mb-3" data-aos="fade-right">
                <form action="" method="post" id="formSearch">
                    <div class="row align-items-center mb-md-4 mb-2">
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="maphieukham" class="form-label-control fw-bold mb-2">Mã phiếu khám</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="maphieukham">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="hoten" class="form-label-control fw-bold mb-2">Họ tên bệnh nhân</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="hoten">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="sdt" class="form-label-control fw-bold mb-2">Số điện thoại</label>
                            <input class="form-control" placeholder="Search Here..." type="text" name="sdt">
                        </div>
                        <div class="mb-3 col-6 col-sm-4 col-md-3">
                            <label for="ngaykham" class="form-label-control fw-bold mb-2">Ngày khám</label>
                            <input class="form-control" placeholder="Search Here..." type="date" name="ngaykham">
                        </div>
                    </div>
                </form>
            </div>
            <div class="data_phieukham" data-aos="fade-up"></div>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        getAllPhieuKham();
        var myInterval = setInterval(() => {
            getAllPhieuKham();
        }, 5000);
        var check = false;
        var date = moment().format('DD-MM-YYYY');

        $('.text-time').show();
        $('.text-time').html("Ngày: " + date);

        function getAllPhieuKham() {
            $.ajax({
                url: "{{ route('api.admin.getAllPhieuKham') }}",
                type: "get",
                success: function(res) {
                    $('.data_phieukham').html(res);
                }
            });
        }
        page = 1;

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            searchAjax(page);
        });

        function searchAjax(page) {
            var formData = $('#formSearch').serialize();
            $.ajax({
                url: "/api/admin/search/searchPhieuKham?page=" + page,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.data_phieukham').html(res);
                    clearInterval(myInterval);
                }
            });
        }

        searchForm();
        function searchForm() {
            $("input[name='ngaykham']").on('change', function() {
                searchAjax(page);
                check_form();
                ngaykham = $(this).val();
                if (ngaykham != '') {
                    ngaykham = moment(ngaykham).format('DD-MM-YYYY');
                    $('.text-time').show();
                    $('.text-time').html("Ngày: " + ngaykham);
                } else {
                    $('.text-time').html("Ngày: " + date);
                }
            });

            $('#formSearch').on('keyup', function() {
                searchAjax(page);
                $('.text-time').hide();
                check_form();
            });
        }

        function check_form() {
            var maphieukham = $("input[name='maphieukham']").val();
            var hoten = $("input[name='hoten']").val();
            var sdt = $("input[name='sdt']").val();
            var ngaykham = $("input[name='ngaykham']").val();

            if (maphieukham == '' && hoten == '' && sdt == '' && ngaykham == '') {
                getAllPhieuKham();
                $('.text-time').show();
                $('.text-time').html("Ngày: " + date);
            }
        }
    </script>
@endpush
