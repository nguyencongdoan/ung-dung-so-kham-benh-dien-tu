@extends('layouts.admin')

@section('content')
    <!-- main content start -->
    <!-- content -->
    @if (session()->has('message'))
      <input type="hidden" id="message" value="{{ session('message') }}">
      <div id="toast"></div>
    @endif
    <div class="container-fluid">
        <div class="row px-4 px-sm-0 justify-content-md-center justify-content-start text-center flex-wrap">
            <h1 class="mb-5 fw-bold" data-aos="fade-up">Phòng khám tai mũi họng</h1>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.phieuhen') }}">
                    <img src="{{ asset('./admin/assets/images/donhang.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý phiếu hẹn</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.phieukham.ds_phieutaikham') }}">
                    <img src="{{ asset('./admin/assets/images/donhang.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý phiếu tái khám</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.phieukham') }}">
                    <img src="{{ asset('./admin/assets/images/file.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý phiếu khám</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.donthuoc') }}">
                    <img src="{{ asset('./admin/assets/images/prescription.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý đơn thuốc</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.lichkhambenh') }}">
                    <img src="{{ asset('./admin/assets/images/calendar.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Lịch làm việc</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{route('admin.baiviet')}}">
                    <img src="{{ asset('./admin/assets/images/blog.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý bài viết</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{route('admin.thuoc')}}">
                    <img src="{{ asset('./admin/assets/images/drugs.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý thông tin thuốc</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{route('admin.phieunhapthuoc')}}">
                    <img src="{{ asset('./admin/assets/images/report.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý nhập thuốc</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{route('admin.hoso.benhnhan')}}">
                    <img src="{{ asset('./admin/assets/images/medical-record.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Hồ sơ bệnh nhân</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{route('admin.hoso.nhanvien')}}">
                    <img src="{{ asset('./admin/assets/images/medical-team.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Hồ sơ nhân viên</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{route('admin.hoadon')}}">
                    <img src="{{ asset('./admin/assets/images/invoice (1).png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý hóa đơn</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.thongke') }}">
                    <img src="{{ asset('./admin/assets/images/statistics.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Thống kê</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.changetheme') }}">
                    <img src="{{ asset('./admin/assets/images/web-design.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Quản lý website</h5>
                </a>
            </div>
            <div class="col-sm-5 col-12 my-3 booking card-menu py-3" data-aos="fade-up">
                <a href="{{ route('admin.notify') }}">
                    <img src="{{ asset('./admin/assets/images/text-message.png') }}" class="img-home-1">
                    <h5 class="fs-5 fw-bold mt-3">Gửi SMS, Mail</h5>
                </a>
            </div>
        </div>
    </div>
    <!-- //content -->
    <!-- main content end-->
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            var message = $('#message').val();
            if(message != '') {
              if(message == 'Bạn không có quyền hạn này!') {
                var res = {
                  'status': 400,
                  'message': message,
                  'alert': 'error'
                }
              }
              else {
                var res = {
                  'status': 200,
                  'message': message,
                  'alert': 'success'
                }
              }
              toast_message(res);
            }
        });
    </script>
@endpush
