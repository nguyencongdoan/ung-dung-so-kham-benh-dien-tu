@section('scripts')
    <script src="{{ asset('./js/moment.js') }}"></script> 
    <!-- jquery -->
    <script src="{{ asset('./js/jquery.min.js') }}"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('./js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('./js/toast.js') }}"></script>
    <script src="{{ asset('./client/assets/js/menu.js') }}"></script>
    <script src="{{ asset('./js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('./js/file_preview.js') }}"></script>
    <script>
        window.addEventListener('scroll', function() {
            var header = document.querySelector('header');
            // header.classList.toggle('fixed-top', window.scrollY > 600);
            header.classList.toggle('sticky-top', window.scrollY < 600);
        });
    </script>
    <script src="{{ asset('./js/aos.js') }}"></script>

    <script>
        AOS.init({
            duration: 1000,
            easing: 'linear'
        });
    </script>
@endsection