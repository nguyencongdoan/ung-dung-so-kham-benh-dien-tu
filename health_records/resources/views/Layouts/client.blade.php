<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="og:title" content="Phòng khám tai mũi họng" />
    <meta property="og:image" content="https://phongkham.byethost9.com/client/assets/images/logo.png" />
    <meta property="og:image:width" content="1280" />
    <meta property="og:image:height" content="720" />
    <meta property="og:url" content="https://phongkham.byethost9.com" />
    <title>Home page</title>
    <!-- icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />
    <!-- font family -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('./css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('./client/assets/css/reset.css') }}" />
    <link href="{{ asset('./css/aos.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('./css/jquery-ui.min.css') }}" type="text/css" media="all" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('./css/toast.css') }}" />
    <link rel="stylesheet" href="{{ asset('./client/assets/css/style.css') }}" />
    @yield('style')
</head>

<body>
    @include('layouts.client.header')
    @include('layouts.client.footer')
    @include('layouts.client.scripts')

    @yield('header')

    <main class="overflow-hidden">
        @yield('content')
    </main>

    @yield('footer')

    @yield('scripts')

    @stack('scripts')
</body>

</html>
