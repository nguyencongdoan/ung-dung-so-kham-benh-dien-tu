@section('header')
    <!-- sidebar menu start -->
    <div class="sidebar-menu sticky-sidebar-menu">

      <!-- logo start -->
      <div class="logo" data-aos="fade-down">
        <h1>
          <a href="{{ route('admin.dashboard') }}">
            @php $url = './admin/assets/images/'.$thongtin->Logo_Header_Admin; @endphp
            <img src="{{asset($url)}}" class="w-200" alt="">
          </a>
        </h1>
      </div>

      <div class="logo-icon text-center" data-aos="fade-right">
        <a href="{{ route('admin.dashboard') }}" title="logo">
          <img src="{{asset('./admin/assets/images/speedometer.png')}}" alt="" class="img-home-2">
        </a>
      </div>

      <div class="sidebar-menu-inner">
        <!-- sidebar nav start -->
        <ul class="nav nav-pills nav-stacked custom-nav" data-aos="fade-right">
          <li class="active">
            <a href="{{ route('admin.dashboard') }}">
              {{-- <i class="fa fa-tachometer fs-3 me-2"></i> --}}
              <img src="{{asset('./admin/assets/images/home-page.png')}}" alt="" class="img-home-2">
              <span class="fw-bold">Trang chủ</span>
            </a>
          </li>
          <li class="menu-list">
            <a href="#">
              <img src="{{asset('./admin/assets/images/list.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý <i class="lnr lnr-chevron-right"></i></span>
            </a>
            <ul class="sub-menu-list">
              <li>
                <a href="{{route('admin.hoso.benhnhan')}}">
                  <img src="{{asset('./admin/assets/images/medical-record.png')}}" class="img-home-2">
                  Hồ sơ bệnh nhân
                </a>
              </li>
              <li>
                <a href="{{route('admin.hoso.nhanvien')}}">
                  <img src="{{asset('./admin/assets/images/medical-team.png')}}" class="img-home-2">
                  Hồ sơ nhân viên
                </a>
              </li>
            </ul>
          </li>
          <li class="menu-list">
            <a href="#">
              <img src="{{asset('./admin/assets/images/file.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý phiếu <i class="lnr lnr-chevron-right"></i></span>
            </a>
            <ul class="sub-menu-list">
              <li>
                <a href="{{ route('admin.phieuhen') }}">
                  <img src="{{asset('./admin/assets/images/donhang.png')}}" class="img-home-2">
                  Phiếu hẹn
                </a>
              </li>
              <li>
                <a href="{{ route('admin.phieukham.ds_phieutaikham') }}">
                  <img src="{{asset('./admin/assets/images/donhang.png')}}" class="img-home-2">
                  Phiếu tái khám
                </a>
              </li>
              <li>
                <a href="{{ route('admin.phieukham') }}">
                  <img src="{{asset('./admin/assets/images/file.png')}}" class="img-home-2">
                  Phiếu khám
                </a>
              </li>
            </ul>
          </li>
          <li>
            <a href="{{ route('admin.donthuoc') }}">
              <img src="{{asset('./admin/assets/images/prescription.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý đơn thuốc</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.baiviet') }}">
              <img src="{{asset('./admin/assets/images/blog.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý bài viết</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.thuoc') }}">
              <img src="{{asset('./admin/assets/images/drugs.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý thuốc</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.phieunhapthuoc') }}">
              <img src="{{asset('./admin/assets/images/report.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý nhập thuốc</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.lichkhambenh') }}">
              <img src="{{asset('./admin/assets/images/calendar.png')}}" class="img-home-2">
              <span class="fw-bold">Lịch làm việc</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.hoadon') }}">
              <img src="{{asset('./admin/assets/images/invoice (1).png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý hóa đơn</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.thongke') }}">
              <img src="{{asset('./admin/assets/images/statistics.png')}}" class="img-home-2">
              <span class="fw-bold">Thống kê</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.changetheme') }}">
              <img src="{{asset('./admin/assets/images/web-design.png')}}" class="img-home-2">
              <span class="fw-bold">Quản lý Website</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.notify') }}">
              <img src="{{asset('./admin/assets/images/text-message.png')}}" class="img-home-2">
              <span class="fw-bold">Gửi SMS, Mail</span>
            </a>
          </li>
        </ul>
        <!-- //sidebar nav end -->
        <!-- toggle button start -->
        <a class="toggle-btn">
          <i class="fa fa-angle-double-left menu-collapsed__left"><span>Collapse Sidebar</span></i>
          <i class="fa fa-angle-double-right menu-collapsed__right"></i>
        </a>
        <!-- //toggle button end -->
      </div>
    </div>
    <!-- //sidebar menu end -->
    <!-- header-starts -->
    <div class="header sticky-header">

      <!-- notification menu start -->
      <div class="menu-right">
        <div class="navbar user-panel-top">
          {{-- <a href="#" class="toggle-btn-bar d-none d-sm-block me-auto fs-2" data-aos="fade-right">
            <i class="uil uil-bars text-reb"></i>
          </a> --}}
          <a href="{{route('admin.dashboard')}}" class="mx-auto mb-sm-0 mb-3" data-aos="fade-down">
            @php $url = './admin/assets/images/'.$thongtin->Logo_Header_Admin; @endphp
            <img src="{{asset($url)}}" class="w-200" alt="">
          </a>
          <div class="search-box d-none d-md-block" data-aos="fade-left">
            <form action="#search-results.html" method="get">
              <input class="search-input" placeholder="Search Here..." type="search" id="search">
              <button class="search-submit" value=""><span class="fa fa-search"></span></button>
            </form>
          </div>
          <div class="user-dropdown-details d-flex" data-aos="fade-left">
            <div class="profile_details_left">
              <ul class="nofitications-dropdown d-flex align-items-center">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                      class="fa fa-bell-o"></i><span class="badge blue">3</span></a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="notification_header">
                        <h3>You have 3 new notifications</h3>
                      </div>
                    </li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar1.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>Johnson purchased template</p>
                          <span>Just Now</span>
                        </div>
                      </a>
                    </li>
                    <li class="odd"><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar2.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>New customer registered </p>
                          <span>1 hour ago</span>
                        </div>
                      </a></li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar3.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>Lorem ipsum dolor sit amet </p>
                          <span>2 hours ago</span>
                        </div>
                      </a></li>
                    <li>
                      <div class="notification_bottom">
                        <a href="#all" class="bg-primary">See all notifications</a>
                      </div>
                    </li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                      class="fa fa-comment-o"></i><span class="badge blue">4</span></a>
                  <ul class="dropdown-menu">
                    <li>
                      <div class="notification_header">
                        <h3>You have 4 new messages</h3>
                      </div>
                    </li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar1.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>Johnson purchased template</p>
                          <span>Just Now</span>
                        </div>
                      </a></li>
                    <li class="odd"><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar2.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>New customer registered </p>
                          <span>1 hour ago</span>
                        </div>
                      </a></li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar3.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>Lorem ipsum dolor sit amet </p>
                          <span>2 hours ago</span>
                        </div>
                      </a></li>
                    <li><a href="#" class="grid">
                        <div class="user_img"><img src="{{ asset('./admin/assets/images/avatar1.jpg') }}" alt=""></div>
                        <div class="notification_desc">
                          <p>Johnson purchased template</p>
                          <span>Just Now</span>
                        </div>
                      </a></li>
                    <li>
                      <div class="notification_bottom">
                        <a href="#all" class="bg-primary">See all messages</a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="profile_details">
              <ul class="pl-3">
                <li class="dropdown profile_details_drop">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu3" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="profile_img">
                      {{-- @php $url = "./uploads/images/".Auth::guard('admin')->user()->AnhDaiDien @endphp
                      <img src="{{ asset($url) }}" class="rounded-circle" alt="" /> --}}
                      <img src="{{ asset('./uploads/images/avatar.png') }}" class="rounded-circle" alt="" />
                      <div class="user-active">
                        <span></span>
                      </div>
                    </div>
                  </a>
                  <ul class="dropdown-menu drp-mnu" aria-labelledby="dropdownMenu3">
                    <li class="user-info">
                      <h5 class="user-name mb-2">{{ Auth::guard('admin')->user()->HoTen }}</h5>
                      <span class="status">
                        @if (session()->has('chucvu')) 
                          {{ session('chucvu') }}
                        @endif
                      </span>
                    </li>
                    <li> <a href="#"><i class="lnr lnr-user"></i>Thông tin của tôi</a> </li>
                    <li> <a href="#"><i class="lnr lnr-cog"></i>Cài đặt</a> </li>
                    <li class="logout"> <a href="{{ route('auth.logout.admin') }}"><i class="fa fa-power-off"></i> Logout</a> </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!--notification menu end -->
    </div>
    <!-- //header-ends -->
@endsection