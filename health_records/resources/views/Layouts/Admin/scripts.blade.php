@section('scripts')
    <script src="{{ asset('./admin/assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('./admin/assets/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('./js/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('./admin/assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('./admin/assets/js/scripts.js') }}"></script>
    
    <script src="{{ asset('./js/file_preview.js') }}"></script>

    <!-- close script -->
    <script>
        var closebtns = document.getElementsByClassName("close-grid");
        var i;

        for (i = 0; i < closebtns.length; i++) {
            closebtns[i].addEventListener("click", function() {
                this.parentElement.style.display = 'none';
            });
        }
    </script>
    <!-- //close script -->

    <!-- disable body scroll when navbar is in active -->
    <script>
        var body = jQuery('body');
        var slide = document.querySelector('.sidebar-menu-inner');
        var btn_toggle = document.querySelector('.toggle-btn');

        $('.toggle-btn-bar').click(function() {
            toggleMenu();
        });

        $('.toggle-btn').click(function() {
            toggleMenu();
        });

        function toggleMenu() {
            document.addEventListener("click", function(e) {
                if(!e.target.matches('.sidebar-menu-inner') && !slide.contains(e.target)) {
                    body.addClass('sidebar-menu-collapsed');
                }
                else {
                    body.removeClass('sidebar-menu-collapsed');
                }
            });
        }
    </script>
    <!-- disable body scroll when navbar is in active -->

    <!-- loading-gif Js -->
    <script src="{{ asset('./admin/assets/js/modernizr.js') }}"></script>
    {{-- <script>
    $(window).load(function () {
      // Animate loader off screen
      $(".se-pre-con").fadeOut("slow");;
    });
  </script> --}}
    <!--// loading-gif Js -->

    <script src="{{ asset('./admin/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('./js/toast.js') }}"></script>
    <script src="{{ asset('./js/moment.js') }}"></script> 
    <script src="{{ asset('./js/aos.js') }}"></script>

    <script>
        AOS.init({
            duration: 1000,
            easing: 'linear'
        });
    </script>
    <script>
        $(document).ready(function() {
            // var init = setInterval(() => {
            //     send();
            // }, 3000);

            function send() {
                $.ajax({
                    url: "{{ route('api.admin.sendSMS_TaiKham') }}",
                    type: "GET",
                    dataType: "JSON",
                    success: function(res) {
                    }
                });
            }
        });
    </script>
@endsection
