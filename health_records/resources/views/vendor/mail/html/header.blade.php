<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
            @if (trim($slot) === 'Laravel')
                <img src="https://phongkham.byethost9.com/admin/assets/images/logo_admin.png" class="img-mail mt-3" style="
                background: transparent;">
            @else
                {{ $slot }}
            @endif
        </a>
    </td>
</tr>
