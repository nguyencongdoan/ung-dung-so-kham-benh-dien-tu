@component('mail::message')
    <div class="container">
        <div class="col-9 booking mx-auto my-5 inner-body box">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <div class="fs-4 fw-bold mt-2 mx-auto">
                    <p class="text-center">
                        <img src="https://phongkham.byethost9.com/client/assets/images/location.png" style="max-width: 140px !important">
                    </p>
                    <p class="text-center">Phòng khám tai - mũi - họng</p>
                    <p class="text-center">
                        41A Quang Trung, TP Nha Trang, Khánh Hòa
                    </p>
                </div>
            </div>
            <h3 class="text-center text-success mt-3 fw-bold mx-auto">
                Thông báo
            </h3>
            <p class="text-justify mt-4">{!! $data !!}</p>
        </div>
    </div>
@endcomponent
