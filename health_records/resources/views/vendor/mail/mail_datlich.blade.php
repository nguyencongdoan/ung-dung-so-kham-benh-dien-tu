@component('mail::message')
    <div class="container">
        <div class="col-9 booking mx-auto my-5 inner-body box">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <div class="fs-4 fw-bold mt-2 mx-auto">
                    <p class="text-center">
                        <img src="https://phongkham.byethost9.com/client/assets/images/location.png" style="max-width: 140px !important">
                    </p>
                    <p class="text-center">Phòng khám tai - mũi - họng</p>
                    <p class="text-center">
                        41A Quang Trung, TP Nha Trang, Khánh Hòa
                    </p>
                </div>
            </div>
            <h3 class="text-center text-success mt-3 fw-bold mx-auto">
                Đặt lịch hẹn khám thành công!
            </h3>
            <p class="fw-bold mt-4">Xin chào: {{ $data['hoten'] }}</p>
            <p>Anh/Chị vừa đặt lịch hẹn tại phòng khám của chúng tôi. Anh/Chị vui lòng kiểm tra lại thông tin phiếu hẹn</p>
            <p class="fs-4 fw-bold">Thông tin phiếu hẹn của anh/chị</p>
            <p>
                <span class="text-success fw-bold">Mã phiếu hẹn:</span>
                {{ $data['maphieuhen'] }}
            </p>
            <p>
                <span class="text-success fw-bold">Họ tên:</span>
                {{ $data['hoten'] }}
            </p>
            <p>
                <span class="text-success fw-bold">Số điện thoại:</span>
                {{ $data['sdt'] }}
            </p>
            <p>
                <span class="text-success fw-bold">Ngày hẹn:</span>
                {{ \Carbon\Carbon::parse($data['ngayhen'])->format('d/m/Y') }}
            </p>
            <p>
                <span class="text-success fw-bold">Giờ hẹn:</span>
                {{ \Carbon\Carbon::parse($data['giohen'])->format('H:i A'); }}
            </p>
            <p>
                <span class="text-success fw-bold">Bác sĩ đã hẹn:</span>
                {{ $data['bacsi'] }}
            </p>
            <p>
                <span class="text-danger fw-bold">Mã OTP:</span>
                {{ $data['otp'] }}
            </p>
            <p class="fst-italic">
                <span class="text-danger fw-bold">Lưu ý:</span>
                Anh/Chị vui lòng nhập mã <span class="text-danger fw-bold">OTP </span>
                để xác nhận phiếu hẹn. Nếu anh/chị không nhập mã
                <span class="text-danger fw-bold">OTP </span> phiếu hẹn sẽ được hủy sau 10 phút
            </p>
        </div>
    </div>
@endcomponent
