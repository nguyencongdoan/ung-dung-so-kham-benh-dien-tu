@component('mail::message')
    <div class="container">
        <div class="col-9 booking mx-auto my-5 inner-body box">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <div class="fs-4 fw-bold mt-2 mx-auto">
                    <p class="text-center">
                        <img src="https://phongkham.byethost9.com/client/assets/images/location.png" style="max-width: 140px !important">
                    </p>
                    <p class="text-center">Phòng khám tai - mũi - họng</p>
                    <p class="text-center">
                        41A Quang Trung, TP Nha Trang, Khánh Hòa
                    </p>
                </div>
            </div>
            <h3 class="text-center text-success mt-3 fw-bold mx-auto">
                Hủy phiếu hẹn khám ngày {{ \Carbon\Carbon::parse($data->NgayHen)->format('d/m/Y') }}
            </h3>
            <p class="fw-bold mt-4">Xin chào: Nguyễn Công Đoan</p>
            <p>Thông tin phiếu hẹn <span class="fw-bold text-danger">{{$data->MaPhieuHen}}</span> đã được hủy.</p>
            <p>Nếu có sai xót xin vui lòng đặt lại lịch hẹn khám thêm một lần nữa</p>
            <p>Xin cảm ơn!</p>
        </div>
    </div>
    @component('mail::button', ['url' => 'http://phongkham.byethost9.com/client/datlich', 'color' => 'green'])
        Đặt lại lịch hẹn
    @endcomponent
@endcomponent