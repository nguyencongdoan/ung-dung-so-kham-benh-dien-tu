@component('mail::message')
    <div class="container">
        <div class="col-9 booking mx-auto my-5 inner-body box">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <div class="fs-4 fw-bold mt-2 mx-auto">
                    <p class="text-center">
                        <img src="https://phongkham.byethost9.com/client/assets/images/location.png" style="max-width: 140px !important">
                    </p>
                    <p class="text-center">Phòng khám tai - mũi - họng</p>
                    <p class="text-center">
                        41A Quang Trung, TP Nha Trang, Khánh Hòa
                    </p>
                </div>
            </div>
            <h3 class="text-center text-success mt-3 fw-bold mx-auto">
                Cảm ơn bạn đã đăng ký tại phòng khám tai mũi họng của chúng tôi
            </h3>
            <p class="fs-4 fw-bold mt-4">Thông tin tài khoản của bạn</p>
            <p>
                <span class="text-success fw-bold">Họ tên:</span> 
                {{$data->HoTen}}
            </p>
            <p>
                <span class="text-success fw-bold">Số điện thoại:</span> 
                {{$data->SDT}}
            </p>
            <p>
                <span class="text-success fw-bold">Địa chỉ:</span> 
                {{$data->DiaChi}}
            </p>
            <p>
                <span class="text-success fw-bold">Email:</span> 
                {{$data->email}}
            </p>
            <p class="fst-italic">
                <span class="fw-bold text-danger">Lưu ý:</span>
                Bạn nên đổi mật khẩu thường xuyên để bảo mật tài khoản tốt nhất
            </p>
        </div>
    </div>
    @component('mail::button', ['url' => 'http://phongkham.byethost9.com', 'color' => 'green'])
        Đi đến website
    @endcomponent
@endcomponent
