@if (!empty(session()->get('chucvu')))
    @if (session()->get('chucvu') == 'Admin')
        @php $disabled = ''; @endphp
    @else
        @php $disabled = 'disabled-link'; @endphp
    @endif
@endif
<div class="overflow-scroll pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã nhân viên</th>
                <th>Họ tên nhân viên</th>
                <th>Tuổi</th>
                <th>Số điện thoại</th>
                <th>Giới tính</th>
                <th>Địa chỉ</th>
                <th>Chức vụ</th>
                <th>Chuyên môn</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody_hoso">
            @if ($hoso->count() > 0)
                @foreach ($hoso as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->HoTen }}</td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $datetime = $date->toDateString();
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NgaySinh)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>0{{ number_format($item->SDT, 0, ' ', ' ') }}</td>
                        <td>{{ $item->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</td>
                        <td>
                            <p class="text-hidden w-200">{{ $item->DiaChi }}</p>
                        </td>
                        <td>{{ $item->TenChucVu }}</td>
                        <td>{{ $item->TenChuyenMon }}</td>
                        <td style="color: #85d385;">
                            {{-- <a href="{{ !empty($disabled) ? '#' : route('admin.phieukham.themphieukham', $item->id) }}"
                                class="btn btn-table-details {{ $disabled }}">Chỉnh sửa</a> --}}
                            <a href="{{ route('admin.hoso.nhanvien.edit', $item->id) }}"
                                class="btn btn-table-details">Chỉnh sửa</a>
                            <a href="{{ route('admin.hoso.nhanvien.detail', $item->id) }}"
                                class="btn btn-table-details">Xem chi tiết</a>
                            <a href="{{ route('admin.hoso.nhanvien.delete', $item->id) }}"
                                class="btn btn-table-details" onclick="return confirm('Bạn có chắc muốn xóa nhân viên này không?')">
                                <i class="uil uil-trash-alt text-danger"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $hoso->links() }}
