<div class="overflow-scroll pr-2">
    <table class="table">
        <thead>
            <tr>
                <th>STT</th>
                <th>Tiêu đề</th>
                <th>Tóm tắt</th>
                <th>Thumnail</th>
                <th>Ngày tạo</th>
                <th>Họ tên người viết</th>
                <th>Thể loại</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($baiviet->count() > 0)
                @foreach ($baiviet as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>
                            <p class="text-hidden w-200">
                                {{ $item->TieuDe }}
                            </p>
                        </td>
                        <td>
                            <p class="text-hidden w-200">
                                {{ $item->TomTat }}
                            </p>
                        </td>
                        <td>
                            <p class="text-hidden">
                                {{ $item->Thumnail }}
                            </p>
                        </td>
                        <td>
                            <p class="text-hidden">
                                {{ \Carbon\Carbon::parse($item->NgayTao)->format('d-m-Y') }}
                            </p>
                        </td>
                        <td>
                            <p class="text-hidden">
                                {{ $item->HoTen }}
                            </p>
                        </td>
                        <td>
                            <p class="text-hidden">
                                {{ $item->TenTL }}
                            </p>
                        </td>
                        <td>
                            <a href="{{ route('admin.baiviet.detail', $item->MaBV) }}" 
                                class="btn btn-table-details">Xem chi tiết</a>
                            <a href="{{ route('admin.baiviet.edit', $item->MaBV) }}"
                                class="btn btn-table-details">Sửa</a>
                            <a href="{{ route('admin.baiviet.delete', $item->MaBV) }}"
                                onclick="return confirm('Bạn có chắc muốn xóa bài viết này!')"
                                class="btn btn-table-details btn-remove">
                                <i class="uil uil-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $baiviet->links() }}
