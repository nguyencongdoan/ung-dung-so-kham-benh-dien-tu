<div class="overflow-scroll pr-2">
    <table class="table">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã hóa đơn</th>
                <th>Họ tên người thanh toán</th>
                <th>Tổng tiền</th>
                <th>Ngày lập</th>
                <th>Phương thức thanh toán</th>
                <th>Tình trạng thanh toán</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($hoadon->count() > 0)
                @foreach ($hoadon as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>
                            <p>
                                {{ $item->MaHD }}
                            </p>
                        </td>
                        <td>
                            <p>
                                {{ $item->HoTen }}
                            </p>
                        </td>
                        <td>
                            <p class="text-danger fw-bold">
                                {{ number_format($item->TongTien, 0, ',', '.')}} VND
                            </p>
                        </td>
                        <td>
                            <p>{{ \Carbon\Carbon::parse($item->NgayLap)->format('d-m-Y') }}</p>
                        </td>
                        <td>
                            <p>{{ $item->PhuongThuc_TT }}</p>
                        </td>
                        <td>
                            <p>{{ $item->TinhTrang_TT == 1 ? 'Đã thanh toán.' : 'Chưa thanh toán.' }}</p>
                        </td>
                        <td>
                            <a href="{{ route('admin.hoadon.detail', $item->MaHD) }}" class="btn btn-table-details">Xem
                                chi tiết</a>
                            <a href="{{ route('admin.hoadon.delete', $item->MaHD) }}"
                                onclick="return confirm('Bạn có chắc muốn xóa hóa đơn này!')"
                                class="btn btn-table-details btn-remove">
                                <i class="uil uil-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $hoadon->links() }}
