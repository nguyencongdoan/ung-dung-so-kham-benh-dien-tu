<div class="overflow-scroll pr-2 col-12">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã đơn thuốc</th>
                <th>Họ tên bệnh nhân</th>
                <th>Họ tên người thân</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
                <th>Ngày tái khám</th>
                <th>Họ tên bác sĩ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($phieutaikham->count() > 0)
                @foreach ($phieutaikham as $key => $item)
                    <tr>
                        <td> {{ ++$key }} </td>
                        <td> {{ $item->MaDonThuoc }} </td>
                        <td> {{ $item->BenhNhan }} </td>
                        <td> {{ $item->HoTenNguoiThan != null ? $item->HoTenNguoiThan : '' }} </td>
                        <td> {{ $item->GioiTinh == 1 ? 'Nam' : 'Nữ' }} </td>
                        <td> 0{{ number_format($item->SDT, 0, ' ', ' ') }} </td>
                        <td> {{ \Carbon\Carbon::parse($item->NgayTaiKham)->format('d-m-Y') }} </td>
                        <td> {{ $item->BacSi }}</td>
                        <td style="color: #85d385;">
                            @if ($item->TinhTrangKham == 4)
                                Đã tạo phiếu tái khám
                            @else
                                <button type="button" value="{{ $item->MaDonThuoc }}" onclick="createPhieuTaiKham(this)" class="btn btn-table-create">
                                    Tạo phiếu tái khám
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $phieutaikham->links() }}
