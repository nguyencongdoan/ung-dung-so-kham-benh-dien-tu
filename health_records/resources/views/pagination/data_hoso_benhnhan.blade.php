<div class="overflow-scroll pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã bệnh nhân</th>
                <th>Họ tên bệnh nhân</th>
                <th>Tuổi</th>
                <th>Số điện thoại</th>
                <th>Giới tính</th>
                <th>Địa chỉ</th>
                <th>Cân nặng</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody_hoso">
            @if ($hoso->count() > 0)
                @foreach ($hoso as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->HoTen }}</td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $datetime = $date->toDateString();
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NgaySinh)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>0{{ number_format($item->SDT, 0, ' ', ' ') }}</td>
                        <td>{{ $item->GioiTinh == 1 ? 'Nam' : 'Nữ' }}</td>
                        <td>{{ $item->DiaChi }}</td>
                        <td>{{ $item->CanNang }}</td>
                        <td style="color: #85d385;">
                            <a href="{{route('admin.phieukham.themphieukham', $item->id)}}" class="btn btn-table-details">Tạo phiếu khám</a>
                            <a href="{{route('admin.hoso.benhnhan.detail', $item->id)}}" class="btn btn-table-details">Xem chi tiết</a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $hoso->links() }}
