<div class="overflow-scroll pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã phiếu nhập</th>
                <th>Ngày nhập</th>
                <th>Họ tên người nhập</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($phieunhap->count() > 0)
                @foreach ($phieunhap as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->MaPhieuNhap }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->NgayNhap)->format('d-m-Y') }}</td>
                        <td>{{ $item->HoTen }}</td>
                        <td>
                            <a href="{{ route('admin.phieunhapthuoc.edit', $item->MaPhieuNhap) }}"
                                class="btn btn-table-details mx-2">
                                <i class="uil uil-pen"></i>
                            </a>
                            <button type="button" onclick="remove_thuoc(this)" value="{{ $item->MaPhieuNhap }}"
                                class="btn btn-table-details ms-0">
                                <i class="uil uil-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="11" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $phieunhap->links() }}
