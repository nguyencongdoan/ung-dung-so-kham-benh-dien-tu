<div class="overflow-scroll pr-2">
    <table class="table">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã phiếu hẹn</th>
                <th>Họ tên người hẹn</th>
                <th>Họ tên bệnh nhân</th>
                <th>Tuổi</th>
                <th>Giới tính</th>
                <th>Số điện thoại</th>
                <th>Ngày hẹn</th>
                <th>Giờ hẹn</th>
                <th>Họ tên Bác sĩ</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($phieuhen->count() > 0)
                @foreach ($phieuhen as $key => $item)
                    <tr>
                        <td> {{ ++$key }} </td>
                        <td> {{ $item->MaPhieuHen }} </td>
                        <td> {{ $item->HoTen }} </td>
                        <td> {{ $item->HoTen_NK }} </td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $datetime = $date->toDateString();
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NamSinh_NK)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>
                            @if ($item->GioiTinh_NK == 1)
                                Nam
                            @else
                                Nữ
                            @endif
                        </td>
                        <td> 0{{ number_format($item->SDT, 0, ' ', ' ') }} </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->NgayHen)->format('d-m-Y') }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->GioHen)->format('H:i A') }}
                        </td>
                        <td> {{ $item->BacSi }} </td>
                        <td style="color: #85d385;">
                            @if ($item->TinhTrang_PH == 1)
                                Đã tạo phiếu khám
                            @else
                                <button type="button" value="{{ $item->MaPhieuHen }}" onclick="createPhieuKham(this)" id="btn_create" class="btn btn-table-details">Tạo phiếu khám</button>
                            @endif
                            <a href="{{ route('admin.phieuhen.edit_phieuhen', $item->MaPhieuHen) }}"
                                class="btn btn-table-details mx-2">
                                <i class="uil uil-pen"></i>
                            </a>
                            <button type="button" onclick="remove_phieuhen(this)" value="{{ $item->MaPhieuHen }}"
                                class="btn btn-table-details ms-0">
                                <i class="uil uil-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="11" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $phieuhen->links() }}