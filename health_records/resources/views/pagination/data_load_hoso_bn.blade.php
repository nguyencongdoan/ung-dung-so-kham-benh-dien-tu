<div class="overflow-scroll my-3 pr-2">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã đơn thuốc</th>
                <th>Mã phiếu khám</th>
                <th>Họ tên bệnh nhân</th>
                <th>Tuổi</th>
                <th>Số điện thoại</th>
                <th>Ngày khám</th>
                <th>Triệu chứng</th>
                <th>Chuẩn đoán bệnh</th>
                <th>Ngày tái khám</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody_hoso">
            @if ($hoso->count() > 0)
                @foreach ($hoso as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->MaDonThuoc }}</td>
                        <td>{{ $item->MaPhieuKham }}</td>
                        <td>{{ $item->HoTen }}</td>
                        <td>
                            @php
                                $date = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
                                $datetime = $date->toDateString();
                                $year = $date->format('Y');
                                $year_bn = \Carbon\Carbon::parse($item->NgaySinh)->format('Y');
                                $age = (int) $year - (int) $year_bn + 1;
                            @endphp
                            {{ $age }} tuổi
                        </td>
                        <td>0{{ number_format($item->SDT, 0, ' ', ' ') }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->NgayKham)->format('d-m-Y') }}</td>
                        <td>{{ $item->TrieuChung }}</td>
                        <td>{{ $item->ChuanDoanBenh }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->NgayTaiKham)->format('d-m-Y') }}</td>
                        <td style="color: #85d385;">
                            <button type="button" value="{{ $item->MaDonThuoc }}"
                                class="btn btn-outline outline-primary d-block" onclick="load_modal_donthuoc(this)">
                                <i class="uil uil-file-edit-alt"></i> Xem toa thuốc
                            </button>
                            <input type="hidden" id="madonthuoc" value="{{ $item->MaDonThuoc }}">
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="11" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $hoso->links() }}
