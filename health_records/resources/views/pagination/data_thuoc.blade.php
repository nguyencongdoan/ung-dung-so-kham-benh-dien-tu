<div class="overflow-scroll pr-2 col-12">
    <table class="table mt-3">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã thuốc</th>
                <th>Tên thuốc</th>
                <th>Số lượng</th>
                <th>Đơn giá</th>
                <th>Ngày sản xuất</th>
                <th>Hạn sử dụng</th>
                <th>Thông tin thuốc</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($thuoc->count() > 0)
                @foreach ($thuoc as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->MaThuoc }}</td>
                        <td class="text-start">{{ $item->TenThuoc }}</td>
                        <td>{{ $item->SoLuong }}</td>
                        <td>
                            <p class="text-danger fw-bold">
                                {{ number_format($item->DonGia, 0, ',', '.')}} VND
                            </p>    
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->NSX)->format('d-m-Y') }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($item->HSD)->format('d-m-Y') }}
                        </td>
                        <td>
                            <p class="text-hidden w-200">{{ $item->ThongTinThuoc }}</p>
                        </td>
                        <td>
                            <a href="{{ route('admin.thuoc.edit_thuoc', $item->MaThuoc) }}"
                                class="btn btn-table-details mx-2">
                                <i class="uil uil-pen"></i>
                            </a>
                            <button type="button" onclick="remove_thuoc(this)" value="{{ $item->MaThuoc }}"
                                class="btn btn-table-details ms-0">
                                <i class="uil uil-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $thuoc->links() }}
