<div class="overflow-scroll pr-2">
    <table class="table">
        <thead>
            <tr>
                <th>STT</th>
                <th>Mã đơn thuốc</th>
                <th>Họ tên bệnh nhân</th>
                <th>Họ tên bác sĩ</th>
                <th>Chuẩn đoán bệnh</th>
                <th>Ngày tạo đơn</th>
                <th>Ngày tái khám</th>
                <th></th>
            </tr>
        </thead>
        <tbody class="tbody">
            @if ($donthuoc->count() > 0)
                @foreach ($donthuoc as $key => $item)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{ $item->MaDonThuoc }}</td>
                        <td>{{ $item->BenhNhan }}</td>
                        <td>{{ $item->BacSi }}</td>
                        <td>{{ $item->ChuanDoanBenh }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->NgayTaoDon)->format('d-m-Y') }}</td>
                        <td>{{ \Carbon\Carbon::parse($item->NgayTaiKham)->format('d-m-Y') }}</td>
                        <td>
                            <a href="{{ route('admin.donthuoc.detailDonThuoc', $item->MaDonThuoc,) }}" class="btn btn-table-details">Xem chi tiết</a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">
                        <img src="{{ asset('./admin/assets/images/thongbao.png') }}" class="img-search">
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
{{ $donthuoc->links() }}