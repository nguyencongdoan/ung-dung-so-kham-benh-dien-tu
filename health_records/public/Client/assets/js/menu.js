function handleToggleMenu() {
    const menu = document.querySelector('.btn-toggle-profile');
    const profile = document.querySelector('.users-left');

    if(!menu || !profile) return; 
    
    menu.addEventListener("click", function() {
        profile.classList.add("active");
    });
    document.addEventListener("click",function(e) {
        if(!profile.contains(e.target) && !e.target.matches(".btn-toggle-profile")) {
            profile.classList.remove("active");
        }
    });
}

handleToggleMenu();