function toast_message(res) {
    const main = document.getElementById("toast");
    if (main) {
        const toast = document.createElement("div");

        // Auto remove toast
        const autoRemoveId = setTimeout(function () {
            main.removeChild(toast);
        }, 3000 + 1000);

        // Remove toast when clicked
        toast.onclick = function (e) {
            if (e.target.closest(".toast__close")) {
                main.removeChild(toast);
                clearTimeout(autoRemoveId);
            }
        };
        toast.classList.add("toast", `toast--${res.alert}`);
        toast.style.animation = `slideInLeft ease .3s, fadeOut linear 1s 5s forwards`;

        title = "Thông báo!";
        if (res.status == "200") {
            icon = '<i class="uil uil-check-circle"></i>';
        } else {
            icon = '<i class="uil uil-exclamation-triangle"></i>';
        }

        toast.innerHTML = `
          <div class="toast__icon">
              ${icon}
          </div>
          <div class="toast__body">
              <h3 class="toast__title">${title}</h3>
              <p class="toast__msg">${res.message}</p>
          </div>
          <div class="toast__close">
              <i class="uil uil-times"></i>
          </div>
          `;
        main.appendChild(toast);
    }
}
