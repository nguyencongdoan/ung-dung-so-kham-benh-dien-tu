function filePreview(className, inputFile) {
    if (inputFile.files && inputFile.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(`.${className}`).attr('src', e.target.result);
            $(`.${className}`).addClass(className);
        };
        reader.readAsDataURL(inputFile.files[0]);
    }
}