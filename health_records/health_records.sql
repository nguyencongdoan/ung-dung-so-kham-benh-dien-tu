-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2022 at 06:44 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `health_records`
--

-- --------------------------------------------------------

--
-- Table structure for table `baiviet`
--

CREATE TABLE `baiviet` (
  `MaBV` bigint(20) UNSIGNED NOT NULL,
  `MaTL` bigint(20) UNSIGNED NOT NULL,
  `MaNV` bigint(20) UNSIGNED NOT NULL,
  `TieuDe` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TomTat` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Thumnail` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NgayTao` date NOT NULL,
  `NoiDung` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `baiviet`
--

INSERT INTO `baiviet` (`MaBV`, `MaTL`, `MaNV`, `TieuDe`, `TomTat`, `Thumnail`, `NgayTao`, `NoiDung`, `created_at`, `updated_at`) VALUES
(6, 1, 1, 'Các bệnh thường gặp ở tai mũi họng', 'Tai mũi họng là nhóm bệnh lý thường gặp ở mọi lứa tuổi, đặc biệt là ở trẻ em. Thay đổi thời tiết, khí hậu, vệ sinh môi trường, khói bụi...là những yếu tố chính dẫn gây bệnh. Các bệnh lý về tai mũi họng thường hay gặp như: Viêm tai giữa, viêm họng, viêm xoang, viêm amidan, viêm mũi dị ứng.', '1654245713.jpg', '2022-06-03', '<h2 style=\"text-align: justify; font-family: Helvetica, Arial, san-serif; line-height: 1.3; color: rgb(51, 51, 51); margin-bottom: 10px; font-size: 27px; background-color: rgb(255, 255, 255);\">1. Viêm tai giữa</h2><div class=\"rich-text\" style=\"color: rgb(51, 51, 51); font-family: Helvetica, Arial, san-serif; font-size: 15px; background-color: rgb(255, 255, 255);\"><p style=\"margin-bottom: 15px;\"></p><p style=\"text-align: justify; margin-bottom: 15px;\"><a href=\"https://www.vinmec.com/vi/benh/viem-tai-giua-cap-tinh-3268/\" class=\"link-in-body\" style=\"color: rgb(0, 102, 166); text-decoration: none; transition-duration: 0.1s;\"><span class=\"link-in-body\" href=\"https://www.vinmec.com/vi/benh/viem-tai-giua-cap-tinh-3268/\" style=\"font-weight: 700;\">Viêm tai giữa</span></a>&nbsp;thường được gây ra bởi nhiễm các tác nhân gây bệnh do virus, vi khuẩn hay nấm. Vi khuẩn gây bệnh&nbsp;<span style=\"font-weight: 700;\">viêm tai giữa</span>&nbsp;thường gặp nhất là Streptococcus pneumoniae. Những trường hợp khác bao gồm Pseudomonas aeruginosa, Haemophilus influenzae và Moraxella catarrhalis. Trong số các thanh thiếu niên lớn tuổi hơn và người lớn trẻ tuổi, nguyên nhân phổ biến nhất của bệnh nhiễm trùng tai là Haemophilus influenzae.</p><p style=\"text-align: justify; margin-bottom: 15px;\">Các loại virus như&nbsp;<span style=\"font-weight: 700;\">virus hợp bào hô hấp</span>&nbsp;(RSV) và những loại gây ra cảm lạnh thông thường cũng có thể dẫn đến viêm tai giữa bằng cách làm tổn hại đến hệ thống phòng thủ bình thường của các tế bào biểu mô đường hô hấp trên.</p><p style=\"text-align: justify; margin-bottom: 15px;\"><span style=\"font-weight: 700;\">Viêm tai giữa</span>&nbsp;tiết dịch hay gặp ở trẻ em, tỷ lệ mắc chiếm khoảng 10 – 20% tuổi nhà trẻ, mẫu giáo. Thời gian giao mùa sang mùa thu, tầm tháng 9, tháng 10 là thời điểm trẻ dễ bị mắc bệnh nhất. Bệnh để lại nhiều biến chứng nguy hiểm cho sức khỏe.</p><p style=\"text-align: justify; margin-bottom: 15px;\">Triệu chứng điển hình của&nbsp;<span style=\"font-weight: 700;\">viêm tai giữa</span>&nbsp;như đau tai, sốt cao, đau nhức đầu, trẻ quấy khóc, chán ăn, tiêu chảy, ói mửa, kém phản ứng với âm thanh. Nguy hiểm hơn bệnh có thể gây biến chứng thủng màng nhĩ khiến người bệnh mất thính lực hoàn toàn. Để phòng ngừa căn bệnh này, cách tốt nhất là giữ ấm cơ thể khi trời lạnh, tránh xa khói thuốc lá, trẻ nhỏ cần phải được bú mẹ ít nhất 6 tháng đầu đời, giữ trẻ ở tư thế thẳng lưng khi cho trẻ bú bình đồng thời cho bé tiêm phòng đầy đủ theo lịch tiêm chủng của quốc gia.</p></div><h2 style=\"text-align: justify; font-family: Helvetica, Arial, san-serif; line-height: 1.3; color: rgb(51, 51, 51); margin-bottom: 10px; font-size: 27px; background-color: rgb(255, 255, 255);\">2. Viêm họng</h2><div class=\"rich-text\" style=\"color: rgb(51, 51, 51); font-family: Helvetica, Arial, san-serif; font-size: 15px; background-color: rgb(255, 255, 255);\"><p style=\"text-align: justify; margin-bottom: 15px;\"><span style=\"font-weight: 700;\">Viêm họng</span>&nbsp;là một trong những bệnh lý tai mũi họng dễ gặp nhất. Căn bệnh này được chia thành 3 loại là&nbsp;<span style=\"font-weight: 700;\">viêm họng trắng</span>,&nbsp;<span style=\"font-weight: 700;\">viêm họng đỏ</span>&nbsp;và&nbsp;<span style=\"font-weight: 700;\">viêm họng loét</span>.&nbsp;<span style=\"font-weight: 700;\">Viêm xoang</span>&nbsp;rất dễ gặp khi thời tiết chuyển mùa, đặc biệt khi tiết trời bắt đầu trở lạnh. Bệnh có thể gặp ở mọi lứa tuổi và nhanh tiến triển, nếu không được xem xét điều trị đúng có thể gây những biến chứng khó lường. Nếu bạn bị viêm xoang thường có biểu hiện đau nhức (vùng má, lông mày, giữa hai mắt, vùng gáy...), dịch nhầy chảy ra phía mũi, nghẹt mũi, điếc mũi...</p></div><figure class=\"post-image full\" style=\"margin: 10px auto; max-width: 420px; text-align: center; color: rgb(51, 51, 51); font-family: Helvetica, Arial, san-serif; font-size: 15px; background-color: rgb(255, 255, 255);\"><div style=\"text-align: justify;\"><img alt=\"Các bệnh thường gặp ở tai mũi họng\" class=\"full uploaded img-in-body\" data-src=\"https://vinmec-prod.s3.amazonaws.com/images/20190927_122818_215705_hqdefault.max-1800x1800.jpg\" src=\"https://vinmec-prod.s3.amazonaws.com/images/20190927_122818_215705_hqdefault.max-1800x1800.jpg\" lazy=\"loaded\" style=\"border: 0px; opacity: 1; border-radius: 5px; width: 420px;\"></div><figcaption class=\"caption\" style=\"font-size: 12px; margin-top: 10px; margin-right: auto; margin-left: auto;\"><div class=\"rich-text\" style=\"text-align: justify; \">Viêm họng là một trong những bệnh lý tai mũi họng dễ gặp nhất</div></figcaption></figure><div class=\"rich-text\" style=\"color: rgb(51, 51, 51); font-family: Helvetica, Arial, san-serif; font-size: 15px; background-color: rgb(255, 255, 255);\"><p style=\"text-align: justify; margin-bottom: 15px;\">Nguyên nhân gây bệnh&nbsp;<span style=\"font-weight: 700;\">viêm họng</span>&nbsp;phần lớn là do virut gây ra, một số trường hợp có thể bị bệnh do vi khuẩn khi gặp phải các yếu tố thuận lợi như thay đổi thời tiết, khói bụi, sức đề kháng kém...</p><p style=\"text-align: justify; margin-bottom: 15px;\">Triệu chứng của<span style=\"font-weight: 700;\">&nbsp;viêm họng</span>&nbsp;là&nbsp;<span style=\"font-weight: 700;\">viêm đỏ niêm mạc họng màn hầu</span>, trụ trước, trụ sau amidan, thành sau họng. Có thể có giả mạc ở họng và amidan, đau rát họng, khát nước, đau mình mẩy. Hạch viêm vùng góc hàm, sốt, ớn lạnh, nhức đầu.</p></div>', '2022-06-03 01:41:53', '2022-06-03 01:41:53'),
(7, 1, 1, 'Những căn bệnh về tai mũi họng phổ biến nhất', 'Khí hậu khắc nghiệt là một trong những nguyên do khiến các bệnh về tai mũi họng có dịp hoành hành ở nước ta. Đây là một căn bệnh nguy hiểm và thường được xuất hiện ở đối tượng trẻ nhỏ, trẻ sơ sinh. Tìm hiểu đặc trưng bệnh, cách phòng và chữa như thế nào chính là điều mà bất kỳ ai cũng phải quan tâm.', '1654245791.jpg', '2022-06-03', '<div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\"><b style=\"font-weight: bold;\">Viêm mũi dị ứng</b><br><br></div><div style=\"background-color: rgb(255, 255, 255); text-align: justify;\"><font color=\"#363636\" face=\"Roboto\"><span style=\"font-size: 14px;\">Nguyên nhân dẫn đến bệnh viêm mũi dị ứng có thể là do thời tiết, môi trường ẩm mốc hoặc do cơ địa của mỗi người…Khách quan mà nói, đây là một căn bệnh không ảnh hưởng nghiêm trọng đến sức khỏe, tuy nhiên nó lại khiến cho mọi người cảm thấy khó chịu và bị ảnh hưởng khá nhiều đến cuộc sống hằng ngày. Vậy nên, muốn phòng tránh bệnh viêm mũi dị ứng, cách tốt nhất bạn nên hạn chế đến mức tối đa các tác nhân gây bệnh.&nbsp;</span></font><br><br><div style=\"text-align: center;\"><font color=\"#363636\" face=\"Roboto\"><span style=\"font-size: 14px; border-style: initial; border-color: initial; border-image: initial; margin-right: auto; margin-left: auto;\"><img alt=\"\" src=\"http://www.benhvientanhung.com/Uploads/Image/benh-tai-mui-hong-1.jpg\" style=\"text-align: justify; border: 0px; display: block; margin: 15px auto;\"></span></font><div style=\"text-align: justify;\"><font color=\"#363636\" face=\"Roboto\"><span style=\"font-size: 14px;\"><br></span></font></div></div><br></div><div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\"><b style=\"font-weight: bold;\">Viêm Amidan</b><br><br></div><div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\">Dấu hiệu nhận biết viêm amidan đó là đau họng, nuốt khó, khạc đờm hay sốt. Tình trạng này còn bị nặng hơn mỗi khi amidan bị sưng lớn hoặc viêm nặng. Và còn tùy vào mức độ phát triển của bệnh tật cũng như các bác sĩ mà đưa ra phương pháp điều trị sao cho hợp lý nhất. Trên thực tế, đã có khá nhiều trường hợp bị viêm amidan nặng và phải cắt bỏ amidan. Thực hiện kỹ thuật này không hề gây ảnh hưởng gì đến sức khỏe của mọi người, do vậy mà mọi người có thể an tâm nhé!<br><br></div><div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\"><b style=\"font-weight: bold;\">Viêm xoang</b><br><br></div><div style=\"background-color: rgb(255, 255, 255); text-align: justify;\"><font color=\"#363636\" face=\"Roboto\"><span style=\"font-size: 14px;\">Nguyên nhân xảy ra căn bệnh này có thể là do sự viêm nhiễm niêm mạc ở trong xoang, kết quả dẫn đến tình trạng tắc nghẽn ở các lỗ thông xoang. Thông thường, viêm xoang được chia làm 2 loại chính là viêm xoang cấp tính, viêm xoang mạn tính, trước khi điều trị các bác sĩ sẽ kiểm tra cẩn thận để đánh giá mức độ viêm xoang như thế nào, từ đó đưa ra phương pháp giải quyết.&nbsp;</span></font><br><br><div style=\"text-align: center;\"><font color=\"#363636\" face=\"Roboto\"><span style=\"font-size: 14px; border-style: initial; border-color: initial; border-image: initial; margin-right: auto; margin-left: auto;\"><img alt=\"\" src=\"http://www.benhvientanhung.com/Uploads/Image/benh-tai-mui-hong-2.jpg\" style=\"text-align: justify; border: 0px; display: block; margin: 15px auto; width: 650px; height: 366px;\"></span></font><div style=\"text-align: justify;\"><font color=\"#363636\" face=\"Roboto\"><span style=\"font-size: 14px;\"><br></span></font></div></div><b style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; font-weight: bold;\"><br></b></div><div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\"><b style=\"font-weight: bold;\">Viêm tai giữa</b><br><br></div><div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\">Căn bệnh này có thể xảy ra với bất kỳ đối tượng nào, nhưng phổ biến nhất vẫn là trẻ nhỏ dưới 4 tuổi. Viêm tai giữa có bệnh lý đơn giản, song nếu không được điều trị kịp thời thì nó có thể dẫn đến các biến chứng vô cùng nguy hiểm như viêm xương chũm, điếc…<br><br></div><div style=\"color: rgb(54, 54, 54); font-family: Roboto; font-size: 14px; background-color: rgb(255, 255, 255); text-align: justify;\">Vì vậy, khi không may mắc phải căn bệnh này thì bạn cần nhanh chóng đến gặp bác sĩ để được thăm khám và điều trị kịp thời. Bạn cũng cần phải tuân thủ theo đúng hướng dẫn của bác sĩ về việc sử dụng thuốc để đảm bảo bệnh được điều trị dứt điểm và phòng tránh nguy cơ tái phát một cách triệt để.</div>', '2022-06-03 01:43:11', '2022-06-03 01:43:11'),
(9, 2, 1, 'CÁC BỆNH GÂY VIÊM ĐƯỜNG HÔ HẤP Ở TRẺ EM VÀ CÁC BIẾN CHỨNG NGUY HIỂM', 'Sau khi chào đời, hệ hô hấp của các cháu hoạt động tương đối hoàn chỉnh cung cấp oxy cho phổi, não đồng thời trao đổi khí vào phổi và từ phổi ra ngoài, giúp các cháu khỏe mạnh, phát triển tâm sinh lý tốt.\r\nBao quanh vùng mũi họng có các cơ quan tham gia chống nhiễm khuẩn bảo vệ cơ thể là Amidan, VA và các xoang.\r\nỞ những năm đầu đời do hệ miễn dịch ở các cháu chưa phát triển đầy đủ nên các cháu rất dễ bị các bệnh liên quan đến bệnh viêm đường hô hấp. Hệ hô hấp bao gồm miệng, mũi, vòm mũi họng, họng, thanh quản, khí quản, phế quản, tiểu phế quản và phế nang. Bệnh đường hô hấp là bệnh có thể xảy ra ở đường hô hấp. Để định bệnh và chữa trị người ta chia bệnh đường hô hấp trên và bệnh dường hô hấp dưới.', '1654246174.jpg', '2022-06-03', '<h3 style=\"margin-bottom: 15px; font-weight: 700; line-height: 30px; font-size: 22px; overflow-wrap: break-word; font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; color: rgb(30, 115, 190); letter-spacing: 0.5px; background-color: rgb(255, 255, 255);\">1. Viêm đường hô hấp trên:</h3><p style=\"margin-bottom: 10px; font-size: 17px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; letter-spacing: 0.5px; background-color: rgb(255, 255, 255);\">Viêm mũi, viêm VA, viêm xoang, viêm Amidan, viêm thanh quản. Bệnh viêm đường hô hấp trên thường xảy ra nhiều hơn, ở các cháu tuổi nhà trẻ, mẫu giáo và cấp một.</p><h3 style=\"margin-bottom: 15px; font-weight: 700; line-height: 30px; font-size: 22px; overflow-wrap: break-word; font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; color: rgb(30, 115, 190); letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\">2. Bệnh đường hố hấp dưới:</h3><p style=\"margin-bottom: 10px; font-size: 17px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\">Thường do điều trị bệnh viêm đường hô hấp trên không hiệu quả sẽ dẫn đến bệnh viêm đường hô hấp dưới. Viêm đường hô hấp dưới để chỉ bệnh ở khí quản, phế quản, và phổi. Khi viêm đường hô hấp dưới là tình trạng bệnh đường hô hấp nặng, suy hô hấp có thể tử vong.</p><p style=\"margin-bottom: 10px; font-size: 17px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\">Để tiện cho các bậc cha mẹ theo dõi và chăm sóc con cái, chúng tôi xin giới thiệu một số bệnh thường gặp do viêm đường hô hấp trên ở các cháu lứa tuổi mẫu giáo, nhà trẻ đến cấp một (từ 01 tuổi đến 10 tuổi).</p><p style=\"margin-bottom: 10px; font-size: 17px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\"><img loading=\"lazy\" class=\"aligncenter wp-image-31241 size-full\" src=\"https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg.jpg\" alt=\"CÁC BỆNH GÂY VIÊM ĐƯỜNG HÔ HẤP Ở TRẺ EM - Taimuihongsg.com\" width=\"1024\" height=\"768\" srcset=\"https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg.jpg 1024w, https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg-300x225.jpg 300w, https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg-768x576.jpg 768w, https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg-200x150.jpg 200w, https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg-500x375.jpg 500w, https://taimuihongsg.com/wp-content/uploads/2020/09/benh-duong-ho-hap-duoi-o-tre_taimuihongsg-1000x750.jpg 1000w\" sizes=\"(max-width: 1024px) 100vw, 1024px\" style=\"margin: 5px auto; border: 0px; height: auto; clear: both; display: block; border-radius: 15px;\"></p><h4 style=\"margin-bottom: 15px; font-weight: 700; line-height: 25px; font-size: 19px; overflow-wrap: break-word; font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; color: rgb(30, 115, 190); letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\">I. BỆNH VIÊM VA</h4><p style=\"margin-bottom: 10px; font-size: 17px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\">Ở Việt Nam khí hậu nhiệt đới nắng nóng, mưa nhiều các cháu nhỏ thường dễ bị viêm mũi họng cấp sau khi đi chơi ngoài trời dưới trời nắng, tắm biển hay do sử dụng máy lạnh, quạt máy…Bệnh thường xảy ra viêm đường hô hấp trên bởi hai ổ viêm VA và Amidan.</p><p style=\"margin-bottom: 10px; font-size: 17px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; letter-spacing: 0.5px; background-color: rgb(255, 255, 255); text-align: justify;\">Bình thường, VA chỉ là một gờ niêm mạc dầy lên ở vòm mũi họng. VA là cơ quan đánh chặn vi khuẩn xâm nhập vào đường hô hấp trên nên dễ bị viêm.</p><h5 style=\"margin-bottom: 15px; font-weight: 700; line-height: 20px; font-size: 18px; overflow-wrap: break-word; font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; color: rgb(30, 115, 190); letter-spacing: 0.5px; background-color: rgb(255, 255, 255);\">a. Biểu hiện lâm sàng</h5><ul style=\"padding-left: 25px; margin-top: 16px; margin-bottom: 16px; overflow-wrap: break-word; color: rgb(51, 51, 51); font-family: Roboto, &quot;Trebuchet MS&quot;, Helvetica, sans-serif; font-size: 17px; letter-spacing: 0.5px; background-color: rgb(255, 255, 255);\"><li style=\"text-align: justify;\"><em>Sốt cao:</em>&nbsp;39 – 40độ, đôi khi co giật.</li><li style=\"text-align: justify;\"><em>Nghẹt mũi:</em>&nbsp;Do niêm mạc mũi họng, VA bị sưng, phù nề gây nghẹt mũi.</li><li style=\"text-align: justify;\"><em>Chảy dịch:</em>&nbsp;Trẻ bị chảy mũi nhiều. Ban đầu dịch trong sau đó chuyển sang màu trắng, có mùi tanh.</li><li style=\"text-align: justify;\"><em>Rối loạn tiêu hóa:</em>&nbsp;Dịch chảy xuống họng bị trẻ nuốt gây rối loạn dường tiêu hóa, tiêu chảy, đầy bụng, dẫn đến chán ăn.</li><li style=\"text-align: justify;\"><em>Ho:</em>&nbsp;Một phần dịch chảy vào khí quản, phế quản gây ra phản xạ ho.</li><li style=\"text-align: justify;\"><em>Khò khè, khó thở:</em>&nbsp;Do dịch chảy vào phế quản gây viêm phế quản, thở khò khè, thở rít như hen.</li><li style=\"text-align: justify;\"><em>Nằm sấp khi ngủ:&nbsp;</em>Khi ngủ trẻ phải nằm sấp mới thở được tuy nhiên giấc ngủ không sâu, dễ thức, ngủ ngáy.</li></ul>', '2022-06-03 01:49:34', '2022-06-03 01:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `benhnhan`
--

CREATE TABLE `benhnhan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `HoTen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DiaChi` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CanNang` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GioiTinh` tinyint(1) NOT NULL DEFAULT 1,
  `NgaySinh` date NOT NULL,
  `MaBHYT` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AnhDaiDien` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benhnhan`
--

INSERT INTO `benhnhan` (`id`, `HoTen`, `DiaChi`, `SDT`, `CanNang`, `GioiTinh`, `NgaySinh`, `MaBHYT`, `AnhDaiDien`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Công Đoan', 'Phú Yên', '0339892651', '50 kg', 1, '2022-05-10', 'CD123456789', '1654184802.png', 'doan.nc.60cntt@ntu.edu.vn', '$2y$10$2WC9t3qF7bHrDXC43maTwe4KYuCcLt8SSeagy8IMst4lzc5Rmy13u', '2022-05-09 21:24:32', '2022-06-09 21:41:28'),
(8, 'Nguyễn Công Đoan', NULL, '0339892651', NULL, 1, '2014-05-14', NULL, 'avatar.png', 'an@gmail.com', '$2y$10$ltwX9ei2xwMtNOsOalHJiOZ26h9eZboJk4D0eInosmboEHeussa9y', '2022-05-14 17:43:07', '2022-05-14 17:43:07'),
(10, 'Nguyễn Đức Huy', 'Ninh Hòa', '0702634575', NULL, 1, '2022-05-12', NULL, 'avatar.png', 'huy@gmail.com', '$2y$10$3aqu7t3dgOgGpGhVHjVaxOaFtqTau3WT6A3L5ztPxeq9VltQREOUC', '2022-05-20 07:44:08', '2022-05-20 07:44:08'),
(11, 'Nguyễn Đức Huy', 'Ninh Hòa', '0123456789', NULL, 1, '2022-05-12', NULL, 'avatar.png', 'gamaistar1@gmail.com', '$2y$10$vQKlagx86eM9TN.jHby/LuDPcOKYmyoDjgZa/xfoHhdi.8MT0GUZW', '2022-05-20 07:45:35', '2022-05-20 07:45:35'),
(12, 'Nguyễn Đức Huy', 'Ninh Hòa', '0339892651', '50 kg', 1, '2018-06-22', NULL, 'avatar.png', 'ngvanhaupro@gmail.com', '$2y$10$MmJBRPWTu6MS4XoemYn9/OMIIsXo/Bo4F5bNeb7JRHKY5o0VX3mYO', '2022-05-22 06:32:02', '2022-05-22 06:33:22'),
(13, 'Nam', 'Nha Trang', '0339892651', NULL, 1, '2022-05-23', NULL, 'avatar.png', 'nam@gmail.com', '$2y$10$gDaCZVbVOLNRcdZepk2m6e/H4hvayBjVFwnMFA92J3A22Am9CkHfy', '2022-05-23 02:36:16', '2022-05-23 02:36:16'),
(15, 'Lê Nguyễn Việt Hoàng', 'Ninh Hòa', '0326514789', NULL, 1, '2016-02-24', NULL, 'avatar.png', 'hoang@gmail.com', '$2y$10$encRclNG9wN0U0rnmZMDr.qXlXCbtLgNyNZ7pAfqzoI/ymMHkqGfq', '2022-05-23 18:30:07', '2022-05-23 18:30:07'),
(16, 'Nguyễn Đức Huy', 'Phú Yên', '0356985265', NULL, 1, '2022-05-20', NULL, 'avatar.png', 'huynd@gmail.com', '$2y$10$yvEYBJafVZXJVoM8Z1xCyuMYmV9fX3nGuQhLJx2wHqYWrSB6HsXW.', '2022-05-28 02:13:10', '2022-05-28 02:13:10'),
(17, 'Hiếu', 'Ninh Hòa', '0339892651', '50 kg', 1, '2022-05-28', NULL, 'avatar.png', 'hieuvt@gmail.com', '$2y$10$Luo/Cs5AjutRZA7csjBMYuhcelIjocU5v7Q8iNvHGBcmQDNeGaUxi', '2022-05-28 07:58:08', '2022-05-28 07:58:33'),
(18, 'Nguyễn Công Đoan', 'Phú Yên', '0339892651', '50 kg', 1, '2022-05-29', NULL, 'avatar.png', 'doan@gmail.com', '$2y$10$IE4/XV/1Btxrm5qdQlXcAuvgtN7Iqw.nNNFRlSoId8S7I5z1MXDQa', '2022-05-29 00:53:19', '2022-05-29 00:54:16'),
(19, 'Nguyễn Công Đoan', 'Phú Yên', '0339892651', NULL, 1, '2022-05-30', NULL, 'avatar.png', 'doan23@gmail.com', '$2y$10$EtKqVg.GtU..IfYahs/qte0KZR4PYBXvwamndSDb/BxN39mp0SbcW', '2022-05-30 00:41:33', '2022-05-30 00:41:33'),
(21, 'Nguyễn Văn Hùng', 'Nha Trang', '0339892651', NULL, 1, '2022-06-03', NULL, 'avatar.png', 'hung@gmail.com', '$2y$10$85aQftGTFxWhCluldvxxgeebtezDuhrPgdegIHw.OtXTNoR2vuJsy', '2022-06-02 06:49:29', '2022-06-02 06:49:29'),
(24, 'Nguyễn Thanh', '02 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa', '0389546241', '45 kg', 1, '2002-06-04', NULL, 'avatar.png', 'thanh@gmail.com', '$2y$10$YknyMMoQ4tY8lOokny7olOppJ/0fIlOhQ.1LRHWNX2OSLRn5T3QfW', '2022-06-07 06:08:00', '2022-06-07 06:16:12'),
(25, 'Trần Văn Nghĩa', '04 Lê Lợi, Nha Trang, Khánh Hòa', '0338954624', NULL, 1, '2008-06-08', NULL, 'avatar.png', 'nghiatv@gmail.com', '$2y$10$NC1N2NT/OOs1itEqPxW/p.ZzFZMIp//1vJKLbbdM1mPUn9AcSFegS', '2022-06-08 05:35:15', '2022-06-08 05:35:15'),
(26, 'Huỳnh Xuân Đại', 'Ninh Hưng, Ninh Hòa', '0358972544', NULL, 1, '2002-02-10', NULL, 'avatar.png', 'daihx@gmail.com', '$2y$10$ruMe/QlYKQqcpgUgK3LwWOA6N7l1ZPfu7XuFsqwy77UZhpCB0D8Du', '2022-06-09 19:14:26', '2022-06-09 19:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `chucvu`
--

CREATE TABLE `chucvu` (
  `MaChucVu` bigint(20) UNSIGNED NOT NULL,
  `TenChucVu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chucvu`
--

INSERT INTO `chucvu` (`MaChucVu`, `TenChucVu`, `created_at`, `updated_at`) VALUES
(1, 'Bác sĩ', '2022-05-10 04:28:20', '2022-05-10 04:28:20'),
(2, 'Admin', '2022-05-10 04:28:20', '2022-05-10 04:28:20'),
(3, 'Nhân viên', '2022-05-28 04:24:30', '2022-05-28 04:24:30');

-- --------------------------------------------------------

--
-- Table structure for table `chuyenmon`
--

CREATE TABLE `chuyenmon` (
  `MaChuyenMon` bigint(20) UNSIGNED NOT NULL,
  `TenChuyenMon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chuyenmon`
--

INSERT INTO `chuyenmon` (`MaChuyenMon`, `TenChuyenMon`, `created_at`, `updated_at`) VALUES
(1, 'Tai-Mũi-Họng', '2022-05-10 04:30:10', '2022-05-10 04:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `ct_donthuoc`
--

CREATE TABLE `ct_donthuoc` (
  `MaCT_DonThuoc` bigint(20) UNSIGNED NOT NULL,
  `MaDonThuoc` bigint(20) UNSIGNED NOT NULL,
  `MaThuoc` bigint(20) UNSIGNED NOT NULL,
  `MaDonVi` bigint(20) UNSIGNED NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `CachDung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LieuDung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ct_donthuoc`
--

INSERT INTO `ct_donthuoc` (`MaCT_DonThuoc`, `MaDonThuoc`, `MaThuoc`, `MaDonVi`, `SoLuong`, `CachDung`, `LieuDung`, `created_at`, `updated_at`) VALUES
(20, 18, 2, 3, 2, 'Uống trước khi ăn 30 phút', 'Mỗi lần 1 viên', '2022-05-22 06:34:39', '2022-05-22 06:34:39'),
(21, 18, 1, 2, 1, 'Uống sau khi ăn 30 phút', 'Mỗi lần 1 viên', '2022-05-22 06:34:39', '2022-05-22 06:34:39'),
(22, 19, 2, 2, 4, 'Uống trước khi ăn 30 phút', 'Mỗi lần 1 viên', '2022-05-22 06:58:53', '2022-05-22 06:58:53'),
(23, 20, 1, 2, 4, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-05-22 22:59:10', '2022-05-22 22:59:10'),
(24, 21, 2, 2, 2, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-05-23 02:13:54', '2022-05-23 02:13:54'),
(25, 21, 1, 2, 1, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-05-23 02:13:54', '2022-05-23 02:13:54'),
(26, 22, 2, 2, 2, 'Uống sau khi ăn', 'Mỗi lần 1 viên', '2022-05-23 02:24:55', '2022-05-23 02:24:55'),
(27, 22, 1, 2, 1, 'Uống sau khi ăn', 'Mỗi lần 1 viên', '2022-05-23 02:24:55', '2022-05-23 02:24:55'),
(37, 32, 1, 2, 4, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-06-07 09:21:18', '2022-06-07 09:21:18'),
(38, 32, 2, 2, 2, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-06-07 09:21:18', '2022-06-07 09:21:18'),
(44, 35, 1, 2, 4, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-06-09 19:54:17', '2022-06-09 21:07:23'),
(45, 35, 2, 2, 10, 'Uống trước khi ăn', 'Mỗi lần 1 viên', '2022-06-09 19:54:17', '2022-06-09 21:07:23');

-- --------------------------------------------------------

--
-- Table structure for table `ct_phieunhapthuoc`
--

CREATE TABLE `ct_phieunhapthuoc` (
  `MaCT_PhieuNhap` bigint(20) UNSIGNED NOT NULL,
  `MaPhieuNhap` bigint(20) UNSIGNED NOT NULL,
  `MaDonVi` bigint(20) UNSIGNED NOT NULL,
  `MaThuoc` bigint(20) UNSIGNED NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `DonGia` int(11) NOT NULL,
  `ThanhTien` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ct_phieunhapthuoc`
--

INSERT INTO `ct_phieunhapthuoc` (`MaCT_PhieuNhap`, `MaPhieuNhap`, `MaDonVi`, `MaThuoc`, `SoLuong`, `DonGia`, `ThanhTien`, `created_at`, `updated_at`) VALUES
(4, 3, 2, 2, 100, 286000, 28600000, '2022-06-06 20:26:03', '2022-06-09 21:22:10'),
(6, 3, 2, 1, 36, 203500, 7326000, '2022-06-07 00:07:27', '2022-06-09 21:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `ct_quyenhan`
--

CREATE TABLE `ct_quyenhan` (
  `MaCT_QH` bigint(20) UNSIGNED NOT NULL,
  `MaQH` bigint(20) UNSIGNED NOT NULL,
  `MaChucVu` bigint(20) UNSIGNED NOT NULL,
  `TrangThai` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `donthuoc`
--

CREATE TABLE `donthuoc` (
  `MaDonThuoc` bigint(20) UNSIGNED NOT NULL,
  `MaNV` bigint(20) UNSIGNED NOT NULL,
  `MaBN` bigint(20) UNSIGNED NOT NULL,
  `MaPhieuKham` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ChuanDoanBenh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NgayTaiKham` date DEFAULT NULL,
  `NgayTaoDon` date DEFAULT NULL,
  `TinhTrang_SMS` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donthuoc`
--

INSERT INTO `donthuoc` (`MaDonThuoc`, `MaNV`, `MaBN`, `MaPhieuKham`, `ChuanDoanBenh`, `NgayTaiKham`, `NgayTaoDon`, `TinhTrang_SMS`, `created_at`, `updated_at`) VALUES
(18, 5, 12, '20220522SPPM02', 'Đau họng', '2022-05-25', '2022-05-22', 1, '2022-05-22 06:34:39', '2022-05-23 20:30:32'),
(19, 5, 1, '20220522SPPM01', 'Viêm màng tai', '2022-05-25', '2022-05-23', 1, '2022-05-22 06:58:53', '2022-05-23 20:28:01'),
(20, 6, 10, '20220523SPPM01', 'Viêm họng', '2022-05-23', '2022-05-23', 1, '2022-05-22 22:59:10', '2022-05-23 20:23:34'),
(21, 6, 1, '20220523SPPM02', 'Viêm họng', '2022-05-30', '2022-05-23', 0, '2022-05-23 02:13:54', '2022-05-23 02:13:54'),
(22, 5, 12, '20220523SPPM03', 'Đau tai', '2022-05-26', '2022-05-23', 0, '2022-05-23 02:24:55', '2022-05-23 02:24:55'),
(32, 3, 24, '20220608SPPM01', 'Đau họng mãn tính', '2022-06-12', '2022-06-07', 0, '2022-06-07 09:21:18', '2022-06-07 09:21:18'),
(34, 3, 24, '20220607SPPM02', 'Đau họng mãn tính', '2022-06-11', '2022-06-08', 0, '2022-06-08 00:49:44', '2022-06-08 00:49:44'),
(35, 6, 26, '20220610SPAM01', 'Viêm tai giữa', '2022-06-25', '2022-06-10', 0, '2022-06-09 19:54:17', '2022-06-09 21:07:23');

-- --------------------------------------------------------

--
-- Table structure for table `donvi`
--

CREATE TABLE `donvi` (
  `MaDonVi` bigint(20) UNSIGNED NOT NULL,
  `TenDonVi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donvi`
--

INSERT INTO `donvi` (`MaDonVi`, `TenDonVi`, `created_at`, `updated_at`) VALUES
(1, 'Ống', '2022-05-20 02:04:43', '2022-05-20 02:04:43'),
(2, 'Hộp', '2022-05-20 02:04:43', '2022-05-20 02:04:43'),
(3, 'Viên', '2022-05-20 02:05:26', '2022-05-20 02:05:26'),
(4, 'Bình xịt', '2022-05-20 02:05:26', '2022-05-20 02:05:26');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `MaHD` bigint(20) UNSIGNED NOT NULL,
  `MaDonThuoc` bigint(20) UNSIGNED NOT NULL,
  `TongTien` double NOT NULL,
  `NgayLap` datetime NOT NULL,
  `PhuongThuc_TT` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TinhTrang_TT` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hoadon`
--

INSERT INTO `hoadon` (`MaHD`, `MaDonThuoc`, `TongTien`, `NgayLap`, `PhuongThuc_TT`, `TinhTrang_TT`, `created_at`, `updated_at`) VALUES
(19, 20, 814000, '2022-06-09 00:00:00', 'Thanh toán trực tiếp', 1, '2022-06-09 09:32:19', '2022-06-09 09:32:19'),
(20, 18, 775500, '2022-06-09 00:00:00', 'Thanh toán trực tiếp', 1, '2022-06-09 09:32:40', '2022-06-09 09:32:40'),
(21, 19, 1144000, '2022-06-09 23:33:27', 'Thanh toán Online.', 1, '2022-06-09 09:33:31', '2022-06-09 09:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `lichkhambenh`
--

CREATE TABLE `lichkhambenh` (
  `MaLK` bigint(20) UNSIGNED NOT NULL,
  `MaNV` bigint(20) UNSIGNED NOT NULL,
  `ThoiGianBD` datetime NOT NULL,
  `ThoiGianKT` datetime NOT NULL,
  `TieuDe` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ClassName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lichkhambenh`
--

INSERT INTO `lichkhambenh` (`MaLK`, `MaNV`, `ThoiGianBD`, `ThoiGianKT`, `TieuDe`, `ClassName`, `created_at`, `updated_at`) VALUES
(2, 1, '2022-05-01 00:00:00', '2022-05-04 00:00:00', 'kham benh', 'primary', '2022-05-26 07:09:27', '2022-05-27 00:15:55'),
(41, 1, '2022-05-04 00:00:00', '2022-05-08 00:00:00', 'Khám bệnh', 'success', '2022-05-26 19:49:04', '2022-05-26 19:54:00'),
(42, 1, '2022-05-27 10:39:00', '2022-05-27 10:39:20', 'Khám bệnh 1', 'primary', '2022-05-26 20:39:47', '2022-05-26 20:39:47'),
(43, 1, '2022-05-27 01:41:00', '2022-05-28 11:42:00', 'Khám bệnh 2', 'info', '2022-05-26 20:42:41', '2022-05-26 20:42:41'),
(45, 1, '2022-05-27 10:48:00', '2022-06-01 10:48:00', 'Khám bệnh', 'primary', '2022-05-26 20:49:09', '2022-06-06 07:22:44'),
(48, 1, '2022-05-15 00:00:00', '2022-05-18 00:00:00', 'kham benh', 'warning', '2022-05-27 00:16:37', '2022-05-27 00:16:37'),
(49, 1, '2022-05-27 14:25:00', '2022-05-27 17:25:00', 'kham benh', 'success', '2022-05-27 00:25:37', '2022-05-27 00:25:37'),
(50, 1, '2022-05-22 00:00:00', '2022-05-24 00:00:00', 'kham benh', 'important', '2022-05-27 07:29:01', '2022-05-27 07:29:01'),
(51, 2, '2022-05-01 10:00:00', '2022-05-04 11:00:00', 'Khám bệnh', 'success', '2022-05-27 22:24:40', '2022-05-27 22:24:40'),
(52, 2, '2022-05-28 06:00:00', '2022-05-28 12:00:00', 'Khám bệnh 1', 'important', '2022-05-28 02:10:07', '2022-05-28 02:10:07'),
(53, 2, '2022-05-28 13:00:00', '2022-05-28 17:30:00', 'Khám bệnh', 'warning', '2022-05-28 02:10:18', '2022-05-28 02:10:18'),
(54, 1, '2022-05-08 00:00:00', '2022-05-11 00:00:00', 'Khám bệnh', 'important', '2022-05-29 23:06:40', '2022-05-29 23:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(37, '2014_10_12_000000_create_users_table', 1),
(38, '2014_10_12_100000_create_password_resets_table', 1),
(39, '2019_08_19_000000_create_failed_jobs_table', 1),
(40, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(41, '2022_05_09_033523_benhnhan', 1),
(42, '2022_05_09_072740_thuoc', 1),
(43, '2022_05_09_072825_chuyenmon', 1),
(44, '2022_05_09_072857_chucvu', 1),
(45, '2022_05_09_072923_nhanvien', 1),
(46, '2022_05_09_072952_phieuhenkham', 1),
(47, '2022_05_09_072956_phieukham', 1),
(48, '2022_05_09_073224_lichkhambenh', 1),
(49, '2022_05_09_073326_donvi', 1),
(50, '2022_05_09_073353_donthuoc', 1),
(51, '2022_05_09_073424_ct_donthuoc', 1),
(52, '2022_05_09_073501_phieunhapthuoc', 1),
(53, '2022_05_09_073528_ct_phieunhap', 1),
(54, '2022_05_09_073605_theloai_bv', 1),
(55, '2022_05_09_073627_baiviet', 1),
(56, '2022_05_09_073651_quyenhan', 1),
(57, '2022_05_09_073725_ct_quyenhan', 1),
(58, '2022_05_20_015047_hoadon', 2),
(59, '2022_05_23_080840_payments', 3);

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `MaChuyenMon` bigint(20) UNSIGNED NOT NULL,
  `MaChucVu` bigint(20) UNSIGNED NOT NULL,
  `HoTen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AnhDaiDien` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `DiaChi` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GioiTinh` tinyint(1) NOT NULL DEFAULT 1,
  `NgaySinh` date NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`id`, `MaChuyenMon`, `MaChucVu`, `HoTen`, `AnhDaiDien`, `DiaChi`, `SDT`, `GioiTinh`, `NgaySinh`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Lê Trọng Kha', 'bs2.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0123456789', 1, '2022-05-09', 'kha.lt.60cntt@ntu.edu.vn', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2022-05-10 04:30:42', '2022-05-10 04:30:42'),
(2, 1, 3, 'Nguyễn Vân Anh', 'bs1.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0123456789', 0, '2022-05-10', 'vananh@gmail.com', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2021-12-03 13:43:31', '2021-12-03 13:43:31'),
(3, 1, 1, 'Phạm Quốc Đạt', 'bs5.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0123456789', 1, '2022-05-11', 'datpq@gmail.com', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2022-05-10 13:39:31', '2022-05-10 13:39:31'),
(4, 1, 1, 'Trần Anh Thư', 'bs4.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0123456789', 0, '2022-05-10', 'anhthu@gmail.com', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2022-05-10 13:39:31', '2022-05-10 13:39:31'),
(5, 1, 1, 'Nguyễn Tuấn Minh', 'bs6.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0339892651', 1, '2022-05-10', 'minh@gmail.com', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2022-05-10 13:42:31', '2022-05-10 13:42:31'),
(6, 1, 1, 'Trần Thanh Hoài', 'bs7.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0123456789', 1, '2022-05-03', 'thanhhoai@gmail.com', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2022-05-10 13:42:31', '2022-05-10 13:42:31'),
(7, 1, 1, 'Nguyễn Phan Hà Phương', 'bs3.png', 'Số 02, Nguyễn Đình Chiểu, TP Nha Trang, Khánh Hòa', '0123456789', 0, '2022-05-11', 'phuong@gmail.com', '$2y$10$ABf72tmQBtG6AECoPibzl.TgSqCb82i7zauL0D6dF/mY6XAGNmE.m', '2022-05-11 13:42:46', '2022-05-11 13:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `MaHD` bigint(20) UNSIGNED NOT NULL COMMENT 'Mã hóa đơn',
  `NoiDung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ghi chú thanh toán',
  `Vnp_response_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Mã phản hồi',
  `Code_bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Mã ngân hàng',
  `Code_vnpay` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Mã giao dịch',
  `ThoiGian` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Thời gian chuyển khoản'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `MaHD`, `NoiDung`, `Vnp_response_code`, `Code_bank`, `Code_vnpay`, `ThoiGian`) VALUES
(3, 21, 'Thanh toán hóa đơn thuốc', '00', 'NCB', '13768012', '20220609233327');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phieuhenkham`
--

CREATE TABLE `phieuhenkham` (
  `MaPhieuHen` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaNV` bigint(20) UNSIGNED NOT NULL,
  `NgayHen` date NOT NULL,
  `GioHen` datetime NOT NULL,
  `HoTen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HoTen_NK` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GioiTinh_NK` tinyint(1) NOT NULL,
  `DiaChi_NK` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NamSinh_NK` date DEFAULT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TinhTrang_PH` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phieuhenkham`
--

INSERT INTO `phieuhenkham` (`MaPhieuHen`, `MaNV`, `NgayHen`, `GioHen`, `HoTen`, `SDT`, `HoTen_NK`, `GioiTinh_NK`, `DiaChi_NK`, `NamSinh_NK`, `Email`, `TinhTrang_PH`, `created_at`, `updated_at`) VALUES
('20220523SPPM05', 4, '2022-05-23', '2022-05-23 16:21:00', 'Nguyễn Công Đoan', '0123456789', 'Nguyễn Công Đoan', 1, NULL, '2022-05-23', 'nguyencongdoan2300@gmail.com', 1, '2022-05-23 06:23:18', '2022-05-23 06:23:46'),
('20220523SPPM06', 5, '2022-05-23', '2022-05-23 16:49:00', 'Nguyễn Công Đoan', '0123456789', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 1, '2022-05-23 07:49:55', '2022-05-28 05:44:31'),
('20220523SPPM08', 3, '2022-05-23', '2022-05-23 16:50:00', 'Nguyễn Công Đoan', '0123456789', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 1, '2022-05-23 07:50:38', '2022-06-09 07:41:08'),
('20220523SPPM09', 3, '2022-05-23', '2022-05-23 16:50:00', 'Nguyễn Công Đoan', '0123456789', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-05-23 07:50:44', '2022-05-23 07:50:44'),
('20220524SPAM01', 4, '2022-06-06', '2022-06-06 10:10:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-05-23 08:07:48', '2022-06-06 02:43:17'),
('20220524SPAM02', 4, '2022-06-03', '2022-06-03 10:30:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-05-23 08:24:32', '2022-06-02 20:29:38'),
('20220524SPAM03', 7, '2022-05-24', '2022-05-24 08:30:00', 'Lê Nguyễn Việt Hoàng', '0326514789', 'Lê Nguyễn Việt Hoàng', 1, 'Ninh Hòa', '2016-02-24', 'hoang@gmail.com', 1, '2022-05-23 18:30:07', '2022-05-23 18:49:19'),
('20220524SPPM01', 4, '2022-05-24', '2022-05-24 17:06:00', 'Nguyễn Công Đoan', '0123456789', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-05-23 08:06:21', '2022-05-23 08:06:21'),
('20220528SPPM01', 4, '2022-05-28', '2022-05-28 15:50:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 1, '2022-05-28 01:50:47', '2022-05-28 01:57:12'),
('20220528SPPM02', 5, '2022-05-28', '2022-05-28 16:13:00', 'Nguyễn Đức Huy', '0326517895', 'Nguyễn Đức Huy', 1, 'Phú Yên', '2022-05-20', 'huynd@gmail.com', 1, '2022-05-28 02:13:10', '2022-05-28 02:18:35'),
('20220607SPAM01', 6, '2022-06-07', '2022-06-07 08:30:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 1, '2022-06-06 18:11:16', '2022-06-06 18:12:58'),
('20220607SPPM03', 3, '2022-06-07', '2022-06-07 14:30:00', 'Nguyễn Công Đoan', '0389546241', 'Nguyễn Thanh', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 1, '2022-06-07 07:25:40', '2022-06-07 07:27:44'),
('20220607SPPM04', 3, '2022-06-07', '2022-06-07 15:30:00', 'Nguyễn Công Đoan', '0389546241', 'Nguyễn Thanh', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-06-07 07:31:36', '2022-06-07 07:31:36'),
('20220608SPPM01', 3, '2022-06-08', '2022-06-08 13:45:00', 'Nguyễn Thanh', '0389546241', 'Nguyễn Thanh', 1, '02 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa', '2002-06-04', 'thanh@gmail.com', 1, '2022-06-07 07:45:25', '2022-06-07 07:48:02'),
('20220610SPPM01', 5, '2022-06-10', '2022-06-10 15:40:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-06-09 21:43:00', '2022-06-09 21:43:00'),
('20220612SPPM01', 3, '2022-06-12', '2022-06-12 14:30:00', 'Nguyễn Thanh', '0389546241', 'Nguyễn Thanh', 1, '02 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa', '2002-06-04', 'thanh@gmail.com', 1, '2022-06-07 09:21:54', '2022-06-07 09:22:26'),
('20220630SPPM01', 3, '2022-06-30', '2022-06-30 15:34:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-06-06 20:36:27', '2022-06-06 20:36:27'),
('20220630SPPM02', 3, '2022-06-30', '2022-06-30 15:34:00', 'Nguyễn Công Đoan', '0339892651', 'Nguyễn Công Đoan', 1, 'Phú Yên', '2022-05-10', 'doan.nc.60cntt@ntu.edu.vn', 0, '2022-06-06 20:36:34', '2022-06-06 20:36:34');

-- --------------------------------------------------------

--
-- Table structure for table `phieukham`
--

CREATE TABLE `phieukham` (
  `MaPhieuKham` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MaBN` bigint(20) UNSIGNED NOT NULL,
  `MaNV` bigint(20) UNSIGNED NOT NULL,
  `NgayKham` date NOT NULL,
  `HoTenNguoiThan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ThoiGianBD` datetime DEFAULT NULL,
  `ThoiGianKT` datetime DEFAULT NULL,
  `TrieuChung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TienSuBenh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SoPhieu` int(11) NOT NULL,
  `TinhTrangKham` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phieukham`
--

INSERT INTO `phieukham` (`MaPhieuKham`, `MaBN`, `MaNV`, `NgayKham`, `HoTenNguoiThan`, `ThoiGianBD`, `ThoiGianKT`, `TrieuChung`, `TienSuBenh`, `SoPhieu`, `TinhTrangKham`, `created_at`, `updated_at`) VALUES
('20220522SPPM01', 1, 5, '2022-05-22', 'Nguyễn Hải', '2022-05-22 20:52:54', '2022-05-22 20:53:24', 'Viêm màng tai', NULL, 1, 4, '2022-05-22 06:46:37', '2022-06-09 06:15:43'),
('20220522SPPM02', 12, 5, '2022-05-22', 'Huy', '2022-05-22 20:33:03', '2022-05-22 20:33:22', 'Đau họng', NULL, 2, 4, '2022-05-22 06:32:02', '2022-06-09 06:15:42'),
('20220523SPPM01', 10, 6, '2022-05-23', NULL, '2022-05-23 12:58:13', '2022-05-23 12:58:32', 'Viêm họng', NULL, 1, 3, '2022-05-22 22:58:08', '2022-05-22 22:59:10'),
('20220523SPPM02', 1, 6, '2022-05-23', NULL, '2022-05-23 16:12:53', '2022-05-23 16:13:08', 'Viêm họng', NULL, 2, 3, '2022-05-23 02:09:07', '2022-05-23 02:13:54'),
('20220523SPPM03', 12, 5, '2022-05-23', NULL, '2022-05-23 16:23:59', '2022-05-23 16:24:12', 'Đau tai', NULL, 3, 3, '2022-05-23 02:23:53', '2022-05-23 02:24:55'),
('20220523SPPM04', 13, 5, '2022-05-23', 'Nam', '2022-05-23 16:38:36', '2022-05-23 16:38:51', 'Viêm vòm họng', 'Viêm vòm họng', 4, 3, '2022-05-23 02:36:16', '2022-05-23 02:39:28'),
('20220523SPPM06', 1, 5, '2022-05-23', NULL, '2022-06-06 21:59:14', '2022-06-06 21:59:31', 'Viêm tai', NULL, 6, 2, '2022-05-28 05:44:31', '2022-06-06 14:59:31'),
('20220523SPPM08', 1, 3, '2022-05-23', NULL, NULL, NULL, NULL, NULL, 8, 0, '2022-06-09 07:41:08', '2022-06-09 07:41:08'),
('20220524SPAM03', 15, 7, '2022-05-24', NULL, '2022-05-24 10:33:39', '2022-05-24 10:34:27', 'Viêm tai giữa', NULL, 3, 3, '2022-05-23 18:49:19', '2022-05-23 20:34:59'),
('20220525SPPM01', 12, 5, '2022-05-25', NULL, NULL, NULL, 'Đau họng', 'Đau họng', 1, 0, '2022-06-09 06:15:42', '2022-06-09 06:15:42'),
('20220525SPPM02', 1, 5, '2022-05-25', NULL, NULL, NULL, 'Viêm màng tai', 'Viêm màng tai', 2, 0, '2022-06-09 06:15:43', '2022-06-09 06:15:43'),
('20220528SPPM01', 1, 4, '2022-05-28', NULL, '2022-05-28 15:57:55', '2022-05-28 15:58:28', 'Viem tai giua', NULL, 1, 3, '2022-05-28 01:57:12', '2022-05-28 02:02:53'),
('20220528SPPM02', 16, 5, '2022-05-28', NULL, '2022-05-28 16:18:47', '2022-05-28 16:18:52', NULL, NULL, 2, 2, '2022-05-28 02:18:35', '2022-05-28 09:18:52'),
('20220528SPPM03', 17, 4, '2022-05-28', 'Nam', '2022-05-28 21:58:17', '2022-05-28 21:58:33', 'Đau tai', NULL, 3, 2, '2022-05-28 07:58:08', '2022-05-28 14:58:33'),
('20220529SPPM01', 18, 4, '2022-05-29', 'Nam', '2022-05-29 14:53:55', '2022-05-29 14:54:16', 'Đau tai', NULL, 1, 3, '2022-05-29 00:53:19', '2022-05-29 00:55:37'),
('20220607SPPM02', 24, 3, '2022-06-07', 'Nguyễn Thanh', '2022-06-08 13:43:25', '2022-06-08 13:45:06', 'Đau họng', NULL, 2, 4, '2022-06-07 06:08:00', '2022-06-08 03:26:53'),
('20220608SPPM01', 24, 3, '2022-06-08', NULL, '2022-06-07 22:08:57', '2022-06-07 22:09:22', 'Đau họng', 'Đau họng mãn tính', 1, 3, '2022-06-07 07:48:02', '2022-06-07 09:21:23'),
('20220608SPPM02', 25, 6, '2022-06-08', 'Trần Văn Ngọc', NULL, NULL, 'Viêm ống tai ngoài', NULL, 2, 0, '2022-06-08 05:35:15', '2022-06-08 05:35:15'),
('20220610SPAM01', 26, 6, '2022-06-10', NULL, '2022-06-10 09:28:13', '2022-06-10 09:28:42', 'Đau tai', NULL, 1, 3, '2022-06-09 19:14:26', '2022-06-09 21:07:44'),
('20220610SPAM02', 12, 5, '2022-06-10', NULL, NULL, NULL, 'Viêm amidan', NULL, 2, 0, '2022-06-09 19:16:40', '2022-06-09 19:16:40'),
('20220611SPPM01', 24, 3, '2022-06-11', NULL, NULL, NULL, 'Đau họng', 'Đau họng mãn tính', 1, 0, '2022-06-08 03:26:53', '2022-06-08 03:26:53'),
('20220612SPPM01', 24, 3, '2022-06-12', NULL, '2022-06-07 23:22:41', '2022-06-07 23:25:06', 'Đau họng', 'Đau họng mãn tính', 1, 2, '2022-06-07 09:22:26', '2022-06-07 16:25:06');

-- --------------------------------------------------------

--
-- Table structure for table `phieunhapthuoc`
--

CREATE TABLE `phieunhapthuoc` (
  `MaPhieuNhap` bigint(20) UNSIGNED NOT NULL,
  `MaNV` bigint(20) UNSIGNED NOT NULL,
  `NgayNhap` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phieunhapthuoc`
--

INSERT INTO `phieunhapthuoc` (`MaPhieuNhap`, `MaNV`, `NgayNhap`, `created_at`, `updated_at`) VALUES
(3, 1, '2022-06-07', '2022-06-06 20:26:03', '2022-06-06 20:26:03');

-- --------------------------------------------------------

--
-- Table structure for table `quyenhan`
--

CREATE TABLE `quyenhan` (
  `MaQH` bigint(20) UNSIGNED NOT NULL,
  `TenQH` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `theloai_bv`
--

CREATE TABLE `theloai_bv` (
  `MaTL` bigint(20) UNSIGNED NOT NULL,
  `TenTL` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `theloai_bv`
--

INSERT INTO `theloai_bv` (`MaTL`, `TenTL`, `created_at`, `updated_at`) VALUES
(1, 'Sức khỏe', '2022-05-31 02:34:03', '2022-05-31 02:34:03'),
(2, 'Y học', '2022-05-31 02:34:03', '2022-05-31 02:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `thuoc`
--

CREATE TABLE `thuoc` (
  `MaThuoc` bigint(20) UNSIGNED NOT NULL,
  `TenThuoc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `DonGia` int(11) NOT NULL,
  `NSX` date NOT NULL,
  `HSD` date NOT NULL,
  `ThongTinThuoc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `thuoc`
--

INSERT INTO `thuoc` (`MaThuoc`, `TenThuoc`, `SoLuong`, `DonGia`, `NSX`, `HSD`, `ThongTinThuoc`, `created_at`, `updated_at`) VALUES
(1, 'Rhinocort Aqua 64mcg', 13, 203500, '2022-05-17', '2023-05-03', 'Ketoconazole&chất ức chế CYP3A4 mạnh khác: giảm chuyển hóa Budesonid.\r\nBarbiturate, phenytoin, rifampicin: gây cảm ứng enzym gan và tăng chuyển hóa Budesonid.', '2022-05-17 02:39:22', '2022-06-09 19:54:17'),
(2, 'Symbicort Turbuhaler 60 Dose', 80, 286000, '2022-05-17', '2023-05-17', 'Thuốc cường B2 tác dụng ngắn,chẳng hạn như salbutamol và salmeterol.\r\nCorticosteroid uống,chẳng hạn như prednisolone\r\nThuốc lợi tiểu,ví dụ như bendroflumethiazide và furosemide\r\nDerivates xanthine,ví dụ như theophylline.\r\nBeta-blocker như atenolol,propran', '2022-05-17 02:39:22', '2022-06-09 19:54:17'),
(6, 'Nước muối sinh lý súc họng Natri Clorid 0.9% 500ml', 84, 15000, '2022-06-06', '2024-07-06', 'Nước muối sinh lý Natri Clorid 0.9% 500ml chứa Natri Clorid được pha loãng với nước tinh khiết tỉ lệ 0.9%.\r\nNước muối súc họng Natri Clorid 0.9% sau khi qua hệ thống lọc đều được chạy qua tia UV để tiêu diệt 99.99% vi khuẩn và loại bỏ tất cả những tạp chất có trong nước, giúp bạn hoàn toàn an tâm về chất lượng sản phẩm.\r\nNatri Clorid dùng để rửa vết thương hở và kín.\r\nSúc miệng để vệ sinh răng miệng, họng mỗi ngày.', '2022-06-06 06:50:20', '2022-06-08 00:49:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baiviet`
--
ALTER TABLE `baiviet`
  ADD PRIMARY KEY (`MaBV`),
  ADD KEY `baiviet_matl_foreign` (`MaTL`),
  ADD KEY `baiviet_manv_foreign` (`MaNV`);

--
-- Indexes for table `benhnhan`
--
ALTER TABLE `benhnhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chucvu`
--
ALTER TABLE `chucvu`
  ADD PRIMARY KEY (`MaChucVu`);

--
-- Indexes for table `chuyenmon`
--
ALTER TABLE `chuyenmon`
  ADD PRIMARY KEY (`MaChuyenMon`);

--
-- Indexes for table `ct_donthuoc`
--
ALTER TABLE `ct_donthuoc`
  ADD PRIMARY KEY (`MaCT_DonThuoc`),
  ADD KEY `ct_donthuoc_madonthuoc_foreign` (`MaDonThuoc`),
  ADD KEY `ct_donthuoc_mathuoc_foreign` (`MaThuoc`),
  ADD KEY `ct_donthuoc_madonvi_foreign` (`MaDonVi`);

--
-- Indexes for table `ct_phieunhapthuoc`
--
ALTER TABLE `ct_phieunhapthuoc`
  ADD PRIMARY KEY (`MaCT_PhieuNhap`),
  ADD KEY `ct_phieunhapthuoc_maphieunhap_foreign` (`MaPhieuNhap`),
  ADD KEY `ct_phieunhapthuoc_madonvi_foreign` (`MaDonVi`),
  ADD KEY `ct_phieunhapthuoc_mathuoc_foreign` (`MaThuoc`);

--
-- Indexes for table `ct_quyenhan`
--
ALTER TABLE `ct_quyenhan`
  ADD PRIMARY KEY (`MaCT_QH`),
  ADD KEY `ct_quyenhan_maqh_foreign` (`MaQH`),
  ADD KEY `ct_quyenhan_machucvu_foreign` (`MaChucVu`);

--
-- Indexes for table `donthuoc`
--
ALTER TABLE `donthuoc`
  ADD PRIMARY KEY (`MaDonThuoc`),
  ADD KEY `donthuoc_maphieukham_foreign` (`MaPhieuKham`),
  ADD KEY `donthuoc_manv_foreign` (`MaNV`),
  ADD KEY `donthuoc_mabn_foreign` (`MaBN`);

--
-- Indexes for table `donvi`
--
ALTER TABLE `donvi`
  ADD PRIMARY KEY (`MaDonVi`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`MaHD`),
  ADD KEY `hoadon_madonthuoc_foreign` (`MaDonThuoc`);

--
-- Indexes for table `lichkhambenh`
--
ALTER TABLE `lichkhambenh`
  ADD PRIMARY KEY (`MaLK`),
  ADD KEY `lichkhambenh_manv_foreign` (`MaNV`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nhanvien_machuyenmon_foreign` (`MaChuyenMon`),
  ADD KEY `nhanvien_machucvu_foreign` (`MaChucVu`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_mahd_foreign` (`MaHD`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `phieuhenkham`
--
ALTER TABLE `phieuhenkham`
  ADD PRIMARY KEY (`MaPhieuHen`),
  ADD KEY `phieuhenkham_manv_foreign` (`MaNV`);

--
-- Indexes for table `phieukham`
--
ALTER TABLE `phieukham`
  ADD PRIMARY KEY (`MaPhieuKham`),
  ADD KEY `phieukham_mabn_foreign` (`MaBN`),
  ADD KEY `phieukham_manv_foreign` (`MaNV`);

--
-- Indexes for table `phieunhapthuoc`
--
ALTER TABLE `phieunhapthuoc`
  ADD PRIMARY KEY (`MaPhieuNhap`),
  ADD KEY `phieunhapthuoc_manv_foreign` (`MaNV`);

--
-- Indexes for table `quyenhan`
--
ALTER TABLE `quyenhan`
  ADD PRIMARY KEY (`MaQH`);

--
-- Indexes for table `theloai_bv`
--
ALTER TABLE `theloai_bv`
  ADD PRIMARY KEY (`MaTL`);

--
-- Indexes for table `thuoc`
--
ALTER TABLE `thuoc`
  ADD PRIMARY KEY (`MaThuoc`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `baiviet`
--
ALTER TABLE `baiviet`
  MODIFY `MaBV` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `benhnhan`
--
ALTER TABLE `benhnhan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `chucvu`
--
ALTER TABLE `chucvu`
  MODIFY `MaChucVu` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `chuyenmon`
--
ALTER TABLE `chuyenmon`
  MODIFY `MaChuyenMon` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ct_donthuoc`
--
ALTER TABLE `ct_donthuoc`
  MODIFY `MaCT_DonThuoc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `ct_phieunhapthuoc`
--
ALTER TABLE `ct_phieunhapthuoc`
  MODIFY `MaCT_PhieuNhap` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ct_quyenhan`
--
ALTER TABLE `ct_quyenhan`
  MODIFY `MaCT_QH` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donthuoc`
--
ALTER TABLE `donthuoc`
  MODIFY `MaDonThuoc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `donvi`
--
ALTER TABLE `donvi`
  MODIFY `MaDonVi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hoadon`
--
ALTER TABLE `hoadon`
  MODIFY `MaHD` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `lichkhambenh`
--
ALTER TABLE `lichkhambenh`
  MODIFY `MaLK` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phieunhapthuoc`
--
ALTER TABLE `phieunhapthuoc`
  MODIFY `MaPhieuNhap` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quyenhan`
--
ALTER TABLE `quyenhan`
  MODIFY `MaQH` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `theloai_bv`
--
ALTER TABLE `theloai_bv`
  MODIFY `MaTL` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `thuoc`
--
ALTER TABLE `thuoc`
  MODIFY `MaThuoc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `baiviet`
--
ALTER TABLE `baiviet`
  ADD CONSTRAINT `baiviet_manv_foreign` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `baiviet_matl_foreign` FOREIGN KEY (`MaTL`) REFERENCES `theloai_bv` (`MaTL`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ct_donthuoc`
--
ALTER TABLE `ct_donthuoc`
  ADD CONSTRAINT `ct_donthuoc_madonthuoc_foreign` FOREIGN KEY (`MaDonThuoc`) REFERENCES `donthuoc` (`MaDonThuoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ct_donthuoc_madonvi_foreign` FOREIGN KEY (`MaDonVi`) REFERENCES `donvi` (`MaDonVi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ct_donthuoc_mathuoc_foreign` FOREIGN KEY (`MaThuoc`) REFERENCES `thuoc` (`MaThuoc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ct_phieunhapthuoc`
--
ALTER TABLE `ct_phieunhapthuoc`
  ADD CONSTRAINT `ct_phieunhapthuoc_madonvi_foreign` FOREIGN KEY (`MaDonVi`) REFERENCES `donvi` (`MaDonVi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ct_phieunhapthuoc_maphieunhap_foreign` FOREIGN KEY (`MaPhieuNhap`) REFERENCES `phieunhapthuoc` (`MaPhieuNhap`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ct_phieunhapthuoc_mathuoc_foreign` FOREIGN KEY (`MaThuoc`) REFERENCES `thuoc` (`MaThuoc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ct_quyenhan`
--
ALTER TABLE `ct_quyenhan`
  ADD CONSTRAINT `ct_quyenhan_machucvu_foreign` FOREIGN KEY (`MaChucVu`) REFERENCES `chucvu` (`MaChucVu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ct_quyenhan_maqh_foreign` FOREIGN KEY (`MaQH`) REFERENCES `quyenhan` (`MaQH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `donthuoc`
--
ALTER TABLE `donthuoc`
  ADD CONSTRAINT `donthuoc_mabn_foreign` FOREIGN KEY (`MaBN`) REFERENCES `benhnhan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `donthuoc_manv_foreign` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `donthuoc_maphieukham_foreign` FOREIGN KEY (`MaPhieuKham`) REFERENCES `phieukham` (`MaPhieuKham`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD CONSTRAINT `hoadon_madonthuoc_foreign` FOREIGN KEY (`MaDonThuoc`) REFERENCES `donthuoc` (`MaDonThuoc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lichkhambenh`
--
ALTER TABLE `lichkhambenh`
  ADD CONSTRAINT `lichkhambenh_manv_foreign` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD CONSTRAINT `nhanvien_machucvu_foreign` FOREIGN KEY (`MaChucVu`) REFERENCES `chucvu` (`MaChucVu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nhanvien_machuyenmon_foreign` FOREIGN KEY (`MaChuyenMon`) REFERENCES `chuyenmon` (`MaChuyenMon`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_mahd_foreign` FOREIGN KEY (`MaHD`) REFERENCES `hoadon` (`MaHD`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phieuhenkham`
--
ALTER TABLE `phieuhenkham`
  ADD CONSTRAINT `phieuhenkham_manv_foreign` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phieukham`
--
ALTER TABLE `phieukham`
  ADD CONSTRAINT `phieukham_mabn_foreign` FOREIGN KEY (`MaBN`) REFERENCES `benhnhan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `phieukham_manv_foreign` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phieunhapthuoc`
--
ALTER TABLE `phieunhapthuoc`
  ADD CONSTRAINT `phieunhapthuoc_manv_foreign` FOREIGN KEY (`MaNV`) REFERENCES `nhanvien` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
